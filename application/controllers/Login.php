<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('Session_model','login');
      $this->load->model('Home_model','home');
      $this->load->library('user_agent');
      if ($this->agent->browser() == 'Internet Explorer' && $this->agent->version() <= 11) {
        redirect(base_url().'index.php/unsupported', 'refresh');
      }
    }
  

    public function index()
    {
        $data['user'] = array();
        $data['errors'] = '';
        $data['rut']    = $this->input->post('user');
        if($this->input->post())
        {
          $this->form_validation->set_rules('pass', 'pass', 'trim|required|callback_checkUser');
          $this->form_validation->set_rules('user', 'Rut', 'trim|required|min_length[8]|max_length[9]|alpha_numeric');

          if ($this->form_validation->run() == FALSE)
          {
              $data['errors'] = validation_errors();
          }
          else
          {
            $rut = $this->input->post('user');
            $pass = $this->input->post('pass');

            $upper_rut = strtoupper($rut);   //cast rut to upper case
            $upper_rut = substr($upper_rut, 0, -1).'-'.substr($upper_rut, -1);  //add dash '-' to rut

            if(!$this->check_database($upper_rut))  //User doesn't exists
            {
                redirect(site_url(). '/sin_acceso');
            }
            else
            {
                $this->session->set_userdata('last_visited' , time());
                redirect(site_url() . '/dashboard');
            }
          }
        }
        $this->load->view('vLogin', $data);
    }

    public function check_database($rut_get)
    {
      $rut = $this->reset_rut($rut_get);
      $result = $this->login->check($rut);
      if(!empty($result)){
        foreach($result as $row) {
          $this->session->set_userdata('collaborator', $row->ID_COLABORADOR);
          $this->session->set_userdata('rut', $row->CODIGO);
          $this->session->set_userdata('name', $row->NOMBRE);
          $this->session->set_userdata('email', $row->EMAIL);
          $this->session->set_userdata('profile_id', $row->PERFIL);
          $this->session->set_userdata('deparment', $row->DEPARTAMENTO);
          $this->session->set_userdata('casino', $row->ID_CASINO);
          $this->session->set_userdata('card', $row->CREDENCIAL);
          $this->session->set_userdata('casino_name', $row->CASINO);
          $this->session->set_userdata('perfil_name', $row->PERFIL_NOMBRE);
          $this->session->set_userdata('last_visited' , time());
          $this->session->set_userdata('login', true);
        }
        return TRUE;
      }else{
        $this->form_validation->set_message('check_database', 'Usuario o Clave no son correctas');
        return FALSE;
      }
    }

    public function checkUser()
    {
      $upper_rut = strtoupper($this->input->post('user'));   //cast rut to upper case
      $upper_rut = substr($upper_rut, 0, -1).'-'.substr($upper_rut, -1);  //add dash '-' to rut

      // die(var_dump($this->input->post()));
      $elemnt = (object) array(
        'rut' => $upper_rut,
        'usuclave' => $this->input->post('pass')
      );
      if ($this->login->checkUser($elemnt)) {
        return true;
      }
      return false;
    }

    private function reset_rut($rut)
    {
      $set_rut   = str_replace(array('.','-'), '', $rut);
      $upper_rut = strtoupper($set_rut);   //cast rut to upper case
      $upper_rut = substr($upper_rut, 0, -1).'-'.substr($upper_rut, -1);  //add dash '-' to rut
      return $upper_rut;
    }

    public function recover_pass()
    {
      if ($this->input->post()) {
        $rut   = $this->input->post('rut_col');
        $id    = ''; 
        $upper_rut = strtoupper($rut);   //cast rut to upper case
        $upper_rut = substr($upper_rut, 0, -1).'-'.substr($upper_rut, -1);  //add dash '-' to rut
        $email = $this->input->post('email');    
        $check = $this->login->recover_pass_col($upper_rut);
        if (!empty($check)) {
          foreach ($check as $key) {
            $id = $key->ID_COLABORADOR; 
          }
          $new = $this->randomString();
          $update_ = $this->login->update_pass($upper_rut,$new,$id);
          if ($update_ == TRUE) {
            $content = 'Su nueva clave es :'.$new;
            $this->send_mail($content,$email);
          }else{
            echo json_encode(array('status' => false,'menssage' => 'Los datos ingresados no son válidos en muestro sistema, debe ingresar adecuadamente sus datos acorde a sus datos originales'));
          }
        }else{
          echo json_encode(array('status' => false,'menssage' => 'Los datos ingresados no son válidos en muestro sistema, debe ingresar adecuadamente sus datos acorde a sus datos originales'));
        }
      }else{
        echo json_encode(array('status' => false,'menssage' => 'Los datos ingresados no son válidos'));
      }
    }

    function randomString($length = 5) {
      $str = "";
      $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
      $max = count($characters) - 1;
      for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
      }
      $str = mb_convert_case($str, MB_CASE_LOWER, "UTF-8"); 
      return $str;
    }

    public function logout()
    {
      $this->session->unset_userdata('name');
      $this->session->unset_userdata('rut');
      $this->session->unset_userdata('lastname');
      $this->session->unset_userdata('login');
      $this->session->sess_destroy();
      redirect(base_url(), 'refresh');
    }

    public function send_mail($content,$new_email)
    {
      $email= $new_email;
      $subject="Sistema Casino Solicitud de Clave Colaborador";
      $message= $content;
      $this->sendEmail($email,$subject,$message);
    }

    public function sendEmail($email,$subject,$message)
    {
      $data = array(
        'title'    => $subject,
        'menssage' => $message,
      );
      $body = $this->load->view('dashboard/email/vEmail', $data, TRUE);
      $result = $this->home->send_email($email,$body,$subject);
      echo json_encode(array('status' => true,'menssage' => 'Se realizo correctamente la operación en unos segundos llegara un correo a su email con su nueva contraseña'));
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
