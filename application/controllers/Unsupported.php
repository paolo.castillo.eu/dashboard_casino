<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unsupported extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('errors/browser/vUnsupported');
	}

	public function stop()
	{
		$this->load->view('vUser_normal');
	}

}

/* End of file Unsupported.php */
/* Location: ./application/controllers/Unsupported.php */