<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cprofile extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->model('Collaborator_model','collaborator');
		$this->load->model('Area_model','area');
		$this->load->model('Profile_model','profile');
    	$this->load->model('Home_model','home');

		if ($this->session->profile_id == 3 || $this->session->profile_id == 4 || $this->session->profile_id == 2) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}

	}

	public function index()
	{
		$data["perfil_collaborator"] = $this->get_data_collaborador();
		$this->load->view($this->header);
		$this->load->view('dashboard/colaborador/vPerfil',$data);
		$this->load->view($this->footer);
	}

	private function get_data_collaborador()
	{
		$id        = $this->session->collaborator;
		$name      = '';
		$rut       = '';
		$card      = '';
		$deparment = '';
		$phy_acc   = '';
		$phy_loc   = '';
		$rut_vi    = '';
		$vi_card   = '';
		$email     = '';
		$id_area   = '';
		$id_perfil = '';
		$data      = $this->collaborator->get_data_colaborador($id);
		$data      = (!empty($data[0])) ? $data[0] : $data;
		$result    = array();
		if (empty($data->ID_AREA)) {
			$id_area = 'Sin datos';
		}else{
			$id_area = $this->area->get_name($data->ID_AREA);
		}
		if (empty($data->ID_PERFIL)) {
			$id_perfil = 'Sin datos';
		}else{
			$id_perfil = $this->profile->get_name($data->ID_PERFIL);
		}
		if (empty($data->NOMBRE)) {
			$name = 'Sin datos';
		}else{
			$name = $data->NOMBRE;
		}
		if (empty($data->CODIGO)) {
			$rut = 'Sin datos';
		}else{
			$rut = $data->CODIGO;
		}
		if (empty($data->CREDENCIAL)) {
			$card = 'Sin datos';
		}else{
			$card = $data->CREDENCIAL;
		}
		if (empty($data->DEPARTAMENTO)) {
			$deparment = 'Sin datos';
		}else{
			$deparment = $data->DEPARTAMENTO;
		}
		if (empty($data->UBICACION_CONTABLE)) {
			$phy_acc = 'Sin datos';
		}else{
			$phy_acc = $data->UBICACION_CONTABLE;
		}
		if (empty($data->UBICACION_FISICA)) {
			$phy_loc = 'Sin datos';
		}else{
			$phy_loc = $data->UBICACION_FISICA;
		}
		if (empty($data->RUTSOLICITANTE)) {
			$rut_vi = 'Sin datos';
		}else{
			$rut_vi = $data->RUTSOLICITANTE;
		}
		if (empty($data->NRO_TARJETA_VISITA)) {
			$vi_card = 'Sin datos';
		}else{
			$vi_card = $data->NRO_TARJETA_VISITA;
		}
		if (empty($data->CORREO)) {
			$email = 'Sin datos';
		}else{
			$email = $data->CORREO;
		}
        $result = (object) array(
			"name"               => $name,
			"rut"                => $rut,
			"card"               => $card,
			"area"               => $id_area,
			"deparment"          => $deparment,
			"physical_accountan" => $phy_acc,
			"physical_location"  => $phy_loc,
			"rut_request"        => $rut_vi,
			"card_visit"         => $vi_card,
			"email"              => $email,
			"perfil"             => $id_perfil,
        );
		return $result;
	}

	public function edit()
	{
		if ($this->input->post()) {
			$id_colaborador = $this->session->collaborator;
			$new_email   = $this->input->post('email');
			$this->form_validation->set_rules('email', 'email', 'required|valid_email');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array('status' => false,'menssage' => 'Algunos datos no son correctos'));
			} else {
				$update_pass = $this->collaborator->update_perfil($id_colaborador,$new_email);
				if ($update_pass == TRUE) {
					$content = 'Su nuevo email es: '.$new_email;
					$this->send_mail($content,$new_email);
					echo json_encode(array('status' => true, 'menssage' => 'Se actualizado correctamente los datos, en 5 segundo mas se redirigirá al Inicio de sesión'));
					$this->logout(); 
				}else{
					echo json_encode(array('status' => false,'menssage' => 'No se han ingresado correctamente la información'));
				}
			}
		}else{
			echo json_encode(array('status' => false,'menssage' => 'La contraseña ingresada no es valida'));
		}
	}

    public function logout()
    {
         $this->session->unset_userdata('name');
         $this->session->unset_userdata('rut');
         $this->session->unset_userdata('lastname');
         $this->session->unset_userdata('login');
         $this->session->sess_destroy();
	}

    public function send_mail($content,$new_email)
    {
      $email= $new_email;
      $subject="Sistema Casino";
      $message= $content;
      $this->sendEmail($email,$subject,$message);
    }

	public function sendEmail($email,$subject,$message)
	{
		$data = array(
		  'title'    => $subject,
		  'menssage' => $message,
		);
		$body = $this->load->view('dashboard/email/vEmail', $data, TRUE);
		$result = $this->home->send_email($email,$body,$subject);
	}
}

/* End of file Cprofile.php */
/* Location: ./application/controllers/Cprofile.php */