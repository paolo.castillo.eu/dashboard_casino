<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccollaborator extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Collaborator_model','collaborator');
		$this->load->model('Area_model','area');
		$this->load->model('Profile_model','profile');
		$this->load->model('Casino_model','casino');

		if ($this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['area']   = $this->get_area();
		$data['perfil'] = $this->get_perfil();
		$data['casino'] = $this->get_casino();

		$this->load->view($this->header);
		$this->load->view('dashboard/colaborador/vCollaborator',$data);
		$this->load->view($this->footer);
	}

	private function get_area()
	{
		$data = $this->area->get_all();
		return $data;	
	}

	private function get_perfil()
	{
		$data = $this->profile->get_all();
		return $data;		
	}

	private function get_casino()
	{
		$data = $this->casino->get_all();
		return $data;
	}

	public function getCollaborator_by($step)
	{
		$data = $this->collaborator->get_by_id($step);
		echo json_encode($data); 
	}


	public function save()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[200]');
			$this->form_validation->set_rules('email', 'Correo', 'required|min_length[2]|max_length[100]|valid_email');
			$this->form_validation->set_rules('rut', 'Rut', 'required');
			$this->form_validation->set_rules('card', 'Credencial', 'required|min_length[2]');
			$this->form_validation->set_rules('rut_request', 'Rut', 'required');
			$this->form_validation->set_rules('deparment', 'Departamento', 'required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('company', 'Empresa', 'required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('profile', 'Perfil', 'required|callback_check_default');
			if ($this->form_validation->run() == FALSE) {
		  		echo json_encode(array("status" => FALSE));
			}else{
				$id_                = $this->collaborator->last_id();
				$id_colaborador     = $id_->LASTID;
				$name               = strtoupper($this->input->post('name'));
				$email              = $this->input->post('email');
				$codigo_            = $this->input->post('rut');
				$codigo             = $this->reset_rut($codigo_);
				$card               = $this->input->post('card');
				$deparment          = ucwords(mb_strtolower($this->input->post('deparment')));
				$company            = ucwords(mb_strtolower($this->input->post('company')));
				$management         = ucwords(mb_strtolower($this->input->post('management')));
				$card_visit         = $this->input->post('card_visit');
				$rut_request        = $this->input->post('rut_request');
				$physical_accountan = $this->input->post('physical_accountan');
				$physical_location  = ucwords(mb_strtolower($this->input->post('physical_location')));
				$id_area            = $this->input->post('area');
				$id_perfil          = $this->input->post('profile');
				$id_casino          = $this->input->post('casino');
				$by_create = $this->session->collaborator;
				$insert = $this->collaborator->add($id_colaborador,$name,$email,$codigo,$card,$deparment,$company,$management,$card_visit,$rut_request,$physical_accountan,$physical_location,$id_area,$id_perfil,$id_casino,$by_create);
				if ($insert == TRUE) {
					echo json_encode(array("status" => TRUE));  
				}else{
					echo json_encode(array("status" => FALSE));  
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[200]');
			$this->form_validation->set_rules('email', 'Correo', 'required|min_length[2]|max_length[100]|valid_email');
			$this->form_validation->set_rules('rut', 'Rut', 'required');
			$this->form_validation->set_rules('card', 'Credencial', 'required|min_length[2]');
			$this->form_validation->set_rules('rut_request', 'Rut', 'required');
			$this->form_validation->set_rules('deparment', 'Departamento', 'required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('company', 'Empresa', 'required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('profile', 'Perfil', 'required|callback_check_default');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));  
			} else {
				$id_colaborador     = $this->input->post('id_collaborator');
				$name               = strtoupper($this->input->post('name'));
				$email              = $this->input->post('email');
				$codigo_            = $this->input->post('rut');
				$codigo             = $this->reset_rut($codigo_);
				$card               = $this->input->post('card');
				$deparment          = ucwords(mb_strtolower($this->input->post('deparment')));
				$company            = ucwords(mb_strtolower($this->input->post('company')));
				$management         = ucwords(mb_strtolower($this->input->post('management')));
				$card_visit         = $this->input->post('card_visit');
				$rut_request        = $this->input->post('rut_request');
				$physical_accountan = $this->input->post('physical_accountan');
				$physical_location  = ucwords(mb_strtolower($this->input->post('physical_location')));
				$id_area            = $this->input->post('area');
				$id_perfil          = $this->input->post('profile');
				$id_casino          = $this->input->post('casino');
				$by_modify = $this->session->collaborator;
				$update = $this->collaborator->update($id_colaborador,$name,$email,$codigo,$card,$deparment,$company,$management,$card_visit,$rut_request,$physical_accountan,$physical_location,$id_area,$id_perfil,$id_casino,$by_modify);
				if ($update == TRUE) {
					echo json_encode(array("status" => TRUE));  
				}else{
					echo json_encode(array("status" => FALSE));  
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

  	public function dele($id)
  	{
  		$by_modify = $this->session->collaborator;
	    $result = $this->collaborator->delete($id,$by_modify);
	    if ($result == true) {
	        echo json_encode(array("status" => TRUE));  
	    } else {
	        echo json_encode(array("status" => FALSE));  
	    }
  	}

	public function getCollaboratorAll()
	{
	    $fields = $this->collaborator->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    $empty  = 'No existe dato';
	    foreach ($fields as $collaborator) {
			$no++;
			$row    = array();
			$row[]  = $collaborator->ID_COLABORADOR;
			$row[]  = ucwords(mb_strtolower($collaborator->NOMBRE));
			if (empty($collaborator->CORREO)) {
				$row[] = $empty; 
			}else{
				$row[]  = $collaborator->CORREO;
			}
			if (empty($collaborator->CODIGO)) {
				$row[] = $empty; 
			}else{
				$row[]  = $collaborator->CODIGO;
			}
			if (empty($collaborator->CREDENCIAL)) {
				$row[] = $empty; 
			}else{
				$row[]  = $collaborator->CREDENCIAL;
			}
			if (empty($collaborator->DEPARTAMENTO)) {
				$row[] = $empty; 
			}else{
				$row[]  = ucwords(mb_strtolower($collaborator->DEPARTAMENTO));
			}
			if (is_numeric($collaborator->EMPRESA)) {
				$row[] = $empty; 
			}else{
				$row[]  = ucwords(mb_strtolower($collaborator->EMPRESA));
			}
			if (empty($collaborator->GERENCIA)) {
				$row[] = $empty; 
			}else{
				$row[]  = ucwords(mb_strtolower($collaborator->GERENCIA));
			}
			if (empty($collaborator->NRO_TARJETA_VISITA)) {
				$row[] = $empty; 
			}else{
				$row[]  = $collaborator->NRO_TARJETA_VISITA;
			}
			if (empty($collaborator->RUTSOLICITANTE)) {
				$row[] = $empty; 
			}else{
				$row[]  = $collaborator->RUTSOLICITANTE;
			}
			if (empty($collaborator->UBICACION_CONTABLE)) {
				$row[] = $empty; 
			}else{
				$row[]  = $collaborator->UBICACION_CONTABLE;
			}
			if (empty($collaborator->UBICACION_FISICA)) {
				$row[] = $empty; 
			}else{
				$row[]  = ucwords(mb_strtolower($collaborator->UBICACION_FISICA));
			}
			if (empty($collaborator->ID_AREA)) {
				$row[] = $empty; 
			}else{
				$row[]  = ucwords(mb_strtolower($this->area->get_name($collaborator->ID_AREA)));
			}
			if (empty($collaborator->ID_PERFIL)) {
				$row[] = $empty; 
			}else{
				$row[]  = ucwords(mb_strtolower($this->profile->get_name($collaborator->ID_PERFIL)));					
			}
			if (empty($collaborator->ID_CASINO)) {
				$row[] = $empty; 
			}else{
				$row[]  = ucwords(mb_strtolower($this->casino->get_name($collaborator->ID_CASINO)));
			}
			$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updateCollaborator('."'".$collaborator->ID_COLABORADOR."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$collaborator->ID_COLABORADOR.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($collaborator->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
            </a>';
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->collaborator->count_all(),
	                    "recordsFiltered" => $this->collaborator->count_filtered(),
	                    "data" => $data,
	    );
	    //output to json format
	    echo json_encode($output);
	}

	function check_default($post_string)
	{
		return $post_string == 'default' ? FALSE : TRUE;
	}

	function alpha_dash_space($str)
    {
	    return ( ! preg_match("/^([-a-z0-9_ ])+$/i", $str)) ? FALSE : TRUE;
    }

	private function reset_rut($rut)
	{
		$set_rut   = str_replace(array('.','-'), '', $rut);
		$upper_rut = strtoupper($set_rut);   //cast rut to upper case
		$upper_rut = substr($upper_rut, 0, -1).'-'.substr($upper_rut, -1);  //add dash '-' to rut
		return $upper_rut;
	}
}

/* End of file Ccollaborator.php */
/* Location: ./application/controllers/Ccollaborator.php */