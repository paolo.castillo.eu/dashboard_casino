<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cservice extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Type_service','service');

		if ($this->session->profile_id == 4) {
		}else{
			redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$this->load->view($this->header);
		$this->load->view('dashboard/servicio/vService');
		$this->load->view($this->footer);
	}

	public function save()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[30]');
			if ($this->form_validation->run() == FALSE) {
		  		echo json_encode(array("status" => FALSE));
			}else{
				$id_service = $this->input->post('id_service');
				$name       = $this->input->post('name');
				$by_create  = $this->session->collaborator;
				$insert    = $this->service->add($id_service,$name,$by_create);
				if ($insert == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[30]');
			if ($this->form_validation->run() == FALSE) {
		  		echo json_encode(array("status" => FALSE));
			} else {
				$id_service = $this->input->post('id_service');
				$name       = $this->input->post('name');
				$by_modify  = $this->session->collaborator;
				$update     = $this->service->update($id_service,$name,$by_modify);
				if ($update == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

  	public function dele($id)
  	{
  		$by_modify = $this->session->collaborator;
	    $result = $this->service->delete($id,$by_modify);
	    if ($result == true) {
	        echo json_encode(array("status" => TRUE));  
	    } else {
	        echo json_encode(array("status" => FALSE));  
	    }
  	}

	public function getService_by($step)
	{
		$data = $this->service->get_by_id($step);
		echo json_encode($data);
	}

	public function getServiceAll()
	{
	    $fields = $this->service->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    foreach ($fields as $service) {
			$no++;
			$row    = array();
			$row[]  = $service->ID_TIPO_SERVICIO;
			$row[]  = $service->NOMBRE;
			$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updateService('."'".$service->ID_TIPO_SERVICIO."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$service->ID_TIPO_SERVICIO.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($service->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
            </a>';
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->service->count_all(),
	                    "recordsFiltered" => $this->service->count_filtered(),
	                    "data" => $data,
	    );
	    //output to json format
	    echo json_encode($output);
	}
}

/* End of file Cservice.php */
/* Location: ./application/controllers/Cservice.php */