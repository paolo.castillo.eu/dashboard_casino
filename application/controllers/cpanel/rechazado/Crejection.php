<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crejection extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';


	public function __construct()
	{
		parent::__construct();
		$this->load->model('Rejection_model','rejection');
		$this->load->model('Casino_model','casino');
		$this->load->model('Ticket_model','ticket');
		$this->load->model('Collation_model','collation');
		$this->load->model('Collaborator_model','collaborator');
    	$this->load->model('Plant_model','plant');
		$this->load->model('Report_model','report');
		
	    if ($this->session->profile_id == 3 || $this->session->profile_id == 4) {
	    }else{
	        redirect(base_url().'index.php/', 'refresh');
	    }
	}

	public function index()
	{
		if ($this->session->profile_id == 4) {
			$data['estado']   = $this->getState(0);
			$data['planta'] = $this->getPlant();
		}elseif ($this->session->profile_id == 3) {
			$data['estado']   = $this->getState(0);
			$data['colacion'] = $this->collation->get_collation_casino($this->session->casino);
		}
		$data['empresa'] = $this->report->get_empresa();
		$this->load->view($this->header);
		$this->load->view('dashboard/rechazo/vRejection',$data);
		$this->load->view($this->footer);
	}

	public function get_rechazado()
	{
	    $fields = $this->rejection->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    $_element = '';
	    foreach ($fields as $rejection) {
			$no++;
			$row    = array();
			$row[]  = $rejection->CODIGO;
			$row[]  = $this->pfalimentos->upper_lower($rejection->NOMBRE);
			$row[]  = $this->pfalimentos->upper_lower($rejection->DEPARTAMENTO);
			if (is_numeric($rejection->EMPRESA) || $rejection->EMPRESA == '.' || $rejection->EMPRESA == '') {
				$row[]  = $this->pfalimentos->upper_lower('SIN EMPRESA');
			}else{
				$row[]  = $this->pfalimentos->upper_lower($rejection->EMPRESA);
			}
			$row[]  = $this->pfalimentos->upper_lower($rejection->PLANTA);
			$row[]  = $this->pfalimentos->upper_lower($rejection->CASINO);
			$row[]  = $rejection->FECHA.' '.$rejection->HORA_ENTREGA;
			$row[]  = $this->pfalimentos->replace_guion_espacio($rejection->COLACION);
			$row[]  = $this->pfalimentos->upper_lower($rejection->ESTADO);
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->rejection->count_all(),
	                    "recordsFiltered" => $this->rejection->count_filtered(),
	                    "data" => $data,
	    );
	    $this->output->set_output(json_encode($output));
	}

	private function getPlant()
	{
		$data = $this->plant->get_all();
		return $data;
	}


	public function get_casino()
	{
		$data = array('success' => false, 'option' => array());
		if ($this->input->post()) {
			$result = $this->casino->get_casino_planta($this->input->post('planta'));
			if (!empty($result)) {
				foreach ($result as $key) {
					$data['option'][] = array(
						'values' => $key->ID_CASINO,
						'text' => $this->pfalimentos->upper_lower($key->NOMBRE)
					);
				}
			}else{
				$data['option'][] = array(
					'values' => '',
					'text' => 'Sin Casino'
				);
			}
		}else{
			$data['option'][] = array(
				'values' => '',
				'text' => 'Sin Casino'
			);
		}		
		$this->output->set_output(json_encode($data));
	}

	public function getState($step)
	{
		$data = $this->ticket->get_all($step);
		return $data;
	}

	public function get_colacion()
	{
    	$data = array('success' => false, 'option' => array());
		if ($this->input->post()) {
			$result = $this->collation->get_collation_tipo($this->input->post('casino'));
			if (!empty($result)) {
				foreach ($result as $key) {
					$data['option'][] = array(
						'values' => $key->ID_COLACION,
						'text' => $this->pfalimentos->replace_guion_espacio($key->NOMBRE)
					);
				}
			}else{
				$data['option'][] = array(
					'values' => '',
					'text' => 'Sin Casino'
				);
			}
		}else{
			$data['option'][] = array(
				'values' => '',
				'text' => 'Sin Casino'
			);
		}
		$this->output->set_output(json_encode($data));
	}

	public function get_five()
	{
		if ($this->input->post()) {
			$casino = $this->input->post('casino');
			if (!empty($casino)) {
				$row['flag'] = $this->rejection->get_ticket_rejection_five($casino);
				if (!empty($row)) {
					$data = $this->load->view('dashboard/rechazo/vHistorial', $row, true);
					$this->output->set_output(json_encode(array('status' => true, 'flag' => $data)));
				}else{
					$flag = '<div class="alert alert-warning alert-dismissible">';
					$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
					$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
					$flag.= 'No existen datos en la búsqueda realizada, debe seleccionar un casino para esta opción.';
					$flag.= '</div>';
					$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));				
				}
			}else{
				if (!empty($this->session->casino)) {					
					$row['flag'] = $this->rejection->get_ticket_rejection_five($this->session->casino);
					if (!empty($row)) {
						$data = $this->load->view('dashboard/rechazo/vHistorial', $row, true);
						$this->output->set_output(json_encode(array('status' => true, 'flag' => $data)));
					}else{
						$flag = '<div class="alert alert-warning alert-dismissible">';
						$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
						$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
						$flag.= 'No existen datos en la búsqueda realizada, debe seleccionar un casino para esta opción.';
						$flag.= '</div>';
						$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));
					}
				}else{
					$flag = '<div class="alert alert-warning alert-dismissible">';
					$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
					$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
					$flag.= 'No existen datos en la búsqueda realizada, debe seleccionar un casino para esta opción.';
					$flag.= '</div>';
					$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));
				}
			}
		}else{
			$result[] = (object)array(
			'CODIGO'       => 'Sin datos',
			'NOMBRE'       => 'Sin datos',
			'PLANTA'       => 'Sin datos',
			'CASINO'       => 'Sin datos',
			'HORA_ENTREGA' => 'Sin datos',
			'FECHA'        => date("d-m-Y"),
			'COLACION'     => 'Sin datos',
			'ESTADO'       => 'Sin datos'
			);
			return $result;
		}
	}

	function check_default($post_string)
	{
		return $post_string == '' ? FALSE : TRUE;
	}

	public function export_excel_data()
	{
		if ($this->input->post()) {
			$result = $this->rejection->export_excel_data();
			$allData = $result->result_array();
			$dataToExports = array();
			foreach ($allData as $data) {
				$arrangeData['RUT'] = $data['CODIGO'];
				$arrangeData['NOMBRE COLABORADOR'] = $this->quitar_tildes($data['NOMBRE']);
				$arrangeData['DEPARTAMENTO'] = $this->quitar_tildes($data['DEPARTAMENTO']);
				$arrangeData['EMPRESA'] = $this->quitar_tildes($data['EMPRESA']);
				$arrangeData['PLANTA'] = $data['PLANTA'];
				$arrangeData['CASINO'] = $data['CASINO'];
				$arrangeData['COLACION'] = $this->quitar_tildes($data['COLACION']);
				$arrangeData['FECHA'] = $data['FECHA'];
				$arrangeData['HORA ENTREGA'] = $data['HORA_ENTREGA'];
				$arrangeData['ESTADO'] = $this->quitar_tildes($data['ESTADO']);
				$dataToExports[] = $arrangeData;
			}
			$filename = "Casino_Export.xls";
			            header("Content-Type: application/vnd.ms-excel");
			            header("Content-Disposition: attachment; filename=\"$filename\"");
			$this->exportExcelData($dataToExports);
		}
	}

 	public function exportExcelData($records)
	{
		$heading = false;
	    if (!empty($records))
	        foreach ($records as $row) {
	            if (!$heading) {
	                echo implode("\t", array_keys($row)) . "\n";
	                $heading = true;
	            }
	            echo implode("\t", ($row)) . "\n";
	        }
	}

	function quitar_tildes($cadena) {
		$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$texto = str_replace($no_permitidas, $permitidas ,$cadena);
		return $texto;
	}
}

/* End of file Cerror.php */
/* Location: ./application/controllers/Cerror.php */