<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Carea extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Area_model','area');
		if ($this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$this->load->view($this->header);
		$this->load->view('dashboard/area/vArea');
		$this->load->view($this->footer);
	}

	public function save()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_area', 'id_area', 'required|min_length[2]|max_length[20]|numeric');
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[30]');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));
			} else {
				$id_area   = $this->input->post('id_area');
				$name      = $this->input->post('name');
				$by_create = $this->session->collaborator;
				$insert    = $this->area->add($id_area,$name,$by_create);
				if ($insert == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_area', 'id_area', 'required|min_length[2]|max_length[20]|numeric');
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[30]');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));
			}else{
				$id_area   = $this->input->post('id_area');
				$name      = $this->input->post('name');
				$by_modify = $this->session->collaborator;
				$update    = $this->area->update($id_area,$name,$by_modify);
				if ($update == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

  	public function dele($id)
  	{
  		$by_modify = $this->session->collaborator;
	    $result = $this->area->delete($id,$by_modify);
	    if ($result == true) {
	        echo json_encode(array("status" => TRUE));  
	    } else {
	        echo json_encode(array("status" => FALSE));  
	    }
  	}

	public function getArea_by($step)
	{
		$data = $this->area->get_by_id($step);
		echo json_encode($data);
	}

	public function getAreaAll()
	{
	    $fields = $this->area->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    foreach ($fields as $area) {
			$no++;
			$row    = array();
			$row[]  = $area->CODIGO;
			$row[]  = $area->ID_AREA;
			$row[]  = ucwords(mb_strtolower($area->NOMBRE));
			$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updateCasino('."'".$area->ID_AREA."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$area->ID_AREA.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($area->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
            </a>';
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->area->count_all(),
	                    "recordsFiltered" => $this->area->count_filtered(),
	                    "data" => $data,
	    );
	    //output to json format
	    echo json_encode($output);
	}

}

/* End of file Carea.php */
/* Location: ./application/controllers/Carea.php */