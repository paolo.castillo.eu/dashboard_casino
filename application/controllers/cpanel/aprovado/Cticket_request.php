<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cticket_request extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Casino_model','casino');
		$this->load->model('Ticket_model','ticket');
		$this->load->model('Collation_model','collation');
    	$this->load->model('Plant_model','plant');
    	$this->load->model('Aprobado_model','aprobado');
		$this->load->model('Report_model','report');

		$this->load->library('export_excel');
		if ($this->session->profile_id == 3 || $this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		if ($this->session->profile_id == 4) {
			$data['planta'] = $this->getPlant();
		}elseif ($this->session->profile_id == 3) {
			$data['colacion'] = $this->get_collation_by($this->session->casino);
		}
		$data['empresa'] = $this->report->get_empresa();
		$this->load->view($this->header);
		$this->load->view('dashboard/aprobado/vTicket_request',$data);
		$this->load->view($this->footer);
	}

	public function get_aprobados()
	{
	    $fields = $this->aprobado->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    $_element = '';
	    foreach ($fields as $aprobado) {
			$no++;
			$row    = array();
			$row[]  = $aprobado->CODIGO;
			$row[]  = $this->pfalimentos->upper_lower($aprobado->NOMBRE);
			$row[]  = $this->pfalimentos->upper_lower($aprobado->DEPARTAMENTO);
			if (is_numeric($aprobado->EMPRESA) || $aprobado->EMPRESA == '.' || $aprobado->EMPRESA == '') {
				$row[]  = $this->pfalimentos->upper_lower('SIN EMPRESA');
			}else{
				$row[]  = $this->pfalimentos->upper_lower($aprobado->EMPRESA);
			}
			$row[]  = $this->pfalimentos->upper_lower($aprobado->PLANTA);
			$row[]  = $this->pfalimentos->upper_lower($aprobado->CASINO);
			$row[]  = $aprobado->FECHA.' '.$aprobado->HORA_ENTREGA;
			$row[]  = $this->pfalimentos->replace_guion_espacio($aprobado->COLACION);
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->aprobado->count_all(),
	                    "recordsFiltered" => $this->aprobado->count_filtered(),
	                    "data" => $data,
	    );
	    $this->output->set_output(json_encode($output));
	}

	private function getCasino()
	{
		$data = $this->casino->get_all();
		return $data;
	}

	private function getPlant()
	{
		$data = $this->plant->get_all();
		return $data;
	}

	public function get_casino()
	{
    	$data = array('success' => false, 'option' => array());
		if ($this->input->post()) {
			$result = $this->casino->get_casino_planta($this->input->post('planta'));
			if (!empty($result)) {
				foreach ($result as $key) {
					$data['option'][] = array(
						'values' => $key->ID_CASINO,
						'text' => ucwords(mb_strtolower($key->NOMBRE))
					);
				}
			}else{
				$data['option'][] = array(
					'values' => '',
					'text' => 'Todos'
				);
			}
		}else{
			$data['option'][] = array(
				'values' => '',
				'text' => 'Todos'
			);
		}		
		$this->output->set_output(json_encode($data));
	}

	public function get_collation()
	{
    	$data = array('success' => false, 'option' => array());
		if ($this->input->post()) {
			$result = $this->collation->get_collation_tipo($this->input->post('casino'));
			if (!empty($result)) {
				foreach ($result as $key) {
					$data['option'][] = array(
						'values' => $key->ID_COLACION,
						'text' => $this->pfalimentos->replace_guion_espacio($key->NOMBRE)
					);
				}
			}else{
				$data['option'][] = array(
					'values' => '',
					'text' => 'Sin Casino'
				);
			}
		}else{
			$data['option'][] = array(
				'values' => '',
				'text' => 'Sin Casino'
			);
		}
		$this->output->set_output(json_encode($data));
	}

	public function get_collation_by($id)
	{
		if (!empty($id)) {
			$row = $this->collation->get_collation_casino($id);
			if (!empty($row)) {
				return $row;
			}else{
				$result[] = (object)array(
					'ID_COLACION' => '',
					'NOMBRE'      => 'Sin datos' 
				);
			}
		}else{
			redirect(site_url(). '/dashboard','refresh');
		}
	}

	public function export_excel_data()
	{
		if ($this->input->post()) {
			$result = $this->aprobado->export_excel_data();
			$allData = $result->result_array();
			$dataToExports = array();
			foreach ($allData as $data) {
				$arrangeData['RUT']                = $data['CODIGO'];
				$arrangeData['NOMBRE COLABORADOR'] = $this->pfalimentos->upper_lower($this->quitar_tildes($data['NOMBRE']));
				$arrangeData['EMPRESA']            = $this->pfalimentos->upper_lower($this->quitar_tildes($data['EMPRESA']));
				$arrangeData['DEPARTAMENTO']       = $this->pfalimentos->upper_lower($this->quitar_tildes($data['DEPARTAMENTO']));
				$arrangeData['PLANTA']             = $this->pfalimentos->upper_lower($data['PLANTA']);
				$arrangeData['CASINO']             = $this->pfalimentos->upper_lower($data['CASINO']);
				$arrangeData['COLACION']           = $this->quitar_tildes($data['COLACION']);
				$arrangeData['FECHA']              = $data['FECHA'];
				$arrangeData['HORA ENTREGA']       = $data['HORA_ENTREGA'];
				$arrangeData['COLACION']           = $this->quitar_tildes($this->pfalimentos->replace_guion_espacio($data['COLACION']));
				$arrangeData['CANTIDAD']           = $data['CANTIDAD'];
				$dataToExports[] = $arrangeData;
			}
			$filename = "Casino_Export.xls";
			            header("Content-Type: application/vnd.ms-excel");
			            header("Content-Disposition: attachment; filename=\"$filename\"");
			$this->exportExcelData($dataToExports);
		}
	}

 	public function exportExcelData($records)
	{
		$heading = false;
	    if (!empty($records))
	        foreach ($records as $row) {
	            if (!$heading) {
	                echo implode("\t", array_keys($row)) . "\n";
	                $heading = true;
	            }
	            echo implode("\t", ($row)) . "\n";
	        }
	}

	function quitar_tildes($cadena) {
		$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$texto = str_replace($no_permitidas, $permitidas ,$cadena);
		return $texto;
	}
}

/* End of file Cticket_request.php */
/* Location: ./application/controllers/Cticket_request.php */