<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csolicitude_req extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';
	
	public function __construct()
	{
		parent::__construct();
    	$this->load->model('Plant_model','plant');
    	$this->load->model('Solicitude_model','solicitude');
    	$this->load->model('Report_model','report');
		$this->load->model('Collation_model','collation');
		$this->load->model('Ticket_model','ticket');
		$this->load->model('Collaborator_model','collaborator');
    	$this->load->model('Home_model','home');


		if ($this->session->profile_id == 3 || $this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['planta']   = $this->getPlant();
		$data['empresa']  = $this->report->get_empresa();
		$data['colacion'] = $this->get_collation_by($this->session->casino);
		$this->load->view($this->header);
		$this->load->view('dashboard/solicitudes/vRequest',$data);
		$this->load->view($this->footer);
	}

	
	public function get_collation_by($id)
	{
		if (!empty($id)) {
			$row = $this->collation->get_collation_casino($id);
			if (!empty($row)) {
				return $row;
			}else{
				$result[] = (object)array(
					'ID_COLACION' => '',
					'NOMBRE'      => 'Sin datos' 
				);
			}
		}else{
			redirect(site_url(). '/dashboard','refresh');
		}
	}


	public function dele()
	{
		$parm['input'] = $this->input->post('param');
		$parm['show']  = 0;
		$data['view']  = $this->load->view('dashboard/solicitudes/vModalR', $parm, true);
	    $this->output->set_output(json_encode($data));
	}


	public function rejection()
	{
		$data = array('success' => false,'message' => '');
		if ($this->input->post()) {
			$rej = $this->solicitude->rejection($this->input->post('id'),$this->session->collaborator);
			$tic = $this->ticket->reject_ticket($this->input->post('id_solicitud'),$this->session->collaborator);
			if ($rej == true && $tic == true) {
				$get_email = $this->collaborator->get_email_by_rut($this->input->post('id_rut'));
				$email = $get_email->CORREO;
				// $email = 'paolo.castillo.eu@gmail.com';
				$subject = 'Rechazo';
				$message = 'Su Solicitud ha sido rechazada. Contactase con el administrador de casino. Casino : '.$this->input->post('id_casino');
				$this->sendEmail($email,$subject,$message,0);
				$data['success'] = true;
				$data['message'] = 'Solicitud rechazada';
			}else{
				$data['message'] = 'Error al rechazar la solicitud';
			}
		}else{
			$data['message'] = 'Error al rechazar la solicitud';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function get_request()
	{

		#solicitude_request
	    $fields = $this->solicitude->get_datatables_apr();
	    $data   = array();
	    $no     = $_POST['start'];
	    $_element = '';
	    foreach ($fields as $solicitude) {
			$no++;
	    	$format = "d/m/Y";
			$date1  = DateTime::createFromFormat($format, date('d/m/Y'));
			$date2  = DateTime::createFromFormat($format, $solicitude->FECHA);
			$row    = array();
			$row[]  = $solicitude->ID_TICKET;
			$row[]  = $solicitude->ID_SOLICITUD;
			$row[]  = $solicitude->FECHA;
			$row[]  = $solicitude->CODIGO;
			$row[]  = $this->pfalimentos->upper_lower($solicitude->NOMBRE);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->DEPARTAMENTO);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->EMPRESA);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->PLANTA);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->CASINO);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->COLACION);
			$row[]  = $this->pfalimentos->replace_guion_espacio($solicitude->JUSTIFICACION);
			if ($date2 >= $date1) {
				$row[] = '<button type="button" class="btn btn-info btn-block btn-show"><i class="fa fa-eye"></i></button>';
			}else{
				$row[] = '-------------';
			}
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->solicitude->count_all_apr(),
	                    "recordsFiltered" => $this->solicitude->count_filtered_apr(),
	                    "data" => $data,
	    );
	    $this->output->set_output(json_encode($output));
	}

	private function getPlant()
	{
		$data = $this->plant->get_all();
		return $data;
	}

	private function date()
	{

		$date = date("d/m/Y");
		return $date;
	}

	function check_default($post_string)
	{
		return $post_string == 'default' ? FALSE : TRUE;
	}


  	public function sendEmail($email,$subject,$message,$lv)
    {

		$data = array(
			'title'    => $subject,
			'menssage' => $message,
		);

    	$body = '';
    	if ($lv == 1) {
			$body = $this->load->view('dashboard/email/vEmail', $data, TRUE);
    	}else{
			$body = $this->load->view('dashboard/email/vError', $data, TRUE);
    	}

		$result = $this->home->send_email($email,$body,$subject);
    }
}

/* End of file Csolicitude_req.php */
/* Location: ./application/controllers/Csolicitude_req.php */