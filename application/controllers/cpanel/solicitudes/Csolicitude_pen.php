<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csolicitude_pen extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Collaborator_model','collaborator');
    	$this->load->model('Plant_model','plant');
    	$this->load->model('Solicitude_model','solicitude');
		$this->load->model('Ticket_model','ticket');
    	$this->load->model('Home_model','home');
    	$this->load->model('Report_model','report');
		$this->load->model('Collation_model','collation');

		if ($this->session->profile_id == 3 || $this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['planta']   = $this->getPlant();
		$data['empresa']  = $this->report->get_empresa();
		$data['colacion'] = $this->get_collation_by($this->session->casino);
		// die(var_dump($data));
		$this->load->view($this->header);
		$this->load->view('dashboard/solicitudes/vPending',$data);
		$this->load->view($this->footer);
	}

	public function get_collation_by($id)
	{
		if (!empty($id)) {
			$row = $this->collation->get_collation_casino($id);
			if (!empty($row)) {
				return $row;
			}else{
				$result[] = (object)array(
					'ID_COLACION' => '',
					'NOMBRE'      => 'Sin datos' 
				);
			}
		}else{
			redirect(site_url(). '/dashboard','refresh');
		}
	}

	public function get_pending()
	{
	    $fields = $this->solicitude->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    $_element = '';
	    foreach ($fields as $solicitude) {
			$no++;
			$row    = array();
			$row[]  = $solicitude->ID_SOLICITUD;
			$row[]  = $solicitude->FECHA;
			$row[]  = $solicitude->CODIGO;
			$row[]  = $this->pfalimentos->upper_lower($solicitude->COLABORADOR);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->DEPARTAMENTO);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->EMPRESA);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->NOMBRE_PLANTA);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->NOMBRE_CASINO);
			$row[]  = $this->pfalimentos->replace_guion_espacio($solicitude->NOMBRE_COLACION);
			$row[]  = $this->pfalimentos->upper_lower($solicitude->JUSTIFICACION);
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->solicitude->count_all(),
	                    "recordsFiltered" => $this->solicitude->count_filtered(),
	                    "data" => $data,
	    );
	    $this->output->set_output(json_encode($output));
	}


	public function aproved()
	{
		$data = array('success' => false,'message' => '');
		if ($this->input->post()) {
			$aproved = $this->solicitude->aproved($this->input->post('id'),$this->session->collaborator,$this->input->post('id_rut'));
			if ($aproved) {
				$get_email = $this->collaborator->get_email_by_rut($this->input->post('id_rut'));
				$email = $get_email->CORREO;
				// $email = 'paolo.castillo.eu@gmail.com';
				$subject = 'Aprobación';
				$message = 'Su Solicitud ha sido aprovado ahora puede ir al casino solicitado. Casino : '.$this->input->post('id_casino');
    			$this->sendEmail($email,$subject,$message,1);
				$data['success'] = true;
				$data['message'] = 'Solicitud Aprobada';
			}else{
				$data['message'] = 'Error al rechazar la solicitud';
			}
		}else{
			$data['message'] = 'Error al rechazar la solicitud';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function rejection()
	{
		$data = array('success' => false,'message' => '');
		if ($this->input->post()) {
			$rej = $this->solicitude->rejection($this->input->post('id'),$this->session->collaborator);
			if ($rej) {
				$get_email = $this->collaborator->get_email_by_rut($this->input->post('id_rut'));
				$email = $get_email->CORREO;
				// $email = 'paolo.castillo.eu@gmail.com';
				$subject = 'Rechazo';
				$message = 'Su Solicitud ha sido rechazada. Contactase con el administrador de casino. Casino : '.$this->input->post('id_casino');
				$this->sendEmail($email,$subject,$message,0);
				$data['success'] = true;
				$data['message'] = 'Solicitud rechazada';
			}else{
				$data['message'] = 'Error al rechazar la solicitud';
			}
		}else{
			$data['message'] = 'Error al rechazar la solicitud';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function get_modal()
	{
		$parm['input']= $this->input->post('param');
		// die(var_dump($parm));

		// (object) array(
		// 	'rut' => $this->input->post('param')[2],
		// 	'nombre' => $this->input->post('param')[3],
		// 	'departamento' => $this->input->post('param')[4],
		// 	'empresa' => $this->input->post('param')[5],
		// 	'planta' => $this->input->post('param')[6],
		// 	'casino' => $this->input->post('param')[7],
		// 	'colacion' => $this->input->post('param')[8],
		// 	'fecha' => $this->input->post('param')[1],
		// 	'id' => [0]
		// );
		// die(var_dump($parm));
		// die(var_dump($this->input->post('param')));
		// $parm['input'][] = (object) array('rut' => $this->input->post('param')[2]);
		// $parm['input'][] = (object) array('nombre' => $this->input->post('param')[3]);
		// $parm['input'][] = (object) array('departamento' => $this->input->post('param')[4]);
		// $parm['input'][] = (object) array('empresa' => $this->input->post('param')[5]);
		// $parm['input'][] = (object) array('planta' => $this->input->post('param')[6]);
		// $parm['input'][] = (object) array('casino' => $this->input->post('param')[7]);
		// $parm['input'][] = (object) array('colacion' => $this->input->post('param')[8]);
		// $parm['input'][] = (object) array('fecha' => $this->input->post('param')[1]);
		// $parm['input'][] = (object) array('id' => $this->input->post('param')[0]);
		// die(var_dump($parm['input']));
		$data['view']  = $this->load->view('dashboard/solicitudes/vModal', $parm, true);
	    $this->output->set_output(json_encode($data));
	}

	private function getPlant()
	{
		$data = $this->plant->get_all();
		return $data;
	}

	private function date()
	{
		$date = date("d/m/Y");
		return $date;
	}

	function check_default($post_string)
	{
		return $post_string == 'default' ? FALSE : TRUE;
	}

  	public function sendEmail($email,$subject,$message,$lv)
    {

		$data = array(
			'title'    => $subject,
			'menssage' => $message,
		);

    	$body = '';
    	if ($lv == 1) {
			$body = $this->load->view('dashboard/email/vEmail', $data, TRUE);
    	}else{
			$body = $this->load->view('dashboard/email/vError', $data, TRUE);
    	}

		$result = $this->home->send_email($email,$body,$subject);
    }
}

/* End of file Csolicitude_pen.php */
/* Location: ./application/controllers/Csolicitude_pen.php */