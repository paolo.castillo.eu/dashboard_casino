<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cprofile extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Profile_model','profile');
	    if ($this->session->profile_id == 4) {
	    }else{
	        redirect(base_url().'index.php/', 'refresh');
	    }
	}

	public function index()
	{
		$this->load->view($this->header);
		$this->load->view('dashboard/perfil/vProfile');
		$this->load->view($this->footer);
	}

	public function save()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[20]');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));
			}else{
				$name      = $this->input->post('name');
				$by_create = $this->session->collaborator;
				$insert    = $this->profile->add($name,$by_create);
				if ($insert == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}	
			}
		}else{
	  		echo json_encode(array("status" => FALSE));
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[20]');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));
			} else {
				$id_perfil = $this->input->post('id_perfil');
				$name      = strtoupper($this->input->post('name'));
				$by_modify = $this->session->collaborator;
				$update    = $this->profile->update($id_perfil,$name,$by_modify);
				if ($update == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}
			}			
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

  	public function dele($id)
  	{
  		$by_modify = $this->session->collaborator;
	    $result = $this->profile->delete($id,$by_modify);
	    if ($result == true) {
	        echo json_encode(array("status" => TRUE));  
	    } else {
	        echo json_encode(array("status" => FALSE));  
	    }
  	}

	public function getProfile_by($step)
	{
		$data = $this->profile->get_by_id($step);
		echo json_encode($data);
	}

  	public function getProfileAll()
  	{
	    $fields = $this->profile->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    foreach ($fields as $profile) {
			$no++;
			$row    = array();
			$row[]  = $profile->ID_PERFIL;
			$row[]  = ucwords(mb_strtolower($profile->NOMBRE));
			if ($profile->ID_PERFIL != 4) {
				$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updatePerfil('."'".$profile->ID_PERFIL."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$profile->ID_PERFIL.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($profile->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
	            </a>';
			}else{
				$row[] = 'Sin funciones';
 			}
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->profile->count_all(),
	                    "recordsFiltered" => $this->profile->count_filtered(),
	                    "data" => $data,
	    );
	    //output to json format
	    echo json_encode($output);
  	}

}

/* End of file Cprofile.php */
/* Location: ./application/controllers/Cprofile.php */