<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crequest extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
    	$this->load->model('Plant_model','plant');
    	$this->load->model('Solicitude_model','solicitud');
    	$this->load->model('Home_model','home');
    	$this->load->model('Collaborator_model','colaborador');

		if ($this->session->profile_id == 3 || $this->session->profile_id == 4 || $this->session->profile_id == 2) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['fecha'] = $this->date();
		$data['planta'] = $this->getPlant();
		$data['empleados'] = $this->solicitud->get_empleado_jefatura($this->session->rut);
		// $this->session->rut
		$data['solicitante'] = $this->solicitud->get_solicitante($this->session->collaborator);
		// $this->session->collaborator
		$this->load->view($this->header);
		$this->load->view('dashboard/solicitar/vRequest',$data);
		$this->load->view($this->footer);
	}

	public function save()
	{
		$data = array('success' => false,'message' => array(), 'text' => '');
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_planta', 'Planta', 'required|callback_check_default');
			$this->form_validation->set_rules('id_casino', 'Casino', 'required|callback_check_default');
			$this->form_validation->set_rules('id_colacion', 'Colación', 'required|callback_check_default');
			$this->form_validation->set_rules('id_fecha', 'Fecha', 'trim|required|min_length[8]|max_length[10]');
			$this->form_validation->set_rules('description', 'Justificación', 'required|min_length[10]|max_length[255]|callback_valid_text');
			if ($this->form_validation->run()) {
				$_email = $this->solicitud->get_email_supervisor($this->input->post('id_casino'));
				$check = $this->solicitud->check_solicitud($this->session->collaborator,$this->input->post('id_casino'),$this->input->post('id_colacion'));
				$check_hour = $this->solicitud->check_hour($this->input->post('id_colacion'),$this->input->post('id_fecha'));
				if ($check_hour == TRUE) {
					$element = (object) array(
						'colaborador' => $this->session->collaborator, 
						'casino' => $this->input->post('id_casino'),
						'colacion' => $this->input->post('id_colacion'),
						'fecha' => $this->input->post('id_fecha'),
						'detalle' => $this->input->post('description'),
						'creado' => $this->session->collaborator,
						'rut' => $this->session->rut
					);
					$flag = false;
					foreach ($check as $key) {
						if ($key->ID_TIPO_SERVICIO == 3) {
							$flag = true;
						}
					}
					if ($flag == true) {
						$save = $this->solicitud->save_solicitud($element);
						if ($save) {
							if (!empty($_email)) {
								$_email = $_email[0]->CORREO_ADMIN_PF;
								$subject  = 'Sistema Casino PF Alimentos Ticket';
								$menssage = 'Se realizo la solicitud de Ticket al Nombre de  : '.ucwords(mb_strtolower($this->session->name)).' '.$element->fecha;
								$this->sendEmail($_email,$subject,$menssage);
							}
							$subject  = 'Sistema Casino PF Alimentos Ticket';
							$menssage = 'Su Ticket esta en solicitud pendiente.
							El cualquier momento aceptaran su solicitud en el cual se 
							notificara por correo.
							Colaborador : '.ucwords(mb_strtolower($this->session->name)).' '.$element->fecha;
							$this->sendEmail($this->session->email,$subject,$menssage);
							$data['text'] = 'Hola .- <p>Se ha realizado <b>Correctamente su Solicitud</b> - En breve momento se realizara su aprobación.</p>';
							$data['success'] = true;
						}else{
							$data['text'] = 'Problema al realizar la solicitud';
						}
					}else{
						$data['text'] = 'Usted no tiene solicitud para esta colación en el casino seleccionado';
					}
				}else{
					$data['text'] = 'El casino que ha seleccionado no esta disponible por horario, de solicitud';
				}
			} else {
				foreach ($this->input->post() as $key => $value) {
					$data['message'][$key] = form_error($key);
				}
			}
		}else{
			$data['text'] = 'Debe conectarse nuevamente para seguir con la solicitud';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function save_solicitud_emp()
	{
		$data = array('success' => false,'message' => array(), 'text' => '');
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_planta_emp', 'Planta', 'required|callback_check_default');
			$this->form_validation->set_rules('id_casino_emp', 'Casino', 'required|callback_check_default');
			$this->form_validation->set_rules('id_colacion_emp', 'Colación', 'required|callback_check_default');
			$this->form_validation->set_rules('id_fecha_emp', 'Fecha', 'trim|required|min_length[8]|max_length[10]');
			$this->form_validation->set_rules('description_emp', 'Justificación', 'required|min_length[10]|max_length[255]|callback_valid_text');
			if ($this->form_validation->run()) {
				$_email = $this->solicitud->get_email_supervisor($this->input->post('id_casino_emp'));
				$check_hour = $this->solicitud->check_hour($this->input->post('id_colacion_emp'),$this->input->post('id_fecha_emp'));
				if ($check_hour) {
					$element = (object) array(
						'colaborador' => $this->input->post('id_colaborador_select'), 
						'casino' => $this->input->post('id_casino_emp'),
						'colacion' => $this->input->post('id_colacion_emp'),
						'fecha' => $this->input->post('id_fecha_emp'),
						'detalle' => $this->input->post('description_emp'),
						'creado' => $this->session->collaborator,
						'rut' => $this->input->post('id_rut')
					);
					$save = $this->solicitud->save_solicitud($element);
					if ($save) {
						if (!empty($_email)) {
							$_email = $_email[0]->CORREO_ADMIN_PF;
							$subject  = 'Sistema Casino PF Alimentos Ticket';
							$menssage = 'Se realizo la solicitud de Ticket al Nombre de  : '.ucwords(mb_strtolower($this->session->name)).' '.$element->fecha;
								$this->sendEmail($_email,$subject,$menssage);
						}
						
						$subject  = 'Sistema Casino PF Alimentos Ticket';
						$menssage = 'Su Ticket esta en solicitud pendiente.
						El cualquier momento aceptaran su solicitud en el cual se 
						notificara por correo.
						Colaborador : '.ucwords(mb_strtolower($this->input->post('id_nombre_emp'))).' '.$element->fecha;
						$this->sendEmail($this->session->email,$subject,$menssage);
						$data['text'] = 'Hola .- <p>Se ha realizado <b>Correctamente la Solicitud</b> - En breve momento se realizara la aprobación.</p>';
						$data['success'] = true;
						unset($_POST);
					}else{
						$data['text'] = 'Problema al realizar la solicitud';
					}
				}else{
					$data['text'] = 'El servicio seleccionado ya termino su servicio para hoy, seleccione otro día';
				}
			} else {
				foreach ($this->input->post() as $key => $value) {
					$data['message'][$key] = form_error($key);
				}
			}
		}else{
			$data['text'] = 'Debe conectarse nuevamente para seguir con la solicitud';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function sendEmail($email,$subject,$message)
	{
      $data = array(
        'title'    => $subject,
        'menssage' => $message,
      );
      $body = $this->load->view('dashboard/email/vAlert', $data, TRUE);
      $result = $this->home->send_email($email,$body,$subject);
	}

	public function get_collations()
	{
		$data = array('colacion' => array());
		if ($this->input->post('casino')) {
			$casino = $this->input->post('casino');
			if ($casino != 'default') {
				$id = $this->input->post('id');
				if (empty($id)) {
					$id = $this->session->collaborator;
				}
				$colacion = $this->solicitud->getColation($casino,$id);
				if (!empty($colacion)) {
					foreach ($colacion as $key) {
						$data['colacion'][] = array(
							'values' => $key->ID_COLACION,
							'text' => ucwords(mb_strtolower($key->NOMBRE))
						);
					}
				}else{
					$data['colacion'][] = array(
						'values' => 'default',
						'text' => 'Sin Colación'
					);	
				}
			}else{
				$data['colacion'][] = array(
					'values' => 'default',
					'text' => 'Sin Colación'
				);	
			}
		}else{
			$data['colacion'][] = array(
				'values' => 'default',
				'text' => 'Sin Colación'
			);
		}
	    $this->output->set_output(json_encode($data));
	}

	public function get_casinos()
	{
		$data = array('casino' => array());
		if ($this->input->post('planta')) {
			$planta = $this->input->post('planta');
			if ($planta != 'default') {
				$id = $this->input->post('id');
				if (empty($id)) {
					$id = $this->session->collaborator;
				}

				$casino = $this->solicitud->getCasino($planta,$id);
				if (!empty($casino)) {
					foreach ($casino as $key) {
						$data['casino'][] = array(
							'values' => $key->ID_CASINO,
							'text' => ucwords(mb_strtolower($key->CASINO))
						);
					}
				}else{
					$data['casino'][] = array(
						'values' => 'default',
						'text' => 'Sin casino'
					);	
				}
			}else{
				$data['casino'][] = array(
					'values' => 'default',
					'text' => 'Sin casino'
				);	
			}
		}else{
			$data['casino'][] = array(
				'values' => 'default',
				'text' => 'Sin casino'
			);
		}
	    $this->output->set_output(json_encode($data));
	}

	public function get_empleado()
	{
		$data = array('view' => null,'success' => false, 'message' => '');
		if ($this->input->post()) {
			# $arreglo = json_decode($this->input->post('json'));
			// $_POST('#rut');
			$element['colaborador'] = $this->colaborador->get_empleado_datos($this->input->post('rut'));
			if (!empty($element['colaborador'])) {
				$element['rut'] = $this->input->post('rut');
				$element['planta']  = $this->getPlant();
				$data['view'] = $this->load->view('dashboard/solicitar/vModal', $element, true);
				$data['success'] = true;
			}else{
				$data['message'] = 'Error al obtener los datos del empleado seleccionado';
			}
		}else{
			$data['message'] = 'Error al obtener los datos del empleado seleccionado';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function get_empleado_sub()
	{
		$data = array('view' => '');
		$table = $this->input->post('table');
		// die(var_dump($table));
		$element['key'] = $table[6];
		$element['rut'] = $table[1];
		$element['nombre'] = $table[2];
		
		$element['empleado'] = $this->solicitud->get_empleado_jefatura($element['rut']); # $element['rut']
		$element['colaborador'] = $this->colaborador->get_empleado_datos($element['rut']);
		// die(var_dump($element['colaborador']));
		$data['view'] = $this->load->view('dashboard/solicitar/vEmpleado', $element, true);
	    $this->output->set_output(json_encode($data));
	}
    
    public function valid_date($date)
    {
        $aux = explode('/', $date);
        $regex = '/^(\d{2})$/';
        $regex2 = '/^(\d{4})$/';
        if(preg_match($regex, $aux[0]) && preg_match($regex, $aux[1]) && preg_match($regex2, $aux[2]))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function valid_text($text)
    {
        $regex = '/[A-Za-záéíóú0-9_~\-!?¿!¡\s,.\"@#\$%\^&\*\(\)\n\r\t]*$/';

        if(preg_match($regex, $text))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

	function check_default($post_string)
	{
		return $post_string == 'default' ? FALSE : TRUE;
	}

	private function getPlant()
	{
		$data = $this->plant->get_all();
		return $data;
	}

	private function date()
	{
		$date = date("d/m/Y");
		return $date;
	}

}

/* End of file Crequest.php */
/* Location: ./application/controllers/Crequest.php */