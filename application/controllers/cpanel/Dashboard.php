<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Casino_model','casino');
		$this->load->model('Plant_model','plant');
		$this->load->model('Home_model','home');
	}

	public function index()
	{
		$data['plant'] = $this->getPlant();
		$this->load->view($this->header);
		$this->load->view('dashboard/dashboard',$data);
		$this->load->view($this->footer);
	}

	private function getPlant()
	{
		$fields = $this->plant->get_print();
    	return $fields;
	}

	public function get_data_home_casino()
	{
		$result = array();
		if ($this->input->post()) {
			$id_casino = $this->input->post('id');

			if ($this->check_default($id_casino) == false) {
				$id_casino = 0;
			}

			$data["graphic"]   = $this->home->get_quantity_week($id_casino);
			$data["quantity"]  = $this->home->get_by_day($id_casino);
			$data["request_c"] = $this->home->get_pending($id_casino);
			$data["request_a"] = $this->home->get_aproved($id_casino);
			$data["request_d"] = $this->home->get_rejection($id_casino);

			foreach ($data["quantity"] as $key) {
				$data['donut'][] = array(
					'colacion' => $this->pfalimentos->replace_guion_espacio($key->Colacion),
					'total' => $key->day,
					'color' => $this->generate_color()
				);
			}
			echo json_encode($data);
		}else{
			$result[] = (object) array (
				"graphic"           => 0,
				"name_col"          => 'Sin datos',
				"quantity_col"      => 'Sin datos',
				'aproved_request'   => 0,
				'rejection_request' => 0
			);
			echo json_encode($result);
		}
	}

	public function getCasino()
	{
		$cat = 'No dispone de casinos';
		if ($this->input->post()) {
			$plant = $this->input->post('planta');
			if ($this->check_default($plant) == false) {
				$plant = 0;
				$cat   = 'Seleccione una opción';
			}
			$casino = $this->casino->get_casino_planta($plant);
			$result = array();
			if (empty($casino)) {
	            $result[] = (object) array(
					"value" => 'default',
					"text"  => $cat
	            );
	            echo json_encode($result);
			}else{
	            foreach ($casino as $key) {
		            $result[] = (object) array(
						"value" => $key->ID_CASINO,
						"text"  => ucwords(mb_strtolower($key->NOMBRE))
		            );
	            }
	            echo json_encode($result);
			}
		}else{
            $result[] = (object) array(
				"value" => 'default',
				"text"  => $cat
            );
            echo json_encode($result);
		}
	}

	function check_default($post_string) {
	 	return $post_string == 'default' ? FALSE : TRUE;
	}

	public function random_color_part()
	{
		return str_pad( dechex( mt_rand( 0, 255) ), 2,'0', STR_PAD_LEFT);
	}

	public function generate_color()
	{
		return '#'.$this->random_color_part().$this->random_color_part().$this->random_color_part();
	}

}

/* End of file Cdashboard.php */
/* Location: ./application/controllers/Cdashboard.php */