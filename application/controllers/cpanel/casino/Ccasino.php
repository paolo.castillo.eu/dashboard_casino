<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccasino extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Casino_model','casino');
		$this->load->model('Plant_model','plant');
		if ($this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['plant'] = $this->getPlant();
		$this->load->view($this->header);
		$this->load->view('dashboard/casino/vCasino',$data);
		$this->load->view($this->footer);
	}

  	private function getPlant()
  	{
    	$fields = $this->plant->get_print();
    	return $fields;
  	}

	public function getCasino_by($step)
	{
		$data = $this->casino->get_by_id($step);
		echo json_encode($data); 
	}

	public function edit()
	{
		$data = array('success' => false,'message' => array(), 'text' => '');
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_casino', 'Código', 'required|min_length[2]|max_length[20]');
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[30]');
			$this->form_validation->set_rules('email', 'Email', 'required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('email_pf', 'Email PF', 'required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('total', 'Total', 'required|min_length[2]|max_length[4]');
			$this->form_validation->set_rules('plant', 'Planta', 'required|callback_check_default');
			if ($this->form_validation->run() ==  FALSE) {
                foreach ($this->input->post() as $key => $value) {
                  $data['message'][$key] = form_error($key);
                }
			} else {
    			$post             = new stdClass;
				$post->id_casino = $this->input->post('id_casino');
				$post->name = $this->input->post('name');
				$post->email = $this->input->post('email');
				$post->email_pf = $this->input->post('email_pf');
				$marca = $this->input->post('v_marca');
				$post->v_marca = $this->check_radio($marca);
				$post->email_notif_adm = $this->input->post('email_notif_adm');
				$post->email_notifi_rrhh = $this->input->post('email_notifi_rrhh');
				$post->email_notifi_seg = $this->input->post('email_notifi_seg');
				$post->email_notifi_mark = $this->input->post('email_notifi_mark');
				$correo_marca = $this->input->post('v_correo_marca');
				$post->v_correo_marca = $this->check_radio($correo_marca);
				$post->total = $this->input->post('total');
				$post->plant = $this->input->post('plant');
				$post->by_modify = $this->session->collaborator;
    			$postObj          = (object) $post;
				// die(var_dump($postObj));
				$update    = $this->casino->update($postObj);
				if ($update == TRUE) {
					$data['success'] = true;
					$data['text'] =  'Casino actualizado';
				}else{
					$data['text'] =  'Error al registrar los datos de casino';
				}
			}
		}else{
			$data['text'] =  'Error al registrar los datos de casino';
		} 
		$this->output->set_output(json_encode($data));
	}

	public function save()
	{
		$data = array('success' => false,'message' => array(), 'text' => '');
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_casino', 'Código', 'required|min_length[2]|max_length[20]');
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[30]');
			$this->form_validation->set_rules('email', 'Email', 'required|min_length[2]|max_length[100]');
			$this->form_validation->set_rules('email_pf', 'Email PF', 'required|min_length[2]|max_length[255]');
			$this->form_validation->set_rules('total', 'Total', 'required|min_length[2]|max_length[4]');
			$this->form_validation->set_rules('plant', 'Planta', 'required|callback_check_default');
			if ($this->form_validation->run() ==  FALSE) {
                foreach ($this->input->post() as $key => $value) {
                  $data['message'][$key] = form_error($key);
                }
			}else{
				$element   = (object) array(
					'id_casino' => $this->input->post('id_casino'),
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'email_pf' => $this->input->post('email_pf'),
					'v_marca' => $this->check_radio($this->input->post('v_marca')),
					'email_notif_adm' => $this->input->post('email_notif_adm'),
					'email_notifi_rrhh' => $this->input->post('email_notifi_rrhh'),
					'email_notifi_seg' => $this->input->post('email_notifi_seg'),
					'v_correo_marca' => $this->check_radio($this->input->post('v_correo_marca')),
					'email_notifi_mark' => $this->input->post('email_notifi_mark'),
					'total' => $this->input->post('total'),
					'plant' => $this->input->post('plant'),
					'by_create' => $this->session->collaborator
				);
				$insert    = $this->casino->add($element);
				if ($insert == TRUE) {
					$data['success'] = true;
					$data['text'] =  'Casino agregado';
				}else{
					$data['text'] =  'Error al registrar los datos de casino';
				} 
			}
		}else{
		  	$data['text'] =  'Debe iniciar sesión nuevamente';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function check_radio($radio)
	{
		if (!empty($radio)) {
			if ($radio == 'Y') {
				return '1';
			}else{
				return '0';
			}
		}
		return '0';
	}

  	public function getCasinoAll()
  	{
	    $fields = $this->casino->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    foreach ($fields as $casino) {
			$no++;
			$row    = array();
			$row[]  = $casino->ID_CASINO;
			$row[]  = ucwords(mb_strtolower($casino->NOMBRE));
			$row[]  = $casino->CORREO_ADMIN;
			$row[]  = $casino->CORREO_ADMIN_PF;
			$row[]  = $casino->CAPACIDAD;
			$row[]  = ucwords(mb_strtolower($this->plant->getName_by_id($casino->ID_PLANTA)));
			$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updateCasino('."'".$casino->ID_CASINO."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$casino->ID_CASINO.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($casino->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
            </a>';
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->casino->count_all(),
	                    "recordsFiltered" => $this->casino->count_filtered(),
	                    "data" => $data,
	    );
	    //output to json format
	    echo json_encode($output);
  	}

  	public function dele($id)
  	{
  		$by_modify = $this->session->collaborator;
	    $result = $this->casino->delete($id,$by_modify);
	    if ($result == true) {
	        echo json_encode(array("status" => TRUE));  
	    } else {
	        echo json_encode(array("status" => FALSE));  
	    }
  	}

	function check_default($post_string)
	{
		return $post_string == '0' ? FALSE : TRUE;
	}
}

/* End of file Ccasino.php */
/* Location: ./application/controllers/Ccasino.php */
