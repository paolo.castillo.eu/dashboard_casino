<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cticket extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ticket_model','ticket');
		$this->load->model('Home_model','home');

		if ($this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$this->load->view($this->header);
		$this->load->view('dashboard/ticket/vTicket');
		$this->load->view($this->footer);
	}

	public function getTicket_by($step)
	{
		$data = $this->ticket->get_by_id($step);
		echo json_encode($data);
	}

	public function save()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[1]|max_length[200]');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));
			} else {
				$name       = $this->input->post('name');
				$by_create  = $this->session->collaborator;
				$last_id    = $this->ticket->last_id();
				$id_estado  = $last_id[0]->LASTID;
				$insert    = $this->ticket->add($id_estado,$name,$by_create);
				if ($insert == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_estado', 'id_estado', 'required');
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[1]|max_length[30]');
			if ($this->form_validation->run() == FALSE) {

				echo json_encode(array("status" => FALSE));
			}else{
				$id_estado = $this->input->post('id_estado');
				$name      = $this->input->post('name');
				$by_modify = $this->session->collaborator;
				$update    = $this->ticket->update($id_estado,$name,$by_modify);
				if ($update == TRUE) {
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(array("status" => FALSE));
				}
			}
		}else{
		  echo json_encode(array("status" => FALSE));
		}
	}

	public function dele($id)
	{
  		$by_modify = $this->session->collaborator;
    	$result = $this->ticket->delete($id,$by_modify);
    	if ($result == TRUE) {
        	echo json_encode(array("status" => TRUE));  
    	}else{
        	echo json_encode(array("status" => FALSE));  
    	}
	}

	public function getTicketAll()
	{
	    $fields = $this->ticket->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    foreach ($fields as $ticket) {
			$no++;
			$row    = array();
			$row[]  = $ticket->ID_ESTADO;
			$row[]  = ucwords(mb_strtolower($ticket->NOMBRE));
			$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updateTicket('."'".$ticket->ID_ESTADO."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$ticket->ID_ESTADO.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($ticket->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
            </a>';
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->ticket->count_all(),
	                    "recordsFiltered" => $this->ticket->count_filtered(),
	                    "data" => $data,
	    );
	    //output to json format
	    echo json_encode($output);
	}
}

/* End of file Cticket.php */
/* Location: ./application/controllers/Cticket.php */