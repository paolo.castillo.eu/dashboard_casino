<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cplant extends PF_Controller{
  private $header  = 'templates/header';
  private $footer  = 'templates/footer';
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Plant_model','plant');
    if ($this->session->profile_id == 4) {
    }else{
          redirect(base_url().'index.php/', 'refresh');
    }
  }

  function index()
  {
    $this->load->view($this->header);
		$this->load->view('dashboard/planta/vPlant');
		$this->load->view($this->footer);
  }

  public function getPlant($step)
  {
    $data = $this->plant->get_by_id($step);
    echo json_encode($data); 
  }

  public function edit()
  {
    if ($this->input->post()) {
      $name       = $this->input->post('name');
      $modify_by  = $this->session->collaborator;
      $update     = $this->plant->update($name,$this->input->post('id_plant'),$modify_by);
      if ($update == TRUE) {
        echo json_encode(array("status" => TRUE));  
      }else{
        echo json_encode(array("status" => false));
      }
    }else{
      echo json_encode(array("status" => FALSE));  
    } 
  }

  public function save()
  {
    if ($this->input->post()) {
      $this->form_validation->set_rules('id_plant', 'Código', 'required|min_length[2]|max_length[20]');
      $this->form_validation->set_rules('name', 'name', 'required|min_length[2]|max_length[60]');
      if ($this->form_validation->run() == FALSE) {
        echo json_encode(array("status" => FALSE));
      } else {
        $id        = $this->input->post('id_plant');
        $name      = strtoupper($this->input->post('name'));
        $create_by = $this->session->collaborator;
        $insert    = $this->plant->add($id,$name,$create_by);
        if ($insert == TRUE) {
          echo json_encode(array("status" => TRUE));  
        }else{
          echo json_encode(array("status" => FALSE));
        } 
      }
    }else{
      echo json_encode(array("status" => FALSE));
    }
  }

  public function getPlantAll()
  {
    $fields = $this->plant->get_datatables();
    $data   = array();
    $no     = $_POST['start'];
    foreach ($fields as $plants) {
        $no++;
        $row = array();
        $row[] = $plants->ID_PLANTA;
        $row[] = ucwords(mb_strtolower($plants->NOMBRE));
        $row[] = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updatePlant('."'".$plants->ID_PLANTA."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$plants->ID_PLANTA.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($plants->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
            </a>';
        $data[] = $row;
    }
    $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->plant->count_all(),
                    "recordsFiltered" => $this->plant->count_filtered(),
                    "data" => $data,
    );
    //output to json format
    echo json_encode($output);
  }

  public function dele($id)
  {
    $modify_by  = $this->session->collaborator;
    $result = $this->plant->delete($id,$modify_by);
    if ($result == true) {
        echo json_encode(array("status" => TRUE));  
     } else {
        echo json_encode(array("status" => TRUE));  
     }
  }

}
