<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccollation_user extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Collaborator_model','collaborator');
		$this->load->model('Collation_user_model','collation_user');
		$this->load->model('Type_service','service');
		$this->load->model('Casino_model','casino');
		$this->load->model('Collation_model','collation');
		
		if ($this->session->profile_id == 3 || $this->session->profile_id == 4 || $this->session->profile_id == 2) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['casino']   = $this->getCasino();
		$data['servicio'] = $this->getService();
		$data['colacion'] = $this->getColation();
		$this->load->view($this->header);
		$this->load->view('dashboard/colacion/vCollation_user',$data);
		$this->load->view($this->footer);
	}

	private function getCasino()
	{
		$data = $this->casino->get_all();
		return $data;
	}

	private function getService()
	{
		$data = $this->service->get_all();
		return $data;
	}

	private function getColation()
	{
		$data = $this->collation->get_all();
		return $data;	
	}

	public function save()
	{
		$data = array('success' => false, 'menssage' => array(), 'text' => '');
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_casino_u', 'Casino', 'required|callback_check_default');
			$this->form_validation->set_rules('id_typeService_u', 'Servicio', 'required|callback_check_default');
			$this->form_validation->set_rules('id_Collation_u', 'Colación', 'required|callback_check_default');
			$this->form_validation->set_rules('maximo_colacion', 'Numerco Colación', 'required|numeric');
			if ($this->form_validation->run()) {
				$element = (object) array(
					'id_colaborador' => $this->input->post('id_colaborador'),
					'id_casino' => $this->input->post('id_casino_u'),
					'id_typeService' => $this->input->post('id_typeService_u'),
					'id_colacion' => $this->input->post('id_Collation_u'),
					'maxcolacion' => $this->input->post('maximo_colacion'),
					'by_created' => $this->session->collaborator 
				);
				if ($this->collation_user->verify($element) == 0) {
					$add = $this->collation_user->add($element);
				if ($add) {
					$data['text']    = 'Colación del Colaborador Creada';
					$data['success'] = true;
				}else{
					$data['text'] = 'Probelma al agregar colación al colaborador';
				}	
				}else{
					$data['text'] = 'El colaborador seleccionado ya tiene registro de colación con los datos ingresados';
				}
			} else {
				foreach ($this->input->post() as $key => $value) {
					$data['menssage'][$key] = form_error($key);
				}
			}
		}else{
			$data['text'] = 'Erro al editar la colación al colaborador seleccionado';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function edit()
	{
		$data = array('success' => false, 'menssage' => array(), 'text' => '');
		if ($this->input->post()) {
			$this->form_validation->set_rules('id_casino_u', 'Casino', 'required|callback_check_default');
			$this->form_validation->set_rules('id_typeService_u', 'Servicio', 'required|callback_check_default');
			$this->form_validation->set_rules('id_Collation_u', 'Colación', 'required|callback_check_default');
			$this->form_validation->set_rules('maximo_colacion', 'Numerco Colación', 'required|numeric');
			if ($this->form_validation->run()) {
				$element = (object) array(
					'id_colaborador' => $this->input->post('id_colaborador'),
					'id_casino' => $this->input->post('id_casino_u'),
					'id_typeService' => $this->input->post('id_typeService_u'),
					'id_colacion' => $this->input->post('id_Collation_u'),
					'maxcolacion' => $this->input->post('maximo_colacion'),
					'by_modify' => $this->session->collaborator 
				);
				$update = $this->collation_user->update($element);
				if ($update) {
					$data['text']    = 'Colación del Colaborador Actualizada';
					$data['success'] = true;
				}else{
					$data['text'] = 'Probelma al actualizar al colaborador';
				}
			} else {
				foreach ($this->input->post() as $key => $value) {
					$data['message'][$key] = form_error($key);
				}
			}
		}else{
			$data['text'] = 'Erro al editar la colación al colaborador seleccionado';
		}
	    $this->output->set_output(json_encode($data));
	}

	public function dele()
	{
		if ($this->input->post()) {
			$collaborator = $this->input->post('id_colaborador');
			$casino       = $this->input->post('id_casino');  
			$collation    = $this->input->post('id_colacio');  
			$by_modify    = $this->session->collaborator;
			if (empty($collaborator) || empty($casino) || empty($collation)) {
		        echo json_encode(array("status" => '',"message" => '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4>
                		No existe colación seleccionada.
              		</div>'));
			}else{
				$deleted = $this->collation_user->delete($collaborator,$casino,$collation,$by_modify);
				if ($deleted == TRUE) {
			        echo json_encode(array("status" => TRUE));  
				}else{
			        echo json_encode(array("status" => FALSE));  
				}	
			}
		}else{
	        echo json_encode(array("status" => FALSE));  
		}
	}

	public function getCollation_user()
	{
		if ($this->input->post()) {
			$id_colaborador = $this->input->post('id_collaborator');
			$id_casino = $this->input->post('id_casino');
			$id_colacion = $this->input->post('id_colacion');
			if (empty($id_colaborador) || empty($id_casino) || empty($id_colacion)) {
		        echo json_encode(array("status" => '',"message" => '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4>
                		No existe colación seleccionada.
              		</div>'));
			}else{
 	        	$get = $this->collation_user->get_maxcollation($id_colaborador,$id_casino,$id_colacion);
				if (!isset($get)) {
 	        		echo json_encode(array("status" => TRUE, "max" => '0', "menssage" => 'El colaborador no se ha agregado colaciones')); 
				}else{
 	        		echo json_encode(array("status" => TRUE, "max" => $get)); 
				}
			}
		}else{
 	       echo json_encode(array("status" => FALSE));  
		}
	}

	public function getCollation_userAll()
	{
		if ($this->input->post()) {
			$step = $this->input->post('id_collaborator');
			$rows = $this->collation_user->get_user_collation($step);

			if ($rows->num_rows() == 0) {
	            $data[] = array(
					'id_colaborador'  => $step,
					'id_casino'       => '',
					'id_colacion'     => '',
					'nombre'          => '',
					'maxtipocolacion' => '',
					'casino'          => '',
					'colacion'        => '',
					'tipo_servicio'   => '',
					'mensaje'         => '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4>
                		No existen Colación asociada al colaborador.
              			</div>'
	            );
				echo json_encode($data);
			}else{
				foreach ($rows->result() as $key) {
					// $colacion = ucwords(mb_strtolower($this->collation->get_name($key->ID_COLACION)))
					$colacion = $this->collation->get_name($key->ID_COLACION);
// print_r($key->ID_COLACION. ' ');
					// die(var_dump($colacion->NOMBRE));
		            $data[] = array(
						'id_colaborador'  => $key->ID_COLABORADOR,
						'id_casino'       => $key->ID_CASINO,
						'id_colacion'     => $key->ID_COLACION,
						'nombre'          => ucwords(mb_strtolower($this->collaborator->get_name($key->ID_COLABORADOR))),
						'casino'          => ucwords(mb_strtolower($this->casino->get_name($key->ID_CASINO))),
						'colacion'        => ucwords(mb_strtolower(  (empty($colacion->NOMBRE) ? '' : $colacion->NOMBRE ))),
						'tipo_servicio'   => ucwords(mb_strtolower($this->service->get_name($key->ID_TIPO_SERVICIO))),
						'maxtipocolacion' => $key->MAXTIPOCOLACION,
						'mensaje'         => ''
		            );					
				}
				echo json_encode($data);
			}
		} else {
            $data[] = array(
				'id_colaborador'  => '',
				'id_casino'       => '',
				'id_colacion'     => '',
				'nombre'          => '',
				'maxtipocolacion' => '',
				'casino'          => '',
				'colacion'        => '',
				'tipo_servicio'   => '',
				'mensaje'         => '
				<div class        ="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> Alerta!</h4>
				Problema en capturar al empleado.
				</div>'
            );
			echo json_encode($data);
		}
	}

	public function getUserAll()
	{
	    $fields = $this->collaborator->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    foreach ($fields as $collaborator) {
			$no++;
			$row    = array();
			$row[]  = $collaborator->ID_COLABORADOR;
			$row[]  = $collaborator->CODIGO;
			$row[]  = ucwords(mb_strtolower($collaborator->NOMBRE));
			$row[]  = $collaborator->CREDENCIAL;
			$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="table_colaborador_collation('."'".$collaborator->ID_COLABORADOR."'".')"><i class="fa fa-hand-o-right"></i> Seleccionar Colaborador</a>';
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->collaborator->count_all(),
	                    "recordsFiltered" => $this->collaborator->count_filtered(),
	                    "data" => $data,
	    );
	    echo json_encode($output);
	}

	function check_default($post_string)
	{
		return $post_string == 'default' ? FALSE : TRUE;
	}


	public function get_casino_all()
	{
		$data = array('success' => false, 'option' => array());
		if ($this->input->post()) {
			$result = $this->casino->get_all();
			if (!empty($result)) {
				foreach ($result as $key) {
					$data['option'][] = array(
						'values' => $key->ID_CASINO,
						'text' => ucwords(mb_strtolower($key->NOMBRE))
					);
				}
			}else{
				$data['option'][] = array(
					'values' => '',
					'text' => 'Sin Casino'
				);
			}
		}else{
			$data['option'][] = array(
				'values' => '',
				'text' => 'Sin Casino'
			);
		}		
		$this->output->set_output(json_encode($data));
		
	}

	/** 06/04/2018 **/
	public function get_by()
	{
		$data = array('option' => array());
		if ($this->input->is_ajax_request()) {
			$element = $this->collation->get_by($this->input->post('casino'));
			if (!empty($element)) {
				foreach ($element as $value) {
					$data['option'][] = array(
						'text' => $value->NOMBRE,
						'values' => $value->ID_COLACION
					);
				}
			}else{
				$data['option'] = array(
					'text' => 'Sin Colación',
					'values' => ''
				);
			}
		}
		echo json_encode($data);
	}
}

/* End of file Ccollation_user.php */
/* Location: ./application/controllers/Ccollation_user.php */