<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccollation extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Collation_model','collation');
		$this->load->model('Casino_model','casino');
		if ($this->session->profile_id == 4) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['casino'] = $this->get_casino();
		$this->load->view($this->header);
		$this->load->view('dashboard/colacion/vCollation',$data);
		$this->load->view($this->footer);
	}

	public function save()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('codigo', 'Código', 'required|min_length[2]|max_length[30]');
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[60]');
			$this->form_validation->set_rules('type', 'Tipo', 'required|min_length[2]|max_length[60]');
			$this->form_validation->set_rules('casino', 'Casino', 'required|callback_check_default');
			$this->form_validation->set_rules('version', 'Versión', 'required|min_length[1]|max_length[6]');
			$this->form_validation->set_rules('preci', 'preci', 'required|min_length[2]|max_length[6]');
			$this->form_validation->set_rules('date_ini', 'Fecha Inicio', 'required');
			$this->form_validation->set_rules('date_end', 'Fecha Vigencia', 'required');
			$this->form_validation->set_rules('hour_cut', 'Hora de corte', 'required');
			$this->form_validation->set_rules('hour_ini', 'Hora de inicio', 'required');
			$this->form_validation->set_rules('hour_end', 'Hora de termino', 'required');
			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));
			}else{
				$id_codigo = $this->input->post('codigo');
				$name      = $this->input->post('name');
				$type      = $this->input->post('type');
				$id_casino = $this->input->post('casino');
				$version   = $this->input->post('version');
				$preci     = $this->input->post('preci');
				$date_ini  = $this->input->post('date_ini');
				$date_end  = $this->input->post('date_end');
				$hour_cut_ = $this->input->post('hour_cut');
				$out_cut   = (int)$this->conver_string_to_number($hour_cut_);
				$hour_ini  = $this->input->post('hour_ini');
				$out_ini   = (int)$this->conver_string_to_number($hour_ini);
				$hour_end  = $this->input->post('hour_end');
				$out_end   = (int)$this->conver_string_to_number($hour_end);
				$by_create = $this->session->collaborator;
				$insert = $this->collation->add($id_codigo,$name,$type,$id_casino,$version,$preci,$date_ini,$date_end,$out_cut,$out_ini,$out_end,$by_create);
				if ($insert == TRUE) {
					echo json_encode(array("status" => TRUE));
				} else {
					echo json_encode(array("status" => FALSE));
				}
			}
		} else {
			echo json_encode(array("status" => FALSE));
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$this->form_validation->set_rules('codigo', 'Código', 'required|min_length[2]|max_length[30]');
			$this->form_validation->set_rules('name', 'Nombre', 'required|min_length[2]|max_length[60]');
			$this->form_validation->set_rules('type', 'Tipo', 'required|min_length[2]|max_length[60]');
			$this->form_validation->set_rules('casino', 'Casino', 'required|callback_check_default');
			$this->form_validation->set_rules('version', 'Versión', 'required|min_length[1]|max_length[6]');
			$this->form_validation->set_rules('preci', 'preci', 'required|min_length[2]|max_length[6]');
			$this->form_validation->set_rules('date_ini', 'Fecha Inicio', 'required');
			$this->form_validation->set_rules('date_end', 'Fecha Vigencia', 'required');
			$this->form_validation->set_rules('hour_cut', 'Hora de corte', 'required');
			$this->form_validation->set_rules('hour_ini', 'Hora de inicio', 'required');
			$this->form_validation->set_rules('hour_end', 'Hora de termino', 'required');

			if ($this->form_validation->run() == FALSE) {
				echo json_encode(array("status" => FALSE));
			} else {
				$id_codigo = $this->input->post('codigo');
				$name      = $this->input->post('name');
				$type      = $this->input->post('type');
				$id_casino = $this->input->post('casino');
				$version   = $this->input->post('version');
				$preci     = $this->input->post('preci');
				$date_ini  = $this->input->post('date_ini');
				$date_end  = $this->input->post('date_end');
				$hour_cut_ = $this->input->post('hour_cut');
				$out_cut   = (int)$this->conver_string_to_number($hour_cut_);
				$hour_ini  = $this->input->post('hour_ini');
				$out_ini   = (int)$this->conver_string_to_number($hour_ini);
				// die(var_dump($out_ini));
				$hour_end  = $this->input->post('hour_end');
				$out_end   = (int)$this->conver_string_to_number($hour_end);
				$min_ini  = (int)$this->conver_string_to_number($this->input->post('min_ini'));
				$min_end  = (int)$this->conver_string_to_number($this->input->post('min_end'));
				$by_modify = $this->session->collaborator;
				$update = $this->collation->update($id_codigo,$name,$type,$id_casino,$version,$preci,$date_ini,$date_end,$out_cut,$out_ini,$out_end,$min_ini,$min_end,$by_modify);
				if ($update == TRUE) {
					echo json_encode(array("status" => TRUE));
				} else {
					echo json_encode(array("status" => FALSE));
				}	
			}

		} else {
			echo json_encode(array("status" => FALSE));
		}
	}

  	public function dele($id)
  	{
  		$by_modify = $this->session->collaborator;
	    $result = $this->collation->delete($id,$by_modify);
	    if ($result == true) {
	        echo json_encode(array("status" => TRUE));  
	    } else {
	        echo json_encode(array("status" => FALSE));  
	    }
  	}
	
	public function getCollation($step)
	{
		$data = $this->collation->get_by_id($step);
        $result = array();
        if(!empty($data))
        {
            $data = $data[0];
        	// $install_date_ini=$data->FECHA_VIGENCIA_INICIAL;
        	// $install_date_end= $data->FECHA_VIGENCIA_FINAL;
            $result = (object) array(
				"ID_COLACION"            => $data->ID_COLACION,
				"NOMBRE"                 => ucwords(mb_strtolower($data->NOMBRE)),
				"TIPO_COLACION"          => $data->TIPO_COLACION,
				"ID_CASINO"              => $data->ID_CASINO,
				"VERSION"                => $data->TOTAL_COLACION,
				"COSTO"                  => $data->COSTO,
				"FECHA_VIGENCIA_INICIAL" => $data->FECHA_VIGENCIA_INICIAL,
				"FECHA_VIGENCIA_FINAL"   => $data->FECHA_VIGENCIA_FINAL,
				"CORTE_SOLICITUD"        => $this->convert_hour($data->CORTE_SOLICITUD),
				"HORA_INICIO"            => $this->convert_hour($data->HORA_INICIO),
				"HORA_FIN"               => $this->convert_hour($data->HORA_FIN),
				"MIN_INI"                => $data->MINUTO_INICIO,
				"MIN_END"                => $data->MINUTO_CORTE,
            );
        }
		echo json_encode($result);
	}

	public function getCollationAll()
	{
	    $fields = $this->collation->get_datatables();
	    $data   = array();
	    $empty  = 'No existe datos';
	    $no     = $_POST['start'];
	    foreach ($fields as $collation) {
			$no++;
			$row    = array();
			$row[]  = $collation->ID_COLACION;
			if (empty($collation->NOMBRE)) {
				$row[]  = $empty;
			} else {
				$row[]  = ucwords(mb_strtolower($collation->NOMBRE));
			}
			if (empty($collation->TIPO_COLACION)) {
				$row[] = $empty;
			} else {
				$row[]  = ucwords(mb_strtolower($collation->TIPO_COLACION));
			}
			$row[]  = ucwords(mb_strtolower($this->casino->get_name($collation->ID_CASINO)));
			if (empty($collation->TOTAL_COLACION)) {
				$row[] = $empty;
			} else {
				$row[]  = $collation->TOTAL_COLACION;
			}
			if (empty($collation->COSTO)) {
				$row[]  = $empty;
			} else {
				$row[]  = '$'.(string)$collation->COSTO;
			}
			if (empty($collation->FECHA_VIGENCIA_INICIAL)) {
				$row[] = $empty;
			} else {
				// $row[]  = date("d/m/Y",strtotime($collation->FECHA_VIGENCIA_INICIAL));
				$row[]  = $collation->FECHA_VIGENCIA_INICIAL;
			}			
			if (empty($collation->FECHA_VIGENCIA_FINAL)) {
				$row[]  = $empty;
			} else {
				// $row[]  = date("d/m/Y",strtotime($collation->FECHA_VIGENCIA_FINAL));
				$row[]  = $collation->FECHA_VIGENCIA_FINAL;
			}
			if (empty($collation->CORTE_SOLICITUD)) {
				$row[]  = $empty;
			} else {
				$row[]  = $this->convert_hour($collation->CORTE_SOLICITUD);
			}
			if (empty($collation->HORA_INICIO)) {
				$row[] = $empty;
			} else {
				$row[]  = $this->convert_hour($collation->HORA_INICIO);
			}
			if (empty($collation->HORA_FIN)) {
				$row[] = $empty;
			} else {
				$row[]  = $this->convert_hour($collation->HORA_FIN);
			}
			if (empty($collation->MINUTO_INICIO)) {
				$row[] = $empty;
			} else {
				$row[]  = $collation->MINUTO_INICIO;
			}
			if (empty($collation->MINUTO_CORTE)) {
				$row[] = $empty;
			} else {
				$row[]  = $collation->MINUTO_CORTE;
			}								
			$row[]  = '<a class="btn btn-primary btn-flat btn-xs" href="javascript:void(0)" title="Edit" onclick="updateCollation('."'".$collation->ID_COLACION."'".')"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="#" data-record-id="'.$collation->ID_COLACION.'" data-toggle="modal" data-target="#confirm-delete" data-record-title="'.ucwords(mb_strtolower($collation->NOMBRE)).'" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i>
            </a>';
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->collation->count_all(),
	                    "recordsFiltered" => $this->collation->count_filtered(),
	                    "data" => $data,
	    );
	    echo json_encode($output);
	}

	private function convert_hour($hour){
		$chain = (string)$hour;
		if (strlen($chain)==4) {
			$convert = substr($chain,0,2).":".substr($chain,2);
			return $convert; 					
		}elseif (strlen($chain)==3) {
			$convert = '0'.substr($chain,0,1).":".substr($chain,1);
			return $convert;
		}elseif (strlen($chain)==2) {
			$convert = "00:".substr($chain,0,1);
			return $convert;
		}elseif (strlen($chain)==1) {
			$convert = "00:0".$chain;
			return $convert;
		}
		return $chain;
	}

	private function conver_string_to_number($chain)
	{
		$set_seconds = str_replace(array(':'), '', $chain);
		$casted = substr($set_seconds,0,1);
		if ($set_seconds == '0000') {
			return 0;
		} else {
			if ($casted == '0') {
				return substr($set_seconds,1);
			} else {
				return $set_seconds;
			}
		}
	}

	function check_default($post_string)
	{
		return $post_string == '0' ? FALSE : TRUE;
	}

	private function get_casino()
	{
		$data = $this->casino->get_all();
		return $data;
	}
}

/* End of file Ccalendario.php */
/* Location: ./application/controllers/Ccalendario.php */
