<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cmycollation extends PF_Controller {
	private $header = 'templates/header';
	private $footer = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Collation_user_model','collation_user');
		if ($this->session->profile_id == 3 || $this->session->profile_id == 4 || $this->session->profile_id == 2 || $this->session->profile_id == 1) {
		}else{
      		redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$id = $this->session->collaborator;
		$data['estado'] = $this->get_estado();
		$data['my_quantity'] = $this->collation_user->get_my_quantity($id);
		$this->load->view($this->header);
		$this->load->view('dashboard/colacion/vMyCollation',$data);
		$this->load->view($this->footer);
	}

	public function get_mis_colacion()
	{
	    $fields = $this->collation_user->get_datatables_mi_colacion();
	    $data   = array();
	    $no     = $_POST['start'];
	    $_element = '';
	    foreach ($fields as $colacion) {
			$no++;
			$row    = array();
			$row[]  = $this->pfalimentos->upper_lower($colacion->PLANTA);
			$row[]  = $this->pfalimentos->upper_lower($colacion->CASINO);
			$row[]  = $this->pfalimentos->upper_lower($colacion->COLACION);
			$row[]  = $this->pfalimentos->upper_lower($colacion->CORRELATIVO);
			$row[]  = $this->pfalimentos->upper_lower($colacion->ESTADO);
			$row[]  = $colacion->FECHA.' '.$colacion->HORA_ENTREGA;
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->collation_user->count_all_mi_colacion(),
	                    "recordsFiltered" => $this->collation_user->count_filtered_mi_colacion(),
	                    "data" => $data,
	    );
	    $this->output->set_output(json_encode($output));
	}

	public function get_estado()
	{
		$colacion = array();
		$data = $this->collation_user->get_estado_colacion();
		if (!empty($data)) {
			foreach ($data as $key) {
				$colacion[] = array(
					'id_colacion' => $key->ID_ESTADO,
					'nombre' => $this->pfalimentos->upper_lower($key->NOMBRE)
				);
			}
		}else{
			$colacion[] = array(
				'id_colacion' => -1,
				'nombre' => 'Error'
			);
		}

		return $colacion;
	}

}

/* End of file Cmycollation.php */
/* Location: ./application/controllers/Cmycollation.php */