<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creport_state extends PF_Controller {
  	private $header  = 'templates/header';
  	private $footer  = 'templates/footer';
  
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Ticket_model','ticket');
		$this->load->model('Report_model','report');
    	$this->load->model('Plant_model','plant');
		$this->load->model('Collation_model','collation');
		$this->load->library('export_excel');
		$this->load->library('email');
		ini_set('memory_limit', '586M');
		if ($this->session->profile_id == 3 || $this->session->profile_id == 4) {
		}else{
			redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		$data['estado'] = $this->get_state();
		$data['empresa'] = $this->report->get_empresa();
	    $this->load->view($this->header);
		$this->load->view('dashboard/report/vReport_state',$data);
		$this->load->view($this->footer);
	}

	public function get_data()
	{
		if ($this->input->post()) {
			$state   = $this->input->post('id_state');
			$time    = $this->input->post('date');
			$company = strtoupper($this->input->post('company'));
			if (empty($company)) {
				$company = NULL;
			}
			$date_d  = substr($time, 0, 10);
			$date_h  = substr($time,-10);
			if ($this->session->profile_id == 4) {
				$result['data'] = $this->report->get_reported($state,$date_d,$date_h,$company);
				if (!empty($result['data'])) {
					$data = $this->load->view('dashboard/report/vReport_state_table', $result , true);
					$this->output->set_output(json_encode(array('status' => true, 'flag' => $data)));
				}else{
					$flag = '<div class="alert alert-warning alert-dismissible">';
					$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
					$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
					$flag.= 'No existen datos en la búsqueda realizada.';
					$flag.= '</div>';
					$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));
				}
			}elseif ($this->session->profile_id == 3) {
				$result['data'] = $this->report->get_reported_by($state,$date_d,$date_h,$this->session->casino,$company);
				if (!empty($result['data'])) {
					$data = $this->load->view('dashboard/report/vReport_state_table', $result , true);
					$this->output->set_output(json_encode(array('status' => true, 'flag' => $data)));
				}else{
					$flag = '<div class="alert alert-warning alert-dismissible">';
					$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
					$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
					$flag.= 'No existen datos en la búsqueda realizada.';
					$flag.= '</div>';
					$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));
				}
			}
		}else{
			$flag = '<div class="alert alert-warning alert-dismissible">';
			$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
			$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
			$flag.= 'No existen datos en la búsqueda realizada.';
			$flag.= '</div>';
			$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));
		}
	}

	public function get_data_service()
	{
		if ($this->input->post()) {
			$time   = $this->input->post('time');
			$state  = $this->input->post('state');
			$casino = $this->input->post('casino');
			$type   = $this->input->post('type');
			$company = $this->input->post('company');
			$company = (!empty($company) ? $company : 'ALL');
			$result['data'] = $this->report->get_service($time,$state,$casino,$type,$company);
			if (!empty($result['data'])) {
				$data = $this->load->view('dashboard/report/vReport_state_service', $result , true);
				$this->output->set_output(json_encode(array('status' => true, 'flag' => $data)));
			 }else{
	 			$flag = '<div class="alert alert-warning alert-dismissible">';
				$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
				$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
				$flag.= 'No existen datos en la búsqueda realizada.';
				$flag.= '</div>';
				$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));		
			 }
		}else{
			$flag = '<div class="alert alert-warning alert-dismissible">';
			$flag.= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
			$flag.= '<h4><i class="icon fa fa-warning"></i> Alerta!</h4>';
			$flag.= 'No existen datos en la búsqueda realizada.';
			$flag.= '</div>';
			$this->output->set_output(json_encode(array('status' => false,'flag' => $flag)));	
		}
	}

	public function load_informe_cliente()
	{
		if ($this->session->profile_id == 4) {
			$data['planta'] = $this->getPlant();
		}elseif ($this->session->profile_id == 3) {
			$data['colacion'] = $this->get_collation_by($this->session->casino);
		}
		$data['empresa'] = $this->report->get_empresa();
		$data['estado']   = $this->report->get_estado();
		$this->load->view($this->header);
		$this->load->view('dashboard/report/vReport_state_cliente',$data);
		$this->load->view($this->footer);
	}

	private function get_state()
	{
		$data = $this->ticket->get_state_ticket();
		return $data;
	}

	private function getPlant()
	{
		$data = $this->plant->get_all();
		return $data;
	}

	private function getState($step)
	{
		$data = $this->ticket->get_all($step);
		return $data;
	}

	public function get_collation_by($id)
	{
		if (!empty($id)) {
			$row = $this->collation->get_collation_casino($id);
			if (!empty($row)) {
				return $row;
			}else{
				$result[] = (object)array(
					'ID_COLACION' => '',
					'NOMBRE'      => 'Sin datos' 
				);
			}
		}else{
			redirect(site_url(). '/dashboard','refresh');
		}
	}


	public function get_data_cliente()
	{
	    $fields = $this->report->get_datatables();
	    $data   = array();
	    $no     = $_POST['start'];
	    $_element = '';
	    foreach ($fields as $element) {
			$no++;
			$row    = array();
			$row[]  = $this->pfalimentos->upper_lower($element->PLANTA);
			$row[]  = $this->pfalimentos->upper_lower($element->CASINO);
			$row[]  = $this->pfalimentos->upper_lower($element->CODIGO);
			$row[]  = $this->pfalimentos->upper_lower($element->NOMBRE);
			$row[]  = $this->pfalimentos->upper_lower($element->DEPARTAMENTO);
			if (is_numeric($element->EMPRESA) || $element->EMPRESA == '.' || $element->EMPRESA == '') {
				$row[]  = $this->pfalimentos->upper_lower('SIN EMPRESA');
			}else{
				$row[]  = $this->pfalimentos->upper_lower($element->EMPRESA);
			}
			$row[]  = $element->FECHA.' '.$element->HORA_ENTREGA;
			$row[]  = $this->pfalimentos->replace_guion_espacio($element->COLACION);
			$row[]  = $this->pfalimentos->upper_lower($element->ESTADO);
			$row[]  = $element->CANTIDAD;
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->report->count_all(),
	                    "recordsFiltered" => $this->report->count_filtered(),
	                    "data" => $data,
	    );
	    $this->output->set_output(json_encode($output));
	}

	public function export_excel_data()
	{
		if ($this->input->post()) {
			$result = $this->report->export_excel_data();
			$allData = $result->result_array();
			$dataToExports = array();
			if (!empty($allData)) {
				foreach ($allData as $data) {
					$arrangeData['PLANTA'] = $data['PLANTA'];
					$arrangeData['CASINO'] = $data['CASINO'];
					$arrangeData['CODIGO'] = $data['CODIGO'];
					$arrangeData['NOMBRE'] = $this->quitar_tildes($data['NOMBRE']);
					$arrangeData['EMPRESA'] = $this->quitar_tildes($data['EMPRESA']);
					$arrangeData['DEPARTAMENTO'] = $this->quitar_tildes($data['DEPARTAMENTO']);
					$arrangeData['FECHA'] = $data['FECHA'];
					$arrangeData['HORA_ENTREGA'] = $data['HORA_ENTREGA'];
					$arrangeData['COLACION'] = $this->quitar_tildes($data['COLACION']);
					$arrangeData['ESTADO'] = $data['ESTADO'];
					// $arrangeData['CANTIDAD'] = $data['CANTIDAD'];
					$dataToExports[] = $arrangeData;
				}
			}else{
				$arrangeData['RUT'] = 'SIN DATOS';
				$arrangeData['NOMBRE COLABORADOR'] = 'SIN DATOS';
				$arrangeData['PLANTA'] = 'SIN DATOS';
				$arrangeData['CASINO'] = 'SIN DATOS';
				$arrangeData['COLACION'] = 'SIN DATOS';
				$arrangeData['FECHA'] = 'SIN DATOS';
				$arrangeData['HORA ENTREGA'] = 'SIN DATOS';
				$arrangeData['COLACION'] = 'SIN DATOS';
				// $arrangeData['CANTIDAD'] = 'SIN DATOS';
				$dataToExports[] = $arrangeData;
			}

			$filename = "Casino_Export.xls";
			            header("Content-Type: application/vnd.ms-excel");
			            header("Content-Disposition: attachment; filename=\"$filename\"");
			$this->exportExcelData($dataToExports);
		}
	}

 	public function exportExcelData($records)
	{
		$heading = false;
	    if (!empty($records))
	        foreach ($records as $row) {
	            if (!$heading) {
	                echo implode("\t", array_keys($row)) . "\n";
	                $heading = true;
	            }
	            echo implode("\t", ($row)) . "\n";
	        }
	}

	function quitar_tildes($cadena) {
		$no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$texto = str_replace($no_permitidas, $permitidas ,$cadena);
		return $texto;
	}

	public function is_numeric($number)
	{
		if (is_numeric($number) || $number == '.' || $number == '') {
			return 'SIN EMPRESA';
		}else{
			return $number;
		}
	}

}

/* End of file Creport_state.php */
/* Location: ./application/controllers/Creport_state.php */