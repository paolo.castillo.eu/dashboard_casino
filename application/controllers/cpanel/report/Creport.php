<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creport extends PF_Controller {
  	private $header  = 'templates/header';
  	private $footer  = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Report_model','report');
    	$this->load->model('Plant_model','plant');
		$this->load->model('Casino_model','casino');
		$this->load->library('email');
		$this->load->model('Collation_model','collation');
    	$this->load->model('Home_model','home');
    	
		if ($this->session->profile_id == 3 || $this->session->profile_id == 4) {
		}else{
			redirect(base_url().'index.php/', 'refresh');
		}
	}

	public function index()
	{
		if ($this->session->profile_id >= 3) {
			$sq = $this->report->call_sequence();
			$this->session->set_userdata('nextval',$sq);
		}
		if ($this->session->profile_id == 3) {
			$data['colacion'] = $this->get_collation_by($this->session->casino);
		}
		$data['planta'] = $this->getPlant();
		// $data['colacion'] = $this->get_collation_by($this->session->casino);
		$data['empresa'] = $this->report->get_empresa();
	    $this->load->view($this->header);
		$this->load->view('dashboard/report/vReporteEditable',$data);
		$this->load->view($this->footer);
	}

	public function get_collation_by($id)
	{
		if (!empty($id)) {
			$row = $this->collation->get_collation_casino($id);
			if (!empty($row)) {
				return $row;
			}else{
				$result[] = (object)array(
					'ID_COLACION' => '',
					'NOMBRE'      => 'Sin datos' 
				);
			}
		}else{
			redirect(site_url(). '/dashboard','refresh');
		}
	}

	private function getPlant()
	{
		$data = $this->plant->get_all();
		return $data;
	}

	public function send_mail()
	{
		$data = array('success' => false, 'message' => '');
		$elemet = array();
		if ($this->input->post()) {
			$row = json_decode($this->input->post('rows'));
			if (!empty($row)) {
				foreach ($row as $key) {

					$cantidad = $this->strip_price($key[3]) + $this->strip_price($key[5]) + $this->strip_price($key[7]) + $this->strip_price($key[9]) + $this->strip_price($key[11]);
					$precio = $this->strip_price($key[4]) + $this->strip_price($key[6]) + $this->strip_price($key[8]) + $this->strip_price($key[10]) + $this->strip_price($key[12]);

					$elemet['elemet'][] = array(
						'casino' => $key[1],
						'fecha_entrega' => $key[2],
						'ticket_desayuno' => $key[3],
						'costo_desayuno' => $key[4],
						'ticket_almuerzo' => $key[5],
						'costo_almuerzo' => $key[6],
						'ticket_once' => $key[7],
						'costo_once' => $key[8],
						'ticket_cena' => $key[9],
						'costo_cena' => $key[10],
						'ticket_cena_noctura' => $key[11],
						'costo_cena_noctura' => $key[12],
						'cantidad' => number_format($cantidad, 0, ',', '.'),
						'precio' => '$'.number_format($precio, 0, ',', '.')
					);
				}
				
				$view = $this->load->view('dashboard/email/vReport', $elemet, true);
				// $this->sendEmail($this->session->email,$view,'Reporte');
				$this->sendEmail('paolo.castillo.eu@gmail.com',$view,'Reporte');
				$data['message'] = 'Se ha enviado el informe a su correo';
				$data['success'] = true;
 			}else{
				$data['message'] = 'No existe datos en el formulario';
			}
		}else{
			$data['message'] = 'Error al guardar reporte en el Sistema Casino';
		}
		$this->output->set_output(json_encode($data));
	}

	// public function sendEmail($email,$subject,$message)
 //    {
	// 	$config['useragent']    = 'CodeIgniter';
	// 	$config['protocol']     = 'smtp';
	// 	$config['smtp_host']    = 'ssl://smtp.googlemail.com';
	// 	$config['smtp_user']    = 'paolo.pruebas.eu@gmail.com'; // Your gmail id
	// 	$config['smtp_pass']    = '123'; // Your gmail Password
	// 	$config['smtp_port']    = 465;
	// 	$config['wordwrap']     = TRUE;    
	// 	$config['wrapchars']    = 76;
	// 	$config['mailtype']     = 'html';
	// 	$config['charset']      = 'UTF-8';
	// 	$config['validate']     = FALSE;
	// 	$config['priority']     = 3;
	// 	$config['newline']      = "\r\n";
	// 	$config['crlf']         = "\r\n";

	// 	$this->load->library('email');
	// 	$this->email->initialize($config);

	// 	$this->email->from('paolo.pruebas.eu@gmail.com', 'Casino');
	// 	$this->email->to($email); 
	// 	$this->email->cc('paolo.pruebas.eu@gmail.com'); 

	// 	$this->email->subject($subject);
	// 	$this->email->message($message);    

	// 	if($this->email->send())
 //     	{
	// 		echo json_encode(array('status' => TRUE));
 //     	}else{
	// 		show_error($this->email->print_debugger());
 //        }
 //    }

	public function get_rows()
	{
		$data = array('datos' => array(), 'success' => false, 'view' => '');
		$edit = array();
		if ($this->input->post()) {
			$planta = $this->check_empty($this->input->post('id_planta'));
			$casino = $this->check_empty($this->input->post('id_casino'));
			$colacion = $this->check_empty($this->input->post('id_colacion'));
			$empresa = $this->check_empty($this->input->post('id_empresa'));
			$id_fecha = $this->check_empty($this->input->post('id_fecha'));
			$elemet = (object) array(
				'planta' => $planta,
				'casino' => $casino,
				'colacion' => $colacion,
				'empresa' => $empresa,
				'fecha' => $id_fecha
			);
			$rows = $this->report->get_rows_reporte($elemet);
			foreach ($rows as $key) {
				$edit['datos'][] = (object) array(
					'id_casino' => $key->ID_CASINO,
					'casino' => ucwords(mb_strtolower($key->CASINO)),
					'fecha_entrega' => $key->FECHA_ENTREGA,
					'ticket_desayuno' => $key->NUM_TICKET_DESAYUNO,
					'costo_desayuno' => number_format($key->COSTO_DESAYUNO, 0, ',', '.'),
					'ticket_almuerzo' => $key->NUM_TICKET_ALMUERZO,
					'costo_almuerzo' => number_format($key->COSTO_ALMUERZO, 0, ',', '.'),
					'ticket_once' => $key->NUM_TICKET_ONCE,
					'costo_once' => number_format($key->COSTO_ONCE, 0, ',', '.'),
					'ticket_cena' => $key->NUM_TICKET_CENA,
					'costo_cena' => number_format($key->COSTO_CENA, 0, ',', '.'),
					'ticket_cena_noctura' => $key->NUM_TICKET_CENA_NOCTURA,
					'costo_cena_noctura' => number_format($key->COSTO_CENA_NOCTURA, 0, ',', '.')
				);
			}

			$data['view'] = $this->load->view('dashboard/report/vReport_Edit', $edit, true);
			$data['success'] = true;
		}
		$this->output->set_output(json_encode($data));
	}

	public function save_rows()
	{
		$data = array('success' => false, 'message' => '');
		if ($this->input->post()) {
			$row = json_decode($this->input->post('rows'));
			if (!empty($row)) {
				foreach ($row as $key) {
					$elemet = (object) array(
						'id_casino' => $key[0],
						'fecha_entrega' => $key[2],
						'ticket_desayuno' => $this->strip_price($key[3]),
						'costo_desayuno' => $this->strip_price($key[4]),
						'ticket_almuerzo' => $this->strip_price($key[5]),
						'costo_almuerzo' => $this->strip_price($key[6]),
						'ticket_once' => $this->strip_price($key[7]),
						'costo_once' => $this->strip_price($key[8]),
						'ticket_cena' => $this->strip_price($key[9]),
						'costo_cena' => $this->strip_price($key[10]),
						'ticket_cena_noctura' => $this->strip_price($key[11]),
						'costo_cena_noctura' => $this->strip_price($key[12]),
						'usuario' => $this->session->collaborator,
						'nextval' => $this->session->nextval
					);
					$data['success'] = $this->report->save_rows($elemet);
					$data['message'] = 'Se ha Guardado Correctamente el reporte';
				}
			}else{
				$data['message'] = 'No existe datos en formulario para continuar';
			}
		}else{
			$data['message'] = 'Error al guardar reporte en el Sistema Casino';
		}
		$this->output->set_output(json_encode($data));
	}

	public function check_empty($input)
	{
		if (!empty($input)) {
			return $input;
		}
		return null;
	}

	function check_default($post_string)
	{
		return $post_string == 'default' ? FALSE : TRUE;
	}

	public function strip_price($price)
	{
		$price = str_replace('.', '',substr($price, 0, -3)) . substr($price, -3);
		$price = preg_replace('/[\$,]/', '', $price);
		if ($price == '') {
			return 0;
		}
		return $price;
	}

  	public function sendEmail($email,$body,$subject)
    {
		$result = $this->home->send_email($email,$body,$subject);
    }

    public function report_save()
    {
	    $this->load->view($this->header);
		$this->load->view('dashboard/report/vReport_state_save');
		$this->load->view($this->footer);
    }

    public function get_reporte()
    {
	    $fields = $this->report->get_datatables_rep();
	    $data   = array();
	    $no     = $_POST['start'];
	    $_element = '';
	    foreach ($fields as $report) {
			$no++;
			$row    = array();
			$row[]  = $report->ID_SEQUENCIA;
			$row[]  = ucwords(mb_strtolower($report->NOMBRE));
			$row[]  = ucwords(mb_strtolower($report->CASINO));
			$row[]  = $report->FECHA;
			$row[]  = $report->FECHA_MODIFICACION;
			$data[] = $row;
	    }
	    $output = array(
	                    "draw" => $_POST['draw'],
	                    "recordsTotal" => $this->report->count_all_rep(),
	                    "recordsFiltered" => $this->report->count_filtered_rep(),
	                    "data" => $data,
	    );
	    $this->output->set_output(json_encode($output));
    }

    public function get_modal()
    {
    	$data = array('view' => '');
    	$param = $this->input->post('param');
    	$elemet['report'] = $this->report->get_reporte_save($param[0]);
    	
    	$data['view'] = $this->load->view('dashboard/report/vReport_modal', $elemet, true);
	    $this->output->set_output(json_encode($data));
    }

    public function update_rows()
    {
		$data = array('success' => false, 'message' => '');
		if ($this->input->post()) {
			$row = json_decode($this->input->post('rows'));
			if (!empty($row)) {
				foreach ($row as $key) {
					$elemet = (object) array(
						'id_reporte' => $key[0],
						'ticket_desayuno' => $this->strip_price($key[3]),
						'costo_desayuno' => $this->strip_price($key[4]),
						'ticket_almuerzo' => $this->strip_price($key[5]),
						'costo_almuerzo' => $this->strip_price($key[6]),
						'ticket_once' => $this->strip_price($key[7]),
						'costo_once' => $this->strip_price($key[8]),
						'ticket_cena' => $this->strip_price($key[9]),
						'costo_cena' => $this->strip_price($key[10]),
						'ticket_cena_noctura' => $this->strip_price($key[11]),
						'costo_cena_noctura' => $this->strip_price($key[12]),
						'usuario' => $this->session->collaborator
					);
					$data['success'] = $this->report->update_rows($elemet);
					$data['message'] = 'Se ha Guardado Correctamente el reporte';
				}
			}else{
				$data['message'] = 'No existe datos en formulario para continuar';
			}
		}else{
			$data['message'] = 'Error al guardar reporte en el Sistema Casino';
		}
		$this->output->set_output(json_encode($data));
    }

    public function get_gosto()
    {
    	$costo = 0;
    	if ($this->input->is_ajax_request()) {
			$element         = new stdClass;
			$element->tipo   = $this->input->post('tipo'); 
			$element->casino = $this->input->post('casino');
			$elementObj      = (object) $element;
			$costo = $this->report->get_costo($elementObj);
    	}
		$this->output->set_output(json_encode($costo));
    }
}

/* End of file Creport.php */
/* Location: ./application/controllers/Creport.php */