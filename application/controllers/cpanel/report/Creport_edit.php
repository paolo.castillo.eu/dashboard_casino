<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creport_edit extends PF_Controller {
	/**
	 * Controlador  Editar Reporte
	 * 
	 * @Package		PF_PACKAGE_CASINO | CASINO
	 * @Autor       Paolo Castillo
	 * @link        <http://intranet_prod/casino-web/index.php/configurar/reporte>
	 */

	/**
	 * Controlador Class
	 *
	 * @subpackage  Libreria pfalimento
	 * @categoria   Libreria
	 */
  	private $header  = 'templates/header';
  	private $footer  = 'templates/footer';

	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '586M');
		$this->load->model('Report_model','reporte');
		$this->load->model('Casino_model','casino');
		$this->load->library('PfAlimentos');
		$this->load->library('export_excel');
	}

	public function view_calendar()
	{
		$data['path']   = 'dashboard/editor/calendario';
		$data['casino'] = $this->casino->get_all();
		$calendario   = array();
		if ($this->input->is_ajax_request()) {
			$start  = date("d-m-Y", strtotime($this->input->get('start')));
			$fechas = $this->reporte->get_fecha_reporte_edit($start);
			if (!empty($fechas)) {
				foreach ($fechas as $value) {
					$calendario[] = array(
						'title' => '['.$value->ID_SEQUENCIA.'] REPORTE CASINO',
						'start' => $value->FECHA_ENTREGA,
						'color' => '#6DA8F0',
						'borderColor' => '#6DA8F0'
					);
				}
			}
			$this->output->set_output(json_encode($calendario));
		}else{
			$this->load->view($this->header);
			$this->load->view($data['path'],$data);
			$this->load->view($this->footer);
		}
	}

	public function get_reporte(){
		$data = array('status' => FALSE, 'view' => '');
		$parte1 = NULL;
		$parte2 = NULL;
		if ($this->input->is_ajax_request()) {
			$start  = date("d-m-Y", strtotime($this->input->post('fecha')));
			$id     = $this->input->post('id');
			if (!empty($id)) {
				$parte1 = explode('[', $id);
				$parte2 = explode(']', $parte1[1]);
			}
			$id = (!empty($parte2[0])) ? $parte2[0] : 0 ;
			$reporte['data'] = $this->reporte->get_reporte_edit($start,$id);
			if (!empty($reporte['data'])) {
				$data['view'] = $this->load->view('dashboard/editor/reporte.php', $reporte, TRUE);
				$data['status'] = TRUE; 
			}else{
				$data['view'] = '<div class="row"><div class="col-sm-12"><div class="callout callout-warning"><h4>Alerta con el reporte!</h4><p> No existe reporte asociado a esta fecha.</p></div></div></div>';
			}
		}
		$this->output->set_output(json_encode($data));
	}

	public function get_excel()
	{
		if ($this->input->post()) {

			$post         = new stdClass;
			$post->desde  = $this->input->post('desde');
			$post->hasta  = $this->input->post('hasta');
			$post->casino = $this->input->post('casino');
			$postObj      = (object) $post;

			$reporte      = $this->reporte->get_reporte_all($post);
			$allData      = $reporte->result_array();

			$dataToExports = array();
			if (!empty($allData)) {
				foreach ($allData as $key) {
					$arrangeData['ID_CASINO']            = $key['ID_CASINO'];
					$arrangeData['NOMBRE']               = $key['NOMBRE'];
					$arrangeData['FECHA']                = $key['FECHA'];
					$arrangeData['TICKET_DESAYUNO']      = $key['NUM_TICKET_DESAYUNO'];
					$arrangeData['COSTO_DESAYUNO']       = $key['COSTO_DESAYUNO'];
					$arrangeData['TICKET_ALMUERZO']      = $key['NUM_TICKET_ALMUERZO'];
					$arrangeData['COSTO_ALMUERZO']       = $key['COSTO_ALMUERZO'];
					$arrangeData['TICKET_ONCE']          = $key['NUM_TICKET_ONCE'];
					$arrangeData['COSTO_ONCE']           = $key['COSTO_ONCE'];
					$arrangeData['TICKET_CENA']          = $key['NUM_TICKET_CENA'];
					$arrangeData['COSTO_CENA']           = $key['COSTO_CENA'];
					$arrangeData['TICKET_CENA_NOCTURNA'] = $key['NUM_TICKET_CENA_NOCTURNA'];
					$arrangeData['COSTO_CENA_NOCTURNA']  = $key['COSTO_CENA_NOCTURNA'];
					$dataToExports[] = $arrangeData;
				}
			}else{
				$arrangeData['ID_CASINO']            = '';
				$arrangeData['NOMBRE']               = '';
				$arrangeData['FECHA']                = '';
				$arrangeData['TICKET_DESAYUNO']      = '';
				$arrangeData['COSTO_DESAYUNO']       = '';
				$arrangeData['TICKET_ALMUERZO']      = '';
				$arrangeData['COSTO_ALMUERZO']       = '';
				$arrangeData['TICKET_ONCE']          = '';
				$arrangeData['COSTO_ONCE']           = '';
				$arrangeData['TICKET_CENA']          = '';
				$arrangeData['COSTO_CENA']           = '';
				$arrangeData['TICKET_CENA_NOCTURNA'] = '';
				$arrangeData['COSTO_CENA_NOCTURNA']  = '';
				$dataToExports[] = $arrangeData;
			}

			$filename = "Casino_Reporte_Export.xls";
			            header("Content-Type: application/vnd.ms-excel");
			            header("Content-Disposition: attachment; filename=\"$filename\"");
			$this->exportExcelData($dataToExports);
		}
	}

 	public function exportExcelData($records)
	{
		$heading = false;
	    if (!empty($records))
	        foreach ($records as $row) {
	            if (!$heading) {
	                echo implode("\t", array_keys($row)) . "\n";
	                $heading = true;
	            }
	            echo implode("\t", ($row)) . "\n";
	        }
	}

}

/* End of file Creport_edit.php */
/* Location: ./application/controllers/Creport_edit.php */