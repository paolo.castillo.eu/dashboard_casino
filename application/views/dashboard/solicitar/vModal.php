<form action="<?php echo site_url()?>/solicitar_colacion/save_solicitud_emp" method="post" accept-charset="utf-8" id="form_emp">
<div class="modal-body">   
    <div class="row">
    	<div class="col-md-6">
    		<div class="form-group">
    			<label for="">Nombre : </label>
    			<input class="form-control" type="text" name="id_nombre_emp" value="<?php echo ucwords(mb_strtolower($colaborador[0]->NOMBRE))?>" readonly>
    		</div>
    	</div>
    	<div class="col-md-6">
    		<div class="form-group">
    			<label for="">Departamento : </label>
    			<input class="form-control" type="text" value="<?php echo ucwords(mb_strtolower($colaborador[0]->DEPARTAMENTO))?>" readonly>
    		</div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-6">
    		<div class="form-group">
    			<label for="">Email : </label>
    			<input class="form-control" type="text" name="id_email_emp" value="<?php echo $colaborador[0]->CORREO?>" readonly>
    		</div>
    	</div>
    	<div class="col-md-6">
    		<div class="form-group">
    			<label for="">Credencial : </label>
    			<input class="form-control" type="text" value="<?php echo ucwords(mb_strtolower($colaborador[0]->CREDENCIAL))?>" readonly>
    		</div>
    	</div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="id_planta_emp">Planta</label>
                <select class="form-control" id="id_planta_emp" name="id_planta_emp">
                    <option value="default">Seleccione Opción</option>
                    <?php foreach ($planta as $plantas): ?>
                        <option value="<?php echo $plantas->ID_PLANTA; ?>">
                            <?php echo ucwords(mb_strtolower($plantas->NOMBRE)); ?>
                        </option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="id_casino_emp">Casino</label>
                <select class="form-control" name="id_casino_emp" id="id_casino_emp">
                    <option value="default">Seleccione Casino</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="id_colacion_emp">Colación</label>
                <select class="form-control" name="id_colacion_emp" id="id_colacion_emp" >
                    <option value="default">Seleccione Casino</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="id_fecha_emp">Fecha Colación</label>
                <div class="date" id="date">
                    <input class="form-control" type="text" name="id_fecha_emp" id="id_fecha_emp">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label id="label_desc">Justificación de la Solicitud</label>
                <span id="span_count" class="pull-right"><small class="pull-right"><strong>Caracteres disponibles</strong>: <span id="count_emp">1000</span> de 1000</small></span>
                <textarea id="description_emp" class="form-control" rows="3" name="description_emp" placeholder="EJ: Se realiza esta solicitud por una reunión de negocio en Planta X en Santiago centro." style="resize: none;"></textarea>
            </div>
        </div>
    </div>
    <input type="hidden"  class="hidden" id="id_colaborador_select" name="id_colaborador_select" value="<?php echo $colaborador[0]->ID_COLABORADOR?>">
    <input type="hidden"  class="hidden" id="id_rut" name="id_rut" value="<?php echo $rut?>">
</div>
<div class="modal-footer">
    <div class="col-md-6">
        <button type="button" class="btn btn-primary col-sm-2 pull-left btn-block" data-dismiss="modal">Cerrar</button>
    </div>
    <div class="col-md-6">
        <button type="submit" class="btn btn-success col-sm-2 pull-right btn-generar btn-block" id="btn-generar">Generar Solicitud</button>
    </div>
</div>
</form>
<script>
	$(function() {
        $('.btn-generar').prop('disabled','disabled');
        $('#id_fecha_emp').val(pf_dayMonthYears('/'));
        $('#id_fecha_emp').datepicker({
            language: "es",
            format: 'dd/mm/yyyy',
            startDate: 'today'
        });
	});

	function pf_dayMonthYears(separador) {
   		var tdate = new Date();
   		var dd = tdate.getDate();
   		var MM = tdate.getMonth();
   		var yyyy = tdate.getFullYear(); 
   		var currentDate= dd + separador +( MM+1) + separador + yyyy;
   		return currentDate;
	}

    $('#id_planta_emp').change(function() {
        var val = $(this).val();
        var cas = $('#id_casino_emp');
        var html = '';
        pf_blockUI();
        $.post('<?php echo site_url()?>/solicitar_colacion/casino', {
            planta : val,
            id : $('#id_colaborador_select').val()
        }, function(data) {
            html = html + '<option value="default">Seleccione Casino</option>';
            $.each(data.casino,function(index, element) {
                html = html + '<option value="' + element.values + '">' + element.text + '</option>';
            });
            cas.find('option')
            .remove()
            .end()
            .append(html);
            cas.trigger('change');
        },'json')
        .always(function() {
            pf_unblockUI();
        });
    });

    $('#id_casino_emp').change(function() {
        var option = $(this).val();
        var colacion = $('#id_colacion_emp');
        var html = '';
        pf_blockUI();
        $.post('<?php echo site_url()?>/solicitar_colacion/collation', {
            casino : option,
            id : $('#id_colaborador_select').val()
        }, function(data) {
            html = html + '<option value="default">Seleccione Colación</option>';
            $.each(data.colacion,function(index, element) {
                html = html + '<option value="' + element.values + '">' + element.text + '</option>';
            });
            colacion.find('option')
            .remove()
            .end()
            .append(html);
        },'json')
        .always(function() {
            pf_unblockUI();
        });
    });

    $('#description_emp').on('change keyup paste', function() {
        var count = 1000 - $('#description_emp').val().length;
        $('#count_emp').text(count);
        if (parseInt(count) <= 990) {
            $(this).parent().removeClass('has-error');
            $(this).parent().addClass('has-success');
            $('.btn-generar').prop('disabled',false);
        }else{
            $(this).parent().removeClass('has-success');
            $(this).parent().addClass('has-error');
            $('.btn-generar').prop('disabled',true);
        }
    });

    $('#form_emp').submit(function(event){
        event.preventDefault();
        $(this).prop('disabled', 'disabled');
        var form = $(this).serialize();
        var url  = $(this).attr('action');
        pf_blockUI();
        $.post(url, form, 
            function(data) {
                if (data.success) {
                    $('#modal_cliente').modal('hide');
                    $('.cliente_plus').empty();
                    $('#id_fecha_emp').val(pf_dayMonthYears('/'));
                    $.notify({
                        icon: 'fa fa-check-circle',
                        title: "<strong> Solicitud de Colación </strong> <br/>",
                        message: data.text
                    }, {
                            type: 'success',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    $('.btn-generar').prop('disabled', false);
                }else{
                    $.each(data.message,function(key, value) {
                        var element = $('#' + key);
                        element.closest('div.form-group')
                        .removeClass('has-success')
                        .removeClass('has-error')
                        .addClass(value.length > 0 ? 'has-error' : 'has-success')
                        .find('.text-danger')
                        .remove();
                    });

                    if (data.text.length) {
                        $.notify({
                            icon: 'fa fa-close',
                            title: "<strong> Solicitud de Colación </strong> <br/>",
                            message: data.text
                        }, {
                                type: 'danger',
                                showProgressbar: false,
                                placement: {
                                from: "bottom",
                                align: "right"
                            },
                                delay: 4000,
                                timer: 3000,
                                z_index: 9999,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                    $('.btn-generar').prop('disabled', false);
                }
        },'json')
        .always(function() {
            pf_unblockUI();
        });
    });
</script>
