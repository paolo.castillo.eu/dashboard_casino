<div class="content-wrapper">
    <section class="content-header">
        <h1>Registro de Solicitud</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Solcitar Colación</li>
        </ol>
    </section>
    <form action="<?php echo site_url()?>/solicitar_colacion/save_solicitud" method="post" accept-charset="utf-8">
    <section class="content-header">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registro de Solicitud</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_planta">Planta</label>
                                    <select class="form-control" id="id_planta" name="id_planta">
                                        <option value="default">Seleccione Opción</option>
                                        <?php foreach ($planta as $plantas): ?>
                                            <option value="<?php echo $plantas->ID_PLANTA; ?>">
                                                <?php echo ucwords(mb_strtolower($plantas->NOMBRE)); ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_casino">Casino</label>
                                    <select class="form-control" name="id_casino" id="id_casino">
                                        <option value="default">Seleccione Planta</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_colacion">Colación</label>
                                    <select class="form-control" name="id_colacion" id="id_colacion" >
                                        <option value="default">Seleccione Casino</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_fecha">Fecha Colación</label>
                                    <div class="date" id="date">
                                        <input class="form-control" type="text" name="id_fecha" id="id_fecha">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>        
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detalle de Justificación de Colación</h3>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label id="label_desc">Justificación de la Solicitud</label>
                                    <span id="span_count" class="pull-right"><small class="pull-right"><strong>Caracteres disponibles</strong>: <span id="count">1000</span> de 1000</small></span>
                                    <textarea id="description" class="form-control" rows="3" name="description" placeholder="EJ: Se realiza esta solicitud por una reunión de negocio en Planta X en Santiago centro."></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="padding-top-5"></div>
                    <div class="box-footer">
                        <a href="<?php echo site_url() ?>/dashboard" class="btn btn-flat btn-default"><strong>Volver</strong></a>
                        <button type="submit" class="btn btn-primary btn-flat pull-right" id="register"><strong>Enviar Solicitud</strong></button>    
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php if (!empty($empleados) || !empty($solicitante)): ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title"><li class="fa fa-users"></li>Realizar Solicitud a tus Colaboradores</h3>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-group" id="accordion">
                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">
                                                Realizar Solicitud a los Empleados a Cargo
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="false" style="">
                                            <div class="box-body">
                                                <?php if (!empty($empleados)): ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group margin-btm-5">
                                                          <label>Mostrar</label>
                                                          <select id="show_record" class="form-control">
                                                              <option value="10">10 registros</option>
                                                              <option value="25">25 registros</option>
                                                              <option value="50">50 registros</option>
                                                              <option value="100">100 registros</option>
                                                          </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Buscar</label>
                                                            <div class="input-group stylish-input-group">
                                                                <input type="text" class="form-control" id="search_input">
                                                                <span class="input-group-addon">
                                                                    <button type="submit">
                                                                        <span class="glyphicon glyphicon-search"></span>
                                                                    </button>  
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="row table-responsive no-left-right-margin">
                                                            <div class="col-md-12">
                                                                <table id="table_empleado" class="table table-bordered display" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr class="bg-light-blue disabled color-palette">
                                                                            <th class="no-sort text-center"></th>
                                                                            <th class="no-sort text-center">Rut</th>
                                                                            <th class="no-sort text-center">Nombre</th>
                                                                            <th class="no-sort text-center">Departamento</th>
                                                                            <th class="no-sort text-center">Correo</th>
                                                                            <th class="no-sort text-center"></th>
                                                                            <th class=""></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php $i = 1?>
                                                                        <?php foreach ($empleados as $key): ?>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td><?php echo $key->RUT_EMPLEADO?></td>
                                                                                <td><?php echo ucwords(mb_strtolower($key->NOMBRE))?></td>
                                                                                <td><?php echo ucwords(mb_strtolower($key->PLANTA))?></td>
                                                                                <td><?php echo $key->CORREO_TRABAJADOR?></td>
                                                                                <td></td>
                                                                                <td><?php echo $i?></td>
                                                                            </tr>
                                                                        <?php $i++?>
                                                                        <?php endforeach ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php else :?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="callout callout-warning">
                                                            <h4> <i class="fa fa-commenting-o"></i> El Colaborador no cuenta con empleado a cargo.</h4>
                                                            <p>El solicitante actual no cuenta con colaborador a cargo para realizar solicitud de Jefatura.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel box box-info">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                                                Rut Solicitante
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="box-body">
                                                <?php if (!empty($solicitante)): ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group margin-btm-5">
                                                          <label>Mostrar</label>
                                                          <select id="show_record_solicitante" class="form-control">
                                                              <option value="10">10 registros</option>
                                                              <option value="25">25 registros</option>
                                                              <option value="50">50 registros</option>
                                                              <option value="100">100 registros</option>
                                                          </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Buscar</label>
                                                            <div class="input-group stylish-input-group">
                                                                <input type="text" class="form-control" id="search_input_solicitante">
                                                                <span class="input-group-addon">
                                                                    <button type="submit">
                                                                        <span class="glyphicon glyphicon-search"></span>
                                                                    </button>  
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="row table-responsive no-left-right-margin">
                                                            <div class="col-md-12">
                                                                <table id="table_empleado_solicitante" class="table table-bordered display" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr class="bg-light-blue disabled color-palette">
                                                                            <th class="no-sort text-center">Rut</th>
                                                                            <th class="no-sort text-center">Departamento</th>
                                                                            <th class="no-sort text-center">Nombre</th>
                                                                            <th class="no-sort text-center">Correo</th>
                                                                            <th class="no-sort text-center"></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach ($solicitante as $key): ?>
                                                                            <tr>
                                                                                <td><?php echo $key->CODIGO?></td>
                                                                                <td><?php echo ucwords(mb_strtolower($key->NOMBRE))?></td>
                                                                                <td><?php echo ucwords(mb_strtolower($key->DEPARTAMENTO))?></td>
                                                                                <td><?php echo ucwords(mb_strtolower($key->CORREO))?></td>
                                                                                <td></td>
                                                                            </tr>
                                                                        <?php endforeach ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php else :?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="callout callout-warning">
                                                            <h4> <i class="fa fa-commenting-o"></i> El Colaborador no cuenta con empleado a cargo.</h4>
                                                            <p>El solicitante actual no cuenta con colaborador a cargo para realizar solicitud de Jefatura.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif?>
    </form>
</div>
<script>
    $(function () {
        $("#register").prop('disabled','disabled');
        $('#id_fecha').val(pf_dayMonthYears('/'));
        $('#id_fecha').datepicker({
            language: "es",
            format: 'dd/mm/yyyy',
            startDate: 'today'
        });

        function pf_dayMonthYears(separador) {
            var tdate = new Date();
            var dd = tdate.getDate(); //yields day
            var MM = tdate.getMonth(); //yields month
            var yyyy = tdate.getFullYear(); //yields year
            var currentDate= dd + separador +( MM+1) + separador + yyyy;
            return currentDate;
        }

        $('#id_planta').change(function() {
            var option = $(this).val();
            var casino = $('#id_casino');
            var html = '';
            pf_blockUI();
            $.post('<?php echo site_url()?>/solicitar_colacion/casino', {
                planta : option
            }, function(data) {
                html = html + '<option value="default">Seleccione Casino</option>';
                $.each(data.casino,function(index, element) {
                    html = html + '<option value="' + element.values + '">' + element.text + '</option>';
                });
                casino.find('option')
                .remove()
                .end()
                .append(html);
                casino.trigger('change');
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });

        $('#id_casino').change(function () {
            var option = $(this).val();
            var colacion = $('#id_colacion');
            var html = '';
            pf_blockUI();
            $.post('<?php echo site_url()?>/solicitar_colacion/collation', {
                casino : option
            }, function(data) {
                html = html + '<option value="default">Seleccione Colación</option>';
                $.each(data.colacion,function(index, element) {
                    html = html + '<option value="' + element.values + '">' + element.text + '</option>';
                });
                colacion.find('option')
                .remove()
                .end()
                .append(html);
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });

        $('#description').on('change keyup paste', function() {
            var count = 1000 - $('#description').val().length;
            $('#count').text(count);
            if (parseInt(count) <= 990) {
                $(this).parent().removeClass('has-error');
                $(this).parent().addClass('has-success');
                $('#register').prop('disabled',false);
            }else{
                $(this).parent().removeClass('has-success');
                $(this).parent().addClass('has-error');
                $('#register').prop('disabled',true);
            }
        });
        

        $('form').submit(function(event) {
            event.preventDefault();
            var url = $(this).attr('action');
            pf_blockUI();
            $.post(url,$('form').serialize(), function(data) {
                if (data.success) {
                    $('form')[0].reset();
                    $('div.form-group').removeClass('has-success').removeClass('has-error');
                    $('#id_fecha').val(pf_dayMonthYears('/'));
                    $.notify({
                        icon: 'fa fa-check-circle',
                        title: "<strong> Solicitud de Colación </strong> <br/>",
                        message: data.text
                    }, {
                            type: 'success',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                }else{
                    $.each(data.message,function(key, value) {
                        var element = $('#' + key);
                        element.closest('div.form-group')
                        .removeClass('has-success')
                        .removeClass('has-error')
                        .addClass(value.length > 0 ? 'has-error' : 'has-success')
                        .find('.text-danger')
                        .remove();
                    });
                    if (data.text.length) {
                        $.notify({
                            icon: 'fa fa-close',
                            title: "<strong> Solicitud de Colación </strong> <br/>",
                            message: data.text
                        }, {
                                type: 'danger',
                                showProgressbar: false,
                                placement: {
                                from: "bottom",
                                align: "right"
                            },
                                delay: 4000,
                                timer: 3000,
                                z_index: 9999,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                }
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });

    });
</script>
<script>
  var table_empleado;
  $(document).ready(function() {
      table_empleado = $('#table_empleado').DataTable({
        "columnDefs": [
            { 
                "targets": -2,
                "data": null,
                "defaultContent": '<button type="button" class="btn btn-info btn-block btn-solicitar">Seleccionar Colaborador</button>' 
            },
            {
                "targets": [ -1 ],
                "visible": false,
                "searchable": false
            },
            { "width": "1%", "targets": 0 },
            { "width": "1%", "targets": 1 },
            { "width": "1%", "targets": 2 },
            { "width": "1%", "targets": 3 },
            { "width": "1%", "targets": 4 },
            { "width": "1%", "targets": 5 },
            { targets: 'no-sort', orderable: false }
        ],
        "columns": [
            {   
                "class": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": '<button type="button" class="btn btn-success btn-block fa fa-plus-circle"></button>'
            },
            null,
            null,
            null,
            null,
            null,
            null
        ],
        "createdRow": function ( row, data, index ) {
            $('td', row).eq(0).addClass('text-center');
            $('td', row).eq(1).addClass('text-center');
            $('td', row).eq(2).addClass('text-center');
            $('td', row).eq(3).addClass('text-center');
            $('td', row).eq(4).addClass('text-center');
            $('td', row).eq(6).addClass('text-center');
        },
        "autoWidth": true,
        "info": false,
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }, 
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    jQuery("#footer").ready(function(){
        jQuery("#table_empleado_length").addClass('hidden');
        jQuery("#table_empleado_filter").addClass('hidden');
        jQuery("#table_empleado_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_empleado_info").text());
        jQuery("#table_empleado_paginate").appendTo(jQuery("#footer-right"));
    });


    $('#search_input').keyup(function(){
        table_empleado.search($(this).val()).draw() ;
    })

    $('#show_record').click(function() {
        table_empleado.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_empleado_inc_info").text());
    });

    jQuery("#table_empleado").on("page.dt", function(){
        var info = table_empleado.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });

    });

    $('#table_empleado tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table_empleado.row(tr);
        var data = row.data();
        var nombre = data[3];
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            row.child( show_empleado(data)).show();
            tr.addClass('shown');
        }
    });

    function show_empleado(table) {
        $var_html = '';
        pf_blockUI();        
        $.ajax({
            url:"<?php echo site_url() ;?>/solicitar_colacion/get_empleado_sub",
            type: 'POST',
            dataType: 'json',
            data: { 
                table : table
            },
            async: false,
            success:function(data) {
                $var_html = data.view; 
            },
            complete : function () {
                pf_unblockUI();
            }
        });
        return $var_html;
    }

    $('#table_empleado tbody').on( 'click', '.btn-solicitar', function () {
        var btn = $('.btn-solicitar');
        btn.prop('disabled','disabled');
        var data = table_empleado.row( $(this).parents('tr') ).data();
        Pace.track(function(){
            pf_blockUI();
            $.post('<?php echo site_url() ;?>/solicitar_colacion/get_empleado', {
                rut : data[1] 
            }, function(data) {
                if (data.success) {
                    $('.cliente_plus').empty().append(data.view);
                    $('#modal_cliente').modal('show');
                    btn.prop('disabled',false);
                }else{
                    $.notify({
                        icon: 'fa fa-close',
                        title: "<strong> Conseguir dato empleado </strong> <br/>",
                        message: data.message
                    }, {
                            type: 'danger',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled',false);
                }
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
    });
</script>

<div class="modal fade" id="modal_cliente" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <h4 class="modal-title">Datos del Cliente</h4>
            </div>
            <div class="cliente_plus">

            </div>
        </div>
    </div>
</div>

<script>
    var table_empleado_solicitante;
    $(document).ready(function() {
        table_empleado_solicitante = $('#table_empleado_solicitante').DataTable({
        "columnDefs": [
            { 
                "targets": -1,
                "data": null,
                "defaultContent": '<button type="button" class="btn btn-info btn-block btn-solicitar">Seleccionar Colaborador</button>' 
            },
            { "width": "1%", "targets": 0 },
            { "width": "1%", "targets": 1 },
            { "width": "1%", "targets": 2 },
            { "width": "1%", "targets": 3 },
            { "width": "1%", "targets": 4 },
            { targets: 'no-sort', orderable: false }
        ],
        "columns": [
            null,
            null,
            null,
            null,
            null
        ],
        "createdRow": function ( row, data, index ) {
            $('td', row).eq(0).addClass('text-center');
            $('td', row).eq(1).addClass('text-center');
            $('td', row).eq(2).addClass('text-center');
            $('td', row).eq(3).addClass('text-center');
            $('td', row).eq(4).addClass('text-center');
        },
        "autoWidth": true,
        "info": false,
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }, 
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    jQuery("#footer").ready(function(){
        jQuery("#table_empleado_solicitante_length").addClass('hidden');
        jQuery("#table_empleado_solicitante_filter").addClass('hidden');
        jQuery("#table_empleado_solicitante_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_empleado_solicitante_info").text());
        jQuery("#table_empleado_solicitante_paginate").appendTo(jQuery("#footer-right"));
    });


    $('#search_input_solicitante').keyup(function(){
        table_empleado_solicitante.search($(this).val()).draw() ;
    })

    $('#show_record_solicitante').click(function() {
        table_empleado.page.len($('#show_record_solicitante').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_empleado_solicitante_inc_info").text());
    });

    jQuery("#table_empleado_solicitante").on("page.dt", function(){
        var info = table_empleado_solicitante.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });

    });


    $('#table_empleado_solicitante tbody').on( 'click', '.btn-solicitar', function () {
        var btn = $('.btn-solicitar');
        btn.prop('disabled','disabled');
        var data = table_empleado_solicitante.row( $(this).parents('tr') ).data();
        Pace.track(function(){
            pf_blockUI();    
            $.post('<?php echo site_url() ;?>/solicitar_colacion/get_empleado', {
                rut : data[0] 
            }, function(data) {
                if (data.success) {
                    $('.cliente_plus').empty().append(data.view);
                    $('#modal_cliente').modal('show');
                    btn.prop('disabled',false);
                }else{
                    $.notify({
                        icon: 'fa fa-close',
                        title: "<strong> Conseguir dato empleado </strong> <br/>",
                        message: data.message
                    }, {
                            type: 'danger',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled',false);
                }
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
    });
</script>