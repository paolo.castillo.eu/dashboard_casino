<div class="well well-sm">    
    <div class="box-header with-border text-light-blue">
        <i class="fa fa-list-alt"></i>
        <h3 class="box-title">Lista Colaborador <?php echo $nombre?></h3>
    </div>
    <div class="row">
        <div class="col-md-6">
        
        </div>
        <div class="col-md-6">
        
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?php if (!empty($empleado)): ?>
                <div class="row table-responsive no-left-right-margin">
                    <div class="col-md-12">
                        <table id="table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>" class="table table-bordered display" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg-navy disabled color-palette">
                                    <th class="no-sort text-center"></th>
                                    <th class="no-sort text-center">Rut</th>
                                    <th class="no-sort text-center">Nombre</th>
                                    <th class="no-sort text-center">Departamento</th>
                                    <th class="no-sort text-center">Correo</th>
                                    <th class="no-sort text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($empleado as $key): ?>
                                    <tr>
                                        <td></td>
                                        <td><?php echo ucwords(mb_strtolower($key->RUT_EMPLEADO))?></td>
                                        <td><?php echo ucwords(mb_strtolower($key->NOMBRE))?></td>
                                        <td><?php echo ucwords(mb_strtolower($key->PLANTA))?></td>
                                        <td><?php echo ucwords(mb_strtolower($key->CORREO_TRABAJADOR))?></td>
                                        <td></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php else :?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="callout callout-warning">
                            <h4> <i class="fa fa-commenting-o"></i> El Colaborador no cuenta con empleado a cargo.</h4>
                            <p>El colaborador seleccionado no tiene empleados a su cargo para generar colación.</p>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
<script>
    <?php if (!empty($colaborador[0]->ID_COLABORADOR)): ?>
    var table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>;
    $(document).ready(function() {
      table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?> = $('#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>').DataTable({
        "columnDefs": [
            { 
                "targets": -2,
                "data": null,
                "defaultContent": '<button type="button" class="btn btn-info btn-block btn-solicitar<?php echo $colaborador[0]->ID_COLABORADOR?>">Seleccionar Colaborador</button>' 
            },
            {
                "targets": [ -1 ],
                "visible": false,
                "searchable": false
            },
            { "width": "1%", "targets": 0 },
            { "width": "1%", "targets": 1 },
            { "width": "1%", "targets": 2 },
            { "width": "1%", "targets": 3 },
            { "width": "1%", "targets": 4 },
            { targets: 'no-sort', orderable: false }
        ],
        "columns": [
            {   
                "class": 'details-control<?php echo $colaborador[0]->ID_COLABORADOR?>',
                "orderable": false,
                "data": null,
                "defaultContent": '<button type="button" class="btn btn-success btn-block fa fa-plus-circle"></button>'
            },
            null,
            null,
            null,
            null,
            null
        ],
        "createdRow": function ( row, data, index ) {
            $('td', row).eq(0).addClass('text-center');
            $('td', row).eq(1).addClass('text-center');
            $('td', row).eq(2).addClass('text-center');
            $('td', row).eq(3).addClass('text-center');
            $('td', row).eq(4).addClass('text-center');
        },
        "autoWidth": true,
        "info": false,
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }, 
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

    jQuery("#footer").ready(function(){
        jQuery("#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>_length").addClass('hidden');
        jQuery("#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>_filter").addClass('hidden');
        jQuery("#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>_info").text());
        jQuery("#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>_paginate").appendTo(jQuery("#footer-right"));
    });


    $('#search_input<?php echo $colaborador[0]->ID_COLABORADOR?>').keyup(function(){
        table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>.search($(this).val()).draw() ;
    })

    $('#show_record<?php echo $colaborador[0]->ID_COLABORADOR?>').click(function() {
        table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>_inc_info").text());
    });

    jQuery("#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>").on("page.dt", function(){
        var info = table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });

    });

    $('#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?> tbody').on( 'click', '.btn-solicitar<?php echo $colaborador[0]->ID_COLABORADOR?>', function () {
        var btn = $('.btn-solicitar<?php echo $colaborador[0]->ID_COLABORADOR?>');
        btn.prop('disabled','disabled');
        var data = table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>.row( $(this).parents('tr') ).data();
        Pace.track(function(){
            pf_blockUI();
            $.post('<?php echo site_url() ;?>/solicitar_colacion/get_empleado', {
                rut : data[1]
            }, function(data) {
                if (data.success) {
                    $('.cliente_plus').empty().append(data.view);
                    $('#modal_cliente').modal('show');
                    btn.prop('disabled',false);
                }else{
                    $.notify({
                        icon: 'fa fa-close',
                        title: "<strong> Conseguir dato empleado </strong> <br/>",
                        message: data.message
                    }, {
                            type: 'danger',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled',false);
                }
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
    });


    $('#table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?> tbody').on('click', 'td.details-control<?php echo $colaborador[0]->ID_COLABORADOR?>', function () {
        var tr = $(this).closest('tr');
        var row = table_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>.row(tr);
        var data = row.data();
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            row.child( show_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>( data[1], data[6])).show();
            tr.addClass('shown');
        }
    });


    function show_empleado<?php echo $colaborador[0]->ID_COLABORADOR?>(rut,id) {
        $var_html = '';
        pf_blockUI();
        $.ajax({
            url:"<?php echo site_url() ;?>/solicitar_colacion/get_empleado_sub",
            type: 'POST',
            dataType: 'json',
            data: { 
                rut : rut,
                id : id
            },
            async: false,
            success:function(data) {
                $var_html = data.view; 
            },
            complete : function () {
                pf_unblockUI();
            }
        });
        return $var_html;
    }
    <?php endif ?>
</script>