<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Configuración de Reporte Casino
            <small><b>Mensual</b></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Configuración de Reporte Casino</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-gears"></i> Configurar Reporte</h3>
                    </div>
                    <div class="box-body no-padding">
                        <div id="calendar"></div>
                    </div>
                    <div class="box-footer">
                        <a href="javascript:window.history.go(-1);" class="btn btn-flat btn-default"><strong>Volver</strong></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-excel-o"></i> Generar Excel </h3>
                    </div>
                    <div class="box-body">
                        <?php echo form_open(site_url('configurar/get_excel')); ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Casino</label>
                                    <select class="form-control select2" name="casino">
                                        <option value="" selected>Seleccione Casino</option>
                                        <?php if ($this->session->profile_id == 4): ?>
                                            <?php foreach ($casino as $value): ?>
                                                <option value="<?php echo $value->ID_CASINO?>"><?php echo ucwords(mb_strtolower($value->NOMBRE))?></option>
                                            <?php endforeach ?>
                                        <?php else :?>
                                            <option value="<?php echo $this->session->casino?>"><?php echo ucwords(mb_strtolower($this->session->casino_name))?></option>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Desde</label>
                                    <div class="date" id="date">
                                        <input type="text" class="form-control fecha" name="desde" id="desde" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Hasta</label>
                                    <div class="date" id="date">
                                        <input type="text" class="form-control fecha" name="hasta" id="hasta" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button href="#" class="btn btn-success btn-block" style=""> Exportar <i class="fa fa-file-excel-o"></i></button>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade in" id="modal-default">
        <div class="modal-dialog modal-lg" style="width: 1200px;margin: auto">
            <div class="modal-content">
                <div class="modal-header modal-header-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Reporte Casino</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary btn-update" id="btn-update">Guardar cambios</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    var calendar;
    var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var monthsShort = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    var days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    var daysShort = ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'];

    $(function() {
        $('.select2').select2();

        $('.fecha').datepicker({
            language: "es",
            format: 'dd/mm/yyyy'
        });

        $('#desde').val(pf_dayMonthYears('/'));
        $('#hasta').val(pf_dayMonthYears('/'));

        calendario = $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
            },
            buttonText: {
                today: 'Hoy',
                month: 'Mes',
                week: 'Semana',
                day: 'Día'
            },
            locale: 'es',
            monthNames: months,
            monthNamesShort: monthsShort,
            dayNames: days,
            dayNamesShort: daysShort,
            allDayText: 'Todo el día',
            editable: false,
            droppable: false,
            eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2200-01-01'
            },
            events:
            {
                start: 'dd/MM/yyyy',
                end: 'dd/MM/yyyy',
                url: '<?php echo site_url('configurar/reporte');?>',
                error: function() {

                },
                success: function(){
                }
            },
            eventClick:  function(event, jsEvent, view) {
                var status, view;
                returnCuerpo(event.start.format(), event.title, function(variable)
                {
                    $('.modal-body').empty().append(variable.view);
                    if (!variable.status) {
                        $.notify({
                            icon: 'fa fa-close',
                            title: "<strong> Reporte </strong> <br/>",
                            message: 'Error, al visualizar el reporte'
                        }, {
                                type: 'danger',
                                showProgressbar: false,
                                placement: {
                                from: "bottom",
                                align: "right"
                            },
                                delay: 4000,
                                timer: 3000,
                                z_index: 9999,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                });
                $('#modal-default').modal();
            }
        });

        function returnCuerpo (fecha, id, callback) {
            $.post('<?php echo site_url('configurar/get_reporte')?>', {fecha: fecha, id : id}, function(data) {
                callback(data);
            },'json');
        }
    });
</script>