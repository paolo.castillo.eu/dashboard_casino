<div class="row">
    <div class="col-sm-12">
        <div class="row table-responsive no-left-right-margin">
            <table id="table_old" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Casino</p></td>
                        <td colspan="1" bgcolor="#CDDC39" style="text-align: center;"><p>Fecha Entrega</p></td>
                        <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Desayuno</p></td>
                        <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Almuerzo</p></td>
                        <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Onces</p></td>
                        <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Cena</p></td>
                        <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Cena Noc</p></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>N° <label class="hidden">DESAYUNO</label></th>
                        <th>Costo</th>
                        <th>N° <label class="hidden">ALMUERZO</label></th>
                        <th>Costo</th>
                        <th>N° <label class="hidden">ONCE</label></th>
                        <th>Costo</th>
                        <th>N° <label class="hidden">CENA</label></th>
                        <th>Costo</th>
                        <th>N° <label class="hidden">CENA_NOCTURNA</label></th>
                        <th>Costo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $key): ?>
                        <tr>
                            <td><?php echo $key->ID_REPORTE?></td>
                            <td><?php echo $key->NOMBRE?></td>
                            <td><?php echo $key->FECHA?></td>
                            <td><?php echo $key->NUM_TICKET_DESAYUNO?></td>
                            <td><?php echo $key->COSTO_DESAYUNO?></td>
                            <td><?php echo $key->NUM_TICKET_ALMUERZO?></td>
                            <td><?php echo $key->COSTO_ALMUERZO?></td>
                            <td><?php echo $key->NUM_TICKET_ONCE?></td>
                            <td><?php echo $key->COSTO_ONCE?></td>
                            <td><?php echo $key->NUM_TICKET_CENA?></td>
                            <td><?php echo $key->COSTO_CENA?></td>
                            <td><?php echo $key->NUM_TICKET_CENA_NOCTURNA?></td>
                            <td><?php echo $key->COSTO_CENA_NOCTURNA?></td>
                            <td><?php echo $key->ID_CASINO?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="text-right"></th>
                        <th></th>
                        <th class="text-right"></th>
                        <th></th>
                        <th class="text-right"></th>
                        <th></th>
                        <th class="text-right"></th>
                        <th></th>
                        <th class="text-right"></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script>
  function get_costo(casino , tipo , callback){
   $.post("<?php echo site_url('reporte/get_costo')?>", { tipo: tipo, casino : casino },
     function(data){ callback(data);  },
     "json");
  }

  var table_old;
  $(function () {
      table_old = $('#table_old').DataTable({
          "order": [2,'desc'],
          "dom": "Bfrtip",
          "columnDefs": [
          { targets: 'no-sort', orderable: false },
          {
              targets: [ 0 ],
              visible: false,
              searchable: false,
              name : "ID_CASINO",         
          },
          {
              targets: [ 1 ],
              name : "NOMBRE_CASINO"
          },
          {
              targets: [ 3 ],
              name : "FECHA"
          },
          {
              targets: [ 4 ],
              name : "N_DESAYUNO"
          },
          {
              targets: [ 5 ],
              name : "D_TOTAL"
          },{
              targets: [ 6 ],
              name : "N_ALMUERZO"
          },{
              targets: [ 7 ],
              name : "A_TOTAL"
          },{
              targets: [ 8 ],
              name : "N_CENA"
          },{
              targets: [ 9 ],
              name : "C_TOTAL"
          },{
              targets: [ 10 ],
              name : "N_ONCE"
          },{
              targets: [ 11 ],
              name : "O_TOTAL"
          },{
              targets: [ 12 ],
              name : "N_CENA_NOCTURNA"
          },{
              targets: [ 4 ],
              name : "CN_TOTAL"
          },
          {
              targets: [ -1 ],
              visible: false,
              searchable: false,
              name : "CASINO",         
          }
          ],
          columns: [
            { name: 'col-1' },
            { name: 'col-2' },
            { name: 'col-3' },
            { name: 'DESAYUNO' },
            { name: 'col-5' },
            { name: 'ALMUERZO' },
            { name: 'col-7' },
            { name: 'ONCE' },
            { name: 'col-9' },
            { name: 'CENA' },
            { name: 'col-11' },
            { name: 'CENA_NOCTURNA' },
            { name: 'col-13' },
            { name: 'col-14' }
          ],
          buttons: [
          {
              extend: 'excelHtml5',
              text: 'Generar Excel',
              className : 'btn btn-default',
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          },
          {
              extend: 'pdfHtml5',
              text: 'Generar PDF',
              className : 'btn btn-default', 
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          }
          ],
          "paging": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "createdRow": function ( row, data, index ) {
              $('td', row).eq(0).addClass('text-center');
              $('td', row).eq(1).addClass('text-center');
              $('td', row).eq(2).addClass('text-center');
              $('td', row).eq(3).addClass('text-right');
              $('td', row).eq(4).addClass('text-center');
              $('td', row).eq(5).addClass('text-right');
              $('td', row).eq(6).addClass('text-center');
              $('td', row).eq(7).addClass('text-right');
              $('td', row).eq(8).addClass('text-center');
              $('td', row).eq(9).addClass('text-right');
              $('td', row).eq(10).addClass('text-center');
              $('td', row).eq(11).addClass('text-right');
          },
          "fnDrawCallback": function () {
            var api = this.api();
            $('td:eq(2), td:eq(4), td:eq(6), td:eq(8), td:eq(10)', api.rows().nodes()).editable(function(value, settings) {
                var data = table_old.row( $(this).parents('tr') ).data();
                var columns = $('#table_old').DataTable().settings().init().columns;
                var colIndex = $('#table_old').DataTable().cell($(this).closest('td')).index().column;
                var idx = table_old.row( $(this).parents('tr') ).index();
                var costo = 0;
                var formatNumber = {
                        separador: ".", // separador para los miles
                        sepDecimal: ',', // separador para los decimales
                        formatear:function (num){
                         num +='';
                         var splitStr = num.split('.');
                         var splitLeft = splitStr[0];
                         var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                         var regx = /(\d+)(\d{3})/;
                         while (regx.test(splitLeft)) {
                             splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                         }
                         return this.simbol + splitLeft +splitRight;
                     },
                     number:function(num, simbol){
                         this.simbol = simbol ||'';
                         if ($.isNumeric(num)) {
                            get_costo(data[13], columns[colIndex].name, function(result){
                              costo = result * num;
                              if (costo <= 0) {
                                table_old.cell( idx, colIndex ).data( costo ).draw();
                                $.notify({
                                  icon: 'pe-7s-look',
                                  message: "<b>Casino no tiene asociado este tipo de servicio</b>."
                                },{
                                  type: 'info',
                                  timer: 4000,
                                  z_index: 2000
                                });
                              }
                              table_old.cell( idx, colIndex + 1 ).data( costo ).draw();
                            });
                            return this.formatear(num.toString().replace('.','').replace('$',''));
                        }else{
                            return this.formatear(0);
                        }
                    }
                }
                numeric = value.toString().replace('.','').replace('$','');
                if ($.isNumeric(numeric)) {
                    return(formatNumber.number(numeric.toString().replace('.','').replace('$','')));
                }else{
                    return(formatNumber.number(0));
                }

            }, {
                tooltip   : 'Click Para Editar',
                placeholder : 'Click',
                height : "20px",
                width : "80px",
                // type: "number",
                callback : function(value, settings) {
                    table_old.rows().invalidate().draw(false);
                }
            });
        },
        "fnFooterCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;
          var intVal = function ( i ) {
              return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
              i : 0;
          };
          var formatNumber = {
                    separador: ".", // separador para los miles
                    sepDecimal: ',', // separador para los decimales
                    formatear:function (num){
                     num +='';
                     var splitStr = num.split('.');
                     var splitLeft = splitStr[0];
                     var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                     var regx = /(\d+)(\d{3})/;
                     while (regx.test(splitLeft)) {
                         splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                     }
                     return this.simbol + splitLeft +splitRight;
                 },
                 number:function(num, simbol){
                     this.simbol = simbol ||'';
                     numeric = num.toString().replace('.','').replace('$','');
                     if ($.isNumeric(numeric)) {
                        return this.formatear(numeric.toString().replace('.','').replace('$',''));
                    }else{
                        return this.formatear(0);
                    }
                }
            }
            totalCostoDesayuno = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(4).footer() ).html(
              'T : '+ formatNumber.number(totalCostoDesayuno,'$')
              );

            totalCostoAlmuerzo = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(6).footer() ).html(
              'T : '+ formatNumber.number(totalCostoAlmuerzo,'$')
              );
            totalCostoOnce = api
            .column( 8, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(8).footer() ).html(
              'T : '+ formatNumber.number(totalCostoOnce,'$')
              );
            totalCostoCena = api
            .column( 10, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(10).footer() ).html(
              'T : '+ formatNumber.number(totalCostoCena,'$')
              );
            totalCostoCenaNoc = api
            .column( 12, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );
            $( api.column(12).footer() ).html(
              'T : '+ formatNumber.number(totalCostoCenaNoc,'$')
              );


        },
        "rowCallback": function( row, data, index ) {
            var total_a = '$' + data[4].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_b = '$' + data[6].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_c = '$' + data[8].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_d = '$' + data[10].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_e = '$' + data[12].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            $('td:eq(3)', row).html( total_a );
            $('td:eq(5)', row).html( total_b );
            $('td:eq(7)', row).html( total_c );
            $('td:eq(9)', row).html(  total_d );
            $('td:eq(11)', row).html( total_e );
        },
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          // "decimal":         ",",
          // "sInfoThousands":  ".",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          }, 
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
  });

function formatNumber(n) {
  return n.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

});
jQuery("#footer").ready(function(){
  jQuery("#table_old_length").addClass('hidden');
  jQuery("#table_old_filter").addClass('hidden');
  jQuery("#table_old_info").addClass('hidden');
  jQuery("#footer-left").text(jQuery("#table_old_info").text());
  jQuery("#table_old_paginate").appendTo(jQuery("#footer-right"));
});
</script>
<script>
    $('#btn-update').on('click', function(event) {
        var btn = $(this);
        btn.prop('disabled', 'disabled');
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();
        ar = [];
        for (var i = 0; i < table_old.data().length; i++) {
            ar[i] = table_old.data()[i];
        }
        json = JSON.stringify(ar);
        Pace.track(function(){
            pf_blockUI();
            $.post('<?php echo site_url('report/update_rows') ?>', {
                rows : json
            }).done(function (data) {
                var obj = jQuery.parseJSON(data);
                if (obj.success) {
                    $.notify({
                        icon: 'fa fa-check-circle',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: obj.message
                    }, {
                            type: 'success',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled', false);
                }else{
                    $.notify({
                        icon: 'fa fa-close',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: obj.message
                    }, {
                            type: 'danger',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled', false);
                }
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
        
    });
</script>