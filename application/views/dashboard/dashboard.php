<div class="content-wrapper">
	<section class="content-header">
		<!-- Link de localización -->
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i>dashboard</a></li>
		</ol>
		<!-- Salto de linea	 -->
	</section>
	<br>
	<!-- Contenido de la Page -->
	<?php if ($this->session->profile_id == 2 || $this->session->profile_id == 1): ?>
	<section class="content">
		<div class="row">
			<!-- Columna de 6 hasta 12 -->
			<div class="col-xs-12">
			<!-- Caja del contenido -->
		        <!-- /.col -->
		        <div class="col-md-12">
		          <div class="box box-primary box-solid">
		            <!-- /.box-header -->
		            <div class="box-body">
		              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		                <ol class="carousel-indicators">
		                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
		                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
		                </ol>
		                <div class="carousel-inner">
		                  <div class="item active">
		                    <!-- <img src="http://placehold.it/1200x499/39CCCC/ffffff&text=PF+Alimento+S.A" alt="First slide"> -->
							<img src="<?php echo base_url();?>assets/dist/img/2_1.jpg">
		                    <div class="carousel-caption">
		                      #1
		                    </div>
		                  </div>
		                  <div class="item">
		                    <img src="<?php echo base_url();?>assets/dist/img/11.jpg">
		                    <div class="carousel-caption" class="img-responsive">
		                      #2
		                    </div>
		                  </div>
		                  <div class="item">
		                    <img src="<?php echo base_url();?>assets/dist/img/congeladas.jpg">
		                    <div class="carousel-caption" class="img-responsive">
		                      #3
		                    </div>
		                  </div>
		                </div>
		                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		                  <span class="fa fa-angle-left"></span>
		                </a>
		                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		                  <span class="fa fa-angle-right"></span>
		                </a>
		              </div>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
		        </div>
			</div>
		</div>
	</section>
	<?php else: ?>
	<section class="content">
		<div class="row">
			<!-- Columna de 6 hasta 12 -->
			<div class="col-md-12">
			<!-- Caja del contenido -->
				<div class="box box-primary box-solid">
				<!-- Cabecera de la caja Titulo -->
					<div class="box-header with-border">
					<?php if ($this->session->profile_id >= 3): ?>
						<h3><span class="fa fa-television"></span> Sistema de Casino / <?php echo ucwords(mb_strtolower($this->session->perfil_name))?></h3>
					<?php else: ?>
						<h3><span class="fa fa-television"></span> Sistema de Casino / <?php echo ucwords(mb_strtolower($this->session->perfil_name))?><small><?php echo ucwords(mb_strtolower($this->session->casino_name)) ?></small></h3>
					<?php endif ?>
      	            <div class="box-tools pull-right">
                    	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    	</button>
                	</div>
					</div>
					<!-- Acá se agrega el contenido de la pagina -->
		            <!-- /.box-header -->
		            <div class="box-body">
		            <?php if ($this->session->profile_id >= 3): ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label>Planta</label>
								  <select class="form-control select2" id="plant" name="plant">
								  <option select value="default">Seleccione Planta</option>
	                              <?php foreach($plant as $plants) {?>
		                                <option value="<?php echo $plants->ID_PLANTA; ?>" 
		                                <?php echo set_select('plants',  $plants->NOMBRE); ?>>
		                                <?php echo ucwords(mb_strtolower($plants->NOMBRE)); ?>
		                                </option>
	                            	<?php } ?>
								  </select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label>Casino</label>
								  <select class="form-control select2" id="casino" name="casino">
								  	<option select value="default">Seleccione Planta</option>
								  </select>
								</div>
							</div>
						</div>
		            <?php endif ?>
						<div class="row">
							<div class="col-md-12">
								<p class="text-center">
									<strong>Ticket Realizado</strong>
								</p>
								<div class="chart">
									<canvas id="oilChart"></canvas>
									<!-- <canvas id="myChart" style="height: 120px;"></canvas> -->
								</div>
								<div class="box-footer">
									<a href="<?php echo site_url(); ?>/informe_sistema_casino"  class="btn bg-navy btn-flat pull-right"><i class="fa fa-book"></i> <span>Ver informes del Sistema Casino</span></a>
								</div>
								<!-- /.chart-responsive -->
							</div>
						</div>
		            </div>
				</div>
			</div>
			<!-- END Caja -->
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-4">
						<div class="info-box bg-aqua">
							<span class="info-box-icon"><i class="icon ion-checkmark-circled"></i></span>

							<div class="info-box-content">
								<span class="info-box-text">Solicitudes Aprovadas</span>
								<span class="info-box-number" id="sp_aproved">Sin datos</span>
					            <div class="progress">
				                  	<div class="progress-bar" style="width: 100%"></div>
				                </div>
								<span class="progress-description">
									Total de solicitudes al Mes
								</span>
							</div>
							<!-- /.info-box-content -->
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-box bg-red">
							<span class="info-box-icon"><i class="icon ion-close-circled"></i></span>
							<div class="info-box-content">
								<span class="info-box-text">Solicitudes Rechazadas</span>
								<span class="info-box-number" id="sp_rejection">Sin datos</span>
					            <div class="progress">
				                  	<div class="progress-bar" style="width: 100%"></div>
				                </div>
								<span class="progress-description">
									Total de solicitudes al Mes
								</span>
							</div>
							<!-- /.info-box-content -->
						</div>
					</div>
					<div class="col-md-4">
						<div class="info-box bg-yellow">
							<span class="info-box-icon"><i class="fa fa-history"></i></span>

							<div class="info-box-content">
								<span class="info-box-text">Solicitudes Pendiente</span>
								<span class="info-box-number" id="sp_pending">Sin datos</span>
					            <div class="progress">
				                  	<div class="progress-bar" style="width: 100%"></div>
				                </div>
								<span class="progress-description">
									Total de solicitudes al Mes
								</span>
							</div>
							<!-- /.info-box-content -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- Columna de 6 hasta 12 -->
			<div class="col-md-12">
				<!-- Caja del contenido -->
				<div class="box box-primary box-solid">
					<!-- Cabecera de la caja Titulo -->
					<div class="box-header with-border">
						<h3 class="box-title">Colaciones emitidas en el Día</h3>
					</div>
					<div class="box-body">
						<!-- /.col -->
						<div class="col-md-4" id="collation_day">
							<div class="info-box bg-green">
								<span class="info-box-icon"><i class="fa fa-cutlery"></i></span>
								<div class="info-box-content">
									<span class="info-box-text">Sin colación</span>
						            <div class="progress">
					                  	<div class="progress-bar" style="width: 100%"></div>
					                </div>
								</div>
								<!-- /.info-box-content -->
							</div>
							<!-- END Group -->
						</div>
						<!-- /.col -->
						<div class="col-md-8">
							<div class="chart pull-right well">

								<canvas id="myChart" style="height: 120px;"></canvas>
							
							</div>
							<div class="box-footer">
									<a id="link2" download="Grafico.jpg" class="btn bg-purple btn-flat pull-right"><i class="fa fa-download"></i> <span>Descargar gráfico</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
	    $(document).ready(function() {
			<?php if(!$this->session->flashdata('flash_msg')) { ?>
				<?php echo $this->session->mensaje; ?>
			<?php } ?> 
	        $("#plant").change(function() {
	            var casino = $("#casino");
	            var html = '';
	            $("#plant option:selected").each(function() {
	                inc_type = $('#plant').val();
	                $.post('<?php echo site_url('dashboard/getcasino'); ?>', {
	                    planta : inc_type
	                }, function(data) {
	                	html = + '<option value="default">Seleccione Casino</option>';
	                    $.each(data,function(index, item) {
	                        html = html + '<option value="' + item.value + '">' + item.text + '</option>';
	                    });
	                    casino.find('option')
	                    .remove()
	                    .end()
	                    .append(html);
	                },'json');
	            });
	            setTimeout(function() {
	            	$("#casino").trigger('change');
	            }, 1000);
	        });
	    });
	</script>
	<script>
		var oilCanvas = document.getElementById("oilChart");
		oilCanvas.height = 500;
		var pieChart = new Chart(oilCanvas, {
			  	type: 'pie',
			  	responsive:true,
			   	percentageInnerCutout: 30,
			   	maintainAspectRatio: false,
			   	innerRadius: "30%"
		});

		var ctx = $("#myChart");
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: ["Lunes", "Marte", "Miércoles", "Jueves", "Viernes", "Sabado", "Domingo"],
		        datasets: [{
		            label: "# Colación",
		            data: [],
		            backgroundColor: [
		                'rgba(0, 137, 123, 0.7)',
		                'rgba(56, 142, 60, 0.7)',
		                'rgba(104, 159, 56, 0.7)',
		                'rgba(175, 180, 43, 0.7)',
		                'rgba(253, 216, 53, 0.7)',
		                'rgba(255, 179, 0, 0.7)',
		                'rgba(251, 140, 0, 0.7)'
		            ],
		            borderColor: [
		                'rgba(0, 137, 123, 1.0)',
		                'rgba(56, 142, 60, 1.0)',
		                'rgba(104, 159, 56, 1.0)',
		                'rgba(175, 180, 43, 1.0)',
		                'rgba(253, 216, 53, 1.0)',
		                'rgba(255, 179, 0, 1.0)',
		                'rgba(251, 140, 0, 1.0)'
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        },
	            animation: {
	                onComplete: function(animation) {
	                    var dataUrl = document.getElementById("myChart").toDataURL();
	                    $("#link2").attr('href', dataUrl);
	                }
	            }
		    }
		});

		$("#casino").change(function() {
			pieChart.destroy(); 
			show_results($("#casino").val());
		});

	    function show_results(id){
	    	var html = '';
	    	var date = getActualDate();
	    	// console.log(id);
	    	var char_data = [];
	    	var label_char = [];
	    	var div_collation = $("#collation_day");

	    	var label_donut = [], data_donut = [], color_donut = [] ;

	    	div_collation.empty();
	    	$.post('<?php echo site_url('dashboard/show'); ?>', 
	    		{
	    			'id' : id
	    		}, 
	    		function(data) {
	    		$.each(data.graphic,function(index, grap) {
	    			char_data.push(grap.day_week);
	    		});
	    		$.each(data.graphic,function(index, grap) {
	    			label_char.push(grap.day);
	    		});

	    		$.each(data.quantity,function(index, box_h) {
	    			html = html + '<div class="info-box '+box_h.class+'">';
	    			html = html + '<span class="info-box-icon"><i class="fa fa-cutlery"></i></span>';
	    			html = html + '<div class="info-box-content">';
	    			html = html + '<span class="info-box-text">'+box_h.Colacion+'</span>';
	    			html = html + '<span class="info-box-number"> Numero de Colación : '+box_h.day+'</span>';
	    			html = html + '<div class="progress">';
	    			html = html + '<div class="progress-bar" style="width: 100%" id="normal"></div>';
	    			html = html + '</div>';
	    			html = html + '<span class="progress-description">'+date+'</span>';
	    			html = html + '</div>';
	    			html = html + '</div>'; 
	    		});

	    		$.each(data.request_a,function(index, box_a) {
	    			$("#sp_aproved").text(box_a.quantity_a);
	    		});
	    		$.each(data.request_d,function(index, box_d) {
	    			$("#sp_rejection").text(box_d.quantity_d);
	    		});

	    		$.each(data.request_c,function(index, box_d) {
	    			$("#sp_pending").text(box_d.quantity_c);
	    		});

	    		$.each(data.donut,function(index, el) {
	    			label_donut.push(el.colacion);
	    			data_donut.push(el.total);
	    			color_donut.push(el.color);
	    		});

				var oilCanvas = document.getElementById("oilChart");
				oilCanvas.height = 500;
				// Chart.defaults.global.defaultFontFamily = "Lato";
				// Chart.defaults.global.defaultFontSize = 18;

				var oilData = {
				    labels: label_donut,
				    datasets: [
				        {
				            data: data_donut,
				            backgroundColor: color_donut
				        }],
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				                    beginAtZero:true
				                }
				            }]
				        },
	                    animation: {
	                        onComplete: function(animation) {
	                            var dataUrl = document.getElementById("oilChart").toDataURL();
	                            $("#link1").attr('href', dataUrl);
	                        }
	                    }
				    }
				};

				// console.log('ok');	
				pieChart = new Chart(oilCanvas, {
				  	type: 'pie',
				  	responsive:true,
				  	data: oilData,
				  	maintainAspectRatio: false,
				   	percentageInnerCutout: 30,
				   	innerRadius: "30%"
				});

	    		$("#collation_day").append(html);

	    		myChart.destroy();

				myChart = new Chart(ctx, {
				    type: 'bar',
				    data: {
				        labels: label_char,
				        datasets: [{
				            label: "# Colación",
				            data: char_data,
				            backgroundColor: [
				                'rgba(0, 137, 123, 0.7)',
				                'rgba(56, 142, 60, 0.7)',
				                'rgba(104, 159, 56, 0.7)',
				                'rgba(175, 180, 43, 0.7)',
				                'rgba(253, 216, 53, 0.7)',
				                'rgba(255, 179, 0, 0.7)',
				                'rgba(251, 140, 0, 0.7)'
				            ],
				            borderColor: [
				                'rgba(0, 137, 123, 1.0)',
				                'rgba(56, 142, 60, 1.0)',
				                'rgba(104, 159, 56, 1.0)',
				                'rgba(175, 180, 43, 1.0)',
				                'rgba(253, 216, 53, 1.0)',
				                'rgba(255, 179, 0, 1.0)',
				                'rgba(251, 140, 0, 1.0)'
				            ],
				            borderWidth: 1
				        }]
				    },
				    options: {
				        scales: {
				            yAxes: [{
				                ticks: {
				                    beginAtZero:true
				                }
				            }]
				        },
	                    animation: {
	                        onComplete: function(animation) {
	                            var dataUrl = document.getElementById("myChart").toDataURL();
	                            $("#link2").attr('href', dataUrl);
	                        }
	                    }
				    }
				});
				if (data.graphic.day_week == 0) {
					myChart.destroy();
				}
	    	},'json');
	    }
	</script>
	<script>
		function addZero(i) {
		    if (i < 10) {
		        i = "0" + i;
		    }
		    return i;
		}

		function getActualFullDate() {
		    var d = new Date();
		    var day = addZero(d.getDate());
		    var month = addZero(d.getMonth()+1);
		    var year = addZero(d.getFullYear());
		    var h = addZero(d.getHours());
		    var m = addZero(d.getMinutes());
		    var s = addZero(d.getSeconds());
		    return day + ". " + month + ". " + year + " (" + h + ":" + m + ")";
		}
		function getActualHour() {
		    var d = new Date();
		    var h = addZero(d.getHours());
		    var m = addZero(d.getMinutes());
		    var s = addZero(d.getSeconds());
		    return h + ":" + m + ":" + s;
		}

		function getActualDate() {
		    var d = new Date();
		    var day = addZero(d.getDate());
		    var month = addZero(d.getMonth()+1);
		    var year = addZero(d.getFullYear());
		    return day + ". " + month + ". " + year;
		}
	</script>
	<script>
	    $().ready(function(){
			show_results(<?php echo $this->session->casino?>);
	    });
	</script>

	<?php endif ?>
</div>
