<div class="content-wrapper">
    <section class="content-header">
        <h1>Reporte Editable de Casino <small><b>Todos</b></small></h1>
        <!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo site_url(); ?>/report"> Reporte</a></li>
            <li class="active">Reporte Casino Guardado</li>
        </ol>
    </section>
    <!-- Contenido de la Page -->
    <section class="content">
      <div class="row">
         <!-- Columna de 6 hasta 12 -->
         <div class="col-xs-12">
            <div class="box box-primary box-solid">
             <div class="box-header with-border">
               <h3 class="box-title">Registro de los reporte Guardado</h3>
           </div>
           <div class="box-body">
              <div class="row">
                 <div class="col-md-4">
                    <div class="form-group margin-btm-5">
                       <label>Mostrar</label>
                       <select id="show_record" class="form-control">
                           <option value="10">10 registros</option>
                           <option value="25">25 registros</option>
                           <option value="50">50 registros</option>
                           <option value="100">100 registros</option>
                           <option value="-1">Todos los registros</option>
                       </select>
                   </div>
               </div>
               <div class="col-md-4">
                <div class="form-group margin-btm-5">
                    <label>Filtro por fecha</label>
                    <div class="btn-group btn-group-justified">
                        <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                            <span>
                                <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                            </span>
                            <input type="hidden" name="id_fecha" value="id_fecha">
                            <div class="pull-right">
                                <i class="fa fa-caret-down"></i>
                            </div>
                        </a>
                        <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn btn-success btn-block" id="btn_aplicar" style="margin-top: 24px">Aplicar Filtro de Búsqueda</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row table-responsive no-left-right-margin">
                    <div class="col-xs-12"> 
                        <table id="table" class="table table-bordered table-hover" style="width: 100%">
                            <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Colaborador</th>
                                  <th>Casino</th>
                                  <th>Fecha Creación</th>
                                  <th>Fecha Modificación</th>
                                  <th>Funciones</th>
                              </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          <tfoot>
                            <tr>
                              <th>#</th>
                              <th>Colaborador</th>
                              <th>Casino</th>
                              <th>Fecha Creación</th>
                              <th>Fecha Modificación</th>
                              <th>Funciones</th>
                          </tr>
                      </tfoot>
                  </table>
              </div>
          </div>
      </div>
  </div>
  <div class="padding-top-5"></div>
  <div class="box-footer">
    <a href="<?php echo site_url() ?>/report" class="btn btn-flat btn-default"><strong>Volver</strong></a>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- END contenido -->
</div>

<!-- Bootstrap modal -->
<div class="modal fade in" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal_content">

        </div>
    </div>
</div>
<!-- END modal --> 

<script>
    var table;
    $(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [3, 'desc'],
            "ajax": {
                "url": "<?php echo site_url('report/get_reporte'); ?>",
                "type": "POST",
                'data': function ( d ) {
                    d.fecha = $('#daterange-btn span').text();
                }
            },
            "columnDefs": [
            {
                "targets": [ -1 ],
                "orderable": false,
            },
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            { 
                "targets": -1,
                "data": null,
                "defaultContent": '<button type="button" class="btn btn-info btn-show"><i class="fa fa-eye"></i></button>' 
            },
            ],
            "paging": true,
            "info": true,
            "responsive": true,
            "createdRow": function ( row, data, index ) {
                $('td', row).eq(0).addClass('text-center');
                $('td', row).eq(1).addClass('text-center');
                $('td', row).eq(2).addClass('text-center');
                $('td', row).eq(3).addClass('text-center');
                $('td', row).eq(4).addClass('text-center');

            },
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }, 
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });

    function reload_table()
    {
        table.ajax.reload();
    }

    jQuery("#footer").ready(function(){
        jQuery("#table_length").addClass('hidden');
        jQuery("#table_filter").addClass('hidden');
        jQuery("#table_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_info").text());
        jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
    });

    $('#search_input').keyup(function(){
        table.search($(this).val()).draw() ;
    })

    $('#show_record').click(function() {
        table.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_inc_info").text());
    });

    jQuery("#table").on("page.dt", function(){
        var info = table.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });

    $('#btn_aplicar').click(function () {
        reload_table();
    });
</script>
<script>
    $(document).ready(function(){
        $('#daterange-btn').daterangepicker(
        {
            opens: 'left',
            showDropdowns: true,
            buttonClasses: 'btn btn-flat btn-sm',
            applyClass: 'btn-primary',
            locale: {
                format:             "DD/MM/YYYY",
                separator:          " --- ",
                applyLabel:         "Aplicar",
                cancelLabel:        "Cancelar",
                fromLabel:          "Desde",
                toLabel:            "A",
                customRangeLabel:   "Definir rango",
                daysOfWeek: [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
                ],
                monthNames: [
                'Enero', 
                'Febrero', 
                'Marzo', 
                'Abril', 
                'Mayo', 
                'Junio', 
                'Julio', 
                'Agosto', 
                'Septiembre', 
                'Octubre', 
                'Noviembre', 
                'Diciembre'
                ],
                firstDay: 1
            },
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Este Año': [moment().startOf('year'), moment().endOf('year')],
                'Año pasado': [moment().subtract(1, 'year').startOf('year'), moment(). subtract(1, 'year').endOf('year')]
            },

        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            $('#daterange-btn input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            format_date(start.format('DD-MM-YYYY'), end.format('DD-MM-YYYY'));
        });
    });
</script>
<script>
    $('#clear_filter').click(function(){
        $('#daterange-btn span').html('<span><i class="fa fa-calendar"></i> Filtrar por rango de fechas</span>');
        $('#daterange-btn input').val('Filtrar por rango de fechas');
        format_date(moment().subtract(500, 'year').startOf('year').format('DD-MM-YYYY'), moment().format('DD-MM-YYYY'));
        filter();
    });
</script>
<script>
    $.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
            var x;
            try {
                var dateA = a.replace(/ /g, '').split("-");
                var day = parseInt(dateA[0], 10);
                var month = parseInt(dateA[1], 10);
                var year = parseInt(dateA[2], 10);
                var date = new Date(year, month - 1, day)
                x = date.getTime();
            }
            catch (err) {
                x = new Date().getTime();
            }

            return x;
        },

        "date-uk-asc": function (a, b) {
            return a - b;
        },

        "date-uk-desc": function (a, b) {
            return b - a;
        }
    });

    var sDate = '';
    var eDate = '';

    function format_date(startDate, endDate) {

        sDate = startDate;
        eDate = endDate;
    }
</script>
<script>
    $('#table tbody').on( 'click', '.btn-show', function (event) {
        event.preventDefault();
        var data = table.row($(this).parents('tr')).data();
        var btn = $('.btn-show');
        btn.prop('disabled','disabled');
        Pace.track(function(){
            pf_blockUI();
            $.post('<?php echo site_url('report/get_modal') ;?>', {
                param : data
            }, function(data) {
                $('.modal_content').empty().append(data.view);
                $('#modal-info').modal('show');
                btn.prop('disabled',false);
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
    });
</script>