<?php if (!empty($datos)): ?>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="box-header with-border">
            <div class="box-body">
                <div class="row table-responsive no-left-right-margin">
<!--                     <div class="col-md-4 pull-right">
                        <button type="button" class="btn btn-info btn-block btn-enviar">Enviar Al Correo</button>
                    </div> -->
                    <table id="table" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Casino</p></td>
                                <td colspan="1" bgcolor="#CDDC39" style="text-align: center;"><p>Fecha Entrega</p></td>
                                <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Desayuno</p></td>
                                <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Almuerzo</p></td>
                                <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Onces</p></td>
                                <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Cena</p></td>
                                <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Cena Noc</p></td>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>N° <label class="hidden">DESAYUNO</label></th>
                                <th>Costo</th>
                                <th>N° <label class="hidden">ALMUERZO</label></th>
                                <th>Costo</th>
                                <th>N° <label class="hidden">CENA</label></th>
                                <th>Costo</th>
                                <th>N° <label class="hidden">ONCE</label></th>
                                <th>Costo</th>
                                <th>N° <label class="hidden">CENA_NOCTURNA</label></th>
                                <th>Costo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($datos as $key): ?>
                                <tr>
                                    <td><?php echo $key->id_casino?></td>
                                    <td><?php echo $key->casino?></td>
                                    <td><?php echo $key->fecha_entrega?></td>
                                    <td><?php echo $key->ticket_desayuno?></td>
                                    <td><?php echo $key->costo_desayuno?></td>
                                    <td><?php echo $key->ticket_almuerzo?></td>
                                    <td><?php echo $key->costo_almuerzo?></td>
                                    <td><?php echo $key->ticket_once?></td>
                                    <td><?php echo $key->costo_once?></td>
                                    <td><?php echo $key->ticket_cena?></td>
                                    <td><?php echo $key->costo_cena?></td>
                                    <td><?php echo $key->ticket_cena_noctura?></td>
                                    <td><?php echo $key->costo_cena_noctura?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="text-right"></th>
                                <th></th>
                                <th class="text-right"></th>
                                <th></th>
                                <th class="text-right"></th>
                                <th></th>
                                <th class="text-right"></th>
                                <th></th>
                                <th class="text-right"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pull-right">
                    <button type="button" class="btn btn-primary btn-block btn-save"> Guardar Cambios </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  function get_costo(casino , tipo , callback){
   $.post("<?php echo site_url('reporte/get_costo')?>", { tipo: tipo, casino : casino },
     function(data){ callback(data);  },
     "json");
  }

  $(function () {
      table = $('#table').DataTable({
          // "order": [2,'desc'],
          "dom": "Bfrtip",
          "columnDefs": [
          { targets: 'no-sort', orderable: false },
          {
              targets: [ 0 ],
              visible: false,
              searchable: false,
              name : "ID_CASINO",         
          },
          {
              targets: [ 1 ],
              name : "NOMBRE_CASINO"
          },
          {
              targets: [ 3 ],
              name : "FECHA"
          },
          {
              targets: [ 4 ],
              name : "N_DESAYUNO"
          },
          {
              targets: [ 5 ],
              name : "D_TOTAL"
          },{
              targets: [ 6 ],
              name : "N_ALMUERZO"
          },{
              targets: [ 7 ],
              name : "A_TOTAL"
          },{
              targets: [ 8 ],
              name : "N_CENA"
          },{
              targets: [ 9 ],
              name : "C_TOTAL"
          },{
              targets: [ 10 ],
              name : "N_ONCE"
          },{
              targets: [ 11 ],
              name : "O_TOTAL"
          },{
              targets: [ 12 ],
              name : "N_CENA_NOCTURNA"
          },{
              targets: [ 4 ],
              name : "CN_TOTAL"
          }
          ],
          columns: [
            { name: 'col-1' },
            { name: 'col-2' },
            { name: 'col-3' },
            { name: 'DESAYUNO' },
            { name: 'col-5' },
            { name: 'ALMUERZO' },
            { name: 'col-7' },
            { name: 'ONCE' },
            { name: 'col-9' },
            { name: 'CENA' },
            { name: 'col-11' },
            { name: 'CENA_NOCTURNA' },
            { name: 'col-13' },
          ],
          buttons: [
          {
              extend: 'excelHtml5',
              text: 'Generar Excel',
              className : 'btn btn-default',
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          },
          {
              extend: 'pdfHtml5',
              text: 'Generar PDF',
              className : 'btn btn-default', 
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          }
          ],
          "paging": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true,
          "createdRow": function ( row, data, index ) {
              $('td', row).eq(0).addClass('text-center');
              $('td', row).eq(1).addClass('text-center');
              $('td', row).eq(2).addClass('text-center');
              $('td', row).eq(3).addClass('text-right');
              $('td', row).eq(4).addClass('text-center');
              $('td', row).eq(5).addClass('text-right');
              $('td', row).eq(6).addClass('text-center');
              $('td', row).eq(7).addClass('text-right');
              $('td', row).eq(8).addClass('text-center');
              $('td', row).eq(9).addClass('text-right');
              $('td', row).eq(10).addClass('text-center');
              $('td', row).eq(11).addClass('text-right');
          },
          "fnDrawCallback": function () {
            var api = this.api();
            $('td:eq(2), td:eq(4), td:eq(6), td:eq(8), td:eq(10)', api.rows().nodes()).editable(function(value, settings) {
                var data = table.row( $(this).parents('tr') ).data();
                var columns = $('#table').DataTable().settings().init().columns;
                var colIndex = $('#table').DataTable().cell($(this).closest('td')).index().column;
                var idx = table.row( $(this).parents('tr') ).index();
                var costo = 0;
                var formatNumber = {
                        separador: ".", // separador para los miles
                        sepDecimal: ',', // separador para los decimales
                        formatear:function (num){
                         num +='';
                         var splitStr = num.split('.');
                         var splitLeft = splitStr[0];
                         var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                         var regx = /(\d+)(\d{3})/;
                         while (regx.test(splitLeft)) {
                             splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                         }
                         return this.simbol + splitLeft +splitRight;
                     },
                     number:function(num, simbol){
                         this.simbol = simbol ||'';
                         if ($.isNumeric(num)) {
                            get_costo(data[0], columns[colIndex].name, function(result){
                              costo = result * num;
                              if (costo <= 0) {
                                table.cell( idx, colIndex ).data( costo ).draw();
                                $.notify({
                                 icon: 'pe-7s-look',
                                 message: "<b>Casino no tiene asociado este tipo de servicio</b>."
                                },{
                                 type: 'info',
                                 timer: 4000
                                });
                              }
                              table.cell( idx, colIndex + 1 ).data( costo ).draw();
                            });
                            return this.formatear(num.toString().replace('.','').replace('$',''));
                        }else{
                            return this.formatear(0);
                        }
                    }
                }
                numeric = value.toString().replace('.','').replace('$','');
                if ($.isNumeric(numeric)) {
                    return(formatNumber.number(numeric.toString().replace('.','').replace('$','')));
                }else{
                    return(formatNumber.number(0));
                }

            }, {
                tooltip   : 'Click Para Editar',
                placeholder : 'Click',
                height : "20px",
                width : "100%",
                // type: "number",
                callback : function(value, settings) {
                    table.rows().invalidate().draw(false);
                }
            });
        },
        "fnFooterCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;
          var intVal = function ( i ) {
              return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
              i : 0;
          };
          var formatNumber = {
                    separador: ".", // separador para los miles
                    sepDecimal: ',', // separador para los decimales
                    formatear:function (num){
                     num +='';
                     var splitStr = num.split('.');
                     var splitLeft = splitStr[0];
                     var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                     var regx = /(\d+)(\d{3})/;
                     while (regx.test(splitLeft)) {
                         splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                     }
                     return this.simbol + splitLeft +splitRight;
                 },
                 number:function(num, simbol){
                     this.simbol = simbol ||'';
                     numeric = num.toString().replace('.','').replace('$','');
                     if ($.isNumeric(numeric)) {
                        return this.formatear(numeric.toString().replace('.','').replace('$',''));
                    }else{
                        return this.formatear(0);
                    }
                }
            }
            totalCostoDesayuno = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(4).footer() ).html(
              'T : '+ formatNumber.number(totalCostoDesayuno,'$')
              );

            totalCostoAlmuerzo = api
            .column( 6, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(6).footer() ).html(
              'T : '+ formatNumber.number(totalCostoAlmuerzo,'$')
              );
            totalCostoOnce = api
            .column( 8, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(8).footer() ).html(
              'T : '+ formatNumber.number(totalCostoOnce,'$')
              );
            totalCostoCena = api
            .column( 10, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );

            $( api.column(10).footer() ).html(
              'T : '+ formatNumber.number(totalCostoCena,'$')
              );
            totalCostoCenaNoc = api
            .column( 12, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
              return intVal(a) + intVal(b.toString().split('.').join(""));
          }, 0 );
            $( api.column(12).footer() ).html(
              'T : '+ formatNumber.number(totalCostoCenaNoc,'$')
              );
        },
        "rowCallback": function( row, data, index ) {
            var total_a = '$' + data[4].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_b = '$' + data[6].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_c = '$' + data[8].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_d = '$' + data[10].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            var total_e = '$' + data[12].toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, '.').replace('$','');
            $('td:eq(3)', row).html( total_a );
            $('td:eq(5)', row).html( total_b );
            $('td:eq(7)', row).html( total_c );
            $('td:eq(9)', row).html(  total_d );
            $('td:eq(11)', row).html( total_e );
        },
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          // "decimal":         ",",
          // "sInfoThousands":  ".",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          }, 
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
  });

function formatNumber(n) {
  return n.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

});
jQuery("#footer").ready(function(){
  jQuery("#table_length").addClass('hidden');
  jQuery("#table_filter").addClass('hidden');
  jQuery("#table_info").addClass('hidden');
  jQuery("#footer-left").text(jQuery("#table_info").text());
  jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
});

$('#search_input').keyup(function(){
  table.search($(this).val()).draw() ;
})

$('#show_record').click(function() {
  table.page.len($('#show_record').val()).draw();
  jQuery("#footer-left").text(jQuery("#table_inc_info").text());
});

jQuery("#table").on("page.dt", function(){
  var info = table.page.info();
  jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
});
</script>
<script>
    $('.btn-save').on('click', function(event) {
        var btn = $(this);
        btn.prop('disabled', 'disabled');
        event.preventDefault();
        ar = [];
        for (var i = 0; i < table.data().length; i++) {
            ar[i] = table.data()[i];
        }
        json = JSON.stringify(ar);
        Pace.track(function(){
            pf_blockUI();
            $.post('<?php echo site_url('report/save_rows') ?>', {
                rows : json
            }).done(function (data) {
                var obj = jQuery.parseJSON(data);
                if (obj.success) {
                    $.notify({
                        icon: 'fa fa-check-circle',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: obj.message
                    }, {
                            type: 'success',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled', false);
                }else{
                    $.notify({
                        icon: 'fa fa-close',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: obj.message
                    }, {
                            type: 'danger',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled', false);
                }
            },'json')
            .always(function() {
              pf_unblockUI();
            });
        });
        
    });

    $('.btn-enviar').click(function(event) {
        var btn = $(this);
        btn.prop('disabled', 'disabled');
        event.preventDefault();
        ar = [];
        for (var i = 0; i < table.data().length; i++) {
            ar[i] = table.data()[i];
        }
        json = JSON.stringify(ar);
        Pace.track(function(){
            pf_blockUI();
            $.post('<?php echo site_url('report/send_mail') ?>', {
                rows : json
            }).done(function (data) {
                var obj = jQuery.parseJSON(data);
                if (obj.success) {
                    $.notify({
                        icon: 'fa fa-check-circle',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: obj.message
                    }, {
                            type: 'success',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled', false);
                }else{
                    $.notify({
                        icon: 'fa fa-close',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: obj.message
                    }, {
                            type: 'danger',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    btn.prop('disabled', false);
                }
            },'json')
            .always(function() {
              pf_unblockUI();
            });
        });
    });
</script>
<?php else :?>
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="box-header with-border">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible">
                            <h4><i class="icon fa fa-warning"></i> No Encontrado</h4>
                            No existe datos acorde a la búsqueda realizada para realizar su edición.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>
