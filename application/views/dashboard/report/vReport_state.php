<div class="content-wrapper">
  <section class="content-header">
    <h1>Informe de Servicio [Sistema Casino]</h1>
    <!-- Link de localización -->
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">Informe Sistema Casino</li>
    </ol>
    <!-- Salto de linea  -->
  </section>
  <!-- Contenido de la Page -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Detalle</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool pull-right" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <label> Estado del ticket:</label>
                <select class="form-control" name="id_state" id="id_state">
                  <?php foreach($estado as $estados) {?>
                  <option value="<?php echo $estados->ID_ESTADO; ?>" 
                    <?php echo set_select('estados',  $estados->NOMBRE); ?>>
                    <?php echo ucwords(mb_strtolower($estados->NOMBRE)); ?>
                  </option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label>Filtro por Empresa:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                              <i class="fa fa-filter"></i>
                          </div>
                          <select class="selectpicker form-control" name="company" id="company" data-live-search="true" data-style="btn-primary">
                            <option value="">Todos</option>
                            <?php foreach ($empresa as $nombre): ?>
                              <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                              <?php else :?>
                                <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                              <?php endif ?>
                            <?php endforeach ?>
                          </select>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                      <label>Filtro por fecha:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="reservation" readonly>
                      </div>
                  </div>
              </div>
            </div>
            <div class="box-footer">

              <div class="btn-group pull-right">
                <button type="button" class="btn bg-olive btn-flat pull-right btn-flat" id="search_data">Consultar</button>
              </div>
              <div class="note pull-right" style="margin-right: 5px;margin-top: 15px">
                <span class="pull-left margin-right-5">
                  <small class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> 
                </span>
                <strong>Nota:</strong> Debe seleccionar la fecha para mostrar todo los ticket.
              </div>
            </div>
          </div>
        </div>         
      </div>
    </div>
  </section>
  <section class="content" id="section_hidden">

  </section>
</div>
<script>
  (function () {
    $('[data-toggle="tooltip"]').tooltip();
  }());
</script>
<script>
  jQuery(document).ready(function($) {
    $('#search_data').click(function () {
      $('#search_data').prop('disabled', 'disabled');
      $('#section_hidden').empty();
      $('#section_hidden').show();
      Pace.track(function(){
        pf_blockUI();
        $.post('<?php echo site_url('report_state/get_report_collation'); ?>', {
          id_state : $('#id_state').val(),
          date : $('#reservation').val(),
          company : $('#company').val()
        }, function(data) {
          /* optional stuff to do after success */
          if (data.status) {
            $(data.flag).hide().appendTo('#section_hidden').fadeIn(999);
            $('#search_data').prop('disabled', false);
          }else if (data.status == false) {
            $(data.flag).hide().appendTo('#section_hidden').fadeIn(999);
            $('#search_data').prop('disabled', false);
          }
        },'json')
        .always(function() {
          pf_unblockUI();
        });
      });
    });
    $('#search_data').trigger('click');
  });
</script>
<script> 
  //Date range picker
  $('#reservation').daterangepicker({
      "locale": {
          "format": "DD/MM/YYYY",
          "separator": " - ",
          "applyLabel": "Aplicar",
          "cancelLabel": "Cancelar",
          "fromLabel": "De",
          "toLabel": "a",
          "customRangeLabel": "Custom",
          "daysOfWeek": [
          "Do",
          "Lu",
          "Ma",
          "Mi",
          "Ju",
          "Vi",
          "Sa"
          ],
          "monthNames": [
          "Enero",
          "Febrero",
          "Marzo",
          "Abril",
          "Mayo",
          "Junio",
          "Julio",
          "Agosto",
          "Septiembre",
          "Octubre",
          "Noviembre",
          "Diciembre"
          ],
          "firstDay": 1
      }
  });
  $('#daterange-btn').daterangepicker(
  {
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: moment().subtract(29, 'days'),
  endDate: moment()
  },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
  );
</script>
