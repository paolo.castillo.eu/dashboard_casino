<div class="content-wrapper">
	<section class="content-header">
		<h1>Informe Casino <small><b>Todos</b></small></h1>
		<!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Informe Casino Colaborador</li>
        </ol>
		<!-- Salto de linea	 -->
	</section>
	<!-- Contenido de la Page -->
	<section class="content-header">
		<div class="row">
			<!-- Columna de 6 hasta 12 -->
        	<div class="col-xs-12">
          		<div class="box box-primary box-solid">
        			<div class="box-header with-border">
              			<h3 class="box-title">Informe Casino Colaborador <li class="fa fa-check-circle-o"></li></h3>
                        <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        </div>
                        <div class="note pull-right" style="margin-right: 5px;margin-top: 15px">
                            <span class="pull-left margin-right-5">
                              <small class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> 
                            </span>
                            <strong>Nota:</strong> Debe seleccionar los filtros para aplicar y si desea dejar todo el valor elija la opción “ Todos ”.
                        </div>
            		</div>
                    <form method="POST" action="<?php echo site_url()?>/informe_sistema_cliente/get_excel">
                    <?php if ($this->session->profile_id == 4) :?>
            		<div class="box-body">
                        
                        <div class="col-xs-12 well">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Planta</label>
                                        <select class="form-control selectpicker" name="id_planta" id="id_planta" data-style="btn-primary" style="width: 100%">
                                            <option value="">Todos</option>
                                            <?php foreach($planta as $plantas) {?>
                                                <option value="<?php echo $plantas->ID_PLANTA; ?>" 
                                                <?php echo set_select('plantas',  $plantas->NOMBRE); ?>>
                                                <?php echo ucwords(mb_strtolower($plantas->NOMBRE)); ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Seleccione Casino</label>
                                        <select class="form-control selectpicker" name="id_casino" id="id_casino" style="width: 100%" data-style="btn-primary">
                                            <option value="">Todos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Seleccione Colación</label>
                                        <select class="form-control selectpicker" name="id_colacion" id="id_colacion" data-style="btn-primary">
                                            <option value="">Todos</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Estado del ticket</label>
                                        <select class="form-control selectpicker" name="id_estado" id="id_estado" data-style="btn-primary">
                                            <option value="">Todos</option>
                                            <?php foreach ($estado as $key): ?>
                                                <option value="<?php echo $key->ID_ESTADO?>"><?php echo $key->NOMBRE?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group margin-btm-5">
                                        <label>Rut</label>
                                        <input type="text" class="form-control filter" name="id_rut" id="id_rut" placeholder="Ej: 18546919-0" data-column-index='0'>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group margin-btm-5">
                                        <label>Cantidad de Colación</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <li class="fa fa-chevron-circle-right"></li>
                                            </span>
                                            <input type="text" class="form-control text-right input-number" name="id_cantidad" id="id_cantidad">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group margin-btm-5">
                                        <label>Empresa</label>
                                          <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-primary">
                                            <option value="">Todos</option>

                                            <?php foreach ($empresa as $nombre): ?>
                                              <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                                <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php else :?>
                                                <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php endif ?>
                                            <?php endforeach ?>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Opciones</label>
                                        <div id="button-container">
                                                <button href="#" class="btn btn-block btn-success">Exportar Excel</button>
                                        </div>
                                    </div>
                                </div>
                                <hr class="hidden-xl hidden-lg hidden-md margin-btm-10">
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Filtro por fecha</label>
                                        <div class="btn-group btn-group-justified">
                                            <a class="btn btn-primary btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                                <span>
                                                <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                                </span>
                                                <input type="hidden" name="id_fecha" value="id_fecha">
                                                <div class="pull-right">
                                                    <i class="fa fa-caret-down"></i>
                                                </div>
                                            </a>
                                            <a id="clear_filter" class="btn btn-primary btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-block btn-success" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                                </div>
                            </div>
                        </div>
	              	</div>



                  <?php elseif ($this->session->profile_id == 3) :?>
                    <div class="box-body">
                        
                        <div class="col-xs-12 well">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Seleccione Colación</label>
                                        <select class="form-control selectpicker" name="id_colacion" id="id_colacion" data-style="btn-primary">
                                            <option value="">Todos</option>
                                            <?php foreach ($colacion as $key): ?>
                                                <option value="<?php echo $key->ID_COLACION?>"><?php echo $key->NOMBRE?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Empresa</label>
                                          <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-primary">
                                            <option value="">Todos</option>

                                            <?php foreach ($empresa as $nombre): ?>
                                              <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                                <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php else :?>
                                                <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php endif ?>
                                            <?php endforeach ?>
                                          </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Estado del ticket</label>
                                        <select class="form-control selectpicker" name="id_estado" id="id_estado" data-style="btn-primary">
                                            <option value="">Todos</option>
                                            <?php foreach ($estado as $key): ?>
                                                <option id="<?php echo $key->ID_ESTADO?>"><?php echo $key->NOMBRE?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group margin-btm-5">
                                        <label>Rut</label>
                                        <input type="text" class="form-control filter input_rut" id="id_rut" placeholder="Ej: 18546919-0" data-column-index='0'>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group margin-btm-5">
                                        <label>Cantidad de Colación</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <li class="fa fa-chevron-circle-right"></li>
                                            </span>
                                            <input type="text" class="form-control text-right" name="id_cantidad" id="id_cantidad">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Opciones</label>
                                        <div id="button-container">
                                                <button href="#" class="btn btn-block btn-success">Exportar Excel</button>
                                        </div>
                                    </div>
                                </div>
                                <hr class="hidden-xl hidden-lg hidden-md margin-btm-10">
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Filtro por fecha</label>
                                        <div class="btn-group btn-group-justified">
                                            <a class="btn btn-primary btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                                <span>
                                                <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                                </span>
                                                <input type="hidden" name="id_fecha" value="id_fecha">
                                                <div class="pull-right">
                                                    <i class="fa fa-caret-down"></i>
                                                </div>
                                            </a>
                                            <a id="clear_filter" class="btn btn-primary btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-block btn-success" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                                </div>
                            </div>
                        </div>

                    </div>
                  <?php endif ?>
            	</div>



                <div class="box box-primary">

                    <div class="box-header with-border">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group margin-btm-5">
                                        <label>Mostrar</label>
                                        <select id="show_record" class="form-control">
                                            <option value="10">10 registros</option>
                                            <option value="25">25 registros</option>
                                            <option value="50">50 registros</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                        <div class="box-body">
                            <div class="row table-responsive no-left-right-margin">
                                <div class="col-xs-12"> 
                                    <table id="table" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th colspan="2" class="text-center" bgcolor="#CDDC39">Lugar</th>
                                                <th colspan="3" class="text-center" bgcolor="#CDDC39">Colaborador</th>
                                                <th colspan="5" class="text-center" bgcolor="#CDDC39">Detalle</th>
                                            </tr>
                                            <tr>
                                                <th class="printable text-center">Planta</th>
                                                <th class="printable text-center">Casino</th>
                                                <th class="printable text-center">Rut</th>
                                                <th class="printable text-center">Colaborador</th>
                                                <th class="printable text-center">Departamento</th>
                                                <th class="printable text-center">Empresa</th>
                                                <th class="printable text-center">Fecha</th>
                                                <th class="printable text-center">Colación</th>
                                                <th class="printable text-center">Estado</th>
                                                <th class="printable no-sort">Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="printable text-center">Planta</th>
                                                <th class="printable text-center">Casino</th>
                                                <th class="printable text-center">Rut</th>
                                                <th class="printable text-center">Colaborador</th>
                                                <th class="printable text-center">Departamento</th>
                                                <th class="printable text-center">Empresa</th>
                                                <th class="printable text-center">Fecha</th>
                                                <th class="printable text-center">Colación</th>
                                                <th class="printable text-center">Estado</th>
                                                <th class="printable no-sort text-center">Cantidad</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <p id="footer-left" class="col-sm-6 footer-dt"></p>
                                <div id="footer-right" class="col-sm-6"></div>
                            </div>
                            <div class="row">
                                <div class="box-tools pull-right">
                                    <div class="note pull-right" style="margin-right: 5px;margin-top: 15px">
                                      <span data-toggle="tooltip" title data-original-title="Este informe incluye los vales contabilizados en el TOTEM, 'No necesariamente fueron consumidos'." class="pull-left margin-right-5">
                                        <small  class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> 
                                      </span>
                                      <strong data-toggle="tooltip">Nota:</strong> Este informe incluye los vales contabilizados en el TOTEM, "No necesariamente fueron consumidos".
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            
            </div>
		</div>
	</section>
</div>
<script>
    $(document).ajaxStart(function() {
        pf_blockUI();
    }).ajaxStop(function() {
        pf_unblockUI();
    }); 
    jQuery(document).ready(function($) {
        var table;
            table = $("#table").DataTable({
                "responsive": true,
                ajax: {
                    'url': "<?php echo site_url('informe_sistema_cliente/get'); ?>",
                    'type': 'POST',
                    'data': function ( d ) {
                        d.planta = $('#id_planta').val();
                        d.casino = $('#id_casino').val();
                        d.colacion = $('#id_colacion').val();
                        d.fecha = $('#daterange-btn span').text();
                        d.cantidad = $('#id_cantidad').val();
                        d.estado = $('#id_estado').val();
                        d.rut = $('#id_rut').val();
                        d.empresa = $('#id_empresa').val();
                    }
                },
                processing: true,
                serverSide: true,
                order: [[ 0, 'desc']],
                paging: true,
                info: true,
                responsive: true,
                dom: 'lfrtip',
                "aoColumnDefs": [
                { 
                    "sType": "date-uk", 
                    "aTargets": [4] 
                }, 
                { targets: 'no-sort', orderable: false },
                { "width": "1%", "targets": 0 },
                { "width": "1%", "targets": 1 },
                { "width": "1%", "targets": 2 },
                { "width": "1%", "targets": 3 },
                { "width": "1%", "targets": 4 },
                { "width": "1%", "targets": 5 },
                { "width": "1%", "targets": 6 },
                { "width": "1%", "targets": 7 },
                { "width": "1%", "targets": 8 },
                { "width": "1%", "targets": 9 }
                ],
                "createdRow": function ( row, data, index ) {
                    $('td', row).eq(0).addClass('text-center');
                    $('td', row).eq(1).addClass('text-center');
                    $('td', row).eq(2).addClass('text-left');
                    $('td', row).eq(3).addClass('text-left');
                    $('td', row).eq(4).addClass('text-center');
                    $('td', row).eq(5).addClass('text-center');
                    $('td', row).eq(6).addClass('text-center');
                    $('td', row).eq(7).addClass('text-center');
                    $('td', row).eq(8).addClass('text-center');
                    $('td', row).eq(9).addClass('text-right');
                },
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }, 
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "colvis": "Cambiar columnas",
                        "print": "Imprimir"
                    }
                },
            });

        jQuery("#footer").ready(function(){
            jQuery("#table_length").addClass('hidden');
            jQuery("#table_filter").addClass('hidden');
            jQuery("#table_info").addClass('hidden');
            jQuery("#footer-left").text(jQuery("#table_info").text());
            jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
        });


        $('#search_input').keyup(function(){
            table.search($(this).val()).draw() ;
        })

        $('#show_record').click(function() {
            table.page.len($('#show_record').val()).draw();
            jQuery("#footer-left").text(jQuery("#table_inc_info").text());
        });

        jQuery("#table").on("page.dt", function(){
            var info = table.page.info();
            jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
        });

        function reload_table()
        {
            table.ajax.reload(); 
        }

        $('#btn_aplicar').click(function () {
            reload_table();
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#daterange-btn').daterangepicker(
        {
            opens: 'left',
            showDropdowns: true,
            buttonClasses: 'btn btn-flat btn-sm',
            applyClass: 'btn-primary',
            locale: {
                format:             "DD/MM/YYYY",
                separator:          " --- ",
                applyLabel:         "Aplicar",
                cancelLabel:        "Cancelar",
                fromLabel:          "Desde",
                toLabel:            "A",
                customRangeLabel:   "Definir rango",
                daysOfWeek: [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                monthNames: [
                    'Enero', 
                    'Febrero', 
                    'Marzo', 
                    'Abril', 
                    'Mayo', 
                    'Junio', 
                    'Julio', 
                    'Agosto', 
                    'Septiembre', 
                    'Octubre', 
                    'Noviembre', 
                    'Diciembre'
                ],
                firstDay: 1
            },
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Este Año': [moment().startOf('year'), moment().endOf('year')],
                'Año pasado': [moment().subtract(1, 'year').startOf('year'), moment(). subtract(1, 'year').endOf('year')]
                },
                
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                $('#daterange-btn input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                format_date(start.format('DD-MM-YYYY'), end.format('DD-MM-YYYY'));
        });
    });
</script>
<script>
    $('#clear_filter').click(function(){
        $('#daterange-btn span').html('<span><i class="fa fa-calendar"></i> Filtrar por rango de fechas</span>');
        $('#daterange-btn input').val('Filtrar por rango de fechas');
        format_date(moment().subtract(500, 'year').startOf('year').format('DD-MM-YYYY'), moment().format('DD-MM-YYYY'));
        filter();
    });
</script>
<script>
    $.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
        var x;
        try {
        var dateA = a.replace(/ /g, '').split("-");
        var day = parseInt(dateA[0], 10);
        var month = parseInt(dateA[1], 10);
        var year = parseInt(dateA[2], 10);
        var date = new Date(year, month - 1, day)
        x = date.getTime();
        }
        catch (err) {
        x = new Date().getTime();
        }

        return x;
        },

        "date-uk-asc": function (a, b) {
        return a - b;
        },

        "date-uk-desc": function (a, b) {
        return b - a;
        }
    });

    var sDate = '';
    var eDate = '';

    function format_date(startDate, endDate) {

        sDate = startDate;
        eDate = endDate;
    }

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex){
            var dateStart = parseDateValue(sDate);
            var dateEnd = parseDateValue(eDate);
            var evalDate = parseDateValue(aData[4]);
            if ((sDate === '' && eDate === '') || (evalDate >= dateStart && evalDate <= dateEnd)) {
                return true;
            }
            else {
                return false;
            }
    });
</script>
<script>
    $('#id_planta').on('change', function(event){
        var casino = $("#id_casino");
        var option = '';
        $('#id_planta option:selected').each(function() {
            inc_type = $('#id_planta').val();
            pf_blockUI();
            $.post('<?php echo site_url('ticket_generado/get_casino'); ?>', {
                planta : inc_type
            }, function(data) {
                option = '<option value="">Todos</option>';
                $.each(data.option,function(index, item) {
                    option = option + '<option value="' + item.values + '">' + item.text + '</option>';
                });
                casino.find('option')
                .remove()
                .end()
                .append(option).selectpicker('refresh');
                $('#id_casino').trigger('change');
            },'json')
            .always(function() {
                pf_unblockUI();
            });
            option = '';
        });
        event.preventDefault();
    });
</script>
<script>
    $('#id_casino').on('change',function(event) {
        var colacion = $('#id_colacion');
        var option = '';
        $('#id_casino option:selected').each(function() {
            time_select = $('#id_casino').val();
            pf_blockUI();
            $.post('<?php echo site_url('ticket_generado/get_collation'); ?>', {
                casino: time_select
            }, function(data) {
                option = '<option value="">Todos</option>';
                $.each(data.option,function(index, item) {
                    option = option + '<option value="' + item.values + '">' + item.text + '</option>';
                });
                colacion.find('option')
                .remove()
                .end()
                .append(option).selectpicker('refresh');
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
        option = '';
        event.preventDefault();
    });
</script>
<script>
    var Fn = {
    validaRut : function (rutCompleto) {
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
            return false;
            var tmp     = rutCompleto.split('-');
            var digv    = tmp[1]; 
            var rut     = tmp[0];
            if ( digv == 'K' ) digv = 'k' ;
            return (Fn.dv(rut) == digv );
    },
    dv : function(T){
        var M=0,S=1;
        
        for(;T;T=Math.floor(T/10))
            S=(S+T%10*(9-M++%6))%11;
            return S?S-1:'k';
        }
    }

    $('.filter').keyup(function () {
        var input = $(this);
        var value = $(this).val();
        if (Fn.validaRut(value)) {
            input.closest('div.form-group')
            .removeClass('has-error')
            .addClass('has-success');
        }else{
            input.closest('div.form-group')
            .removeClass('has-success');
        }
    });

    $(function () {
        $('.input-number').attr("maxlength", 3);
        $('.input-number').on('input', function () { 
            this.value = this.value.replace(/[^0-9]/g,'');
        });
    });
</script>