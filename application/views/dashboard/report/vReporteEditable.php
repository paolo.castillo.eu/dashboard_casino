<style type="text/css">
.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
    color: #938E8E;
}

.btn_show_informe {
    background-color: #00acd6 !important;
}

</style>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Reporte Editable de Casino</h1>
		<!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Reporte</li>
        </ol>
    </section>
    <section class="content-header">
      <div class="row">
       <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Reporte Editable Casino <li class="fa fa-edit"></li></h3>
                <div class="pull-right col-md-3">
                    <a href="<?php echo site_url() ;?>/report/guardado" class="btn btn-info btn-block btn_show_informe"> <i class="fa fa-database"></i> Ver Reporte Guardados 
                    </a>
                </div>
            </div>
            <form action="<?php echo site_url() ;?>/report/get_rows" method="post" accept-charset="utf-8">
                <div class="box-body">
                    <?php if ($this->session->profile_id == 4): ?>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group margin-btm-5">
                                <label>Planta</label>
                                <select class="form-control selectpicker" name="id_planta" id="id_planta" data-style="btn-default" style="width: 100%">
                                    <option value="">Todos</option>
                                    <?php foreach($planta as $plantas) {?>
                                    <option value="<?php echo $plantas->ID_PLANTA; ?>" 
                                        <?php echo set_select('plantas',  $plantas->NOMBRE); ?>>
                                        <?php echo ucwords(mb_strtolower($plantas->NOMBRE)); ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group margin-btm-5">
                                <label>Seleccione Casino</label>
                                <select class="form-control selectpicker" name="id_casino" id="id_casino" style="width: 100%" data-style="btn-default">
                                    <option value="">Todos</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group margin-btm-5">
                                <label>Seleccione Colación</label>
                                <select class="form-control selectpicker" name="id_colacion" id="id_colacion" data-style="btn-default">
                                    <option value="">Todos</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <hr class="hidden-xl hidden-lg hidden-md margin-btm-10">
                        <div class="col-md-4">
                                <div class="form-group">
                                    <label>Filtro por Empresa:</label>
                                    <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-default">
                                        <option value="">Todos</option>

                                        <?php foreach ($empresa as $nombre): ?>
                                          <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                            <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                        <?php else :?>
                                            <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 pull-right">
                            <button type="button" class="btn btn-block btn-success" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                        </div>
                        <div class="col-md-4 pull-right">
                            <div class="form-group margin-btm-5">
                                <label>Filtro por fecha</label>
                                <div class="btn-group btn-group-justified">
                                    <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                        <span>
                                            <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                        </span>
                                        <input type="hidden" name="id_fecha" value="id_fecha">
                                        <div class="pull-right">
                                            <i class="fa fa-caret-down"></i>
                                        </div>
                                    </a>
                                    <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php elseif ($this->session->profile_id == 3) :?>
                    <div class="box-body">
                        <div class="row">
                            <hr class="hidden-xl hidden-lg hidden-md margin-btm-10">
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Filtro por fecha</label>
                                    <div class="btn-group btn-group-justified">
                                        <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                            <span>
                                                <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                            </span>
                                            <input type="hidden" name="id_fecha" value="id_fecha">
                                            <div class="pull-right">
                                                <i class="fa fa-caret-down"></i>
                                            </div>
                                        </a>
                                        <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Colación</label>
                                    <select class="form-control" name="id_colacion" id="id_colacion">
                                        <option value="">Todos</option>
                                        <?php foreach($colacion as $colaciones) {?>
                                        <option value="<?php echo $colaciones->ID_COLACION; ?>" 
                                            <?php echo set_select('colaciones',  $colaciones->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($colaciones->NOMBRE)); ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Filtro por Empresa:</label>
                                    <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-default">
                                        <option value="">Todos</option>
                                        <?php foreach ($empresa as $nombre): ?>
                                          <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                            <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                        <?php else :?>
                                            <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="hidden form-group margin-btm-5">
                                <label>Cantidad de Colación</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <li class="fa fa-chevron-circle-right"></li>
                                    </span>
                                    <input type="text" class="form-control text-right" name="id_cantidad" id="id_cantidad">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pull-right">
                            <button type="button" class="btn btn-block btn-success ns-succes" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </form>
    </div>
</div>

<div class="edit">
    
</div>
</div>
</div>
</section>
</div>
<script>
    $(document).ready(function(){
        $('#daterange-btn').daterangepicker(
        {
            opens: 'left',
            showDropdowns: true,
            buttonClasses: 'btn btn-flat btn-sm',
            applyClass: 'btn-primary',
            locale: {
                format:             "DD/MM/YYYY",
                separator:          " --- ",
                applyLabel:         "Aplicar",
                cancelLabel:        "Cancelar",
                fromLabel:          "Desde",
                toLabel:            "A",
                customRangeLabel:   "Definir rango",
                daysOfWeek: [
                "Dom",
                "Lun",
                "Mar",
                "Mie",
                "Jue",
                "Vie",
                "Sab"
                ],
                monthNames: [
                'Enero', 
                'Febrero', 
                'Marzo', 
                'Abril', 
                'Mayo', 
                'Junio', 
                'Julio', 
                'Agosto', 
                'Septiembre', 
                'Octubre', 
                'Noviembre', 
                'Diciembre'
                ],
                firstDay: 1
            },
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Este Año': [moment().startOf('year'), moment().endOf('year')],
                'Año pasado': [moment().subtract(1, 'year').startOf('year'), moment(). subtract(1, 'year').endOf('year')]
            },

        },
        function (start, end) {
            $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            $('#daterange-btn input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            format_date(start.format('DD-MM-YYYY'), end.format('DD-MM-YYYY'));
        });
    });
</script>
<script>
    $('#clear_filter').click(function(){
        $('#daterange-btn span').html('<span><i class="fa fa-calendar"></i> Filtrar por rango de fechas</span>');
        $('#daterange-btn input').val('Filtrar por rango de fechas');
        format_date(moment().subtract(500, 'year').startOf('year').format('DD-MM-YYYY'), moment().format('DD-MM-YYYY'));
        filter();
    });
</script>
<script>
    $.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
            var x;
            try {
                var dateA = a.replace(/ /g, '').split("-");
                var day = parseInt(dateA[0], 10);
                var month = parseInt(dateA[1], 10);
                var year = parseInt(dateA[2], 10);
                var date = new Date(year, month - 1, day)
                x = date.getTime();
            }
            catch (err) {
                x = new Date().getTime();
            }

            return x;
        },

        "date-uk-asc": function (a, b) {
            return a - b;
        },

        "date-uk-desc": function (a, b) {
            return b - a;
        }
    });

    var sDate = '';
    var eDate = '';

    function format_date(startDate, endDate) {

        sDate = startDate;
        eDate = endDate;
    }
</script>
<?php if ($this->session->profile_id == 4): ?>
    <script>
        $('#id_planta').on('change', function(event){
            var casino = $("#id_casino");
            var option = '';
            $('#id_planta option:selected').each(function() {
                inc_type = $('#id_planta').val();
                pf_blockUI();
                $.post('<?php echo site_url('ticket_generado/get_casino'); ?>', {
                    planta : inc_type
                }, function(data) {
                    option = '<option value="">Todos</option>';
                    $.each(data.option,function(index, item) {
                        option = option + '<option value="' + item.values + '">' + item.text + '</option>';
                    });
                    casino.find('option')
                    .remove()
                    .end()
                    .append(option).selectpicker('refresh');
                    $('#id_casino').trigger('change');
                },'json')
                .always(function() {
                    pf_unblockUI();
                });
                option = '';
            });
            event.preventDefault();
        });
    </script>
    <script>
        $('#id_casino').on('change',function(event) {
            var colacion = $('#id_colacion');
            var option = '';
            $('#id_casino option:selected').each(function() {
                time_select = $('#id_casino').val();
                pf_blockUI();
                $.post('<?php echo site_url('ticket_generado/get_collation'); ?>', {
                    casino: time_select
                }, function(data) {
                    option = '<option value="">Todos</option>';
                    $.each(data.option,function(index, item) {
                        option = option + '<option value="' + item.values + '">' + item.text + '</option>';
                    });
                    colacion.find('option')
                    .remove()
                    .end()
                    .append(option).selectpicker('refresh');
                },'json')
                .always(function() {
                    pf_unblockUI();
                });
            });
            option = '';
            event.preventDefault();
        });
    </script>
<?php endif ?>

<script>
    $('#btn_aplicar').click(function() {
        var form = $('form');
        Pace.track(function(){
            pf_blockUI();
            $.post(form.attr('action'), form.serialize() , function(data) {
                $('.edit').empty().append(data.view);
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
    });

    $('#btn_aplicar').trigger('click');
</script>
