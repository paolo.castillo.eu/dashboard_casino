<!-- Contenido de la Page -->
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Informe Servicio Casino</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group margin-btm-5">
              <label>Mostrar</label>
              <select id="show_record" class="form-control">
                <option value="10">10 registros</option>
                <option value="25">25 registros</option>
                <option value="50">50 registros</option>
                <option value="100">100 registros</option>
                <option value="-1">Todos los registros</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group margin-btm-5">
              <label>Buscar</label>
              <div class="input-group stylish-input-group">
                <input type="text" class="form-control"  placeholder="Ej: 18546919-0" id="search_input">
                <span class="input-group-addon">
                  <button type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>  
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="row table-responsive no-left-right-margin">
          <div class="col-xs-12"> 
            <table id="table" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td colspan="8" bgcolor="#CDDC39" style="text-align: center;"><p>Colaborador</p></td>
                </tr>
                <tr>
                  <th style="text-align: center;">#</th>
                  <th style="text-align: center;">Rut</th>
                  <th style="text-align: center;">Number</th>
                  <th style="text-align: center;">Credencial</th>
                  <th style="text-align: center;">Departamento</th>
                  <th style="text-align: center;">Fecha</th>
                  <th style="text-align: center;">Hora</th>
                </tr>
              </thead>
              <tbody>
                <?php $var = 1 ?>
                <?php foreach ($data as $key): ?>
                  <?php $install_date = date("d-m-Y",strtotime($key->FECHA_ENTREGA));?>
                  <tr>
                    <td><?php echo $var?></td>
                    <td><?php echo $key->RUT?></td>
                    <td><?php echo ucwords(mb_strtolower($key->NOMBRE))?></td>
                    <td><?php echo $key->CREDENCIAL?></td>
                    <td><?php echo $key->DEPARTAMENTO?></td>
                    <td><?php echo $key->FECHA_ENTREGA?></td>
                    <td><?php echo $key->HORA_ENTREGA?></td>
                  </tr>
                <?php $var ++ ?>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>         
  </div>
</div>
<script>
  var table;
  $(function () {
    table = $('#table').DataTable({
        "order": [0,'desc'],
        "dom": "Bfrtip",
        "iDisplayLength": 50,
        buttons: [
          {
              extend: 'excelHtml5',
              text: 'Generar Excel',
              className : 'btn btn-default',
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          },
          {
              extend: 'pdfHtml5',
              text: 'Generar PDF',
              className : 'btn btn-default', 
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          }
        ],
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "createdRow": function ( row, data, index ) {
             $('td', row).eq(0).addClass('text-center');
             $('td', row).eq(1).addClass('text-center');
             $('td', row).eq(2).addClass('text-center');
             $('td', row).eq(3).addClass('text-center');
             $('td', row).eq(4).addClass('text-center');
             $('td', row).eq(5).addClass('text-center');
             $('td', row).eq(6).addClass('text-center');

        },
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }, 
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });
  });

  jQuery("#footer").ready(function(){
      jQuery("#table_length").addClass('hidden');
      jQuery("#table_filter").addClass('hidden');
      jQuery("#table_info").addClass('hidden');
      jQuery("#footer-left").text(jQuery("#table_info").text());
      jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
  });

  $('#search_input').keyup(function(){
      table.search($(this).val()).draw() ;
  })

  $('#show_record').click(function() {
      table.page.len($('#show_record').val()).draw();
      jQuery("#footer-left").text(jQuery("#table_inc_info").text());
  });

  jQuery("#table").on("page.dt", function(){
      var info = table.page.info();
      jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
  });
</script>