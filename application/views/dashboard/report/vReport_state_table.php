<!-- Contenido de la Page -->
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Informe Servicio Casino</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group margin-btm-5">
              <label>Mostrar</label>
              <select id="show_record" class="form-control">
                <option value="10">10 registros</option>
                <option value="25">25 registros</option>
                <option value="50">50 registros</option>
                <option value="100">100 registros</option>
                <option value="-1">Todos los registros</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group margin-btm-5">
              <label>Buscar</label>
              <div class="input-group stylish-input-group">
                <input type="text" class="form-control"  placeholder="Ej: Casino 1" id="search_input">
                <span class="input-group-addon">
                  <button type="submit">
                    <span class="glyphicon glyphicon-search"></span>
                  </button>  
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="row table-responsive no-left-right-margin">
          <div class="col-xs-12"> 
            <table id="table" class="table table-bordered table-hover" width="100%">
              <thead>
                <tr>
                  <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Casino</p></td>
                  <td colspan="1" bgcolor="#CDDC39" style="text-align: center;"><p>Fecha Entrega</p></td>
                  <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Desayuno</p></td>
                  <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Almuerzo</p></td>
                  <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Onces</p></td>
                  <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Cena</p></td>
                  <td colspan="2" bgcolor="#CDDC39" style="text-align: center;"><p>Cena Noc</p></td>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>N° <label class="hidden">DESAYUNO</label></th>
                  <th>Costo</th>
                  <th>N° <label class="hidden">ALMUERZO</label></th>
                  <th>Costo</th>
                  <th>N° <label class="hidden">CENA</label></th>
                  <th>Costo</th>
                  <th>N° <label class="hidden">ONCE</label></th>
                  <th>Costo</th>
                  <th>N° <label class="hidden">CENA_NOCTURNA</label></th>
                  <th>Costo</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($data as $key): ?>
                  <?php $install_date = date("d-m-Y",strtotime($key->FECHA_ENTREGA));?>
                  <tr>
                    <td><?php echo $key->ID_CASINO?></td>
                    <td><?php echo $key->CASINO?></td>                    
                    <td><?php echo $install_date?></td>
                    <td><a href="javascript:;" class="info"><?php echo $key->NUM_TICKET_DESAYUNO?></a></td>
                    <td><?php echo '$'.$key->COSTO_DESAYUNO?></td>
                    <td><a href="javascript:;" class="info"><?php echo $key->NUM_TICKET_ALMUERZO?></a></td>
                    <td><?php echo '$'.$key->COSTO_ALMUERZO?></td>
                    <td><a href="javascript:;" class="info"><?php echo $key->NUM_TICKET_ONCE?></a></td>
                    <td><?php echo '$'.$key->COSTO_ONCE?></td>
                    <td><a href="javascript:;" class="info"><?php echo $key->NUM_TICKET_CENA?></a></td>
                    <td><?php echo '$'.$key->COSTO_CENA?></td>
                    <td><a href="javascript:;" class="info"><?php echo $key->NUM_TICKET_CENA_NOCTURA?></a></td>
                    <td><?php echo '$'.$key->COSTO_CENA_NOCTURA?></td>
                  </tr>
                <?php endforeach ?>
              </tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th class="text-right"></th>
                  <th></th>
                  <th class="text-right"></th>
                  <th></th>
                  <th class="text-right"></th>
                  <th></th>
                  <th class="text-right"></th>
                  <th></th>
                  <th class="text-right"></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
          <div class="row">
              <div class="box-tools pull-right">
                  <div class="note pull-right" style="margin-right: 5px;margin-top: 15px">
                    <span data-toggle="tooltip" title data-original-title="Este informe incluye los vales contabilizados en el TOTEM, 'No necesariamente fueron consumidos'." class="pull-left margin-right-5">
                      <small  class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> 
                    </span>
                    <strong data-toggle="tooltip">Nota:</strong> Este informe incluye los vales contabilizados en el TOTEM, "No necesariamente fueron consumidos".
                  </div>
              </div>
          </div> 
      </div>
    </div>         
  </div>
</div>
<script>
  var table;
  $(function () {
    table = $('#table').DataTable({
        "order": [2,'desc'],
        "dom": "Bfrtip",
        "iDisplayLength": 50,
        "columnDefs": [
            { targets: 'no-sort', orderable: false },
            {
                targets: [ 0 ],
                visible: false,
                searchable: false
            },
            {
              "targets": [4],
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  var $currencyCell = $(nTd);
                  var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                  $currencyCell.text(commaValue);
                }
            },
            {
              "targets": [6],
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  var $currencyCell = $(nTd);
                  var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                  $currencyCell.text(commaValue);
                }
            },
            {
              "targets": [8],
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  var $currencyCell = $(nTd);
                  var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                  $currencyCell.text(commaValue);
                }
            },
            {
              "targets": [10],
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  var $currencyCell = $(nTd);
                  var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                  $currencyCell.text(commaValue);
                }
            },
            {
              "targets": [12],
                "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  var $currencyCell = $(nTd);
                  var commaValue = $currencyCell.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                  $currencyCell.text(commaValue);
                }
            },
        ],
        columns: [
          { name: 'col-1' },
          { name: 'col-2' },
          { name: 'col-3' },
          { name: 'DESAYUNO' },
          { name: 'col-5' },
          { name: 'ALMUERZO' },
          { name: 'col-7' },
          { name: 'ONCE' },
          { name: 'col-9' },
          { name: 'CENA' },
          { name: 'col-11' },
          { name: 'CENA_NOCTURNA' },
          { name: 'col-13' },
        ],
        buttons: [
          {
              extend: 'excelHtml5',
              text: 'Generar Excel',
              className : 'btn btn-default',
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          },
          {
              extend: 'pdfHtml5',
              text: 'Generar PDF',
              className : 'btn btn-default', 
              exportOptions: { 
                  orthogonal: 'fullNotes',
                  columns: ':visible', 
              }
          }
        ],
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "createdRow": function ( row, data, index ) {
             $('td', row).eq(0).addClass('text-center');
             $('td', row).eq(1).addClass('text-center');
             $('td', row).eq(2).addClass('text-center');
             $('td', row).eq(3).addClass('text-right');
             $('td', row).eq(4).addClass('text-center');
             $('td', row).eq(5).addClass('text-right');
             $('td', row).eq(6).addClass('text-center');
             $('td', row).eq(7).addClass('text-right');
             $('td', row).eq(8).addClass('text-center');
             $('td', row).eq(9).addClass('text-right');
             $('td', row).eq(10).addClass('text-center');
             $('td', row).eq(11).addClass('text-right');
        },
        "footerCallback": function ( row, data, start, end, display ) {
          var api = this.api(), data;
          var intVal = function ( i ) {
              return typeof i === 'string' ?
                  i.replace(/[\$,]/g, '')*1 :
                  typeof i === 'number' ?
                      i : 0;
          };

          totalCostoDesayuno = api
              .column( 4, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
          }, 0 );

          $( api.column(4).footer() ).html(
              'T : '+ totalCostoDesayuno
          );


          totalCostoAlmuerzo = api
              .column( 6, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
          }, 0 );

          $( api.column(6).footer() ).html(
              'T : '+ totalCostoAlmuerzo
          );

          totalCostoOnce = api
              .column( 8, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
          }, 0 );

          $( api.column(8).footer() ).html(
              'T : '+ totalCostoOnce
          );

          totalCostoCena = api
              .column( 10, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
          }, 0 );

          $( api.column(10).footer() ).html(
              'T : '+ totalCostoCena
          );

          totalCostoCenaNoc = api
              .column( 12, { page: 'current'} )
              .data()
              .reduce( function (a, b) {
                  return intVal(a) + intVal(b);
          }, 0 );

          $( api.column(12).footer() ).html(
              'T : '+ totalCostoCenaNoc
          );

        },
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }, 
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });

    function formatNumber(n) {
      return n.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }

  });

  jQuery("#footer").ready(function(){
      jQuery("#table_length").addClass('hidden');
      jQuery("#table_filter").addClass('hidden');
      jQuery("#table_info").addClass('hidden');
      jQuery("#footer-left").text(jQuery("#table_info").text());
      jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
  });

  $('#search_input').keyup(function(){
      table.search($(this).val()).draw() ;
  })

  $('#show_record').click(function() {
      table.page.len($('#show_record').val()).draw();
      jQuery("#footer-left").text(jQuery("#table_inc_info").text());
  });

  jQuery("#table").on("page.dt", function(){
      var info = table.page.info();
      jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
  });
</script>
<script>
  $('#table').on('click', '.info', function(){
    var data = $('#table').DataTable().row($(this).closest('tr')).data();
    var columns = $('#table').DataTable().settings().init().columns;
    var colIndex = $('#table').DataTable().cell($(this).closest('td')).index().column;
    Pace.track(function(){
      pf_blockUI();
      $.post('<?php echo site_url('report_state/get_services')?>', {
        time : data[2],
        state : $('#id_state').val(),
        casino : data[0],
        type : columns[colIndex].name,
        company : $('#company').val() 
      }, function(data) {
        $('#section_hidden').empty();
        if (data.status) {
          $(data.flag).hide().appendTo('#section_hidden').fadeIn(999);
        }else if(data.status == false){
          $(data.flag).hide().appendTo('#section_hidden').fadeIn(999);
        }
      },'json')
      .always(function() {
        pf_unblockUI();
      });
    });
  });
</script>