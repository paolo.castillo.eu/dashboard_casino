<form action="#" method="post" accept-charset="utf-8" id="id_from_modal">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title" id="myModalLabel">Detalle de Solicitud</h4>
</div>
<div class="modal-body">
    <div class="row">
        <input type="hidde" name="id" class="hidden" value="<?php echo $input[0]?>">
        <div class="col-md-6">
            <div class="form-group">
                <label>Rut</label>
                <input type="text" name="id_rut" class="form-control" value="<?php echo $input[2]?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" name="id_nombre" class="form-control" value="<?php echo $input[3]?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Departamento</label>
                <input type="text" name="id_departamento" class="form-control" value="<?php echo $input[4]?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Empresa</label>
                <input type="text" name="id_empresa" class="form-control" value="<?php echo $input[5]?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Planta</label>
                <input type="text" name="id_planta" class="form-control" value="<?php echo $input[6]?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Casino</label>
                <input type="text" name="id_casino" class="form-control" value="<?php echo $input[7]?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Colación</label>
                <input type="text" name="id_colacion" class="form-control" value="<?php echo $input[8]?>" readonly>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Fecha Solicitud</label>
                <input type="text" name="id_fecha" class="form-control" value="<?php echo $input[1]?>" readonly>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Justificación </label>
                <textarea class="form-control" rows="3" readonly><?php echo $input[9]?></textarea>
            </div> 
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal" id="btn_cancel">Cancelar</button>
    <button type="button" class="btn btn-ok btn-success">Aprobar Solicitud</button>
</div>
</form>

<!-- Bootstrap modal -->
<div class="modal modal-info fade" id="confirm-aproved" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Confirmar Aprobación</h4>
            </div>
            <div class="modal-body">
                <p>Estás a punto de aprobar la solicitud seleccionada <b><i class="title"></i></b>. Este procedimiento es irreversible.</p>
                <p>¿Quieres proceder?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal" id="btn_cancel">Cancelar</button>
                <button type="button" class="btn btn-primary btn-aproved">Aprobar</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).on({
    'show.bs.modal': function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    },
    'hidden.bs.modal': function() {
        if ($('.modal:visible').length > 0) {
            setTimeout(function() {
                $(document.body).addClass('modal-open');
            }, 0);
        }
    }
}, '.modal');
</script>
<!-- END modal --> 
<script>

    $('.btn-ok').click(function(){
        $('#confirm-aproved').modal('show');
    });

    $('.btn-aproved').on('click', function(event) {
        event.preventDefault();
        var form = $('#id_from_modal').serialize();
        pf_blockUI();    
        $.post('<?php echo site_url() ;?>/solicitudes_rechazadas/aproved', form , 
            function(data) {
                if (data.success) {
                    $.notify({
                        icon: 'fa fa-check-circle',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: data.message
                    }, {
                            type: 'success',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    reload_table();
                    $('#confirm-aproved').modal('hide');
                    $('#modal-info').modal('hide');
                }else{
                    $.notify({
                        icon: 'fa fa-close',
                        title: "<strong> Solicitud </strong> <br/>",
                        message: data.message
                    }, {
                            type: 'danger',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                }
        },'json')
        .always(function() {
            pf_unblockUI();
        });
    });
</script>