<style type="text/css">
  .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
    color: #938E8E;
}
</style>
<script>
$(document).on({
    'show.bs.modal': function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    },
    'hidden.bs.modal': function() {
        if ($('.modal:visible').length > 0) {
            // restore the modal-open class to the body element, so that scrolling works
            // properly after de-stacking a modal.
            setTimeout(function() {
                $(document.body).addClass('modal-open');
            }, 0);
        }
    }
}, '.modal');
</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Solicitudes Pendientes</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Solicitudes pendientes</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Solicitudes de Casino Pendientes <li class="fa fa-history"></li></h3>
                        <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?php if ($this->session->profile_id == 4) :?>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Seleccione Planta</label>
                                    <select class="form-control selectpicker" name="id_planta" id="id_planta" data-live-search="true">
                                        <option value="">Seleccione una Planta</option>
                                        <?php foreach ($planta as $plantas): ?>
                                            <option value="<?php echo $plantas->ID_PLANTA; ?>" 
                                            <?php echo set_select('plantas',  $plantas->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($plantas->NOMBRE)); ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Seleccione Casino</label>
                                    <select class="form-control selectpicker" name="id_casino" id="id_casino" data-live-search="true">
                                        <option value="">Seleccione un Casino</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Colación</label>
                                        <select class="form-control selectpicker" name="id_colacion" id="id_colacion" data-live-search="true">
                                            <option value="">Seleccione una Colación</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Rut</label>
                                        <input type="text" class="form-control filter" id="id_rut" placeholder="Ej: 18546919-0" data-column-index='2'>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group margin-btm-5">
                                        <label>Filtro por fecha</label>
                                        <div class="btn-group btn-group-justified">
                                            <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                                <span>
                                                <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                                </span>
                                                <input type="hidden" name="id_fecha" value="id_fecha">
                                                <div class="pull-right">
                                                    <i class="fa fa-caret-down"></i>
                                                </div>
                                            </a>
                                            <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Empresa</label>
                                          <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-default">
                                            <option value="">Todos</option>
                                            <?php foreach ($empresa as $nombre): ?>
                                              <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                                <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php else :?>
                                                <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php endif ?>
                                            <?php endforeach ?>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pull-right">
                                    <button type="button" class="btn btn-block btn-success" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                                </div>
                            </div>
                        <?php elseif ($this->session->profile_id == 3): ?>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Colación</label>
                                        <select class="form-control selectpicker" name="id_colacion" id="id_colacion" data-live-search="true">
                                        <option value="">Todos</option>
                                        <?php foreach($colacion as $colaciones) {?>
                                            <option value="<?php echo $colaciones->ID_COLACION; ?>" 
                                            <?php echo set_select('colaciones',  $colaciones->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($colaciones->NOMBRE)); ?>
                                            </option>
                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group margin-btm-5">
                                        <label>Filtro por fecha</label>
                                        <div class="btn-group btn-group-justified">
                                            <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                                <span>
                                                <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                                </span>
                                                <input type="hidden" name="id_fecha" value="id_fecha">
                                                <div class="pull-right">
                                                    <i class="fa fa-caret-down"></i>
                                                </div>
                                            </a>
                                            <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Rut</label>
                                        <input type="text" class="form-control filter" id="id_rut" placeholder="Ej: 18546919-0" data-column-index='2'>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Empresa</label>
                                          <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-default">
                                            <option value="">Todos</option>
                                            <?php foreach ($empresa as $nombre): ?>
                                              <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                                <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php else :?>
                                                <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                              <?php endif ?>
                                            <?php endforeach ?>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pull-right">
                                    <button type="button" class="btn btn-block btn-success" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group margin-btm-5">
                                        <label>Mostrar</label>
                                        <select id="show_record" class="form-control">
                                            <option value="10">10 registros</option>
                                            <option value="25">25 registros</option>
                                            <option value="50">50 registros</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group margin-btm-5">
                                        <label>Buscar</label>
                                        <div class="input-group stylish-input-group">
                                            <input type="text" class="form-control"  placeholder="EJ: Felipe, Planta 1" id="search_input">
                                            <span class="input-group-addon">
                                                <button type="submit">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>  
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row table-responsive no-left-right-margin">
                                <div class="col-xs-12"> 
                                    <table id="table" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th class="printable  text-center">#</th>
                                                <th class="printable  text-center">Fecha</th>
                                                <th class="printable  text-center">Rut</th>
                                                <th class="printable  text-center">Nombre</th>
                                                <th class="printable  text-center">Departamento</th>
                                                <th class="printable  text-center">Empresa</th>
                                                <th class="printable  text-center">Planta</th>
                                                <th class="printable  text-center">Casino</th>
                                                <th class="printable  text-center">Colación</th>
                                                <th class="printable  text-center">#</th>
                                                <th class="printable  text-center">Función</th>    
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="printable  text-center">#</th>
                                                <th class="printable  text-center">Fecha</th>
                                                <th class="printable  text-center">Rut</th>
                                                <th class="printable  text-center">Nombre</th>
                                                <th class="printable  text-center">Departamento</th>
                                                <th class="printable  text-center">Empresa</th>
                                                <th class="printable  text-center">Planta</th>
                                                <th class="printable  text-center">Casino</th>
                                                <th class="printable  text-center">Colación</th>
                                                <th class="printable  text-center">#</th>
                                                <th class="printable  text-center">Función</th> 
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <p id="footer-left" class="col-sm-6 footer-dt"></p>
                                <div id="footer-right" class="col-sm-6"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>

<div class="modal fade in" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal_content">
            
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#daterange-btn').daterangepicker(
        {
            opens: 'left',
            showDropdowns: true,
            buttonClasses: 'btn btn-flat btn-sm',
            applyClass: 'btn-primary',
            minDate : moment().endOf("day"),
            locale: {
                format:             "DD/MM/YYYY",
                separator:          " --- ",
                applyLabel:         "Aplicar",
                cancelLabel:        "Cancelar",
                fromLabel:          "Desde",
                toLabel:            "A",
                customRangeLabel:   "Definir rango",
                daysOfWeek: [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                monthNames: [
                    'Enero', 
                    'Febrero', 
                    'Marzo', 
                    'Abril', 
                    'Mayo', 
                    'Junio', 
                    'Julio', 
                    'Agosto', 
                    'Septiembre', 
                    'Octubre', 
                    'Noviembre', 
                    'Diciembre'
                ],
                firstDay: 1
            }
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                $('#daterange-btn input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                format_date(start.format('DD-MM-YYYY'), end.format('DD-MM-YYYY'));
        });
    });

    $.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
        var x;
        try {
        var dateA = a.replace(/ /g, '').split("-");
        var day = parseInt(dateA[0], 10);
        var month = parseInt(dateA[1], 10);
        var year = parseInt(dateA[2], 10);
        var date = new Date(year, month - 1, day)
        x = date.getTime();
        }
        catch (err) {
        x = new Date().getTime();
        }

        return x;
        },

        "date-uk-asc": function (a, b) {
        return a - b;
        },

        "date-uk-desc": function (a, b) {
        return b - a;
        }
    });

    var sDate = '';
    var eDate = '';

    function format_date(startDate, endDate) {

        sDate = startDate;
        eDate = endDate;
    }

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex){
            var dateStart = parseDateValue(sDate);
            var dateEnd = parseDateValue(eDate);
            var evalDate = parseDateValue(aData[4]);
            if ((sDate === '' && eDate === '') || (evalDate >= dateStart && evalDate <= dateEnd)) {
                return true;
            }
            else {
                return false;
            }
    });

</script>

<script>
    var table;
    jQuery(document).ready(function() {
        var date = new Date();
        var y = date.getFullYear();
        var m = date.getMonth();
        var d = date.getDate();
        var n_date = (d)+'/'+(m+1)+'/'+y;
        if(m < 10){
            n_date = (d)+'/0'+(m+1)+'/'+y;
        }
        var titleExport = 'Historial de Aprobación';
        var header = 'Sistema Casino --- '+n_date;
        var orientation = 'landscape'; //portrait
        var pageSize = 'LETTER';
        table = $("#table").DataTable({
                "responsive": true,
                ajax: {
                    'url': "<?php echo site_url('solicitudes_pendiente/get_rows'); ?>",
                    'type': 'POST',
                    'data': function ( d ) {
                        d.planta = $('#id_planta').val();
                        d.casino = $('#id_casino').val();
                        d.colacion = $('#id_colacion').val();
                        d.fecha = $('#daterange-btn span').text();
                        d.empresa = $('#id_empresa').val();
                        d.rut = $('#id_rut').val();
                    }
                },
                processing: true,
                serverSide: true,
                order: [[ 0, 'desc']],
                paging: true,
                info: true,
                responsive: true,
                dom: 'lfrtip',
                "aoColumnDefs": [
                { 
                    "sType": "date-uk", 
                    "aTargets": [4] 
                }, { targets: 'no-sort', orderable: false },
                { 
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<button type="button" class="btn btn-info btn-block btn-show"><i class="fa fa-eye"></i></button>' 
                },
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ -2 ],
                    "visible": false,
                    "searchable": false
                },
                ],
                "createdRow": function ( row, data, index ) {
                    $('td', row).eq(0).addClass('text-center');
                    $('td', row).eq(1).addClass('text-center');
                    $('td', row).eq(2).addClass('text-center');
                    $('td', row).eq(3).addClass('text-center');
                    $('td', row).eq(4).addClass('text-center');
                    $('td', row).eq(5).addClass('text-center');
                    $('td', row).eq(6).addClass('text-center');
                    $('td', row).eq(7).addClass('text-center');
                    $('td', row).eq(8).addClass('text-center');
                    $('td', row).eq(9).addClass('text-center');
                    $('td', row).eq(10).addClass('text-center');
                    $('td', row).eq(11).addClass('text-center');
                },
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }, 
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "colvis": "Cambiar columnas",
                        "print": "Imprimir"
                    }
                },
            });

        $('#table')
            .on( 'processing.dt', function ( e, settings, processing ) {
                if(processing){
                    Pace.start();
                } else {
                    Pace.stop();
                }
            })
        .dataTable();

        jQuery("#footer").ready(function(){
          jQuery("#table_length").addClass('hidden');
          jQuery("#table_filter").addClass('hidden');
          jQuery("#table_info").addClass('hidden');
          jQuery("#footer-left").text(jQuery("#table_info").text());
          jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
        });


        $('#search_input').keyup(function(){
          table.search($(this).val()).draw() ;
        })

        $('#show_record').click(function() {
          table.page.len($('#show_record').val()).draw();
          jQuery("#footer-left").text(jQuery("#table_inc_info").text());
        });

        jQuery("#table").on("page.dt", function(){
          var info = table.page.info();
          jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
        });


        $('#btn_aplicar').click(function () {
            reload_table();
        });
    });

    function reload_table()
    {
        table.ajax.reload(); 
    }
</script>
<script>
    $('#table tbody').on( 'click', '.btn-show', function (event) {
        event.preventDefault();
        var data = table.row($(this).parents('tr')).data();
        var btn = $('.btn-show');
        btn.prop('disabled','disabled');
        Pace.track(function(){
            pf_blockUI();        
            $.post('<?php echo site_url('solicitudes_pendiente/get_modal') ;?>', {
                param : data
            }, function(data) {
                $('.modal_content').empty().append(data.view);
                $('#modal-info').modal('show');
                btn.prop('disabled',false);
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
    });
</script>
<script>
    $(function() {
        $("#id_planta").change(function() {
            var casino = $("#id_casino");
            var html = '';
            $("#id_planta option:selected").each(function() {
                inc_type = $('#id_planta').val();
                pf_blockUI();        
                $.post('<?php echo site_url('ticket_generado/get_casino'); ?>', {
                    planta : inc_type
                }, function(data) {
                    $.each(data.option,function(index, item) {
                        html = html + '<option value="' + item.values + '">' + item.text + '</option>';
                    });
                    casino.find('option')
                    .remove()
                    .end()
                    .append(html).selectpicker('refresh');
                    $('#id_casino').trigger('change');
                },'json')
                .always(function() {
                    pf_unblockUI();
                });
            });
        });

        $('#id_casino').on('change',function(event) {
            var colacion = $('#id_colacion');
            var option = '';
            $('#id_casino option:selected').each(function() {
                time_select = $('#id_casino').val();
                pf_blockUI();
                $.post('<?php echo site_url('ticket_generado/get_collation'); ?>', {
                    casino: time_select
                }, function(data) {
                    option = '<option value="">Todos</option>';
                    $.each(data.option,function(index, item) {
                        option = option + '<option value="' + item.values + '">' + item.text + '</option>';
                    });
                    colacion.find('option')
                    .remove()
                    .end()
                    .append(option).selectpicker('refresh');
                },'json')
                .always(function() {
                    pf_unblockUI();
                });
            });
            option = '';
            event.preventDefault();
        });
    });
</script>