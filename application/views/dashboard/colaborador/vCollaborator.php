<div class="content-wrapper">
  <section class="content-header">
    <h1>Colaboradores <small><b>Todos</b></small></h1>
    <!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Colaboradores</li>
        </ol>
    <!-- Salto de linea  -->
  </section>
  <!-- Contenido de la Page -->
  <section class="content" id="content_table">
    <div class="row">
      <!-- Columna de 6 hasta 12 -->
          <div class="col-xs-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                    <h3 class="box-title">Método de Búsqueda</h3>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group margin-btm-5">
                        <label>Filtrar por Credencial</label>
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control"  placeholder="EJ: 00000000" id="input_credencial">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="fa fa-credit-card"></span>
                                </button>  
                            </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group margin-btm-5">
                        <label>Filtrar por Empresa</label>
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control"  placeholder="EJ: PF Alimentos" id="input_empresa">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="fa fa-filter"></span>
                                </button>  
                            </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box box-primary box-solid">
              <div class="box-header with-border">
                    <h3 class="box-title">Registro de los Colaboradores</h3>
                </div>
                <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <button id="action" class="btn btn-primary btn-block" aria-pressed="true">Agregar nuevo Colaborador</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i></button>
                            </div>
                        </div>
                        <br>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group margin-btm-5">
                            <label>Mostrar</label>
                            <select id="show_record" class="form-control">
                                <option value="10">10 registros</option>
                                <option value="25">25 registros</option>
                                <option value="50">50 registros</option>
                                <option value="100">100 registros</option>
                                <option value="-1">Todos los registros</option>
                            </select>
                          </div>
                        </div>
                          <div class="col-md-6">
                            <div class="form-group margin-btm-5">
                                <label>Buscar</label>
                                <div class="input-group stylish-input-group">
                                    <input type="text" class="form-control"  placeholder="EJ: 185469190 Ó LOPEZ QUEZADA" id="search_input">
                                    <span class="input-group-addon">
                                        <button type="submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>  
                                    </span>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="row table-responsive no-left-right-margin">
                        <div class="col-xs-12"> 
                        <table id="table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <th>Rut</th>
                          <th>Credencial</th>
                          <th>Departamento</th>
                          <th>Empresa</th>
                          <th>Gerencia</th>
                          <th>Credencial de Visita</th>
                          <th>Rut del Solicitante</th>
                          <th>Ubicación</th>
                          <th>Ubicación del lugar</th>
                          <th>Área</th>
                          <th>Perfil</th>
                          <th>Casino</th>
                          <th>Funciones</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                          <th>#</th>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <th>Rut</th>
                          <th>Credencial</th>
                          <th>Departamento</th>
                          <th>Empresa</th>
                          <th>Gerencia</th>
                          <th>Credencial de Visita</th>
                          <th>Rut del Solicitante</th>
                          <th>Ubicación</th>
                          <th>Ubicación del lugar</th>
                          <th>Área</th>
                          <th>Perfil</th>
                          <th>Casino</th>
                          <th>Funciones</th>
                        </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                  <div class="padding-top-5"></div>
                    <div class="box-footer">
                        <a href="<?php echo site_url() ?>/dashboard" class="btn btn-flat btn-default"><strong>Volver</strong></a>
                    </div>
                  </div>
              </div>
            </div>
    </div>
  </section>
  <section class="content" id="content_form" style="display: none">
    <div class="row">
      <!-- Columna de 6 hasta 12 -->
          <div class="col-xs-12">
              <div class="box box-primary box-solid">
              <div class="box-header with-border">
                    <h3 class="box-title">Registro de Colaborador</h3>
              </div>
              <div class="box-body">
              <form id="form" name="form" action="#">
              <input type="hidden" name="id_collaborator" id="id_collaborator">
                <div class="row">
                  <div class="col-md-3">
                      <div id="fg_rut" class="form-group">
                          <label class="control-label" for="name">Nombre/Apellido</label>
                          <input type="text" class="form-control" name="name" id="name" placeholder="Leal Robles">
                      </div>
                  </div>
                  <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="email">Correo</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="pfalimentos@pfalimentos.cl">
                        </div>
                  </div>
                  <div class="col-md-3">
                      <div class="form-group">
                          <label class="control-label" for="rut">Rut</label>
                          <input type="text" class="form-control" name="rut" id="rut" placeholder="18546919-0">
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="form-group">
                          <label class="control-label" for="card">Credencial</label>
                          <input type="text" class="form-control" name="card" id="card"" placeholder="00030008">
                      </div>
                  </div>
                </div>
                <div class="row">                  
                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="control-label" for="deparment">Departamento</label>
                        <input type="text" id="deparment" name="deparment" class="form-control" placeholder="Seguridad">
                      </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="company">Empresa</label>
                            <input type="text" id="company" name="company" class="form-control" placeholder="pfAlimentos">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" for="management">Gerencia</label>
                            <input  type="text" id="management" name="management" class="form-control" placeholder="Operaciones">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label"  for="card_visit">Credencial de Visita</label>
                            <input  type="text" id="card_visit" name="card_visit" class="form-control" placeholder="00029926">
                        </div>
                    </div>
                </div>
                <div class="row">                  
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label" for="rut_request">Rut del Solicitante</label>
                        <input type="text" id="rut_request" name="rut_request" class="form-control" placeholder="14259160-K">
                      </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="physical_accountan">Ubicación Contable</label>
                            <input type="text" id="physical_accountan" name="physical_accountan" class="form-control" placeholder="30409999">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="physical_location">Ubicación del lugar</label>
                            <input  type="text" id="physical_location" name="physical_location" class="form-control" placeholder="Planta 3">
                        </div>
                    </div>
                </div>
                <hr class="hrline">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="area">Área</label><span class="pull-right-container"><small data-toggle="tooltip" data-placement="top" title="Seleccione Casino." class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> </span>
                            <select class="form-control" id="area" name="area">
                              <option select value="default">Seleccione Área</option>
                                <?php foreach($area as $areas) {?>
                                  <option value="<?php echo $areas->ID_AREA;?>" 
                                  <?php echo set_select('areas',  $areas->NOMBRE); ?>>
                                  <?php echo ucwords(mb_strtolower($areas->NOMBRE)); ?>
                                  </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="profile">Perfil</label><span class="pull-right-container"><small data-toggle="tooltip" data-placement="top" title="Seleccione Casino." class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> </span>
                            <select class="form-control" id="profile" name="profile">
                              <option select value="default">Seleccione Perfil</option>
                                <?php foreach($perfil as $profiles) {?>
                                  <option value="<?php echo $profiles->ID_PERFIL;?>" 
                                  <?php echo set_select('profiles',  $profiles->NOMBRE); ?>>
                                  <?php echo ucwords(mb_strtolower($profiles->NOMBRE)); ?>
                                  </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="casino">Casino</label><span class="pull-right-container"><small data-toggle="tooltip" data-placement="top" title="Seleccione Casino." class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> </span>
                            <select class="form-control" id="casino" name="casino">
                              <option select value="default">Seleccione Casino</option>
                                <?php foreach($casino as $casinos) {?>
                                  <option value="<?php echo $casinos->ID_CASINO;?>" 
                                  <?php echo set_select('casinos',  $casinos->NOMBRE); ?>>
                                  <?php echo ucwords(mb_strtolower($casinos->NOMBRE)); ?>
                                  </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="button" id="save" class="btn btn-primary btn-flat pull-right"><strong>Agregar Colaborador</strong></button>
                </div>
              </form>
              <div class="padding-top-5"></div>
              <div class="box-footer">
                <button id="action_hide" class="btn btn-flat btn-default">Volver</button> 
              </div>
              </div>
            </div>
    </div>
  </section>
  <!-- END contenido -->
</div>
<!-- Bootstrap modal -->
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Confirmar eliminación</h4>
            </div>
            <div class="modal-body">
                <p>Estás a punto de eliminar <b><i class="title"></i></b>. Este procedimiento es irreversible.</p>
                <p>¿Quieres proceder?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat pull-left" data-dismiss="modal" id="btn_cancel">Cancelar</button>
                <button type="button" class="btn btn-danger btn-ok">Eliminar</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal --> 
<script>
    $('#confirm-delete').on('click', '.btn-ok', function(e) {
        var $modalDiv = $(e.delegateTarget);
        var id = $(this).data('recordId');
        pf_blockUI();
        $.ajax({
            url : "<?php echo site_url(); ?>/colaborador/del/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $("#btn_cancel").prop('disable',true);
                if (data.status) {
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>La operación se realizo correctamente</b>."
                    },{
                        type: 'info',
                        timer: 4000
                    });
                    reload_table();
                    $("#btn_cancel").prop('disable',false);
                } else {
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>Error al borrar los datos</b>."
                    },{
                        type: 'danger',
                        timer: 4000
                    });    
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $.notify({
                    icon: 'pe-7s-look',
                    message: "<b>Error al borrar los datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });              
            },
            complete : function () {
                pf_unblockUI();
            }
        });
        $modalDiv.addClass('loading');
        setTimeout(function() {
            $modalDiv.modal('hide').removeClass('loading');
        }, 1000)
    });
    $(function () {
        $(".select2").select2();
    }); 
    $('#confirm-delete').on('show.bs.modal', function(e) {
        var data = $(e.relatedTarget).data();
        $('.title', this).text(data.recordTitle);
        $('.btn-ok', this).data('recordId', data.recordId);
    });
</script> 
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
        $("#total").attr("maxlength", 4);
        $('#form').validate({
          rules:{
            name: {
                required: true,
                maxlength: 255,
                minlength: 2
            },
            email: {
                required: true,
                maxlength: 255,
                minlength: 2,
                email: true
            },
            rut: {
                required: true,
                rut: true
            },
            card: {
                required: true,
                minlength: 8
            },
            rut_request: {
                required: true,
                rut: true
            },
            deparment: {
                required: true,
                maxlength: 255,
                minlength: 2
            },
            company: {
                required: true,
                maxlength: 255,
                minlength: 2,
                alphanumeric: true
            },
            management: {
                alphanumeric: true
            },
            physical_accountan:{
                required: true,
                maxlength: 255,
                minlength: 2
            },
            physical_location:{
                required: true,
                maxlength: 255,
                minlength: 2,
                alphanumeric: true
            },
            profile:{
                valueNotEquals: "default"
            },
            casino:{
                valueNotEquals: "default"
            }
          },
          submitHandler: function(form){
            form.submit();
          },
          highlight: function(element){ //elemento donde esta posicionado
            $(element).parent().removeClass('has-success').addClass('has-error');
          },
          success: function(element){
            $(element).parent().removeClass('has-error').addClass('has-success');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error,element){
            if(element.parent('.input-group').length){
                error.insertAfter(element.parent());
            }else{
                error.insertAfter(element);
            }
          }
        });
        $("#save").prop('disabled','disabled');
        $("#form").on('keyup blur',function(){
          if ($("#form").valid()) {
            $("#save").prop('disabled',false);
          }else{
            $("#save").prop('disabled','disabled');
          }
        });
    });
</script>
<script>
    var table;
    $(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url(); ?>/colaborador/get",
                "type": "POST"
            },
            "columnDefs": [
            {
                "targets": [ -1 ],
                "orderable": false,
            },
            ],
            "paging": true,
            "info": true,
            "autoWidth": false,
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }, 
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });


    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    jQuery("#footer").ready(function(){
        jQuery("#table_length").addClass('hidden');
        jQuery("#table_filter").addClass('hidden');
        jQuery("#table_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_info").text());
        jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
    });

    $('#search_input').keyup(function(){
        table.search($(this).val()).draw() ;
    })

    $('#show_record').click(function() {
        table.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_inc_info").text());
    });

    jQuery("#table").on("page.dt", function(){
        var info = table.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });
</script>
<script>
  var save_method; //for save method string 
  $("#action").click(function(){
    $("#content_table").hide();
    save_method = 'add';
    $("#content_form").show(1000);
  });

  $("#action_hide").click(function(){
    $("#content_form").hide();
    var validator = $('#form').validate();
    validator.resetForm();
    $("#form").find('.has-error').removeClass("has-error");
    $("#form").find('.has-success').removeClass("has-success");
    $('#form').find('.form-control-feedback').remove();
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $("#content_table").show(1000);
  });

  function updateCollaborator(step) {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      pf_blockUI();
      $.ajax({
          url : "<?php echo site_url(); ?>/colaborador/get/"+step,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('[name="id_collaborator"]').val(data.ID_COLABORADOR);
              $('[name="name"]').val(data.NOMBRE.toLowerCase());
              $('[name="email"]').val(data.CORREO);
              $('[name="rut"]').val(data.CODIGO);
              $('[name="card"]').val(data.CREDENCIAL);
              $('[name="deparment"]').val(data.DEPARTAMENTO);
              $('[name="company"]').val(data.EMPRESA);
              $('[name="management"]').val(data.GERENCIA);
              $('[name="card_visit"]').val(data.NRO_TARJETA_VISITA);
              $('[name="rut_request"]').val(data.RUTSOLICITANTE);
              $('[name="physical_accountan"]').val(data.UBICACION_CONTABLE);
              $('[name="physical_location"]').val(data.UBICACION_FISICA);
              $('[name="area"]').val(data.ID_AREA).change();
              $('[name="profile"]').val(data.ID_PERFIL).change();
              $('[name="casino"]').val(data.ID_CASINO).change();
              $("#content_table").hide();
              $("#content_form").show(1000);
              $('#save').text('Editar');
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
             $.notify({
              icon: 'pe-7s-look',
              message: "<b>Error obtener datos</b>."
              },{
                  type: 'danger',
                  timer: 4000
              });
          },
          complete : function () {
              pf_unblockUI();
          }
      });
  }

  $("#save").click(function(){
    $('#save').text('Guardando...');
    $('#save').attr('disabled',true);
    var url;
    var datos;
    if(save_method == 'add') {
      url = "<?php echo site_url(); ?>/colaborador/add";
      datos = $( "form#form" ).serialize();
    } else {
      url = "<?php echo site_url(); ?>/colaborador/upd";
      datos = $( "form#form" ).serialize();
    }
    pf_blockUI();
    $.ajax({
      url : url,
      type: "POST",
      data: datos,
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#save').text('Guardando'); //change button text
            $('#save').attr('disabled',false); //set button enable
            reload_table();
            $("#action_hide").trigger("click");
            $.notify({
            icon: 'pe-7s-smile',
            message: "<b>Se han almacenados los datos exitosamente.</b>."
            },{
                type: 'info',
                timer: 4000
            });
          }else{
            $.notify({
            icon: 'pe-7s-look',
            message: "<b>Error al agregar o actualizar datos Verifique los datos</b>."
            },{
                type: 'danger',
                timer: 4000
            });
            $('#save').attr('disabled',false);
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        $.notify({
        icon: 'pe-7s-look',
        message: "<b>Error al agregar / actualizar datos</b>."
        },{
            type: 'danger',
            timer: 4000
        });
        $('#save').text('Guardar'); //change button text
        $('#save').attr('disabled',false); //set button enable
      },
      complete : function () {
          pf_unblockUI();
      }
    });
  });
</script>
<script>
  $('#input_credencial').on( 'keyup', function () {
    table.search( this.value ).draw();
  });
  $('#input_empresa').on( 'keyup', function () {
    table.search( this.value ).draw();
  });
</script>