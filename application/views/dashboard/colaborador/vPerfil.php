<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Colaborador Perfil
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>/ dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Colaborador Perfil</li>
      </ol>
    <!-- Salto de linea  -->
    </section>
    <!-- Contenido de la Page -->
    <section class="content">
      <div class="box box-primary box-solid">
          <form id="form" name="form" accept-charset="utf-8" action="">
              <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-user"></i> Perfil de <?php echo ucwords(mb_strtolower($this->session->name))?></h3>
              </div>
              <div class="box-body with-border">
                  <span class="pull-left margin-right-5">
                      <small class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> 
                  </span>
                  <div class="note">
                      <strong>Nota:</strong> Si desea recibir notificaciones por correo, es necesario que actualice o ingrese su correo institucional.
                  </div>
              </div>
              <div class="box-body">
                  <div class="row">
                      <div class="col-md-3">
                          <div class="form-group">
                              <label for="telefono">Correo</label>
                              <span class="pull-right-container"><small data-toggle="tooltip" data-placement="top" title="Ingrese el correo." class="label bg-aqua pull-right"> <i class="fa fa-info padding-top-3"></i></small> </span>
                              <input autocomplete="off" type="text" name="email" id="email" class="form-control" placeholder="EJ: felipe.ortiz@pf.cl" value="<?php echo $perfil_collaborator->email?>">
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                              <label for="rut">Rut</label>
                              <input disabled type="text" name="rut" class="form-control" value="<?php echo $perfil_collaborator->rut?>">
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                              <label for="card">Credencial</label>
                              <input disabled type="text" name="card" class="form-control" value="<?php echo $perfil_collaborator->card?>">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="area">Área</label>
                              <input disabled type="text" name="area" class="form-control" value="<?php echo $perfil_collaborator->area?>">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="deparment">Departamento</label>
                              <input disabled type="text" name="deparment" class="form-control" value="<?php echo $perfil_collaborator->deparment?>">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="physical_accountan">Ubicación Contable</label>
                              <input disabled type="text" name="physical_accountan" class="form-control" value="<?php echo $perfil_collaborator->physical_accountan?>">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="physical_location">Ubicacion Física</label>
                              <input disabled type="text" name="physical_location" class="form-control" value="<?php echo $perfil_collaborator->physical_location?>">
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group"> 
                              <label for="rut_request">Rut Solicitante</label>
                              <input disabled type="text" name="rut_request" class="form-control" value="<?php echo $perfil_collaborator->rut_request?>">
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="card_visit">Credencial de visita</label>
                              <input disabled type="text" name="card_visit" class="form-control" value="<?php echo $perfil_collaborator->card_visit?>">
                          </div>
                      </div>
                  </div>
              </div>
              <div class="box-footer">
                  <a href="<?php echo site_url(); ?>/dashboard" class="btn btn-default btn-flat pull-left"><strong>Volver</strong></a>
                  <button id="search_btn" type="button" class="btn btn-primary btn-flat pull-right"><strong>Actualizar Datos</strong></button>
              </div>
          </form>
      </div>
    </section>
    <!-- END contenido -->
</div>
<script>
  jQuery(document).ready(function($) {
        $('#form').validate({
        rules:{
        email: {
          required: true,
          email: true
        }
      },
      submitHandler: function(form){
        form.submit();
      },
      highlight: function(element){ //elemento donde esta posicionado
        $(element).parent().removeClass('has-success').addClass('has-error');
      },
      success: function(element){
        $(element).parent().removeClass('has-error').addClass('has-success');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error,element){
        if(element.parent('.input-group').length){
            error.insertAfter(element.parent());
        }else{
            error.insertAfter(element);
        }
      }
    });

    $("#search_btn").prop('disabled','disabled');

    $("#form").on('keyup blur',function(){
      if ($("#form").valid()) {
        $("#search_btn").prop('disabled',false);
      }else{
        $("#search_btn").prop('disabled','disabled');
      }
    });
  });
</script>
<script>
  $("#search_btn").click(function() {
    pf_blockUI();
    $.post('<?php echo site_url(); ?>/perfil_colaborador/upd', {
      email: $("#email").val(),
      password: $("#password").val(),
      password_re: $("#password_re").val()
    }, function(data) {
      if (data.status) {
        $.notify({
        icon: 'pe-7s-smile',
        message: '<b>'+data.menssage+'</b>'
        },{
            type: 'info',
            timer: 4000
        });
        window.setTimeout(function() {
            window.location.replace("<?php echo site_url(); ?>/");
        }, 3000);
      }else{
        $.notify({
            icon: 'pe-7s-look',
            message: '<b>'+data.menssage+'</b>'
            },{
                type: 'warning',
                timer: 1000
        });
      }
    },'json')
    .always(function() {
      pf_unblockUI();
    });
  });
</script>