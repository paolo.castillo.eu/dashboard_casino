<div class="content-wrapper">
	<section class="content-header">
		<h1>Mis Colaciones <small><b>Todos</b></small></h1>
		<!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Mis Colaciones</li>
        </ol>
        <!-- Salto de linea	 -->
    </section>
    <!-- Contenido de la Page -->
    <section class="content-header">
      <div class="row">
       <!-- Columna de 6 hasta 12 -->
       <div class="col-xs-12">
        <div class="box box-primary box-solid">
         <div class="box-header with-border">
         <h3 class="box-title">Colaciones de <em><?php echo ucwords(mb_strtolower($this->session->name))?></em>  <li class="fa fa-cutlery"></li></h3>
        </div>
        <div class="box-body">
            <div class="row" id="div_total">
                <?php foreach ($my_quantity as $my_quantitys) { ?>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box <?php echo $my_quantitys->class?>">
                      <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                      <div class="info-box-content">
                        <span class="info-box-text"><?php echo 'Casino : '.ucwords(mb_strtolower($my_quantitys->casino_q))?></span>
                        <span class="info-box-number"><?php echo 'Total Colación : '.$my_quantitys->total?></span>
                        <div class="progress">
                          <div class="progress-bar" style="width: 100%"></div>
                        </div>
                        <span class="progress-description">
                          <?php echo 'Última de Colación Solicitada : '.$my_quantitys->fecha?>
                        </span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
</div>
</div>
</div>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group margin-btm-5">
                            <label>Mostrar</label>
                            <select id="show_record" class="form-control">
                                <option value="10">10 registros</option>
                                <option value="25">25 registros</option>
                                <option value="50">50 registros</option>
                                <option value="100">100 registros</option>
                                <option value="-1">Todos los registros</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group margin-btm-5">
                            <label>Buscar</label>
                            <div class="input-group stylish-input-group">
                                <input type="text" class="form-control"  placeholder="Casino 1" id="search_input">
                                <span class="input-group-addon">
                                    <button type="submit">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>  
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group margin-btm-5">
                          <label>Filtro por fecha</label>
                          <div class="btn-group btn-group-justified">
                              <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                  <span>
                                  <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                  </span>
                                  <input type="hidden" name="id_fecha" value="id_fecha">
                                  <div class="pull-right">
                                      <i class="fa fa-caret-down"></i>
                                  </div>
                              </a>
                              <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                          </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <button class="btn btn-info btn-block" type="button" id="btn_search" style="margin-top: 25px">Aplicar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row table-responsive no-left-right-margin">
                            <div class="col-xs-12"> 
                                <table id="table" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                          <th >Planta</th>
                                          <th >Casino</th>
                                          <th >Colación</th>
                                          <th >Correlativo</th>
                                          <th >Estado</th>
                                          <th >Fecha</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                  </tbody>
                                  <tfoot>
                                      <tr>
                                        <th>Planta</th>
                                        <th>Casino</th>
                                        <th>Colación</th>
                                        <th>Correlativo</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                    </tr>
                                </tfoot>
                          </table>
                      </div>
                  </div>
                  <div class="padding-top-5"></div>
                  <div class="box-footer">
                    <a href="<?php echo site_url(); ?>/dashboard" class="btn btn-flat btn-default"><strong>Volver</strong></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
<!-- END contenido -->
</div>
<script>
    var table;
    $(function () {
        table = $("#table").DataTable({
            "responsive": true,
            ajax: {
                'url': "<?php echo site_url('get_mis_colacion'); ?>",
                'type': 'POST',
                'data': function ( d ) {
                    d.fecha = $('#daterange-btn span').text();
                }
            },
            processing: true,
            serverSide: true,
            // order: [[ 0, 'desc']],
            paging: true,
            info: true,
            responsive: true,
            dom: 'lfrtip',
            "aoColumnDefs": [
            { 
                "sType": "date-uk", 
                "aTargets": [4] 
            }, { targets: 'no-sort', orderable: false }
            ],
            "createdRow": function ( row, data, index ) {
              $('td', row).eq(0).addClass('text-center text-muted');
              $('td', row).eq(1).addClass('text-center text-muted');
              $('td', row).eq(2).addClass('text-center text-muted');
              $('td', row).eq(3).addClass('text-center text-muted');
              $('td', row).eq(4).addClass('text-center text-muted');
              $('td', row).eq(5).addClass('text-center text-muted');
              $('td', row).eq(6).addClass('text-center text-muted');
              var estado = '<?php echo json_encode($estado)?>';
              

              $.each(JSON.parse(estado),function(index, element) {
                console.log(element.id_colacion);
                if (element.id_colacion == '1') {
                    $(row).find('td:eq(4)').addClass('text-muted');
                }else{
                    $(row).find('td:eq(4)').addClass('text-muted');
                }
              });
            },
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }, 
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });

    jQuery("#footer").ready(function(){
        jQuery("#table_length").addClass('hidden');
        jQuery("#table_filter").addClass('hidden');
        jQuery("#table_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_info").text());
        jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
    });

    $('#search_input').keyup(function(){
        table.search($(this).val()).draw() ;
    })

    $('#show_record').click(function() {
        table.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_inc_info").text());
    });

    jQuery("#table").on("page.dt", function(){
        var info = table.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });
</script>
<script>
    $(document).ready(function(){
        $('#daterange-btn').daterangepicker(
        {
            opens: 'left',
            showDropdowns: true,
            buttonClasses: 'btn btn-flat btn-sm',
            applyClass: 'btn-primary',
            locale: {
                format:             "DD/MM/YYYY",
                separator:          " --- ",
                applyLabel:         "Aplicar",
                cancelLabel:        "Cancelar",
                fromLabel:          "Desde",
                toLabel:            "A",
                customRangeLabel:   "Definir rango",
                daysOfWeek: [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                monthNames: [
                    'Enero', 
                    'Febrero', 
                    'Marzo', 
                    'Abril', 
                    'Mayo', 
                    'Junio', 
                    'Julio', 
                    'Agosto', 
                    'Septiembre', 
                    'Octubre', 
                    'Noviembre', 
                    'Diciembre'
                ],
                firstDay: 1
            },
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Este Año': [moment().startOf('year'), moment().endOf('year')],
                'Año pasado': [moment().subtract(1, 'year').startOf('year'), moment(). subtract(1, 'year').endOf('year')]
                },
                
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                $('#daterange-btn input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                format_date(start.format('DD-MM-YYYY'), end.format('DD-MM-YYYY'));
        });
    });
</script>
<script>
    $('#clear_filter').click(function(){
        $('#daterange-btn span').html('<span><i class="fa fa-calendar"></i> Filtrar por rango de fechas</span>');
        $('#daterange-btn input').val('Filtrar por rango de fechas');
        format_date(moment().subtract(500, 'year').startOf('year').format('DD-MM-YYYY'), moment().format('DD-MM-YYYY'));
        filter();
    });
</script>
<script>
    $.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
        var x;
        try {
        var dateA = a.replace(/ /g, '').split("-");
        var day = parseInt(dateA[0], 10);
        var month = parseInt(dateA[1], 10);
        var year = parseInt(dateA[2], 10);
        var date = new Date(year, month - 1, day)
        x = date.getTime();
        }
        catch (err) {
        x = new Date().getTime();
        }

        return x;
        },

        "date-uk-asc": function (a, b) {
        return a - b;
        },

        "date-uk-desc": function (a, b) {
        return b - a;
        }
    });

    var sDate = '';
    var eDate = '';

    function format_date(startDate, endDate) {

        sDate = startDate;
        eDate = endDate;
    }

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex){
            var dateStart = parseDateValue(sDate);
            var dateEnd = parseDateValue(eDate);
            var evalDate = parseDateValue(aData[4]);
            if ((sDate === '' && eDate === '') || (evalDate >= dateStart && evalDate <= dateEnd)) {
                return true;
            }
            else {
                return false;
            }
    });
</script>
<script>
  function reload_table()
  {
      table.ajax.reload(); 
  }
  $('#btn_search').click(function () {
    reload_table();
  });
</script>