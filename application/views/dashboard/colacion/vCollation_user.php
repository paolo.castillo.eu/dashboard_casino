<div class="content-wrapper">
    <section class="content-header">
        <h1>Colaborador por Colación <small><b>Todos</b></small></h1>
        <!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Configuración</li>
        </ol>
        <!-- Salto de linea  -->
    </section>
    <!-- Contenido de la Page -->
    <section class="content" id="show_content">
        <div class="row">
            <!-- Columna de 6 hasta 12 -->
            <div class="col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registro de las colaciones por colaborador</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Mostrar</label>
                                    <select id="show_record" class="form-control">
                                        <option value="10">10 registros</option>
                                        <option value="25">25 registros</option>
                                        <option value="50">50 registros</option>
                                        <option value="100">100 registros</option>
                                        <option value="-1">Todos los registros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Buscar</label>
                                    <div class="input-group stylish-input-group">
                                        <input type="text" class="form-control"  placeholder="Rut 185469190" id="search_input">
                                        <span class="input-group-addon">
                                            <button type="submit">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>  
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row table-responsive no-left-right-margin">
                            <div class="col-xs-12"> 
                                <table id="table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Rut</th>
                                  <th>Nombre</th>
                                  <th>Credencial</th>
                                  <th>Funciones</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                  <th>#</th>
                                  <th>Rut</th>
                                  <th>Nombre</th>
                                  <th>Credencial</th>
                                  <th>Funciones</th>
                                </tr>
                                </tfoot>
                              </table>
                          </div>
                      </div>
                    <div class="padding-top-5"></div>
                    <div class="box-footer">
                        <a href="<?php echo site_url() ?>/dashboard" class="btn btn-flat btn-default"><strong>Volver</strong></a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contenido de la Page -->
    <section class="content" id="hide_content"  style="display: none">
        <div class="row">
            <!-- Columna de 6 hasta 12 -->
            <div class="col-xs-12">
                <div id="message">
                        
                </div>
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title" id="title_name_user"></h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <button id="action" class="btn btn-primary btn-block" aria-pressed="true">Agregar nueva Colación al colaborador</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i></button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Mostrar</label>
                                    <select id="show_record_collation" class="form-control">
                                        <option value="10">10 registros</option>
                                        <option value="25">25 registros</option>
                                        <option value="50">50 registros</option>
                                        <option value="100">100 registros</option>
                                        <option value="-1">Todos los registros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Buscar</label>
                                    <div class="input-group stylish-input-group">
                                        <input type="text" class="form-control"  placeholder="Almuerzo 1" id="search_input_collation">
                                        <span class="input-group-addon">
                                            <button type="submit">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>  
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <div class="row table-responsive no-left-right-margin">
                            <div class="col-xs-12"> 
                                <table id="table_collation" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                    <th>id_colaborador</th>
                                    <th>id_casino</th>
                                    <th>id_colacio</th>
                                    <th>Casino</th>
                                    <th>Tipo Servicio</th>
                                    <th>Colación</th>
                                    <th>Maximo Colación</th>
                                    <th>Eliminar</th>
                                    <th>Actualizar</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                    <th>id_colaborador</th>
                                    <th>id_casino</th>
                                    <th>id_colacio</th>
                                    <th>Casino</th>
                                    <th>Tipo Servicio</th>
                                    <th>Colación</th>
                                    <th>Maximo Colación</th>
                                    <th>Eliminar</th>
                                    <th>Actualizar</th>
                                    </tr>
                                    </tfoot>
                                  </table>
                            </div>
                        </div>
                    <div class="padding-top-5"></div>
                    <div class="box-footer">
                        <button id="action_hide" class="btn btn-flat btn-default">Volver</button> 
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END contenido -->
</div>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-title-my">Datos de Colacion</h4>
            </div>
            <div class="modal-body form">
                <form id="form" name="form" action="#">
                    <input type="hidden" name="id_colaborador" id="id_colaborador">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="id_casino_u" class="col-sm-2 control-label">Casino:</label>
                                    <select class="form-control" name="id_casino_u" id="id_casino_u">
                                        <option value="default">Seleccione una opción</option>
                                        <?php foreach($casino as $casinos) {?>
                                            <option value="<?php echo $casinos->ID_CASINO; ?>" 
                                            <?php echo set_select('casinos',  $casinos->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($casinos->NOMBRE)); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                         </div>
                         <div class="row">
                             <div class="col-md-12">
                                 <div class="form-group">
                                    <label for="id_typeService_u" class="col-sm-2 control-label">Tipo Servicio:</label>
                                    <select class="form-control" name="id_typeService_u" id="id_typeService_u">
                                        <option value="default">Seleccione una opción</option>
                                        <?php foreach($servicio as $servicios) {?>
                                            <option value="<?php echo $servicios->ID_TIPO_SERVICIO; ?>" 
                                            <?php echo set_select('servicios',  $servicios->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($servicios->NOMBRE)); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                             </div>
                         </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Colación:</label>
                                    <select class="form-control" name="id_Collation_u" id="id_Collation_u">
                                        <?php foreach ($colacion as $key): ?>
                                            <option value="<?php echo $key->ID_COLACION?>"><?php echo $key->NOMBRE?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                  <label for="maximo_colacion" class="col-sm-2 control-label">Maximo Colación:</label>
                                    <input id="maximo_colacion" name="maximo_colacion" class="slider form-control" type="text" data-slider-min="0" data-slider-max="30" data-slider-step="1" data-slider-value="0" data-slider-enabled="false"/>
                                    <input type="text" class="form-control" id="range_in" name="range_in" onKeyPress="return number(event)">
                                </div>
                            </div>
                        </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" id="save" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
     </div>
</div> 
<!--    End Modal    -->
<!-- Bootstrap modal -->
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Confirmar eliminación</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id_colaborador_del" id="id_colaborador_del">
                <input type="hidden" name="id_casino_del" id="id_casino_del">
                <input type="hidden" name="id_colacio_del" id="id_colacio_del">
                <p>Estás a punto de eliminar <b><i class="title title_collation"></i></b> del Colaborador seleccionado. Este procedimiento es irreversible.</p>
                <p>¿Quieres proceder?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat pull-left" data-dismiss="modal" id="btn_cancel">Cancelar</button>
                <button type="button" class="btn btn-danger btn-ok">Eliminar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("#id_casino_u").change(function() {
        var colacion_op = $("#id_Collation_u");
        var option = '';
        pf_blockUI();
        $("#id_casino_u option:selected").each(function() {
            inc_type = $('#id_casino_u').val();
            $.post('<?php echo site_url(); ?>/colacion_colaborador/get_by', {
                casino : inc_type
            }, function(data) {
                $.each(data.option,function(index, item) {
                    option = option + '<option value="'+item.values+'">'+item.text+'</option>';
                });
                // console.log(option);
                colacion_op.find('option')
                .remove()
                .end()
                .append(option);
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
    });
</script>
<script>
    $(document).ready(function(){
        $("#range_in").attr("maxlength", 2);
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script>
    $('#confirm-delete').on('click', '.btn-ok', function() {
        pf_blockUI();
        $.ajax({
            url : "<?php echo site_url(); ?>/colacion_colaborador/del",
            type: "POST",
            data: {
                id_colaborador:$("#id_colaborador_del").val(),
                id_casino:$("#id_casino_del").val(),
                id_colacio:$("#id_colacio_del").val()
            },
            dataType: "JSON",
            success: function(data)
            {
                $("#btn_cancel").prop('disable',true);
                if (data.status) {
                    $('#confirm-delete').modal('hide');
                    table_colaborador_collation($("#id_colaborador_del").val());
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>La operación se realizo correctamente</b>."
                    },{
                        type: 'info',
                        timer: 4000
                    });
                    $("#btn_cancel").prop('disable',false);
                } else {
                    $('#confirm-delete').modal('hide');
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>Error al borrar los datos del colaborador seleccionado</b>."
                    },{
                        type: 'danger',
                        timer: 4000
                    });    
                }
                if (data.message != '') {
                    $("#message").append(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#confirm-delete').modal('hide');
                $.notify({
                    icon: 'pe-7s-look',
                    message: "<b>Error al borrar los datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });              
            },
            complete : function () {
                pf_unblockUI();
            }
        });
    });

    $(function () {
        $(".select2").select2();
    });
</script> 
<script>
    var table;
    $(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url(); ?>/colaborador_colacion/get_user",
                "type": "POST"
            },
            "columnDefs": [
            {
                "targets": [ -1 ],
                "orderable": false,
            },
            ],
            "paging": true,
            "info": true,
            "autoWidth": true,
            "createdRow": function ( row, data, index ) {
                $('td', row).eq(3).addClass('text-center');
            },
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }, 
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });

    jQuery("#footer").ready(function(){
        jQuery("#table_length").addClass('hidden');
        jQuery("#table_filter").addClass('hidden');
        jQuery("#table_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_info").text());
        jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
    });

    $('#search_input').keyup(function(){
        table.search($(this).val()).draw() ;
    })

    $('#show_record').click(function() {
        table.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_inc_info").text());
    });

    jQuery("#table").on("page.dt", function(){
        var info = table.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });
</script>
<script>
    $('#maximo_colacion').slider({
        enabled: false,
        formatter: function(value) {
            return 'Current value: ' + value;
        }
    });

    var slider = new Slider('#maximo_colacion', {
        formatter: function(value) {
            return 'Current value: ' + value;
        }
    });

    var table_collation;
    var save_method;

    $(function () {
        table_collation = $("#table_collation").DataTable({
            "order": [],
            "columnDefs": [
                { "targets": -2,"data": null,"defaultContent": '<button id="btn_deleted" class="btn btn-danger btn-flat btn-xs"><i class="fa fa-trash"></i></button>' },
                { "targets": -1,"data": null,"defaultContent": '<button id="btn_edited" class="btn btn-primary btn-flat btn-xs"><i class="glyphicon glyphicon-pencil"></i> Editar</button>' },
                { targets: 'no-sort', orderable: false },
                { "targets": [ 0 ], "visible": false, "searchable": false },
                { "targets": [ 1 ], "visible": false, "searchable": false },
                { "targets": [ 2 ], "visible": false, "searchable": false },
            ], 
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "createdRow": function ( row, data, index ) {
                $('td', row).eq(0).addClass('text-center');
                $('td', row).eq(1).addClass('text-center');
                $('td', row).eq(2).addClass('text-center');
                $('td', row).eq(3).addClass('text-center');
                $('td', row).eq(4).addClass('text-center');
            },
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }, 
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });

    $('#table_collation tbody').on( 'click', '#btn_deleted', function () {
        var data = table_collation.row( $(this).parents('tr') ).data();
        $('.title_collation').text(data[5]);
        $('#id_colaborador_del').val(data[0]);
        $('#id_casino_del').val(data[1]);
        $('#id_colacio_del').val(data[2]);
        $('#confirm-delete').modal('show');
    } );

    $('#table_collation tbody').on( 'click', '#btn_edited', function () {
        var data = table_collation.row( $(this).parents('tr') ).data();
        var id_colaborador = data[0];
        var id_casino = data[1];
        var id_colacion = data[2];
        save_method = 'update';
        option = '';
        pf_blockUI();
        $.ajax({
            url : "<?php echo site_url(); ?>/colacion_colaborador/get",
            type: "POST",
            data: {
                id_collaborator : id_colaborador,
                id_casino : id_casino,
                id_colacion : id_colacion
            },
            dataType: "JSON",
            success: function(data)
            {
                $("#form").validate().resetForm();
                $("#form").find('.has-error').removeClass("has-error");
                $("#form").find('.has-success').removeClass("has-success");
                $('#form').find('.form-control-feedback').remove();
                var range = parseInt(data.max.MAXTIPOCOLACION);
                $.post('<?php echo site_url()?>/ticket_rechazado/get_colacion', {
                    casino : id_casino
                }, function(colacion) {
                    $.each(colacion.option,function(index, item) {
                        if (id_colacion == item.values) {
                            option = option + '<option value="'+item.values+'" selected>'+item.text+'</option>';
                        }else{
                            option = option + '<option value="'+item.values+'">'+item.text+'</option>';
                        }
                    });
                    $('#id_Collation_u').find('option')
                    .remove()
                    .end()
                    .append(option);
                },'json');
                $('#id_colaborador').val(id_colaborador);
                $('#id_casino_u').val(id_casino);
                $('#id_casino_u').css('pointer-events','none');
                $('#id_casino_u').css("background-color", "#E0E0E0");                
                // ------------------------------------------------------------ //
                $('#id_typeService_u').val(data.max.ID_TIPO_SERVICIO).change();
                slider.setValue(range);
                $('#range_in').val(range);
                // ------------------------------------------------------------ //
                // $('#id_Collation_u').val(id_colacion);
                $('#id_Collation_u').css('pointer-events','none');
                $('#id_Collation_u').css("background-color", "#E0E0E0");
                // ------------------------------------------------------------ //
                $('#save').text('Editar');
                $('modal-title-my').text('Actualizar colación del Colaborador');
                // ------------------------------------------------------------ //
                // setTimeout(function () {
                //     $('#id_Collation_u').val(id_colacion).change();
                // },1000);
                $('#modal_form').modal('show'); 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               $.notify({
                icon: 'pe-7s-look',
                message: "<b>Error obtener datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });
            },
            complete : function () {
                pf_unblockUI();
            }
        });
    } );

    function reload_table()
    {
        table_collation.data.refresh;
    }


    jQuery("#footer").ready(function(){
        jQuery("#table_collation_length").addClass('hidden');
        jQuery("#table_collation_filter").addClass('hidden');
        jQuery("#table_collation_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_collation_info").text());
        jQuery("#table_collation_paginate").appendTo(jQuery("#footer-right"));
    });
    
    $('#search_input_collation').keyup(function(){
        table_collation.search($(this).val()).draw() ;
    })

    $('#show_record_collation').click(function() {
        table_collation.page.len($('#show_record_collation').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_inc_info").text());
    });

    jQuery("#table_collation").on("page.dt", function(){
        var info = table_collation.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });
    
    function table_colaborador_collation(step) {
        $("#show_content").hide();
        table_collation.clear().draw();;
        var mensaje = "";
        var title = "";
        pf_blockUI();
        $.post(
            "<?php echo site_url(); ?>/colacion_colaborador/get_collation",
        {
            id_collaborator:step  
        },
        function(data) {
            $.each(data,function(index, rows) {
                table_collation.row.add( [
                    rows.id_colaborador,
                    rows.id_casino,
                    rows.id_colacion,   
                    rows.casino,
                    rows.tipo_servicio,
                    rows.colacion,
                    rows.maxtipocolacion
                ] ).draw().node();
                mensaje = rows.mensaje;
                title = rows.nombre;
                $('#id_colaborador').val(rows.id_colaborador);
            });
            $("#title_name_user").text(title);
            $("#message").append(mensaje);
        },'json')
        .always(function() {
            pf_unblockUI();
        });
        $("#hide_content").show(1000);
    }

    $("#action_hide").click(function(){
        $("#hide_content").hide();
        $("#show_content").show(1000);
    });

    $("#action").click(function() {
        save_method = 'add';
        slider.setValue(0);
        var validator = $('#form').validate(),
        id = '';
        validator.resetForm();
        $("#form").find('.has-error').removeClass("has-error");
        $("#form").find('.has-success').removeClass("has-success");
        $('#form').find('.form-control-feedback').remove();
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#id_casino_u').css('pointer-events','auto');
        $('#id_Collation_u').css('pointer-events','auto');
        // table_collation.columns([0]).visible(false);
        // for (var i=0;i<10;i++) {
        //    id= table_collation.cells({ row: i, column: 0 }).data()[0];
        // }  
        // $('#id_colaborador').val(id);
        $('#modal_form').modal('show');
        $('#modal-title-my').text('Agregar Colación');
        $('#id_Collation_u').css("background-color", "#FAFAFA");
        $('#id_casino_u').css("background-color", "#FAFAFA");
        $('#save').text('Agregar');
    });
    
    $('.slider').on('slide', function (ev) {
        var value = slider.getValue();
        $('#range_in').val(value);
        // console.log(value);
    });

    $('#range_in').on('input', function() {
        var input = parseInt($('#range_in').val());
        slider.setValue(input);
    });

    $("#save").click(function () {
        $('#save').text('Guardando...'); //change button text
        // $('#save').attr('disabled',true); //set button disable
        var url;
        var datos;
        var range = slider.getValue();
        if(save_method == 'add') {
            url = "<?php echo site_url(); ?>/colacion_colaborador/add";
        } else {
            url = "<?php echo site_url(); ?>/colacion_colaborador/upd";
        }
        pf_blockUI();
        $.ajax({
            url : url,
            type: "POST",
            data: {
                id_colaborador : $('#id_colaborador').val(),
                id_casino_u : $('#id_casino_u').val(),
                id_typeService_u: $('#id_typeService_u').val(),
                id_Collation_u : $('#id_Collation_u').val(),
                maximo_colacion : range
            },
            dataType: "JSON",
            success: function(data)
            {
                if (data.success) {
                    $('#save').text('Guardando'); 
                    var step = $('#id_colaborador').val();
                    table_colaborador_collation(step);
                    reload_table();
                    $('#modal_form').modal('hide');
                    $.notify({
                        icon: 'fa fa-check-circle',
                        title: "<strong> Editar Colación </strong> <br/>",
                        message: data.text
                    }, {
                            type: 'success',
                            showProgressbar: false,
                            placement: {
                            from: "bottom",
                            align: "right"
                        },
                            delay: 4000,
                            timer: 3000,
                            z_index: 9999,
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                }else{
                    // console.log('ok');
                    $('#save').text('Repetir...');
                    $.each(data.menssage,function(key, value) {
                        var element = $('#' + key);
                        element.closest('div.form-group')
                        .removeClass('has-success')
                        .removeClass('has-error')
                        .addClass(value.length > 0 ? 'has-error' : 'has-success')
                        .find('.text-danger')
                        .remove();
                    });
                    if (data.text.length) {
                        $.notify({
                            icon: 'fa fa-close',
                            title: "<strong> Editar Colación </strong> <br/>",
                            message: data.text
                        }, {
                                type: 'danger',
                                showProgressbar: false,
                                placement: {
                                from: "bottom",
                                align: "right"
                            },
                                delay: 4000,
                                timer: 3000,
                                z_index: 9999,
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               $.notify({
                icon: 'pe-7s-look',
                message: "<b>Error al agregar / actualizar datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });
                $('#save').text('Guardar'); //change button text
                $('#save').attr('disabled',false); //set button enable
            },
            complete : function () {
                pf_unblockUI();
            }
        });
    });

    function number(e)
    {
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
        return true;

        return /\d/.test(String.fromCharCode(keynum));
    }
</script>