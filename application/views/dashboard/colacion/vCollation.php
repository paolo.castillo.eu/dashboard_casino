<style type="text/css" media="screen">
    .ui-timepicker-container{ 
     z-index:999999999 !important; 
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Colaciones <small><b>Todos</b></small></h1>
        <!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Colación</li>
        </ol>
        <!-- Salto de linea  -->
    </section>
    <!-- Contenido de la Page -->
    <section class="content">
        <div class="row">
            <!-- Columna de 6 hasta 12 -->
            <div class="col-xs-12">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registro de las Colaciones</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <button id="action" class="btn btn-primary btn-block" aria-pressed="true">Agregar nueva Colación</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i></button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Mostrar</label>
                                    <select id="show_record" class="form-control">
                                        <option value="10">10 registros</option>
                                        <option value="25">25 registros</option>
                                        <option value="50">50 registros</option>
                                        <option value="100">100 registros</option>
                                        <option value="-1">Todos los registros</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Buscar</label>
                                    <div class="input-group stylish-input-group">
                                        <input type="text" class="form-control"  placeholder="Colación 1" id="search_input">
                                        <span class="input-group-addon">
                                            <button type="submit">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>  
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row table-responsive no-left-right-margin">
                        <div class="col-xs-12"> 
                        <table id="table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                          <th>Código</th>
                          <th>Nombre</th>
                          <th>Tipo Colación</th>
                          <th>Casino</th>
                          <th>Total</th>
                          <th>Costo</th>
                          <th>Fecha de Vigencia inicial</th>
                          <th>Fecha de Vigencia final</th>
                          <th>Hora de corte Solicitud</th>
                          <th>Hora de inicio</th>
                          <th>Hora de fin</th>
                          <th>Min. Inicio</th>
                          <th>Min. Termino</th>
                          <th>Funciones</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                        <tr>
                          <th>Código</th>
                          <th>Nombre</th>
                          <th>Tipo Colación</th>
                          <th>Casino</th>
                          <th>Total</th>
                          <th>Costo</th>
                          <th>Fecha de Vigencia inicial</th>
                          <th>Fecha de Vigencia final</th>
                          <th>Hora de corte Solicitud</th>
                          <th>Hora de inicio</th>
                          <th>Hora de fin</th>
                          <th>Min. Inicio</th>
                          <th>Min. Termino</th>
                          <th>Funciones</th>
                        </tr>
                        </tfoot>
                      </table>
                      </div>
                      </div>
                    <div class="padding-top-5"></div>
                    <div class="box-footer">
                        <a href="<?php echo site_url() ?>/dashboard" class="btn btn-flat btn-default"><strong>Volver</strong></a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END contenido -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-title-my">Datos del Colación</h4>
            </div>
            <div class="modal-body form">
                <form id="form" name="form" action="#">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-1 control-label" for="codigo">Código</label>
                            <div class="col-sm-3">
                              <input type="text" placeholder="3200" class="form-control" id="codigo" name="codigo">
                            </div>
                            <label class="col-sm-1 control-label" for="name">Nombre</label>
                            <div class="col-sm-3">
                              <input type="text" placeholder="Cena Nocturna" class="form-control" id="name" name="name">
                            </div>
                            <label class="col-sm-1 control-label" for="type">Tipo Colación</label>
                            <div class="col-sm-3">
                              <!-- <input type="text" placeholder="Cena" class="form-control" id="type" name="type"> -->
                              <select class="form-control" id="type" name="type">
                                  <option value="DESAYUNO">Desayuno</option>
                                  <option value="ALMUERZO">Almuerzo</option>
                                  <option value="ONCE">Once</option>
                                  <option value="CENA">Cena</option>
                                  <option value="CENA_NOCTURNA">Cena Nocturna</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-1 control-label" for="textinput">Casino</label>
                            <div class="col-sm-3">
                            <select class="form-control" id="casino" name="casino">
                              <option select value="default">Seleccione Casino</option>
                                <?php foreach($casino as $casinos) {?>
                                  <option value="<?php echo $casinos->ID_CASINO;?>" 
                                  <?php echo set_select('casinos',  $casinos->NOMBRE); ?>>
                                  <?php echo ucwords(mb_strtolower($casinos->NOMBRE)); ?>
                                  </option>
                                <?php } ?>
                            </select>
                            </div>
                            <label class="col-sm-1 control-label" for="version">Total Col. Día</label>
                            <div class="col-sm-3">
                              <input type="text" placeholder="16" class="form-control" id="version" name="version">
                            </div>
                            <label class="col-sm-1 control-label" for="preci">Costo Colación</label>
                            <div class="col-sm-3">
                                  <input type="text" class="form-control" id="preci" name="preci" aria-label="Amount (to the nearest dollar)" placeholder="1300">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-1 control-label" for="date_ini">Fecha Inicial</label>
                            <div class="col-sm-3">
                              <input type="text"  class="form-control" id="date_ini" name="date_ini" placeholder="dd/mm/yyyy">
                            </div>
                            <label class="col-sm-1 control-label" for="date_end">Fecha Termino</label>
                            <div class="col-sm-3">
                              <input type="text"  class="form-control" id="date_end" name="date_end" placeholder="dd/mm/yyyy">
                            </div>
                            <label class="col-sm-1 control-label" for="hour_cut">Hora Corte</label>
                            <div class="col-sm-3">
                                  <input  type="text" id="hour_cut" name="hour_cut" class="form-control timepicker" value="00:00">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group"> 
                            <label class="col-sm-2 control-label" for="hour_ini">Hora de inicio</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                  <input  type="text" id="hour_ini" name="hour_ini" class="form-control timepicker" value="00:00">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label" for="hour_end">Hora de Termino</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                  <input  type="text" id="hour_end" name="hour_end" class="form-control timepicker" value="00:00">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group"> 
                            <label class="col-sm-2 control-label" for="min_ini">Minuto de Inicio de Colación</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                  <input  type="text" id="min_ini" name="min_ini" class="form-control" value="">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label" for="min_end">Minuto de Termino de Colación</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                  <input  type="text" id="min_end" name="min_end" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" id="save" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
     </div>
</div> 
<!--    End Modal           -->
<!-- Bootstrap modal -->
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Confirmar eliminación</h4>
            </div>
            <div class="modal-body">
                <p>Estás a punto de eliminar <b><i class="title"></i></b>. Este procedimiento es irreversible.</p>
                <p>¿Quieres proceder?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat pull-left" data-dismiss="modal" id="btn_cancel">Cancelar</button>
                <button type="button" class="btn btn-danger btn-ok">Eliminar</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal --> 
<script>
    $('#confirm-delete').on('click', '.btn-ok', function(e) {
        var $modalDiv = $(e.delegateTarget);
        var id = $(this).data('recordId');
        pf_blockUI();
        $.ajax({
            url : "<?php echo site_url(); ?>/colacion/del/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $("#btn_cancel").prop('disable',true);
                if (data.status) {
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>La operación se realizo correctamente</b>."
                    },{
                        type: 'info',
                        timer: 4000
                    });
                    reload_table();
                    $("#btn_cancel").prop('disable',false);
                } else {
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>Error al borrar los datos</b>."
                    },{
                        type: 'danger',
                        timer: 4000
                    });    
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $.notify({
                    icon: 'pe-7s-look',
                    message: "<b>Error al borrar los datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });              
            },
            complete : function () {
                pf_unblockUI();
            }
        });
        $modalDiv.addClass('loading');
        setTimeout(function() {
            $modalDiv.modal('hide').removeClass('loading');
        }, 1000)
    });
    $(function () {
        $(".select2").select2();
    }); 
    $('#confirm-delete').on('show.bs.modal', function(e) {
        var data = $(e.relatedTarget).data();
        $('.title', this).text(data.recordTitle);
        $('.btn-ok', this).data('recordId', data.recordId);
    });
</script> 
<script>
    $(document).ready(function(){
        $('#type').val('DESAYUNO').trigger('change');
        $('[data-toggle="tooltip"]').tooltip(); 
        $("#total").attr("maxlength", 4);
        $('#form').validate({
          rules:{
            codigo: {
                required: true,
                maxlength: 30,
                minlength:2,
                number: true
            },
            name: {
                required: true,
                maxlength: 255,
                minlength: 2,
                alphanumeric: true
            },
            casino:{
                valueNotEquals: "default"
            },
            version: {
                required: true,
                number: true,
                minlength:1
            },
            preci: {
                required: true,
                number: true,
                maxlength: 19,
                minlength:2
            },
            date_ini: {
                required: true,
            },
            date_end: {
                required: true,
            },
            hour_cut: {
                required: true
            },
            hour_ini:{
                required: true
            },
            hour_end:{
                required: true
            },
            min_ini: {
                required: true,
                number: true
            },
            min_end: {
                required: true,
                number: true
            }
          },
          submitHandler: function(form){
            form.submit();
          },
          highlight: function(element){ //elemento donde esta posicionado
            $(element).parent().removeClass('has-success').addClass('has-error');
          },
          success: function(element){
            $(element).parent().removeClass('has-error').addClass('has-success');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error,element){
            if(element.parent('.input-group').length){
                error.insertAfter(element.parent());
            }else{
                error.insertAfter(element);
            }
          }
        });

        $("#save").prop('disabled','disabled');
        $("#form").on('keyup blur',function(){
          if ($("#form").valid()) {
            $("#save").prop('disabled',false);
          }else{
            $("#save").prop('disabled','disabled');
          }
        });
    });
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>
<script>
    var table;
    $(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "keys": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url(); ?>/colacion/get",
                "type": "POST"
            },
            "columnDefs": [
            {
                "targets": [ -1 ],
                "orderable": false,
            },
            ],
            "createdRow": function ( row, data, index ) {
                $('td', row).eq(0).addClass('text-center');
                $('td', row).eq(1).addClass('text-center');
                $('td', row).eq(2).addClass('text-center');
                $('td', row).eq(3).addClass('text-center');
                $('td', row).eq(4).addClass('text-center');
                $('td', row).eq(5).addClass('text-center');
                $('td', row).eq(6).addClass('text-center');
                $('td', row).eq(7).addClass('text-center');
                $('td', row).eq(8).addClass('text-center');
                $('td', row).eq(9).addClass('text-center');
                $('td', row).eq(10).addClass('text-center');
                $('td', row).eq(11).addClass('text-center');
                $('td', row).eq(12).addClass('text-center');
                $('td', row).eq(13).addClass('text-center');
                $('td', row).eq(14).addClass('text-center');

            // $(row).find('td:eq(6)').addClass('bg-purple disabled color-palette');
            },
            "paging": true,
            "info": true,
            "autoWidth": true,
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }, 
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    jQuery("#footer").ready(function(){
        jQuery("#table_length").addClass('hidden');
        jQuery("#table_filter").addClass('hidden');
        jQuery("#table_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_info").text());
        jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
    });

    $('#search_input').keyup(function(){
        table.search($(this).val()).draw() ;
    })

    $('#show_record').click(function() {
        table.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_inc_info").text());
    });

    jQuery("#table").on("page.dt", function(){
        var info = table.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });
</script>
<script>
    var save_method; //for save method string
 
    $("#action").click(function() {
        save_method = 'add';
        var validator = $('#form').validate();
        validator.resetForm();
        $("#form").find('.has-error').removeClass("has-error");
        $("#form").find('.has-success').removeClass("has-success");
        $('#form').find('.form-control-feedback').remove();
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('[name="codigo"]').prop('readonly', false);
        $('#modal_form').modal('show');
        $('#modal-title-my').text('Agregar Colación');
        $('#save').text('Agregar');
    });

    function updateCollation(step) {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        pf_blockUI();
        $.ajax({
            url : "<?php echo site_url(); ?>/colacion/get/"+step,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $("#form").validate().resetForm();
                $("#form").find('.has-error').removeClass("has-error");
                $("#form").find('.has-success').removeClass("has-success");
                $('#form').find('.form-control-feedback').remove();
                $('[name="codigo"]').val(data.ID_COLACION).prop('readonly', true);
                $('[name="name"]').val(data.NOMBRE);
                $('[name="type"]').val(data.TIPO_COLACION);
                $('[name="casino"]').val(data.ID_CASINO);
                $('[name="version"]').val(data.VERSION);
                $('[name="preci"]').val(data.COSTO);
                $('[name="date_ini"]').val(data.FECHA_VIGENCIA_INICIAL);
                $('[name="date_end"]').val(data.FECHA_VIGENCIA_FINAL);
                $('[name="hour_cut"]').val(data.CORTE_SOLICITUD);
                $('[name="hour_ini"]').val(data.HORA_INICIO);
                $('[name="hour_end"]').val(data.HORA_FIN);
                $('[name="min_ini"]').val(data.MIN_INI);
                $('[name="min_end"]').val(data.MIN_END);

                $('#save').text('Editar');
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('modal-title-my').text('Editar Colación'); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               $.notify({
                icon: 'pe-7s-look',
                message: "<b>Error obtener datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });
            },
            complete : function () {
                pf_unblockUI();
            }
        });
    }

    $("#save").click(function () {
        $('#save').text('Guardando...'); //change button text
        $('#save').attr('disabled',true); //set button disable
        var url;
        var datos;
        if(save_method == 'add') {
            url = "<?php echo site_url(); ?>/colacion/add";
            datos = $( "form#form" ).serialize();
        } else {
            url = "<?php echo site_url(); ?>/colacion/upd";
            datos = $( "form#form" ).serialize();
        }
        pf_blockUI();
        $.ajax({
            url : url,
            type: "POST",
            data: datos,
            dataType: "JSON",
            success: function(data)
            {
                if (data.status) {
                    $('#save').text('Guardando'); //change button text
                    $('#save').attr('disabled',false); //set button enable
                    reload_table();
                    $('#modal_form').modal('hide');
                    $.notify({
                    icon: 'pe-7s-smile',
                    message: "<b>Se han almacenados los datos exitosamente.</b>."
                    },{
                        type: 'info',
                        timer: 4000
                    });
                }else{
                   $('#modal_form').modal('hide');
                   $.notify({
                    icon: 'pe-7s-look',
                    message: "<b>Error al crear el área, ya existe.</b>."
                    },{
                        type: 'danger',
                        timer: 4000
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               $.notify({
                icon: 'pe-7s-look',
                message: "<b>Error al agregar / actualizar datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });
                $('#save').text('Guardar'); //change button text
                $('#save').attr('disabled',false); //set button enable
            },
            complete : function () {
                pf_unblockUI();
            }
        });
    });
</script>
<script>
    var $confModal = $("#modal_form");
    var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function () { };
    $confModal.on('hidden', function () {
        $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
    });

    $(".timepicker").timepicker({
        minuteStep: 1,
        secondStep: 5,
        showInputs: false,
        template: 'modal',
        modalBackdrop: true,
        showMeridian: false
    });

    $(function () {
        $('#date_ini').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('#date_end').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('#date_end').datepicker('setStartDate', null);
        });

        $('#date_end').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            todayHighlight: true,
        }).on('changeDate', function (selected) {
            var endDate = new Date(selected.date.valueOf());
            $('#date_ini').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
            $('#date_ini').datepicker('setEndDate', null);
        });

    });
</script>

