<div class="content-wrapper">
	<section class="content-header">
		<h1>Perfiles <small><b>Todos</b></small></h1>
		<!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Perfiles</li>
        </ol>
		<!-- Salto de linea	 -->
	</section>
	<!-- Contenido de la Page -->
	<section class="content">
		<div class="row">
			<!-- Columna de 6 hasta 12 -->
        	<div class="col-xs-12">
          		<div class="box box-primary box-solid">
        			<div class="box-header with-border">
              			<h3 class="box-title">Registro de los Perfiles</h3>
            		</div>
            		<div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <button id="action" class="btn btn-primary btn-block" aria-pressed="true">Agregar nuevo Perfil</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i></button>
                            </div>
                        </div>
                        <br>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group margin-btm-5">
                                	<label>Mostrar</label>
                                	<select id="show_record" class="form-control">
                                    	<option value="10">10 registros</option>
                                    	<option value="25">25 registros</option>
                                    	<option value="50">50 registros</option>
                                    	<option value="100">100 registros</option>
                                    	<option value="-1">Todos los registros</option>
                                	</select>
                            	</div>
							</div>
							<div class="col-md-6">
								<div class="form-group margin-btm-5">
                                	<label>Buscar</label>
                                    <div class="input-group stylish-input-group">
                                        <input type="text" class="form-control"  placeholder="Perfil 1" id="search_input">
                                        <span class="input-group-addon">
                                            <button type="submit">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>  
                                        </span>
                                    </div>
                           		</div>
							</div>
						</div>
                        <div class="row table-responsive no-left-right-margin">
                        <div class="col-xs-12"> 
		            	<table id="table" class="table table-bordered table-hover">
		                <thead>
		                <tr>
		                  <th>#</th>
                          <th>Nombre</th>
		                  <th>Funciones</th>
		                </tr>
		                </thead>
		                <tbody>

		                </tbody>
		                <tfoot>
	            	    <tr>
                          <th>#</th>
                          <th>Nombre</th>
                          <th>Funciones</th>
		                </tr>
		                </tfoot>
		              </table>
                      </div>
                      </div>
                    <div class="padding-top-5"></div>
                    <div class="box-footer">
                        <a href="<?php echo site_url() ?>/dashboard" class="btn btn-flat btn-default"><strong>Volver</strong></a>
                    </div>
	              	</div>
            	</div>
            </div>
		</div>
	</section>
	<!-- END contenido -->
</div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-title-my">Datos del Perfil</h4>
            </div>
            <div class="modal-body form">
                <form id="form" name="form" class="form-horizontal" action="#">
                    <input type="hidden" name="id_perfil" id="id_perfil">
                    <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Nombre:</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" id="name">
                      </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" id="save" class="btn btn-primary">Actualizar</button>
            </div>
        </div>
     </div>
</div> 
<!--    End Modal           -->
<!-- Bootstrap modal -->
<div class="modal modal-danger fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Confirmar eliminación</h4>
            </div>
            <div class="modal-body">
                <p>Estás a punto de eliminar <b><i class="title"></i></b>. Este procedimiento es irreversible.</p>
                <p>¿Quieres proceder?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat pull-left" data-dismiss="modal" id="btn_cancel">Cancelar</button>
                <button type="button" class="btn btn-danger btn-ok">Eliminar</button>
            </div>
        </div>
    </div>
</div>
<!-- END modal --> 
<script>
    $('#confirm-delete').on('click', '.btn-ok', function(e) {
        var $modalDiv = $(e.delegateTarget);
        var id = $(this).data('recordId');
        pf_blockUI();
        $.ajax({
            url : "<?php echo site_url(); ?>/perfiles/del/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $("#btn_cancel").prop('disable',true);
                if (data.status) {
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>La operación se realizo correctamente</b>."
                    },{
                        type: 'info',
                        timer: 4000
                    });
                    reload_table();
                    $("#btn_cancel").prop('disable',false);
                } else {
                    $.notify({
                        icon: 'pe-7s-look',
                        message: "<b>Error al borrar los datos</b>."
                    },{
                        type: 'danger',
                        timer: 4000
                    });    
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $.notify({
                    icon: 'pe-7s-look',
                    message: "<b>Error al borrar los datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });              
            },
            complete : function () {
                pf_unblockUI();
            }
        });
        $modalDiv.addClass('loading');
        setTimeout(function() {
            $modalDiv.modal('hide').removeClass('loading');
        }, 1000)
    });
    $(function () {
        $(".select2").select2();
    }); 
    $('#confirm-delete').on('show.bs.modal', function(e) {
        var data = $(e.relatedTarget).data();
        $('.title', this).text(data.recordTitle);
        $('.btn-ok', this).data('recordId', data.recordId);
    });
</script> 
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
        $("#total").attr("maxlength", 4);
        $('#form').validate({
          rules:{
            name: {
                required: true,
                maxlength: 30,
                minlength: 2
            },
            id_perfil:{
                required: true,
                number: true,
                maxlength: 20,
                minlength: 2
            }
          },
          submitHandler: function(form){
            form.submit();
          },
          highlight: function(element){ //elemento donde esta posicionado
            $(element).parent().removeClass('has-success').addClass('has-error');
          },
          success: function(element){
            $(element).parent().removeClass('has-error').addClass('has-success');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error,element){
            if(element.parent('.input-group').length){
                error.insertAfter(element.parent());
            }else{
                error.insertAfter(element);
            }
          }
        });

        $("#save").prop('disabled','disabled');
        $("#form").on('keyup blur',function(){
          if ($("#form").valid()) {
            $("#save").prop('disabled',false);
          }else{
            $("#save").prop('disabled','disabled');
          }
        });
    });
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>
<script>
    var table;
    $(function () {
        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url(); ?>/perfiles/get",
                "type": "POST"
            },
            "columnDefs": [
            {
                "targets": [ -1 ],
                "orderable": false,
            },
            ],
            "paging": true,
            "info": true,
            "autoWidth": true,
            "createdRow": function ( row, data, index ) {
                $('td', row).eq(2).addClass('text-center');
            },
            "language": {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }, 
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    jQuery("#footer").ready(function(){
        jQuery("#table_length").addClass('hidden');
        jQuery("#table_filter").addClass('hidden');
        jQuery("#table_info").addClass('hidden');
        jQuery("#footer-left").text(jQuery("#table_info").text());
        jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
    });

    $('#search_input').keyup(function(){
        table.search($(this).val()).draw() ;
    })

    $('#show_record').click(function() {
        table.page.len($('#show_record').val()).draw();
        jQuery("#footer-left").text(jQuery("#table_inc_info").text());
    });

    jQuery("#table").on("page.dt", function(){
        var info = table.page.info();
        jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
    });
</script>
<script>
    var save_method; //for save method string
 
    $("#action").click(function() {
        save_method = 'add';
        var validator = $('#form').validate();
        validator.resetForm();
        $("#form").find('.has-error').removeClass("has-error");
        $("#form").find('.has-success').removeClass("has-success");
        $('#form').find('.form-control-feedback').remove();
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('#modal-title-my').text('Agregar Perfil');
        $('#save').text('Agregar');
    });

    function updatePerfil(step) {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        pf_blockUI();
        $.ajax({
            url : "<?php echo site_url(); ?>/perfiles/get/"+step,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $("#form").validate().resetForm();
                $("#form").find('.has-error').removeClass("has-error");
                $("#form").find('.has-success').removeClass("has-success");
                $('#form').find('.form-control-feedback').remove();
                $('[name="name"]').val(data.NOMBRE);
                $('[name="id_perfil"]').val(data.ID_PERFIL);
                $('#save').text('Editar');
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('modal-title-my').text('Editar Casino'); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               $.notify({
                icon: 'pe-7s-look',
                message: "<b>Error obtener datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });
            },
            complete : function () {
                pf_unblockUI();
            }
        });
    }

    $("#save").click(function () {
        $('#save').text('Guardando...'); //change button text
        $('#save').attr('disabled',true); //set button disable
        var url;
        var datos;
        if(save_method == 'add') {
            url = "<?php echo site_url(); ?>/perfiles/add";
            datos = $( "form#form" ).serialize();
        } else {
            url = "<?php echo site_url(); ?>/perfiles/upd";
            datos = $( "form#form" ).serialize();
        }
        pf_blockUI();
        $.ajax({
            url : url,
            type: "POST",
            data: datos,
            dataType: "JSON",
            success: function(data)
            {
                if (data.status) {
                    $('#save').text('Guardando'); //change button text
                    $('#save').attr('disabled',false); //set button enable
                    reload_table();
                    $('#modal_form').modal('hide');
                    $.notify({
                    icon: 'pe-7s-smile',
                    message: "<b>Se han almacenados los datos exitosamente.</b>"
                    },{
                        type: 'info',
                        timer: 4000
                    });
                }else{
                   $('#modal_form').modal('hide');
                   $.notify({
                    icon: 'pe-7s-look',
                    message: "<b>Error al crear el área, ya existe.</b>"
                    },{
                        type: 'danger',
                        timer: 4000
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
               $.notify({
                icon: 'pe-7s-look',
                message: "<b>Error al agregar / actualizar datos</b>."
                },{
                    type: 'danger',
                    timer: 4000
                });
                $('#save').text('Guardar'); //change button text
                $('#save').attr('disabled',false); //set button enable
            },
            complete : function () {
                pf_unblockUI();
            }
        });
    });
</script>

