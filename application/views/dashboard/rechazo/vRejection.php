<div class="content-wrapper">
  <section class="content-header">
    <h1>Ticket Rechazados <small><b>Todos</b></small></h1>
    <!-- Link de localización -->
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Ticket Rechazados</li>
        </ol>
    <!-- Salto de linea  -->
  </section>
  <!-- Contenido de la Page -->
  <section class="content-header">
    <div class="row">
      <!-- Columna de 6 hasta 12 -->
          <div class="col-xs-12">
              <div class="box box-danger box-solid">
              <div class="box-header with-border">
                    <h3 class="box-title">Registro de los Ticket Rechazados <li class="fa fa-hand-stop-o"></li></h3>
                        <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        </div>
                </div>
                <form method="POST" action="<?php echo site_url()?>/export_excel_data_r">
                <?php if ($this->session->profile_id == 4) :?>
                <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group margin-btm-5">
                                    <label>Planta</label>
                                    <select class="form-control selectpicker" name="id_planta" id="id_planta" data-style="btn-danger">
                                        <option value="">Todos</option>
                                        <?php foreach ($planta as $key): ?>
                                            <option value="<?php echo $key->ID_PLANTA?>"><?php echo ucwords(mb_strtolower($key->NOMBRE))?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group margin-btm-5">
                                    <label>Casino</label>
                                    <select class="form-control selectpicker" name="id_casino" id="id_casino" data-style="btn-danger">
                                        <option value="">Todos</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group margin-btm-5">
                                    <label>Colación</label>
                                    <select class="form-control selectpicker" name="id_colacion" id="id_colacion" data-style="btn-danger">
                                        <option value="">Todos</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Filtro por Empresa:</label>
                                      <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-danger">
                                        <option value="">Todos</option>
                                        <?php foreach ($empresa as $nombre): ?>
                                          <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                            <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                          <?php else :?>
                                            <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                          <?php endif ?>
                                        <?php endforeach ?>
                                      </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Estado del ticket</label>
                                    <select class="form-control selectpicker" name="id_estado" id="id_estado" data-style="btn-danger">
                                        <option value="">Todos</option>
                                        <?php foreach($estado as $estados) {?>
                                            <option value="<?php echo $estados->ID_ESTADO; ?>" 
                                            <?php echo set_select('estados',  $estados->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($estados->NOMBRE)); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Filtro por fecha</label>
                                    <div class="btn-group btn-group-justified">
                                        <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                            <span>
                                            <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                            </span>
                                            <input type="hidden" name="id_fecha" value="id_fecha">
                                            <div class="pull-right">
                                                <i class="fa fa-caret-down"></i>
                                            </div>
                                        </a>
                                        <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Rut</label>
                                    <input type="text" class="form-control filter" id="id_rut" placeholder="Ej: 18546919-0" data-column-index='0'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Opciones</label>
                                    <div id="button-container">
                                            <button href="#" class="btn btn-block btn-primary">Exportar Excel</button>
                                    </div>
                                </div>
                            </div>
                            <hr class="hidden-xl hidden-lg hidden-md margin-btm-10">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-block btn-success" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                            </div>
                        </div>
                  </div>
                  <?php elseif ($this->session->profile_id == 3) :?>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Colación</label>
                                    <select class="form-control selectpicker" name="id_colacion" id="id_colacion" data-live-search="true" data-style="btn-danger">
                                        <option value="">Todos</option>
                                        <?php foreach($colacion as $colaciones) {?>
                                            <option value="<?php echo $colaciones->ID_COLACION; ?>" 
                                            <?php echo set_select('colaciones',  $colaciones->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($colaciones->NOMBRE)); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Estado del ticket</label>
                                    <select class="form-control selectpicker " name="id_estado" id="id_estado" data-live-search="true" data-style="btn-danger">
                                        <option value="">Todos</option>
                                        <?php foreach($estado as $estados) {?>
                                            <option value="<?php echo $estados->ID_ESTADO; ?>" 
                                            <?php echo set_select('estados',  $estados->NOMBRE); ?>>
                                            <?php echo ucwords(mb_strtolower($estados->NOMBRE)); ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group margin-btm-5">
                                    <label>Rut</label>
                                    <input type="text" class="form-control filter" id="id_rut" placeholder="Ej: 18546919-0" data-column-index='0'>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Filtro por Empresa:</label>
                                      <select class="selectpicker form-control" name="id_empresa" id="id_empresa" data-live-search="true" data-style="btn-danger">
                                        <option value="">Todos</option>
                                        <?php foreach ($empresa as $nombre): ?>
                                          <?php if ($nombre->EMPRESA == 'SIN EMPRESA'): ?>
                                            <option value=""><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                          <?php else :?>
                                            <option value="<?php echo $nombre->EMPRESA?>"><?php echo ucwords(mb_strtolower($nombre->EMPRESA))?></option>
                                          <?php endif ?>
                                        <?php endforeach ?>
                                      </select>
                                </div>
                            </div>
                            <hr class="hidden-xl hidden-lg hidden-md margin-btm-10">
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Filtro por fecha</label>
                                    <div class="btn-group btn-group-justified">
                                        <a class="btn btn-default btn-flat" id="daterange-btn" style="width: 5%;" title="Filtrar por rango de fechas">
                                            <span>
                                            <i class="fa fa-calendar"></i> Filtrar por rango de fechas
                                            </span>
                                            <div class="pull-right">
                                                <i class="fa fa-caret-down"></i>
                                            </div>
                                        </a>
                                        <a id="clear_filter" class="btn btn-default btn-flat" title="Limpiar filtro de fechas"><i class="fa fa-close"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group margin-btm-5">
                                    <label>Opciones</label>
                                    <div id="button-container">
                                            <button href="#" class="btn btn-block btn-primary">Exportar Excel</button>
                                    </div>
                                </div>
                            </div>
                            <hr class="hidden-xl hidden-lg hidden-md margin-btm-10">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-block btn-success" id="btn_aplicar" style="margin-top: 24px"> Aplicar Filtro de Búsqueda</button>
                            </div>
                        </div>
                    </div>     
                  <?php endif ?>
              </div>
                <div class="box box-danger box-solid collapsed-box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Los 5 más recientes</h3>
                    <?php if ($this->session->profile_id == 4): ?>
                    <div class="note" style="margin-right: 5px;margin-top: 15px">
                        <span class="pull-left margin-right-5">
                            <small class="label bg-aqua"> <i class="fa fa-info padding-top-3"></i></small> 
                        </span>
                        <strong>Nota:</strong> Debe seleccionar el casino para ver el historial.
                    </div>    
                    <?php endif ?>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse" id="div_show"><i class="fa fa-plus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body" style="display: none;" id="box_recient">
                    
                  </div>
                  <!-- /.box-body -->
                </div>
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group margin-btm-5">
                                        <label>Mostrar</label>
                                        <select id="show_record" class="form-control">
                                            <option value="10">10 registros</option>
                                            <option value="25">25 registros</option>
                                            <option value="50">50 registros</option>
                                            <option value="100">100 registros</option>
                                            <option value="-1">Todos los registros</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group margin-btm-5">
                                        <label>Buscar</label>
                                        <div class="input-group stylish-input-group">
                                            <input type="text" class="form-control"  placeholder="EJ: Felipe, Planta 1" id="search_input">
                                            <span class="input-group-addon">
                                                <button type="submit">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>  
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </from>
                        <div class="box-body">
                            <div class="row table-responsive no-left-right-margin">
                                <div class="col-xs-12"> 
                                    <table id="table" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th colspan="4" class="text-center" bgcolor="#CDDC39">Colaborador</th>
                                                <th colspan="2" class="text-center" bgcolor="#CDDC39">Lugar</th>
                                                <th colspan="4" class="text-center" bgcolor="#CDDC39">Detalle</th>
                                            </tr>
                                            <tr>
                                                <th class="printable text-center">Rut</th>
                                                <th class="printable text-center">Colaborador</th>
                                                <th class="printable text-center">Departamento</th>
                                                <th class="printable text-center">Empresa</th>
                                                <th class="printable text-center">Planta</th>
                                                <th class="printable text-center">Casino</th>
                                                <th class="printable text-center">Fecha</th>
                                                <th class="printable text-center">Colación</th>
                                                <th class="printable text-center">Estado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="printable text-center">Rut</th>
                                                <th class="printable text-center">Colaborador</th>
                                                <th class="printable text-center">Departamento</th>
                                                <th class="printable text-center">Empresa</th>
                                                <th class="printable text-center">Planta</th>
                                                <th class="printable text-center">Casino</th>
                                                <th class="printable text-center">Fecha</th>
                                                <th class="printable text-center">Colación</th>
                                                <th class="printable text-center">Estado</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <p id="footer-left" class="col-sm-6 footer-dt"></p>
                                <div id="footer-right" class="col-sm-6"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
  </section>
</div>
<script>
    $(document).ajaxStart(function() {
        pf_blockUI();
    }).ajaxStop(function() {
        pf_unblockUI();
    }); 
    jQuery(document).ready(function() {
        var table;
        var date = new Date();
        var y = date.getFullYear();
        var m = date.getMonth();
        var d = date.getDate();
        var n_date = (d)+'/'+(m+1)+'/'+y;
        if(m < 10){
            n_date = (d)+'/0'+(m+1)+'/'+y;
        }
        var titleExport = 'Historial de Aprobación';
        var header = 'Sistema Casino --- '+n_date;
        var orientation = 'landscape'; //portrait
        var pageSize = 'LETTER';
        table = $("#table").DataTable({
                "responsive": true,
                ajax: {
                    'url': "<?php echo site_url('ticket_rechazado/get_data'); ?>",
                    'type': 'POST',
                    'data': function ( d ) {
                        d.planta = $('#id_planta').val();
                        d.casino = $('#id_casino').val();
                        d.colacion = $('#id_colacion').val();
                        d.fecha = $('#daterange-btn span').text();
                        d.estado = $('#id_estado').val();
                        d.rut = $('#id_rut').val();
                        d.empresa = $('#id_empresa').val();
                    }
                },
                processing: true,
                serverSide: true,
                order: [[ 0, 'desc']],
                paging: true,
                info: true,
                responsive: true,
                dom: 'lfrtip',
                "aoColumnDefs": [
                { 
                    "sType": "date-uk", 
                    "aTargets": [4] 
                }, { targets: 'no-sort', orderable: false }
                ],
                "createdRow": function ( row, data, index ) {
                $('td', row).eq(0).addClass('text-center');
                $('td', row).eq(1).addClass('text-center');
                $('td', row).eq(2).addClass('text-center');
                $('td', row).eq(3).addClass('text-center');
                $('td', row).eq(4).addClass('text-center');
                $('td', row).eq(5).addClass('text-center');
                $('td', row).eq(6).addClass('text-center');
                $('td', row).eq(7).addClass('text-center');
                $('td', row).eq(8).addClass('text-center');
                // $(row).find('td:eq(6)').addClass('bg-purple disabled color-palette');
                },
                "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }, 
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "colvis": "Cambiar columnas",
                        "print": "Imprimir"
                    }
                },
            });

        
        $('#table')
        .on( 'processing.dt', function ( e, settings, processing ) {
            if(processing){
                Pace.start();
            } else {
                Pace.stop();
            }
        })

        jQuery("#footer").ready(function(){
          jQuery("#table_length").addClass('hidden');
          jQuery("#table_filter").addClass('hidden');
          jQuery("#table_info").addClass('hidden');
          jQuery("#footer-left").text(jQuery("#table_info").text());
          jQuery("#table_paginate").appendTo(jQuery("#footer-right"));
        });


        $('#search_input').keyup(function(){
          table.search($(this).val()).draw() ;
        })

        $('#show_record').click(function() {
          table.page.len($('#show_record').val()).draw();
          jQuery("#footer-left").text(jQuery("#table_inc_info").text());
        });

        jQuery("#table").on("page.dt", function(){
          var info = table.page.info();
          jQuery("#footer-left").text("Mostrando registros del "+(info.start+1)+" al "+info.end+" de un total de "+info.recordsTotal+" registros");
        });

        function reload_table()
        {
            table.ajax.reload(); 
        }

        $('#btn_aplicar').click(function () {
            reload_table();
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#daterange-btn').daterangepicker(
        {
            opens: 'left',
            showDropdowns: true,
            buttonClasses: 'btn btn-flat btn-sm',
            applyClass: 'btn-primary',
            locale: {
                format:             "DD/MM/YYYY",
                separator:          " --- ",
                applyLabel:         "Aplicar",
                cancelLabel:        "Cancelar",
                fromLabel:          "Desde",
                toLabel:            "A",
                customRangeLabel:   "Definir rango",
                daysOfWeek: [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mie",
                    "Jue",
                    "Vie",
                    "Sab"
                ],
                monthNames: [
                    'Enero', 
                    'Febrero', 
                    'Marzo', 
                    'Abril', 
                    'Mayo', 
                    'Junio', 
                    'Julio', 
                    'Agosto', 
                    'Septiembre', 
                    'Octubre', 
                    'Noviembre', 
                    'Diciembre'
                ],
                firstDay: 1
            },
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
                'Este mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Este Año': [moment().startOf('year'), moment().endOf('year')],
                'Año pasado': [moment().subtract(1, 'year').startOf('year'), moment(). subtract(1, 'year').endOf('year')]
                },
                
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                $('#daterange-btn input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                format_date(start.format('DD-MM-YYYY'), end.format('DD-MM-YYYY'));
        });
    });
</script>
<script>
    $('#clear_filter').click(function(){
        $('#daterange-btn span').html('<span><i class="fa fa-calendar"></i> Filtrar por rango de fechas</span>');
        $('#daterange-btn input').val('Filtrar por rango de fechas');
        format_date(moment().subtract(500, 'year').startOf('year').format('DD-MM-YYYY'), moment().format('DD-MM-YYYY'));
        filter();
    });
</script>
<script>
    $.extend(jQuery.fn.dataTableExt.oSort, {
        "date-uk-pre": function (a) {
        var x;
        try {
        var dateA = a.replace(/ /g, '').split("-");
        var day = parseInt(dateA[0], 10);
        var month = parseInt(dateA[1], 10);
        var year = parseInt(dateA[2], 10);
        var date = new Date(year, month - 1, day)
        x = date.getTime();
        }
        catch (err) {
        x = new Date().getTime();
        }

        return x;
        },

        "date-uk-asc": function (a, b) {
        return a - b;
        },

        "date-uk-desc": function (a, b) {
        return b - a;
        }
    });

    var sDate = '';
    var eDate = '';

    function format_date(startDate, endDate) {

        sDate = startDate;
        eDate = endDate;
    }

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex){
            var dateStart = parseDateValue(sDate);
            var dateEnd = parseDateValue(eDate);
            var evalDate = parseDateValue(aData[4]);
            if ((sDate === '' && eDate === '') || (evalDate >= dateStart && evalDate <= dateEnd)) {
                return true;
            }
            else {
                return false;
            }
    });
</script>
<script>
    $('#id_planta').on('change', function(event){
        var casino = $("#id_casino");
        var option = '';

        $('#id_planta option:selected').each(function() {
            inc_type = $('#id_planta').val();
            pf_blockUI();
            $.post('<?php echo site_url('ticket_rechazado/get_casino'); ?>', {
                planta : inc_type
            }, function(data) {
                option = '<option value="">Todos</option>';
                $.each(data.option,function(index, item) {
                    option = option + '<option value="' + item.values + '">' + item.text + '</option>';
                });
                casino.find('option')
                .remove()
                .end()
                .append(option).selectpicker('refresh');
                $('#id_casino').trigger('change');
            },'json')
            .always(function() {
                pf_unblockUI();
            });
            option = '';
        });
        event.preventDefault();
    });
</script>
<script>
    $('#id_casino').on('change',function(event) {
        var colacion = $('#id_colacion');
        var option = '';
        $('#id_casino option:selected').each(function() {
            time_select = $('#id_casino').val();
            pf_blockUI();
            $.post('<?php echo site_url('ticket_rechazado/get_colacion'); ?>', {
                casino: time_select
            }, function(data) {
                option = '<option value="">Todos</option>';
                $.each(data.option,function(index, item) {
                    option = option + '<option value="' + item.values + '">' + item.text + '</option>';
                });
                colacion.find('option')
                .remove()
                .end()
                .append(option).selectpicker('refresh');
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        });
        option = '';
        event.preventDefault();
    });
</script>
<script>
    $('#div_show').on('click', function(event) {
        $('#box_recient').append('<p>Cargando ...</p>');
        Pace.track(function(){
            pf_blockUI();
            $.post('<?php echo site_url('ticket_rechazado/get_rejection_five'); ?>', {
                casino : $('#id_casino').val()
            }, function(data) {
                /*optional stuff to do after success */
                $('#box_recient').empty();
                if (data.status) {
                    $('#box_recient').append(data.flag);
                }else if (data.status == false){
                    $('#box_recient').append(data.flag);
                }
            },'json')
            .always(function() {
                pf_unblockUI();
            });
        })
        event.preventDefault();
    });
</script>