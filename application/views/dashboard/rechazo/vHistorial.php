<!-- row -->
<div class="row">
  <div class="col-md-12">
    <!-- The time line -->
    <ul class="timeline">
      <!-- timeline time label -->
      <li class="time-label">
        <span class="bg-red">
          <?php echo date("d-m-Y");?>
        </span>
      </li>
      <!-- /.timeline-label -->
      <?php foreach ($flag as $key): ?>
      <!-- timeline item -->
        <li>
          <i class="fa fa-envelope bg-blue"></i>

          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i><?php echo $key->HORA_ENTREGA?></span>

            <h3 class="timeline-header"><a href="#">Soporte Informática</a> Mensaje Enviado</h3>

            <div class="timeline-body">
              <?php if (!empty($key->NOMBRE) && !empty($key->CODIGO)): ?>
                <p><?php echo 'Nombre : '.ucwords(mb_strtolower($key->NOMBRE)).' Rut : '.$key->CODIGO?></p>
                <p><?php echo 'Planta : '.ucwords(mb_strtolower($key->PLANTA)).' Casino : '.$key->CASINO?></p>
                <p><?php echo 'Colación : '.$key->COLACION?></p>
                <p><?php echo 'Mensaje : '.$key->ESTADO?></p>
              <?php endif ?>
            </div>
          </div>
        </li>
        <!-- END timeline item -->
      <?php endforeach ?>
      <li>
        <i class="fa fa-clock-o bg-gray"></i>
      </li>
    </ul>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
