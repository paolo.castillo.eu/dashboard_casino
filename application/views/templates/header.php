<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Felipe Ortiz">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://www.pfalimentos.cl/sites/default/files/faviconpf_0.png" type="image/png">
    <title>PF Alimentos</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/select2/select2.min.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.print.css" media="print">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/extensions/bootstrap_extensions/css/buttons.bootstrap.min.css">
    <!-- TagsInput -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/tagsinput/src/bootstrap-tagsinput.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/animate.css">

    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/Style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/pace/pace.min.css">
    <!-- Bootstrap File Upload -->
    <link src="<?php echo base_url();?>assets/plugins/bootstrap-fileinput/css/fileinput.min.css">
    <!-- JGallery -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/JGallery/css/jgallery.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-selected/bootstrap-select.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--- JS -->
    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!--Select 2 -->
    <script src="<?php echo base_url();?>assets/plugins/select2/select2.full.min.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/plugins/chartjs/Chart.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/chartjs/Chart.bundle.min.js"></script>
    <!-- InputMask -->
    <script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.bundle.min.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
    <!-- bootstrap color picker -->
    <script src="<?php echo base_url();?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/extensions/bootstrap_extensions/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/extensions/bootstrap_extensions/js/buttons.bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="http://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="http://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/extensions/bootstrap_extensions/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/extensions/bootstrap_extensions/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/extensions/bootstrap_extensions/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/jquery.jeditable.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/formatted-numbers.js"></script>
    <!-- <script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.editor.min.js"></script> -->

    
    

    <!-- Tags Input -->
    <script src="<?php echo base_url();?>assets/plugins/tagsinput/src/bootstrap-tagsinput.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
    <!-- Pace -->
    <script src="<?php echo base_url();?>assets/plugins/pace/pace.min.js"></script>
    <!-- Full Calendar -->
    <script src="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url();?>assets/dist/js/app.min.js" type="text/javascript"></script>
    <!-- Bootstrap File Upload -->
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-fileinput/js/plugins/purify.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-fileinput/js/locales/es.js"></script>
    <!-- Ajax file upload -->
    <script src="<?php echo base_url();?>assets/plugins/ajaxfileupload/jquery.ajaxfileupload.js"></script>
    <!-- JGallery -->
    <script src="<?php echo base_url();?>assets/plugins/JGallery/js/jgallery.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/JGallery/js/touchswipe.min.js"></script>
    <!-- Rut plugin -->
    <script src="<?php echo base_url();?>assets/plugins/jquery.rut.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-slider/bootstrap-slider.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/notefy/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/jQueryValidate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/jQueryValidate/es.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/report/excellentexport.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/report/jspdf.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/report/jspdf.plugin.autotable.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/multiselect/bootstrap-multiselect.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-selected/bootstrap-select.js"></script>

    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css">

    <script src="<?php echo base_url();?>assets/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datetimepicker/locales/bootstrap-datetimepicker.es.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/blockui/jquery.blockUI.js"></script>

    <script src="<?php echo base_url(); ?>assets/pf_genericos/js_pf.js"></script>
    

    <style type="text/css">
    .stylish-input-group .input-group-addon{
    background: white !important; 
    }
    .stylish-input-group .form-control{
        border-right:0; 
        box-shadow:0 0 0; 
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
    </style>
    <style type="text/css">
      .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:active {
        color: #fbfbfb;
    }
    </style>

</head>
<body class="hold-transition fixed skin-purple sidebar-mini" id="imprime">
<div class="wrapper">
    <header class="main-header">

        <a href="<?php echo site_url(); ?>/dashboard" class="logo">
            <div class="logo-mini logo-img">
                <img class="logo-img" src="<?php echo base_url();?>assets/dist/img/logos/logo_pf.png"/>
            </div>
            <span class="logo-lg logo-img">
                <img class="logo-img" src="<?php echo base_url();?>assets/dist/img/logos/logo_pf_alimentos_3.png"/>
            </span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" id="toggle_btn">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user margin-right-5"></i>
                            <span class="margin-right-5"> <?php echo ucwords(mb_strtolower($this->session->name));?></span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu logout">
                            <li class="header">Opciones</li>
                            <li>
                                <ul class="menu">
                                    <li>
                                    
                                        <a id="click-me" href="<?php echo site_url(); ?>/perfil_colaborador">
                                            <i class="fa fa-user text-blue"></i> Mi Perfil
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url(); ?>/logout">
                                            <i class="fa fa-sign-out text-blue"></i> Cerrar Sesión
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">

        <section class="sidebar">
            <ul class="sidebar-menu">
            <?php if ($this->session->profile_id == 3 || $this->session->profile_id == 4): ?>
                <li class="header">INFORMES Y ESTADÍSTICAS</li>
                <li>
                  <a href="<?php echo site_url(); ?>/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                </li>
            <?php endif?>
            <?php if ($this->session->profile_id == 2 || $this->session->profile_id == 3 || $this->session->profile_id == 4 || $this->session->profile_id == 1):?>
                <li class="header">COLACIONES</li>
                <li>
                  <a href="<?php echo site_url(); ?>/solicitar_colacion"><i class="fa fa-ticket"></i> <span>Solicitar Colación</span></a>
                </li>
                <li>
                  <a href="<?php echo site_url(); ?>/mis_colaciones"><i class="fa fa-eye"></i> <span>Ver Mis Colaciones</span></a>
                </li>
            <?php endif?>
            <?php if ($this->session->profile_id == 3 || $this->session->profile_id == 4): ?>
                <li class="header">CONFIGURACIONES</li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-list"></i> <span>Ticket Casino</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display: none;">
                        <li>
                           <a href="<?php echo site_url(); ?>/ticket_generado"><i class="fa fa-check"></i> <span>Ticket Generados</span>
                            </a>
                        </li>
                        <li>
                          <a href="<?php echo site_url(); ?>/ticket_rechazado"><i class="fa fa-certificate"></i> <span>Ticket Rechazados</span>
                          </a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-list"></i> <span>Solicitudes Casino</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/solicitudes_pendiente"><i class="fa fa-dot-circle-o"></i><span>Pendientes</span></a></li>
                    <li><a href="<?php echo site_url(); ?>/solicitudes_aprovadas"><i class="fa fa-check-square"></i><span>Aprobado</span></a></li>
                    <li><a href="<?php echo site_url(); ?>/solicitudes_rechazadas"><i class="fa fa-times"></i><span>Rechazadas</span></a></li>
                </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-list"></i> <span>Informes Casino</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                      <a href="<?php echo site_url(); ?>/configurar/reporte"><i class="fa fa-gears"></i> <span>Configurar Reporte</span></a>
                    </li>
                    <li>
                      <a href="<?php echo site_url(); ?>/informe_sistema_casino"><i class="fa fa-clipboard"></i> <span>Informe Sistema Casino</span></a>
                    </li>
                    <li>
                      <a href="<?php echo site_url(); ?>/informe_sistema_cliente"><i class="fa fa-users"></i> <span>Informe Colaborador Casino</span></a>
                    </li>
                </ul>
                </li>
                <li>
                  <a href="<?php echo site_url(); ?>/colacion_colaborador"><i class="fa fa-cutlery"></i> <span>Configurar Colaciones</span></a>
                </li>
                <?php if ($this->session->profile_id == 4): ?>
                    <li>
                      <a href="<?php echo site_url(); ?>/ticket"><i class="fa fa-edit"></i> <span>Configurar Estado Ticket</span></a>
                    </li>    
                <?php endif ?>
            <?php endif?>
            <?php if ($this->session->profile_id == 4): ?>
                <li class="header">lISTA DE MANTENEDORES</li>
                <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i> <span>Mantenedor Área</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/area"><i class="fa fa-circle-o"></i> Áreas</a></li>
                </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i> <span>Mantenedor Planta</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/planta"><i class="fa fa-circle-o"></i> Plantas</a></li>
                </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i> <span>Mantenedor Casinos</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/casino"><i class="fa fa-circle-o"></i> Casinos</a></li>
                </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i> <span>Mantenedor Colaciones</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/colacion"><i class="fa fa-circle-o"></i> Colaciones</a></li>
                </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i> <span>Mantenedor Perfiles</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/perfiles"><i class="fa fa-circle-o"></i> Perfiles</a></li>
                </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i> <span>Mantenedor Colaboradores</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/colaborador"><i class="fa fa-circle-o"></i> Colaborador</a></li>
                </ul>
                </li>
                <li class="treeview">
                <a href="#"><i class="fa fa-tag"></i> <span>Mantenedor Servicios</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="<?php echo site_url(); ?>/tipo_servicio"><i class="fa fa-circle-o"></i> Tipo de Servicio</a></li>
                </ul>
                </li>
            <?php endif?>
            </ul>
        </section>
    </aside>