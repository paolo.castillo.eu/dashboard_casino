<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller']                = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override']                      = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes']              = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                    = 'login';
$route['404_override']                          = '';
$route['translate_uri_dashes']                  = FALSE;
$route['unsupported-browser']                   = 'unsupported';
$route['sin_acceso']                            = 'unsupported/stop';

// -----------------------------------------------------//
$route['logout']                                = 'login/logout';
$route['logout/recover_pass']                   = 'login/recover_pass';
// -----------------------------------------------------//
$route['span/get']                              = 'cpanel/dashboard/get_collation';
// -----------------------------------------------------// 
$route['dashboard']                             = 'cpanel/dashboard';
$route['dashboard/getcasino']                   = 'cpanel/dashboard/getCasino';
$route['dashboard/show']                        = 'cpanel/dashboard/get_data_home_casino';
// -----------------------------------------------------//
$route['solicitar_colacion']                    = 'cpanel/solicitar/crequest';
$route['solicitar_colacion/casino']             = 'cpanel/solicitar/crequest/get_casinos';
$route['solicitar_colacion/collation']          = 'cpanel/solicitar/crequest/get_collations';
$route['solicitar_colacion/save_solicitud']     = 'cpanel/solicitar/crequest/save';
$route['solicitar_colacion/get_empleado']       = 'cpanel/solicitar/crequest/get_empleado';
$route['solicitar_colacion/save_solicitud_emp'] = 'cpanel/solicitar/crequest/save_solicitud_emp';
$route['solicitar_colacion/get_empleado_sub']   = 'cpanel/solicitar/crequest/get_empleado_sub';
// -----------------------------------------------------//
$route['solicitudes_pendiente']                 = 'cpanel/solicitudes/csolicitude_pen';
$route['solicitudes_pendiente/get_rows']        = 'cpanel/solicitudes/csolicitude_pen/get_pending';
$route['solicitudes_pendiente/rejection']       = 'cpanel/solicitudes/csolicitude_pen/rejection';
$route['solicitudes_pendiente/aproved']         = 'cpanel/solicitudes/csolicitude_pen/aproved';
$route['solicitudes_pendiente/get_modal']       = 'cpanel/solicitudes/csolicitude_pen/get_modal';

// -----------------------------------------------------//
$route['solicitudes_aprovadas']                 = 'cpanel/solicitudes/Csolicitude_req';
$route['solicitudes_aprovadas/get_rows']        = 'cpanel/solicitudes/Csolicitude_req/get_request';
$route['solicitudes_aprovadas/del']             = 'cpanel/solicitudes/Csolicitude_req/dele';
$route['solicitudes_aprovadas/rejection']       = 'cpanel/solicitudes/Csolicitude_req/rejection';
// -----------------------------------------------------//
$route['solicitudes_rechazadas']                = 'cpanel/solicitudes/Csolicitude_rej';
$route['solicitudes_rechazadas/get_rows']       = 'cpanel/solicitudes/Csolicitude_rej/get_rejection';
$route['solicitudes_rechazadas/apr']            = 'cpanel/solicitudes/Csolicitude_rej/apr';
$route['solicitudes_rechazadas/aproved']        = 'cpanel/solicitudes/Csolicitude_rej/aproved';
// -----------------------------------------------------//
$route['mis_colaciones']                        = 'cpanel/collation/cmycollation';
$route['get_mis_colacion']                      = 'cpanel/collation/cmycollation/get_mis_colacion';
// -----------------------------------------------------//
$route['colacion']                              = 'cpanel/collation/ccollation';
$route['colacion/get']                          = 'cpanel/collation/ccollation/getCollationAll';
$route['colacion/get/(:num)']                   = 'cpanel/collation/ccollation/getCollation/$1';
$route['colacion/add']                          = 'cpanel/collation/ccollation/save';
$route['colacion/upd']                          = 'cpanel/collation/ccollation/edit';
$route['colacion/del/(:num)']                   = 'cpanel/collation/ccollation/dele/$1';
// -----------------------------------------------------//
$route['colacion_colaborador']                  = 'cpanel/collation/ccollation_user';
$route['colaborador_colacion/get_user']         = 'cpanel/collation/ccollation_user/getUserAll';
$route['colacion_colaborador/get_collation']    = 'cpanel/collation/ccollation_user/getCollation_userAll';
$route['colacion_colaborador/get']              = 'cpanel/collation/ccollation_user/getCollation_user';
$route['colacion_colaborador/add']              = 'cpanel/collation/ccollation_user/save';
$route['colacion_colaborador/upd']              = 'cpanel/collation/ccollation_user/edit';
$route['colacion_colaborador/del']              = 'cpanel/collation/ccollation_user/dele';
$route['colacion_colaborador/get_casino_all']   = 'cpanel/collation/ccollation_user/get_casino_all';
$route['colacion_colaborador/get_by']           = 'cpanel/collation/ccollation_user/get_by';
// -----------------------------------------------------//
$route['planta']                                = 'cpanel/plant/cplant';
$route['plant/get']                             = 'cpanel/plant/cplant/getPlantAll';
$route['plant/get/(:num)']                      = 'cpanel/plant/cplant/getPlant/$1';
$route['plant/add']                             = 'cpanel/plant/cplant/save';
$route['plant/upd']                             = 'cpanel/plant/cplant/edit';
$route['plant/del/(:num)']                      = 'cpanel/plant/cplant/dele/$1';
// -----------------------------------------------------//
$route['casino']                                = 'cpanel/casino/ccasino';
$route['casino/get']                            = 'cpanel/casino/ccasino/getCasinoAll';
$route['casino/get/(:num)']                     = 'cpanel/casino/ccasino/getCasino_by/$1';
$route['casino/add']                            = 'cpanel/casino/ccasino/save';
$route['casino/upd']                            = 'cpanel/casino/ccasino/edit';
$route['casino/del/(:num)']                     = 'cpanel/casino/ccasino/dele/$1';
// -----------------------------------------------------//
$route['area']                                  = 'cpanel/area/carea';
$route['area/get']                              = 'cpanel/area/carea/getAreaAll';
$route['area/get/(:num)']                       = 'cpanel/area/carea/getArea_by/$1';
$route['area/add']                              = 'cpanel/area/carea/save';
$route['area/upd']                              = 'cpanel/area/carea/edit';
$route['area/del/(:num)']                       = 'cpanel/area/carea/dele/$1';
// -----------------------------------------------------//
$route['perfiles']                              = 'cpanel/profile/cprofile';
$route['perfiles/get']                          = 'cpanel/profile/cprofile/getProfileAll';
$route['perfiles/get/(:num)']                   = 'cpanel/profile/cprofile/getProfile_by/$1';
$route['perfiles/add']                          = 'cpanel/profile/cprofile/save';
$route['perfiles/upd']                          = 'cpanel/profile/cprofile/edit';
$route['perfiles/del/(:num)']                   = 'cpanel/profile/cprofile/dele/$1';
// -----------------------------------------------------//
$route['colaborador']                           = 'cpanel/user/ccollaborator';
$route['colaborador/get']                       = 'cpanel/user/ccollaborator/getCollaboratorAll';
$route['colaborador/get/(:num)']                = 'cpanel/user/ccollaborator/getCollaborator_by/$1';
$route['colaborador/add']                       = 'cpanel/user/ccollaborator/save';
$route['colaborador/upd']                       = 'cpanel/user/ccollaborator/edit';
$route['colaborador/del/(:num)']                = 'cpanel/user/ccollaborator/dele/$1';
// -----------------------------------------------------//
$route['perfil_colaborador']                    = 'cpanel/user/cprofile';
$route['perfil_colaborador/upd']                = 'cpanel/user/cprofile/edit';
// -----------------------------------------------------//
$route['tipo_servicio']                         = 'cpanel/servicio/cservice';
$route['tipo_servicio/get']                     = 'cpanel/servicio/cservice/getServiceAll';
$route['tipo_servicio/get/(:num)']              = 'cpanel/servicio/cservice/getService_by/$1';
$route['tipo_servicio/add']                     = 'cpanel/servicio/cservice/save';
$route['tipo_servicio/upd']                     = 'cpanel/servicio/cservice/edit';
$route['tipo_servicio/del/(:num)']              = 'cpanel/servicio/cservice/dele/$1';
// -----------------------------------------------------//
$route['ticket']                                = 'cpanel/ticket/cticket';
$route['ticket/get']                            = 'cpanel/ticket/cticket/getTicketAll';
$route['ticket/get/(:any)']                     = 'cpanel/ticket/cticket/getTicket_by/$1';
$route['ticket/add']                            = 'cpanel/ticket/cticket/save';
$route['ticket/upd']                            = 'cpanel/ticket/cticket/edit';
$route['ticket/del/(:any)']                     = 'cpanel/ticket/cticket/dele/$1';
// -----------------------------------------------------//
$route['report']                                = 'cpanel/report/creport';
$route['report/get_casino']                     = 'cpanel/report/creport/get_casino';
$route['report/get_rows']                       = 'cpanel/report/creport/get_rows';
$route['report/save_rows']                      = 'cpanel/report/creport/save_rows';
$route['report/send_mail']                      = 'cpanel/report/creport/send_mail';
$route['report/guardado']                       = 'cpanel/report/creport/report_save';
$route['report/get_reporte']                    = 'cpanel/report/creport/get_reporte';
$route['report/get_modal']                      = 'cpanel/report/creport/get_modal';
$route['report/update_rows']                    = 'cpanel/report/creport/update_rows';
$route['reporte/get_costo']                     = 'cpanel/report/creport/get_gosto';
// -----------------------------------------------------//
$route['informe_sistema_casino']                = 'cpanel/report/creport_state';
$route['report_state/get_report_collation']     = 'cpanel/report/creport_state/get_data';
$route['report_state/get_services']             = 'cpanel/report/creport_state/get_data_service';
// -----------------------------------------------------//
$route['ticket_rechazado']                      = 'cpanel/rechazado/crejection';
$route['ticket_rechazado/get_casino']           = 'cpanel/rechazado/crejection/get_casino';
$route['ticket_rechazado/get_colacion']         = 'cpanel/rechazado/crejection/get_colacion';
$route['ticket_rechazado/get_rejection_five']   = 'cpanel/rechazado/crejection/get_five';
$route['ticket_rechazado/get_data']             = 'cpanel/rechazado/crejection/get_rechazado';
$route['export_excel_data_r']                   = 'cpanel/rechazado/crejection/export_excel_data';
// -----------------------------------------------------//
$route['ticket_generado']                       = 'cpanel/aprovado/cticket_request';
$route['ticket_generado/get_casino']            = 'cpanel/aprovado/cticket_request/get_casino';
$route['ticket_generado/get_collation']         = 'cpanel/aprovado/cticket_request/get_collation';
// -----------------------------------------------------//
$route['ticket_aprobado']                       = 'cpanel/aprovado/cticket_request/get_aprobados';
$route['export_excel_data']                     = 'cpanel/aprovado/cticket_request/export_excel_data';
// -----------------------------------------------------//
$route['informe_sistema_cliente']               = 'cpanel/report/creport_state/load_informe_cliente';
$route['informe_sistema_cliente/get']           = 'cpanel/report/creport_state/get_data_cliente';
$route['informe_sistema_cliente/get_excel']     = 'cpanel/report/creport_state/export_excel_data';
// -----------------------------------------------------//
$route['configurar/reporte']                    = 'cpanel/report/creport_edit/view_calendar';
$route['configurar/get_reporte']                = 'cpanel/report/creport_edit/get_reporte';
$route['configurar/get_excel']                  = 'cpanel/report/creport_edit/get_excel';