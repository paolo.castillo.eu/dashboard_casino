<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Collaborator_model extends CI_Model {
    
    private $table              = 'PFSC_COLABORADOR';
    private $id_colaborador     = 'ID_COLABORADOR';
    private $state              = 'ESTADO';
    private $name               = 'NOMBRE';
    private $password           = 'CLAVE';
    private $codigo             = 'CODIGO';
    private $email              = 'CORREO';
    private $card               = 'CREDENCIAL';
    private $deparment          = 'DEPARTAMENTO';
    private $company            = 'EMPRESA';
    private $management         = 'GERENCIA';
    private $name_company       = 'NOMBRE_EMPRESA';
    private $card_visit         = 'NRO_TARJETA_VISITA';
    private $rut_request        = 'RUTSOLICITANTE';
    private $physical_accountan = 'UBICACION_CONTABLE';
    private $physical_location  = 'UBICACION_FISICA';
    private $id_area            = 'ID_AREA';
    private $id_perfil          = 'ID_PERFIL';
    private $id_casino          = 'ID_CASINO';
    private $by_created         = 'CREADO_POR';
    private $by_modify          = 'MODIFICADO_POR';

    // TABLE Function //
    private $column_order  = array('ID_COLABORADOR','NOMBRE','CORREO','CODIGO','CREDENCIAL','DEPARTAMENTO','EMPRESA','GERENCIA','NOMBRE_EMPRESA','NRO_TARJETA_VISITA','RUTSOLICITANTE','UBICACION_CONTABLE','UBICACION_FISICA','ID_AREA','ID_PERFIL','ID_CASINO',null);
    private $column_search = array('NOMBRE','CODIGO');
    private $order         = array('ID_COLABORADOR' => 'ID_COLABORADOR');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($id_colaborador,$name,$email,$codigo,$card,$deparment,$company,$management,$card_visit,$rut_request,$physical_accountan,$physical_location,$id_area,$id_perfil,$id_casino,$by_create)
    {
        $this->db->trans_start();
        $this->db->set($this->id_colaborador, $id_colaborador);
        $this->db->set($this->name, $name);
        $this->db->set($this->codigo, $codigo);
        $this->db->set($this->email, $email);
        $this->db->set($this->card, $card);
        $this->db->set($this->deparment, $deparment);
        $this->db->set($this->company, $company);
        $this->db->set($this->management, $management);
        $this->db->set($this->card_visit, $card_visit);
        $this->db->set($this->rut_request, $rut_request);
        $this->db->set($this->physical_accountan, $physical_accountan);
        $this->db->set($this->physical_location, $physical_location);
        $this->db->set($this->id_area, $id_area);
        $this->db->set($this->id_perfil, $id_perfil);
        $this->db->set($this->id_casino, $id_casino);
        $this->db->set($this->by_created,$by_create);
        $this->db->insert($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->where($this->state, 1);
        $i = 0;
        foreach ($this->column_search as $item) {

            if ($_POST['search']['value']) {

                if ($i == 0) {
                    $this->db->like($item, strtoupper($_POST['search']['value']));
                } else {
                    $this->db->or_like($item, strtoupper($_POST['search']['value']));
                }
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
           $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function delete($id,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->by_modify, $by_modify);
        $this->db->set($this->state, 0);
        $this->db->where($this->id_colaborador,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function update($id_colaborador,$name,$email,$codigo,$card,$deparment,$company,$management,$card_visit,$rut_request,$physical_accountan,$physical_location,$id_area,$id_perfil,$id_casino,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->id_colaborador,$id_colaborador);
        $this->db->set($this->name, $name);
        $this->db->set($this->codigo, $codigo);
        $this->db->set($this->email, $email);
        $this->db->set($this->card, $card);
        $this->db->set($this->deparment, $deparment);
        $this->db->set($this->company, $company);
        $this->db->set($this->management, $management);
        $this->db->set($this->card_visit, $card_visit);
        $this->db->set($this->rut_request, $rut_request);
        $this->db->set($this->physical_accountan, $physical_accountan);
        $this->db->set($this->physical_location, $physical_location);
        $this->db->set($this->id_area, $id_area);
        $this->db->set($this->id_perfil, $id_perfil);
        $this->db->set($this->id_casino, $id_casino);
        $this->db->where($this->id_colaborador,$id_colaborador);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_by_id($id)
    {
        $this->db->select($this->id_colaborador);
        $this->db->select($this->name);
        $this->db->select($this->codigo);
        $this->db->select($this->email);
        $this->db->select($this->card);
        $this->db->select($this->deparment);
        $this->db->select($this->company);
        $this->db->select($this->management);
        $this->db->select($this->card_visit);
        $this->db->select($this->rut_request);
        $this->db->select($this->physical_accountan);
        $this->db->select($this->physical_location);
        $this->db->select($this->id_area);
        $this->db->select($this->id_perfil);
        $this->db->select($this->id_casino);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador, $id);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->row();
    }

    public function last_id()
    {
        $this->db->select('MAX(ID_COLABORADOR)+1 AS LASTID');
        $this->db->from('PFSC_COLABORADOR');
        $query = $this->db->get();
        return $query->row();
    }

    public function get_name($id)
    {
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador,$id);
        $this->db->where($this->state, 1);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->NOMBRE;
    }

    public function get_data_colaborador($id_colaborador)
    {
        $this->db->select($this->name);
        $this->db->select($this->codigo);
        $this->db->select($this->card);
        $this->db->select($this->id_area);
        $this->db->select($this->deparment);
        $this->db->select($this->physical_accountan);
        $this->db->select($this->physical_location);
        $this->db->select($this->rut_request);
        $this->db->select($this->card_visit);            
        $this->db->select($this->email);
        $this->db->select($this->id_perfil);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador, $id_colaborador);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function update_perfil($id_colaborador,$new_email)
    {
        $this->db->trans_start();
        $this->db->set($this->email,$new_email);
        $this->db->where($this->id_colaborador,$id_colaborador);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_email($id)
    {
        $this->db->select($this->email);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador,$id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_email_by_rut($id)
    {
        $this->db->select($this->email);
        $this->db->from($this->table);
        $this->db->where($this->codigo,$id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }


    public function get_empleado_datos($rut)
    {
        $this->db->select($this->id_colaborador);
        $this->db->select($this->name);
        $this->db->select($this->deparment);
        $this->db->select($this->email);
        $this->db->select($this->card);
        $this->db->from($this->table);
        $this->db->where($this->codigo,$rut);

        $query = $this->db->get();

        return $query->result();
    }
}

/* End of file Collaborator_model.php */
/* Location: ./application/models/Collaborator_model.php */