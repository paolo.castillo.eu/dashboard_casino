<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aprobado_model extends CI_Model {

	private $correlativo = 'TICKET.CORRELATIVO';
	private $codigo = 'COLAB.CODIGO';
	private $nombre_colaborador = 'COLAB.NOMBRE';
	private $id_colaborador = 'COLAB.ID_COLABORADOR';
	private $id_casino = 'CASINO.ID_CASINO';
	private $nombre_planta = 'PLANTA.NOMBRE AS PLANTA';
	private $nombre_casino = 'CASINO.NOMBRE AS CASINO';
	private $hora = 'TICKET.HORA_ENTREGA';
	private $fecha = "TO_CHAR(TICKET.FECHA_ENTREGA,'DD/MM/RRRR') AS FECHA";
	private $id_colacion = 'COLAC.ID_COLACION';
	private $nombre_colacion = 'COLAC.TIPO_COLACION AS COLACION';
	private $id_estado = 'ESTADO.ID_ESTADO';
    private $cantidad = ' COUNT(*) AS CANTIDAD ';
    private $empresa = 'COLAB.EMPRESA';
    private $departamento = 'COLAB.DEPARTAMENTO';

	private $table_ticket = 'PFSC_TICKET TICKET';
	private $table_casino = 'PFSC_CASINO CASINO';
	private $table_colaborador = 'PFSC_COLABORADOR COLAB';
	private $table_colacion = 'PFSC_COLACION COLAC';
	private $table_estado = 'PFSC_ESTADO_TICKET ESTADO';
	private $table_planta = 'PFSC_PLANTA PLANTA';

	private $fk_ticket_casino = 'TICKET.ID_CASINO = CASINO.ID_CASINO';
	private $fk_colab_ticket = 'COLAB.ID_COLABORADOR = TICKET.ID_COLABORADOR';
	private $fk_colac_ticket = 'COLAC.ID_COLACION = TICKET.ID_COLACION';
	private $fk_estad_ticket = 'ESTADO.ID_ESTADO = TICKET.ID_ESTADO';
	private $fk_casino_plant = 'CASINO.ID_PLANTA = PLANTA.ID_PLANTA';

    // datatable function //
    private $column_order  = array('TICKET.CORRELATIVO','COLAB.CODIGO','COLAB.NOMBRE','COLAB.EMPRESA','COLAB.DEPARTAMENTO','COLAB.ID_COLABORADOR','CASINO.ID_CASINO','PLANTA.NOMBRE','CASINO.NOMBRE','TICKET.FECHA_ENTREGA','TICKET.HORA_ENTREGA','COLAC.ID_COLACION','COLAC.NOMBRE','ESTADO.ID_ESTADO',null);
    private $column_search = array('COLAB.CODIGO','COLAB.NOMBRE','PLANTA.NOMBRE','CASINO.NOMBRE','COLAC.NOMBRE');
    private $order         = array('TICKET.CORRELATIVO' => 'TICKET.CORRELATIVO');

    private $nombre_planta_o = 'PLANTA.NOMBRE';
    private $nombre_casino_o = 'CASINO.NOMBRE';
    private $nombre_colacion_o = 'COLAC.TIPO_COLACION';

    private $id_planta = 'PLANTA.ID_PLANTA';
    private $fecha_entrega = 'TICKET.FECHA_ENTREGA';
    private $estado_ticket = 'TICKET.ID_ESTADO';
    private $having = 'COUNT(*) = ';

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
        ini_set('memory_limit', '586M');
	}

    /*=============================================
    =            Section comment block            =
    =============================================*/      

    private function _get_datatables_query()
    {
        $this->db->select($this->correlativo);
        $this->db->select($this->codigo);
        $this->db->select($this->nombre_colaborador);
        $this->db->select($this->id_colaborador);
        $this->db->select($this->id_casino);
        $this->db->select($this->nombre_planta);
        $this->db->select($this->nombre_casino);
        $this->db->select($this->hora);
        $this->db->select($this->fecha);
        $this->db->select($this->id_colacion);
        $this->db->select($this->nombre_colacion);
        $this->db->select($this->id_estado);
        $this->db->select($this->cantidad);
        $this->db->select($this->empresa);
        $this->db->select($this->departamento);

        $this->db->from($this->table_ticket);

		$this->db->join($this->table_casino, $this->fk_ticket_casino);
		$this->db->join($this->table_colaborador, $this->fk_colab_ticket);
		$this->db->join($this->table_colacion, $this->fk_colac_ticket);
		$this->db->join($this->table_estado, $this->fk_estad_ticket);
		$this->db->join($this->table_planta, $this->fk_casino_plant);

        $this->db->where($this->estado_ticket, 1);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->id_planta,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->id_casino,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->id_colacion,$_POST['colacion']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->id_casino,$this->session->casino);
        }

        
        if (!empty($_POST['empresa'])) {
            $empresa = $_POST['empresa'];
            $this->db->where("TRIM(UPPER($this->empresa)) =", "UPPER('$empresa')" , FALSE);
        }

        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }

        $this->db->group_by($this->correlativo);
        $this->db->group_by($this->codigo);
        $this->db->group_by($this->nombre_colaborador);
        $this->db->group_by($this->id_colaborador);
        $this->db->group_by($this->id_casino);
        $this->db->group_by($this->nombre_planta_o);
        $this->db->group_by($this->nombre_casino_o);
        $this->db->group_by($this->hora);
        $this->db->group_by($this->fecha_entrega);
        $this->db->group_by($this->id_colacion);
        $this->db->group_by($this->nombre_colacion_o);
        $this->db->group_by($this->id_estado);
        $this->db->group_by($this->empresa);
        $this->db->group_by($this->departamento);

        if (!empty($_POST['cantidad'])) {
           $this->db->having($this->having.' '.$_POST['cantidad'], NULL, FALSE); 
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select($this->correlativo);
        $this->db->select($this->codigo);
        $this->db->select($this->nombre_colaborador);
        $this->db->select($this->id_colaborador);
        $this->db->select($this->id_casino);
        $this->db->select($this->nombre_planta);
        $this->db->select($this->nombre_casino);
        $this->db->select($this->hora);
        $this->db->select($this->fecha);
        $this->db->select($this->id_colacion);
        $this->db->select($this->nombre_colacion);
        $this->db->select($this->id_estado);
        $this->db->select($this->cantidad);
        $this->db->select($this->empresa);
        $this->db->select($this->departamento);

        $this->db->from($this->table_ticket);

		$this->db->join($this->table_casino, $this->fk_ticket_casino);
		$this->db->join($this->table_colaborador, $this->fk_colab_ticket);
		$this->db->join($this->table_colacion, $this->fk_colac_ticket);
		$this->db->join($this->table_estado, $this->fk_estad_ticket);
		$this->db->join($this->table_planta, $this->fk_casino_plant);
        
        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->id_planta,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->id_casino,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->id_colacion,$_POST['colacion']);
        }

        if (!empty($_POST['empresa'])) {
            $empresa = $_POST['empresa'];
            $this->db->where("TRIM(UPPER($this->empresa)) =", "UPPER('$empresa')" , FALSE);
        }

        $this->db->group_by($this->correlativo);
        $this->db->group_by($this->codigo);
        $this->db->group_by($this->nombre_colaborador);
        $this->db->group_by($this->id_colaborador);
        $this->db->group_by($this->id_casino);
        $this->db->group_by($this->nombre_planta_o);
        $this->db->group_by($this->nombre_casino_o);
        $this->db->group_by($this->hora);
        $this->db->group_by($this->fecha_entrega);
        $this->db->group_by($this->id_colacion);
        $this->db->group_by($this->nombre_colacion_o);
        $this->db->group_by($this->id_estado);
        $this->db->group_by($this->empresa);
        $this->db->group_by($this->departamento);


        if (!empty($_POST['cantidad'])) {
           $this->db->having($this->having.' '.$_POST['cantidad'], NULL, FALSE); 
        }
                
        return $this->db->count_all_results();
    }

    /*=====  End of Section comment block  ======*/

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return $d && $d->format('d/m/Y') === $date;
    }

    public function export_excel_data()
    {
        $this->db->select($this->correlativo);
        $this->db->select($this->codigo);
        $this->db->select($this->nombre_colaborador);
        $this->db->select($this->nombre_planta);
        $this->db->select($this->nombre_casino);
        $this->db->select($this->fecha);
        $this->db->select($this->hora);
        $this->db->select($this->nombre_colacion);
        $this->db->select($this->cantidad);
        $this->db->select($this->empresa);
        $this->db->select($this->departamento);

        $this->db->from($this->table_ticket);

        $this->db->join($this->table_casino, $this->fk_ticket_casino);
        $this->db->join($this->table_colaborador, $this->fk_colab_ticket);
        $this->db->join($this->table_colacion, $this->fk_colac_ticket);
        $this->db->join($this->table_estado, $this->fk_estad_ticket);
        $this->db->join($this->table_planta, $this->fk_casino_plant);
        $this->db->where($this->estado_ticket, 1);
        
        if (!empty($_POST['id_fecha'])) {
            $desde           = substr($_POST['id_fecha'], 0, 10);
            $hasta           = substr($_POST['id_fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);
            }
        }else{
            $query_date = date("d-m-Y");
            // First day of the month.
            $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
            
            // Last day of the month.
            $hasta = date('t-m-Y', strtotime($query_date));
            $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
            $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['id_planta'])) {
            $this->db->where($this->id_planta,$_POST['id_planta']);
        }

        if (!empty($_POST['id_casino'])) {
            $this->db->where($this->id_casino,$_POST['id_casino']);
        }

        if (!empty($_POST['id_colacion'])) {
            $this->db->where($this->id_colacion,$_POST['id_colacion']);
        }


        if (!empty($_POST['id_empresa'])) {
           $empresa = $_POST['id_empresa'];
            $this->db->where("TRIM(UPPER($this->empresa)) =", "UPPER('$empresa')" , FALSE);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->id_casino,$this->session->casino);
        }
        
        $this->db->group_by($this->correlativo);
        $this->db->group_by($this->codigo);
        $this->db->group_by($this->nombre_colaborador);
        $this->db->group_by($this->nombre_planta_o);
        $this->db->group_by($this->nombre_casino_o);
        $this->db->group_by($this->hora);
        $this->db->group_by($this->fecha_entrega);
        $this->db->group_by($this->nombre_colacion_o);
        $this->db->group_by($this->empresa);
        $this->db->group_by($this->departamento);

        if (!empty($_POST['id_cantidad'])) {
           $this->db->having($this->having.' '.$_POST['id_cantidad'], NULL, FALSE); 
        }
        $query = $this->db->get();
        return $query;
    }
}

/* End of file Aprobado_model.php */
/* Location: ./application/models/Aprobado_model.php */