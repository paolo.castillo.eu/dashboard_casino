<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model {

    private $planta = 'PLANTA.NOMBRE AS PLANTA';
    private $casino = 'CASINO.NOMBRE AS CASINO';
    private $rut = 'COLAB.CODIGO';
    private $nombre = 'COLAB.NOMBRE';
    private $empresa = 'COLAB.EMPRESA';
    private $departamento = 'COLAB.DEPARTAMENTO';
    private $fecha = "TO_CHAR(TICKET.FECHA_ENTREGA,'DD-MM-YYYY') AS FECHA";
    private $hora = 'TICKET.HORA_ENTREGA';
    private $colacion = 'COLAC.TIPO_COLACION AS COLACION';
    private $estado = 'ESTADO.NOMBRE AS ESTADO';
    private $cantidad = 'COUNT(*) AS CANTIDAD';

    private $table_ticket = 'PFSC_TICKET TICKET';
    private $table_casino = 'PFSC_CASINO CASINO';
    private $table_colabo = 'PFSC_COLABORADOR COLAB';
    private $table_colaci = 'PFSC_COLACION COLAC';
    private $table_estado = 'PFSC_ESTADO_TICKET ESTADO';
    private $table_planta = 'PFSC_PLANTA PLANTA';

    private $fk_casino_tick = 'TICKET.ID_CASINO = CASINO.ID_CASINO';
    private $fk_colabo_tick = 'COLAB.ID_COLABORADOR = TICKET.ID_COLABORADOR';
    private $fk_colaci_tick = 'COLAC.ID_COLACION = TICKET.ID_COLACION';
    private $fk_estado_tick = 'ESTADO.ID_ESTADO = TICKET.ID_ESTADO';
    private $fk_casino_plan = 'CASINO.ID_PLANTA = PLANTA.ID_PLANTA';

    private $group_planta = 'PLANTA.NOMBRE';
    private $group_casino = 'CASINO.NOMBRE';
    private $group_rut = 'COLAB.CODIGO';
    private $group_nombre_colaborador = 'COLAB.NOMBRE';
    private $group_departamento = 'COLAB.DEPARTAMENTO';
    private $group_empresa = 'COLAB.EMPRESA';
    private $group_fecha = 'TICKET.FECHA_ENTREGA';
    private $group_colacion = 'COLAC.TIPO_COLACION';
    private $group_estado = 'ESTADO.NOMBRE';
    private $group_hora = 'TICKET.HORA_ENTREGA';

    private $fecha_entrega = 'TICKET.FECHA_ENTREGA';
    private $casino_id = 'CASINO.ID_CASINO';
    private $planta_id = 'PLANTA.ID_PLANTA';
    private $colacion_id = 'COLAC.ID_COLACION';
    private $estado_id = 'ESTADO.ID_ESTADO';
    private $rut_id = 'COLAB.CODIGO';
    private $having = 'COUNT(*) = ';
    private $empresa_id = 'COLAB.EMPRESA';

    // datatable function //
    private $column_order  = array('PLANTA.NOMBRE','CASINO.NOMBRE','COLAB.CODIGO','COLAB.NOMBRE','COLAB.DEPARTAMENTO','COLAB.EMPRESA','TICKET.FECHA_ENTREGA','TICKET.HORA_ENTREGA','COLAC.TIPO_COLACION','ESTADO.NOMBRE',null);
    private $column_search = array('PLANTA.NOMBRE','CASINO.NOMBRE','COLAB.CODIGO','COLAB.NOMBRE','COLAB.EMPRESA');
    private $order         = array('TICKET.FECHA_ENTREGA' => 'TICKET.FECHA_ENTREGA');


    private $id_estado_ticket = 'ID_ESTADO';
    private $nombre_estado = 'NOMBRE';


    /*===============================
    =            Empresa            =
    ===============================*/
    
    private $nombre_empresa = "DISTINCT NVL(EMPRESA,'SIN EMPRESA') EMPRESA ";
    private $table_empresa = 'PFSC_COLABORADOR';
    private $order_by_nombre_empresa = 'EMPRESA';

    /*===============================
    =            Reporte            =
    ===============================*/
    private $table_reporte_rep = 'PFSC_REPORTE_CASINO';
    private $id_sequencia = 'ID_SEQUENCIA';

    
    private $id_sequencia_rep = 'REP.ID_SEQUENCIA';
    private $nombre_rep = 'COL.NOMBRE NOMBRE';
    private $casino_rep = 'CAS.NOMBRE CASINO';
    private $fecha_rep = "TO_CHAR(REP.FECHA_CREACION,'DD/MM/RRRR') FECHA";
    private $fecha_md_rep = "TO_CHAR(REP.FECHA_MODIFICACION,'DD/MM/RRRR') FECHA_MODIFICACION";

    
    private $table_reporte_casino_rep = 'PFSC_REPORTE_CASINO REP';
    private $table_casino_rep = 'PFSC_CASINO CAS';
    private $table_colaborador_rep = 'PFSC_COLABORADOR COL';

    private $fk_casino_report_rep = 'REP.ID_CASINO = CAS.ID_CASINO';
    private $fk_colaborador_report_rep = 'REP.CREADO_POR = COL.ID_COLABORADOR';

    private $where_colaborador_rep = 'REP.CREADO_POR';
    private $where_fecha_rep = 'REP.FECHA_CREACION';

    private $group_sequencia_rep = 'REP.ID_SEQUENCIA';
    private $group_nombre_rep = 'COL.NOMBRE';
    private $group_casino_rep = 'CAS.NOMBRE';
    private $group_fecha_rep = 'REP.FECHA_CREACION';
    private $group_fecha_md_rep = 'REP.FECHA_MODIFICACION';

    // datatable function //
    private $column_order_rep  = array('REP.ID_SEQUENCIA','COL.NOMBRE','CAS.NOMBRE','REP.FECHA_CREACION','REP.FECHA_MODIFICACION',null);
    private $column_search_rep = array('CASINO.NOMBRE','CAS.NOMBRE');
    private $order_rep         = array('REP.FECHA_CREACION' => 'REP.FECHA_CREACION');



    /*===============================
    =            Query reporte      =
    ===============================*/
    private $rep_id_casino = 'REP.ID_CASINO';
    private $rep_id_reporte = 'REP.ID_REPORTE';
    private $rep_nombre = 'CAS.NOMBRE';
    private $rep_fecha_entrega = "TO_CHAR(REP.FECHA_ENTREGA,'DD/MM/RRRR') FECHA";
    private $rep_num_ticket_desayuno = 'REP.NUM_TICKET_DESAYUNO';
    private $rep_costo_desayuno = 'REP.COSTO_DESAYUNO';
    private $rep_num_ticket_almuerzo = 'REP.NUM_TICKET_ALMUERZO';
    private $rep_costo_almuerzo = 'REP.COSTO_ALMUERZO';
    private $rep_num_ticket_once = 'REP.NUM_TICKET_ONCE';
    private $rep_costo_once = 'REP.COSTO_ONCE';
    private $rep_num_ticket_cena = 'REP.NUM_TICKET_CENA';
    private $rep_costo_cena = 'REP.COSTO_CENA';
    private $rep_num_ticket_cena_nocturna = 'REP.NUM_TICKET_CENA_NOCTURNA';
    private $rep_costo_cena_nocturna = 'REP.COSTO_CENA_NOCTURNA';
    
    private $rep_table_reporte = 'PFSC_REPORTE_CASINO REP';
    private $rep_table_casino = 'PFSC_CASINO CAS';

    private $rep_fk_casino = 'REP.ID_CASINO = CAS.ID_CASINO';

    private $rep_where_id_sequencia = 'REP.ID_SEQUENCIA';
    private $rep_where_fecha_entrega = "REP.FECHA_ENTREGA";
    private $rep_where_casino = "REP.ID_CASINO";

    private $rep_order_by = 'REP.FECHA_ENTREGA';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        ini_set('memory_limit', '586M');
    }

    public function get_data()
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","REPORTE_CASINO_TODOS", array
            (
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;
    }

    public function get_data_search($casino,$fecha,$fecha_h)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","REPORTE_CASINO_COLACION", array
            (
                array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':FECHA','value'=>$fecha,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':FECHA_H','value'=>$fecha_h,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;
    }

    public function get_reported($state, $date_d, $date_h,$company)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","REPORTE_CASINO", array
            (
                array('name' =>':ESTADO_C','value'=>$state,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':DESDE','value'=>$date_d,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':HASTA','value'=>$date_h,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':EMPRESA','value'=>$company,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;        
    }

    public function get_service($time,$state,$casino,$type,$company)
    {  
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CASINO_SERVICIO", array
            (
                array('name' =>':FECHA','value'=>$time,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':ESTADO_C','value'=>$state,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':TIPO','value'=>$type,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':Var_Empresa','value'=>$company,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;
    }

    public function get_reported_by($state,$date_d,$date_h,$casino,$company)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","REPORTE_CASINO_POR", array
            (
                array('name' =>':ESTADO_COLACION','value'=>$state,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':DESDE','value'=>$date_d,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':HASTA','value'=>$date_h,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CASINO_C','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':EMPRESA','value'=>$company,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );
        
        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data; 
    }

    /*=============================================
    =            Section comment block            =
    =============================================*/      

    private function _get_datatables_query()
    {
        $this->db->select($this->planta);
        $this->db->select($this->casino);
        $this->db->select($this->rut);
        $this->db->select($this->nombre);
        $this->db->select($this->departamento);
        $this->db->select($this->empresa);
        $this->db->select($this->fecha);
        $this->db->select($this->hora);
        $this->db->select($this->colacion);
        $this->db->select($this->estado);
        $this->db->select($this->cantidad);

        $this->db->from($this->table_ticket);

        $this->db->join($this->table_casino, $this->fk_casino_tick);
        $this->db->join($this->table_colabo, $this->fk_colabo_tick);
        $this->db->join($this->table_colaci, $this->fk_colaci_tick);
        $this->db->join($this->table_estado, $this->fk_estado_tick);
        $this->db->join($this->table_planta, $this->fk_casino_plan);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd/mm/rrrr')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd/mm/rrrr')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd/mm/rrrr')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd/mm/rrrr')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd/mm/rrrr')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd/mm/rrrr')",false);   
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->planta_id,$_POST['planta']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->casino_id,$this->session->casino);
        }
        
            // $this->db->where('asdsa',$this->session->casino);


        if (!empty($_POST['casino'])) {
            $this->db->where($this->casino_id,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->colacion_id,$_POST['colacion']);
        }

        if (!empty($_POST['estado'])) {
            $this->db->where($this->estado_id,1);
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->rut_id, $_POST['rut'] , 'both');
        }

        if (!empty($_POST['empresa'])) {
            $empresa = $_POST['empresa'];
            $this->db->where("TRIM(UPPER($this->empresa_id)) =", "UPPER('$empresa')" , FALSE);
        }

        $this->db->group_by($this->group_planta);
        $this->db->group_by($this->group_casino);
        $this->db->group_by($this->group_rut);
        $this->db->group_by($this->group_nombre_colaborador);
        $this->db->group_by($this->group_departamento);
        $this->db->group_by($this->group_empresa);
        $this->db->group_by($this->group_fecha);
        $this->db->group_by($this->group_hora);
        $this->db->group_by($this->group_colacion);
        $this->db->group_by($this->group_estado);

        if (!empty($_POST['cantidad'])) {
           $this->db->having($this->having.' '.$_POST['cantidad'], NULL, FALSE); 
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select($this->planta);
        $this->db->select($this->casino);
        $this->db->select($this->rut);
        $this->db->select($this->nombre);
        $this->db->select($this->departamento);
        $this->db->select($this->empresa);
        $this->db->select($this->fecha);
        $this->db->select($this->hora);
        $this->db->select($this->colacion);
        $this->db->select($this->estado);
        $this->db->select($this->cantidad);

        $this->db->from($this->table_ticket);

        $this->db->join($this->table_casino, $this->fk_casino_tick);
        $this->db->join($this->table_colabo, $this->fk_colabo_tick);
        $this->db->join($this->table_colaci, $this->fk_colaci_tick);
        $this->db->join($this->table_estado, $this->fk_estado_tick);
        $this->db->join($this->table_planta, $this->fk_casino_plan);
        
        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->planta_id,$_POST['planta']);
        }


        if ($this->session->profile_id == 3) {
            $this->db->where($this->casino_id,$this->session->casino);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->casino_id,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->colacion_id,$_POST['colacion']);
        }

        if (!empty($_POST['estado'])) {
            $this->db->where($this->estado_id,$_POST['estado']);
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->rut_id, $_POST['rut'] , 'both');
        }

        if (!empty($_POST['empresa'])) {
            $empresa = $_POST['empresa'];
            $this->db->where("TRIM(UPPER($this->empresa_id)) =", "UPPER('$empresa')" , FALSE);
        }

        $this->db->group_by($this->group_planta);
        $this->db->group_by($this->group_casino);
        $this->db->group_by($this->group_rut);
        $this->db->group_by($this->group_nombre_colaborador);
        $this->db->group_by($this->group_departamento);
        $this->db->group_by($this->group_empresa);
        $this->db->group_by($this->group_fecha);
        $this->db->group_by($this->group_hora);
        $this->db->group_by($this->group_colacion);
        $this->db->group_by($this->group_estado);

        if (!empty($_POST['cantidad'])) {
           $this->db->having($this->having.' '.$_POST['cantidad'], NULL, FALSE); 
        }
                
        return $this->db->count_all_results();
    }

    /*=====  End of Section comment block  ======*/

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return $d && $d->format('d/m/Y') === $date;
    }

    public function export_excel_data()
    {
        $this->db->select($this->planta);
        $this->db->select($this->casino);
        $this->db->select($this->rut);
        $this->db->select($this->nombre);
        $this->db->select($this->departamento);
        $this->db->select($this->empresa);
        $this->db->select($this->fecha);
        $this->db->select($this->hora);
        $this->db->select($this->colacion);
        $this->db->select($this->estado);
        // $this->db->select($this->cantidad);

        $this->db->from($this->table_ticket);

        $this->db->join($this->table_casino, $this->fk_casino_tick);
        $this->db->join($this->table_colabo, $this->fk_colabo_tick);
        $this->db->join($this->table_colaci, $this->fk_colaci_tick);
        $this->db->join($this->table_estado, $this->fk_estado_tick);
        $this->db->join($this->table_planta, $this->fk_casino_plan);
        if (!empty($_POST['id_fecha'])) {
            $desde           = substr($_POST['id_fecha'], 0, 10);
            $hasta           = substr($_POST['id_fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        
        if (!empty($_POST['id_planta'])) {
            $this->db->where($this->planta_id,$_POST['id_planta']);
        }

        if (!empty($_POST['id_casino'])) {
            $this->db->where($this->casino_id,$_POST['id_casino']);
        }

        if (!empty($_POST['id_colacion'])) {
            $this->db->where($this->colacion_id,$_POST['id_colacion']);
        }

        if (!empty($_POST['id_estado'])) {
            $this->db->where($this->estado_id,$_POST['id_estado']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->casino_id,$this->session->casino);
        }

        if (!empty($_POST['id_rut'])) {
            $this->db->like($this->rut_id, $_POST['id_rut'] , 'both');
        }

        if (!empty($_POST['id_empresa'])) {
            $empresa = $_POST['id_empresa'];
            $this->db->where("TRIM(UPPER($this->empresa_id)) =", "UPPER('$empresa')" , FALSE);
        }
        
        // $this->db->group_by($this->group_planta);
        // $this->db->group_by($this->group_casino);
        // $this->db->group_by($this->group_rut);
        // $this->db->group_by($this->group_nombre_colaborador);
        // $this->db->group_by($this->group_departamento);
        // $this->db->group_by($this->group_empresa);
        // $this->db->group_by($this->group_fecha);
        // $this->db->group_by($this->group_hora);
        // $this->db->group_by($this->group_colacion);
        // $this->db->group_by($this->group_estado);

        if (!empty($_POST['id_cantidad'])) {
           $this->db->having($this->having.' '.$_POST['id_cantidad'], NULL, FALSE); 
        }
        
        $query = $this->db->get();
        return $query;
    }

    public function get_estado()
    {
        $this->db->select($this->id_estado_ticket);
        $this->db->select($this->nombre_estado);
        $this->db->from($this->table_estado);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_empresa()
    {
        $this->db->select($this->nombre_empresa);
        $this->db->from($this->table_empresa);
        $this->db->order_by($this->order_by_nombre_empresa,'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_rows_reporte($element)
    {
        $desde = substr($element->fecha, 0, 10);
        $hasta = substr($element->fecha,-10);
        if ($element->fecha != null) {
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $desde = substr($element->fecha, 0, 10);
                $hasta = substr($element->fecha,-10);
            }else{
                $desde = date("d-m-Y");
                $hasta = date("d-m-Y");
            }
        }
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CASINO_REPORTE_EDIT", array
            (
                array('name' =>':VAR_CASINO','value'=>$element->casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COLACION','value'=>$element->colacion,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_EMPRESA','value'=>$element->empresa,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_PLANTA','value'=>$element->planta,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_DESDE','value'=>$desde,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_HASTA','value'=>$hasta,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR_REP','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );
        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }
        // die(var_dump($data));
        oci_free_statement($curs);
        return $data; 
    }

    public function save_rows($element)
    {
        // die(var_dump($element));
        $this->db->trans_start();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","SAVE_REPORTE_CASINO", array
            (
                array('name' =>':VAR_CASINO','value'=>$element->id_casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_FECHA_ENTREGA','value'=>$element->fecha_entrega,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_DESAYUNO','value'=>$element->ticket_desayuno,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_DESAYUNO','value'=>$element->costo_desayuno,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_ALMUERZO','value'=>$element->ticket_almuerzo,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_ALMUERZO','value'=>$element->costo_almuerzo,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_ONCE','value'=>$element->ticket_once,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_ONCE','value'=>$element->costo_once,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_CENA','value'=>$element->ticket_cena,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_CENA','value'=>$element->costo_cena,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_CENA_NOCTURA','value'=>$element->ticket_cena_noctura,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_CENA_NOCTURA','value'=>$element->costo_cena_noctura,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_USUARIO','value'=>$element->usuario,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_NEXTVAL','value'=>$element->nextval,'type'=>SQLT_CHR, 'length'=>-1)
            )
        );
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function call_sequence()
    {
        $query = $this->db->query("SELECT REPORTE_SEQUENCE.NEXTVAL FROM DUAL");
        $result = $query->result();
        return $result[0]->NEXTVAL;
    }



    /*=============================================
    =            Section comment block            =
    =============================================*/      

    private function _get_datatables_query_rep()
    {

        $this->db->select($this->id_sequencia_rep);
        $this->db->select($this->nombre_rep);
        $this->db->select($this->casino_rep);
        $this->db->select($this->fecha_rep);
        $this->db->select($this->fecha_md_rep);

        $this->db->from($this->table_reporte_casino_rep);


        $this->db->join($this->table_casino_rep, $this->fk_casino_report_rep);
        $this->db->join($this->table_colaborador_rep, $this->fk_colaborador_report_rep);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->where_fecha_rep >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rep <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->where_fecha_rep >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rep <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->where_fecha_rep >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rep <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }


        if ($this->session->profile_id == 3) {
            $this->db->where($this->where_colaborador_rep,$this->session->collaborator);
        }

        $this->db->group_by($this->group_sequencia_rep);
        $this->db->group_by($this->group_nombre_rep);
        $this->db->group_by($this->group_casino_rep);
        $this->db->group_by($this->group_fecha_rep);
        $this->db->group_by($this->group_fecha_md_rep);
        
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_rep[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_rep)){
            $order = $this->order_rep;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables_rep()
    {
        $this->_get_datatables_query_rep();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered_rep()
    {
        $this->_get_datatables_query_rep();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_rep()
    {

        $this->db->select($this->id_sequencia_rep);
        $this->db->select($this->nombre_rep);
        $this->db->select($this->casino_rep);
        $this->db->select($this->fecha_rep);
        $this->db->select($this->fecha_md_rep);

        $this->db->from($this->table_reporte_casino_rep);


        $this->db->join($this->table_casino_rep, $this->fk_casino_report_rep);
        $this->db->join($this->table_colaborador_rep, $this->fk_colaborador_report_rep);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->where_fecha_rep >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rep <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->where_fecha_rep >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rep <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-1 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->where_fecha_rep >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rep <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }


        if ($this->session->profile_id == 3) {
            $this->db->where($this->where_colaborador_rep,$this->session->collaborator);
        }

        $this->db->group_by($this->group_sequencia_rep);
        $this->db->group_by($this->group_nombre_rep);
        $this->db->group_by($this->group_casino_rep);
        $this->db->group_by($this->group_fecha_rep);
        $this->db->group_by($this->group_fecha_md_rep);

        return $this->db->count_all_results();
    }

    public function get_reporte_save($id)
    {
        $this->db->select($this->rep_id_casino);
        $this->db->select($this->rep_id_reporte);
        $this->db->select($this->rep_nombre);
        $this->db->select($this->rep_fecha_entrega);
        $this->db->select($this->rep_num_ticket_desayuno);
        $this->db->select($this->rep_costo_desayuno);
        $this->db->select($this->rep_num_ticket_almuerzo);
        $this->db->select($this->rep_costo_almuerzo);
        $this->db->select($this->rep_num_ticket_once);
        $this->db->select($this->rep_costo_once);
        $this->db->select($this->rep_num_ticket_cena);
        $this->db->select($this->rep_costo_cena);
        $this->db->select($this->rep_num_ticket_cena_nocturna);
        $this->db->select($this->rep_costo_cena_nocturna);
        $this->db->from($this->rep_table_reporte);
        $this->db->join($this->rep_table_casino, $this->rep_fk_casino);
        $this->db->where($this->rep_where_id_sequencia,$id);
        $this->db->order_by($this->rep_order_by,'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function update_rows($element)
    {
        $this->db->trans_start();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","UPDATE_REPORTE", array
            (
                array('name' =>':VAR_REPORTE','value'=>$element->id_reporte,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_DESAYUNO','value'=>$element->ticket_desayuno,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_DESAYUNO','value'=>$element->costo_desayuno,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_ALMUERZO','value'=>$element->ticket_almuerzo,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_ALMUERZO','value'=>$element->costo_almuerzo,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_ONCE','value'=>$element->ticket_once,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_ONCE','value'=>$element->costo_once,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_CENA','value'=>$element->ticket_cena,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_CENA','value'=>$element->costo_cena,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_TICKET_CENA_NOCTURA','value'=>$element->ticket_cena_noctura,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_COSTO_CENA_NOCTURA','value'=>$element->costo_cena_noctura,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_USUARIO','value'=>$element->usuario,'type'=>SQLT_CHR, 'length'=>-1)
            )
        );
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_costo($element)
    {
        // die(var_dump($element));
        
        $query = $this->db->query("SELECT SUM(COSTO) COSTO FROM PFSC_COLACION WHERE ID_CASINO = '".$element->casino."' AND TIPO_COLACION = '".$element->tipo."' ");
        $q = $query->result();
        return (!empty($q[0]->COSTO)) ? $q[0]->COSTO : 0 ;
    }

   public function get_fecha_reporte_edit($post)
    {
        $query = $this->db->query("SELECT DISTINCT TO_CHAR(FECHA_ENTREGA,'RRRR-MM-DD') FECHA_ENTREGA,ID_SEQUENCIA FROM PFSC_REPORTE_CASINO WHERE FECHA_ENTREGA <= TRUNC(SYSDATE)");
        return $query->result();
    }

    public function get_reporte_edit($fecha,$id)
    {
        $this->db->select($this->rep_id_casino);
        $this->db->select($this->rep_id_reporte);
        $this->db->select($this->rep_nombre);
        $this->db->select($this->rep_fecha_entrega);
        $this->db->select($this->rep_num_ticket_desayuno);
        $this->db->select($this->rep_costo_desayuno);
        $this->db->select($this->rep_num_ticket_almuerzo);
        $this->db->select($this->rep_costo_almuerzo);
        $this->db->select($this->rep_num_ticket_once);
        $this->db->select($this->rep_costo_once);
        $this->db->select($this->rep_num_ticket_cena);
        $this->db->select($this->rep_costo_cena);
        $this->db->select($this->rep_num_ticket_cena_nocturna);
        $this->db->select($this->rep_costo_cena_nocturna);
        $this->db->from($this->rep_table_reporte);
        $this->db->join($this->rep_table_casino, $this->rep_fk_casino);
        if ($this->session->profile_id == 3) {
            $this->db->where($this->rep_where_casino, $this->session->casino);
        }
        $this->db->where($this->rep_where_id_sequencia,$id);
        $this->db->where("$this->rep_where_fecha_entrega =", "to_date('$fecha','dd-mm-yyyy')",false);
        $this->db->order_by($this->rep_order_by,'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_reporte_all($post)
    {
        $this->db->select($this->rep_id_casino);
        $this->db->select($this->rep_id_reporte);
        $this->db->select($this->rep_nombre);
        $this->db->select($this->rep_fecha_entrega);
        $this->db->select($this->rep_num_ticket_desayuno);
        $this->db->select($this->rep_costo_desayuno);
        $this->db->select($this->rep_num_ticket_almuerzo);
        $this->db->select($this->rep_costo_almuerzo);
        $this->db->select($this->rep_num_ticket_once);
        $this->db->select($this->rep_costo_once);
        $this->db->select($this->rep_num_ticket_cena);
        $this->db->select($this->rep_costo_cena);
        $this->db->select($this->rep_num_ticket_cena_nocturna);
        $this->db->select($this->rep_costo_cena_nocturna);
        $this->db->from($this->rep_table_reporte);
        $this->db->join($this->rep_table_casino, $this->rep_fk_casino);
        $this->db->where("$this->rep_where_fecha_entrega >=", "to_date('$post->desde','dd-mm-yyyy')",false);
        $this->db->where("$this->rep_where_fecha_entrega <=", "to_date('$post->hasta','dd-mm-yyyy')",false);
        if ($this->session->profile_id == 3) {
            $this->db->where($this->rep_where_casino, $this->session->casino);
        }else{
            if (!empty($post->casino)) {
                $this->db->where($this->rep_where_casino, $post->casino);
            }
        }
        $this->db->order_by($this->rep_where_fecha_entrega,'desc');
        $query = $this->db->get();
        return $query;
    }
    
}

/* End of file Report_model.php */
/* Location: ./application/models/Report_model.php */