<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Collation_user_model extends CI_Model {

	private $table            = 'PFSC_COLABORADOR_COLACION';
	private $id_colaborador   = 'ID_COLABORADOR';
	private $state            = 'ESTADO';
	private $maxpoint         = 'MAXTIPOCOLACION';
	private $id_casino        = 'ID_CASINO';
	private $id_colacion      = 'ID_COLACION';
	private $id_tipo_servicio = 'ID_TIPO_SERVICIO';
	private $by_created       = 'CREADO_POR';
	private $by_modify        = 'MODIFICADO_POR';


    private $nombre_planta  = 'PAL.NOMBRE AS PLANTA';
    private $nombre_casino  = 'CAS.NOMBRE AS CASINO';
    private $nombre_colacion  = 'COL.TIPO_COLACION AS COLACION';
    private $hora = 'TIK.HORA_ENTREGA';
    private $fecha  = "TO_CHAR(TIK.FECHA_ENTREGA,'DD/MM/YYYY') AS FECHA";
    private $correlativo  = 'TIK.CORRELATIVO AS CORRELATIVO';
    private $nombre_estado = 'EST.NOMBRE AS ESTADO';

    private $search_id_estado = 'TIK.ID_ESTADO';
    private $search_id_colaborador = 'COLA.ID_COLABORADOR';
    private $search_fecha_entrega = 'TIK.FECHA_ENTREGA';

    private $tabla_planta  = 'PFSC_PLANTA PAL';
    private $tabla_casino  = 'PFSC_CASINO CAS';
    private $tabla_ticket  = 'PFSC_TICKET TIK';
    private $tabla_colacion  = 'PFSC_COLACION COL';
    private $tabla_estado_ticket  = 'PFSC_ESTADO_TICKET EST';
    private $tabla_colaborador  = 'PFSC_COLABORADOR COLA';


    private $fk_tabla_casino = 'PAL.ID_PLANTA = CAS.ID_PLANTA';
    private $fk_tabla_tick_casino = 'TIK.ID_CASINO = CAS.ID_CASINO';
    private $fk_tabla_tick_colacion = 'TIK.ID_COLACION = COL.ID_COLACION';
    private $fk_tabla_tick_estado = 'TIK.ID_ESTADO = EST.ID_ESTADO';
    private $fk_tabla_tick_colaborador = 'TIK.ID_COLABORADOR = COLA.ID_COLABORADOR';


    private $group_by_planta = 'PAL.NOMBRE';
    private $group_by_casino = 'CAS.NOMBRE';
    private $group_by_colaciion = 'COL.TIPO_COLACION';
    private $group_by_hora = 'TIK.HORA_ENTREGA';
    private $group_by_fecha = 'TIK.FECHA_ENTREGA';
    private $group_by_correlativo = 'TIK.CORRELATIVO';
    private $group_by_estado = 'EST.NOMBRE';


    private $column_order_colacion  = array('TIK.FECHA_ENTREGA','PAL.NOMBRE','CAS.TIPO_COLACION','COL.NOMBRE','TIK.HORA_ENTREGA','TIK.FECHA_ENTREGA','TIK.CORRELATIVO',null);
    private $column_search_colacion = array('PAL.NOMBRE','CAS.NOMBRE','COL.TIPO_COLACION','TIK.HORA_ENTREGA','TIK.FECHA_ENTREGA','TIK.CORRELATIVO');
    private $order_colacion         = array('PAL.NOMBRE' => 'PAL.NOMBRE');


    private $tabla_estado = 'PFSC_ESTADO_TICKET';
    private $id_estado = 'ID_ESTADO';
    private $estado_nombre = 'NOMBRE';
    private $estado_ticket = 'ESTADO';

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
	}

   	private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->where($this->state, 1);

        $i = 0;

        foreach ($this->column_search as $item) {

            if ($_POST['search']['value']) {

                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
     	   $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function add($element)
    {
        $this->db->trans_start();
        $this->db->set($this->id_colaborador, $element->id_colaborador);
        $this->db->set($this->id_casino, $element->id_casino);
        $this->db->set($this->id_tipo_servicio, $element->id_typeService);
        $this->db->set($this->id_colacion, $element->id_colacion);
        $this->db->set($this->maxpoint, $element->maxcolacion);
        $this->db->set($this->by_created, $element->by_created);
        $this->db->insert($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }   
    }

    public function update($element)
    {
        $this->db->trans_start();
        $this->db->set($this->id_tipo_servicio, $element->id_typeService);
        $this->db->set($this->maxpoint, $element->maxcolacion);
        $this->db->set($this->by_modify, $element->by_modify);
        $this->db->where($this->id_colaborador,$element->id_colaborador);
        $this->db->where($this->id_casino,$element->id_casino);
        $this->db->where($this->id_colacion,$element->id_colacion);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_maxcollation($collaborator,$casino,$collation)
    {
        $this->db->select($this->maxpoint);
        $this->db->select($this->id_tipo_servicio);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador, $collaborator);
        $this->db->where($this->id_casino, $casino);
        $this->db->where($this->id_colacion, $collation);
        $query = $this->db->get();
        return $query->row();
    }

    public function delete($collaborator,$casino,$collation,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->by_modify, $by_modify);
        $this->db->set($this->state, 0);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador,$collaborator);
        $this->db->where($this->id_casino,$casino);
        $this->db->where($this->id_colacion,$collation);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_user_collation($id)
    {
        $this->db->distinct();
        $this->db->select($this->id_colaborador);
        $this->db->select($this->maxpoint);
        $this->db->select($this->id_casino);
        $this->db->select($this->id_colacion);
        $this->db->select($this->id_tipo_servicio);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador,$id);
        // $this->db->where($this->state, 1);
        $this->db->order_by($this->id_colaborador,'asc');
        $query = $this->db->get();
        //         $query = $this->db->query("SELECT COLABCA.ID_COLABORADOR,
        //        COLABCA.ID_CASINO,
        //        COLABCA.ID_COLACION,
        //        COLABORADOR.NOMBRE,
        //        CASINO.NOMBRE,
        //        COLACION.NOMBRE,
        //        TIPO.NOMBRE,
        //        COLABCA.MAXTIPOCOLACION
        // FROM   CONTROL_CASINO.PFSC_COLABORADOR_COLACION COLABCA,
        //        CONTROL_CASINO.PFSC_COLABORADOR COLABORADOR,
        //        CONTROL_CASINO.PFSC_CASINO CASINO,
        //        CONTROL_CASINO.PFSC_COLACION COLACION,
        //        CONTROL_CASINO.PFSC_TIPO_SERVICIO TIPO
        // WHERE      COLABCA.ID_COLABORADOR = COLABORADOR.ID_COLABORADOR
        //        AND COLABCA.ID_CASINO = CASINO.ID_CASINO
        //        AND COLABCA.ID_COLACION = COLACION.ID_COLACION
        //        AND COLABCA.ID_CASINO = COLACION.ID_CASINO
        //        AND COLABCA.ID_TIPO_SERVICIO = TIPO.ID_TIPO_SERVICIO
        //        AND COLABCA.ID_COLABORADOR = ".$ID."
        //        ORDER BY COLABCA.ID_CASINO, COLABCA.ID_COLACION");
        return $query;
    }

    
    public function verify($element)
    {
        $this->db->select($this->id_colacion);
        $this->db->from($this->table);
        $this->db->where($this->id_colaborador,$element->id_colaborador);
        $this->db->where($this->id_colacion,$element->id_colacion);
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query_mi_colacion()
    {
        $this->db->select($this->nombre_planta);
        $this->db->select($this->nombre_casino);
        $this->db->select($this->nombre_colacion);
        $this->db->select($this->hora);
        $this->db->select($this->fecha);
        $this->db->select($this->correlativo);
        $this->db->select($this->nombre_estado);

        $this->db->from($this->tabla_ticket);

        $this->db->join($this->tabla_casino, $this->fk_tabla_tick_casino);
        $this->db->join($this->tabla_colacion, $this->fk_tabla_tick_colacion);
        $this->db->join($this->tabla_estado_ticket, $this->fk_tabla_tick_estado);
        $this->db->join($this->tabla_colaborador, $this->fk_tabla_tick_colaborador);
        $this->db->join($this->tabla_planta, $this->fk_tabla_casino);

        // $this->db->where($this->search_id_estado, 1);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->search_fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->search_fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->search_fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->search_fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->search_fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->search_fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        // $this->db->where($this->search_id_estado,1);
        $this->db->where($this->search_id_colaborador, $this->session->collaborator);
        $i = 0;

        foreach ($this->column_search_colacion as $item) {
            if ($_POST['search']['value']) {
                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }

        $this->db->group_by($this->group_by_planta);
        $this->db->group_by($this->group_by_casino);
        $this->db->group_by($this->group_by_colaciion);
        $this->db->group_by($this->group_by_hora);
        $this->db->group_by($this->group_by_fecha);
        $this->db->group_by($this->group_by_correlativo);
        $this->db->group_by($this->group_by_estado);

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_colacion[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_colacion)){
            $order = $this->order_colacion;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables_mi_colacion()
    {
        $this->_get_datatables_query_mi_colacion();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered_mi_colacion()
    {
        $this->_get_datatables_query_mi_colacion();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_mi_colacion()
    {
        $this->db->select($this->nombre_planta);
        $this->db->select($this->nombre_casino);
        $this->db->select($this->nombre_colacion);
        $this->db->select($this->hora);
        $this->db->select($this->fecha);
        $this->db->select($this->correlativo);
        $this->db->select($this->nombre_estado);

        $this->db->from($this->tabla_ticket);

        $this->db->join($this->tabla_casino, $this->fk_tabla_tick_casino);
        $this->db->join($this->tabla_colacion, $this->fk_tabla_tick_colacion);
        $this->db->join($this->tabla_estado_ticket, $this->fk_tabla_tick_estado);
        $this->db->join($this->tabla_colaborador, $this->fk_tabla_tick_colaborador);
        $this->db->join($this->tabla_planta, $this->fk_tabla_casino);

        // $this->db->where($this->search_id_estado, 1);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->search_fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->search_fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->search_fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->search_fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->search_fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->search_fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        // $this->db->where($this->search_id_estado,1);
        $this->db->where($this->search_id_colaborador, $this->session->collaborator);
        $i = 0;

        foreach ($this->column_search_colacion as $item) {
            if ($_POST['search']['value']) {
                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }

        $this->db->group_by($this->group_by_planta);
        $this->db->group_by($this->group_by_casino);
        $this->db->group_by($this->group_by_colaciion);
        $this->db->group_by($this->group_by_hora);
        $this->db->group_by($this->group_by_fecha);
        $this->db->group_by($this->group_by_correlativo);
        $this->db->group_by($this->group_by_estado);

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_colacion[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_colacion)){
            $order = $this->order_colacion;
            $this->db->order_by(key($order), $order[key($order)]);
        }
                
        return $this->db->count_all_results();
    }

    /*=====  End of Section comment block  ======*/

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return $d && $d->format('d/m/Y') === $date;
    }


    public function get_my_quantity($id)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","MIS_CANTIDAD_COLACION", array
            (
                array('name' =>':COLABORADOR','value'=>$id,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );
        
        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }
        oci_free_statement($curs);
        
        $result = array();

        if (!empty($data)) {
            foreach ($data as $key) {
                $result [] = (object) array(
                    "casino_q" => ucwords(mb_strtolower($key->CASINO)),
                    "fecha"    => $key->FECHA,
                    "total"    => $key->TOTAL,
                    "class"    => $this->elementClass()
                );
            }
        }else{
            $result[] = (object) array(
                "casino_q" => 'Sin datos',
                "fecha"    => 'Sin datos',
                "total"    => 'N° Ticket : Sin datos',
                "class"    => $this->elementClass()
            ); 
        }

        return $result;
    }

    private function elementClass()
    {
        $color = array("bg-yellow", "bg-orange", "bg-blue", "bg-maroon", "bg-gray","bg-aqua");
        return $color[array_rand($color)];
    }

    public function get_estado_colacion()
    {
        $this->db->select($this->id_estado);
        $this->db->select($this->estado_nombre);
        $this->db->from($this->tabla_estado);
        $this->db->where($this->estado_ticket, 1);
        $query = $this->db->get();
        return $query->result();
    }
}

/* End of file Collation_user_model.php */
/* Location: ./application/models/Collation_user_model.php */