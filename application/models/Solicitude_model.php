<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Solicitude_model extends CI_Model {
	
	private $table        = 'PFSC_SOLICITUD';
	private $id_solicitud = 'ID_SOLICITUD';
	private $by_modify    = 'MODIFICADO_POR';
	private $state        = 'ESTADO';
	private $activity     = 'ACTIVA';


    private $servicio = 'ID_TIPO_SERVICIO';
    private $table_colaborador = 'PFSC_COLABORADOR_COLACION';
    private $id_colaborador = 'ID_COLABORADOR';
    private $id_casino = 'ID_CASINO';
    private $id_colacion = 'ID_COLACION'; 
    private $id_tipo = 'ID_TIPO_SERVICIO';
    private $id_estado = 'ESTADO';
    private $correo_admin = 'CORREO_ADMIN_PF';
    private $table_casino = 'PFSC_CASINO';

    /*=============================================
    =               Query Pendiente              =
    =============================================*/
    private $id_solicitud_pen = 'SOL.ID_SOLICITUD';
    private $fecha_pend = "TO_CHAR(SOL.FECHA_COLACION,'DD/MM/RRRR') FECHA";
    private $id_colaborador_pen = 'SOL.ID_COLABORADOR';
    private $codigo_colaborador_pen = 'COL.CODIGO';
    private $nombre_colaborador_pen = 'COL.NOMBRE COLABORADOR';
    private $departamento_pen = 'COL.DEPARTAMENTO';
    private $empresa_pen = 'COL.EMPRESA';
    private $nombre_planta_pen = 'PLA.NOMBRE NOMBRE_PLANTA';
    private $nombre_casino_pen = 'CAS.NOMBRE NOMBRE_CASINO';
    private $nombre_colacion_pen = 'COLA.TIPO_COLACION NOMBRE_COLACION';
    private $comentario_pen = 'SOL.JUSTIFICACION';


    private $id_actividad = 'SOL.ACTIVA';
    private $fecha_pend_noas = 'SOL.FECHA_COLACION';
    private $fecha_pend_sys = "TO_DATE(SYSDATE,'DD/MM/RRRR')";

    private $table_pend_sol = 'PFSC_SOLICITUD SOL';
    private $table_pend_col = 'PFSC_COLABORADOR COL';
    private $table_pend_cas = 'PFSC_CASINO CAS';
    private $table_pend_pla = 'PFSC_PLANTA PLA';
    private $table_pend_cola = 'PFSC_COLACION COLA';


    private $fk_table_col = 'SOL.ID_COLABORADOR = COL.ID_COLABORADOR';
    private $fk_table_cas = 'SOL.ID_CASINO = CAS.ID_CASINO';
    private $fk_table_pla = 'CAS.ID_PLANTA = PLA.ID_PLANTA';
    private $fk_table_cola = 'SOL.ID_COLACION = COLA.ID_COLACION';


    private $column_order  = array('SOL.ID_SOLICITUD','SOL.FECHA_COLACION','COL.CODIGO','COL.NOMBRE','COL.DEPARTAMENTO','COL.EMPRESA','PLA.NOMBRE','CAS.NOMBRE','COLA.TIPO_COLACION',null);
    private $column_search = array('COL.CODIGO','COL.NOMBRE','COL.DEPARTAMENTO','PLA.NOMBRE','CAS.NOMBRE','COLA.TIPO_COLACION');
    private $order         = array('SOL.FECHA_COLACION' => 'SOL.FECHA_COLACION');

    private $id_planta_pen = 'PLA.ID_PLANTA';
    private $id_casino_pen = 'CAS.ID_CASINO';
    private $id_colacion_pen = 'COLA.ID_COLACION';


    /*=============================================
    =               Query Aprobado               =
    =============================================*/

    private $id_ticket_apr = 'TIC.ID_TICKET';
    private $id_solicitud_apr = 'SOL.ID_SOLICITUD';
    private $fecha_apr = "TO_CHAR(SOL.FECHA_COLACION,'DD/MM/RRRR') FECHA";
    private $id_rut_apr = 'COL.CODIGO';
    private $nombre_apr = 'COL.NOMBRE';
    private $departamento_apr = 'COL.DEPARTAMENTO';
    private $empresa_apr = 'COL.EMPRESA';
    private $casino_apr = 'CAS.NOMBRE CASINO';
    private $planta_apr = 'PLA.NOMBRE PLANTA';
    private $colacion_apr = 'CLA.TIPO_COLACION COLACION';
    private $comentario_apr = 'SOL.JUSTIFICACION';

    private $table_ticket_apr = 'PFSC_TICKET TIC';
    private $table_solicitud_apr = 'PFSC_SOLICITUD SOL';
    private $table_colaborador_apr = 'PFSC_COLABORADOR COL';
    private $table_casino_apr = 'PFSC_CASINO CAS';
    private $table_colacion_apr = 'PFSC_COLACION CLA';
    private $table_planta_apr = 'PFSC_PLANTA PLA';

    private $fk_table_solicitud_apr = 'TIC.ID_SOLICITUD = SOL.ID_SOLICITUD';
    private $fk_table_colaborador_tick_apr = 'TIC.ID_COLABORADOR = COL.ID_COLABORADOR';
    private $fk_table_colaborador_soli_apr = 'SOL.ID_COLABORADOR = COL.ID_COLABORADOR';
    private $fk_table_casino_tick_apr = 'TIC.ID_CASINO = CAS.ID_CASINO';
    private $fk_table_casino_soli_apr = 'SOL.ID_CASINO = CAS.ID_CASINO';
    private $fk_table_colacion_tick_apr = 'TIC.ID_COLACION = CLA.ID_COLACION';
    private $fk_table_colacion_soli_apr = 'SOL.ID_COLACION = CLA.ID_COLACION';
    private $fk_table_planta_apr = 'CAS.ID_PLANTA = PLA.ID_PLANTA';

    private $where_sol_estado_apr = 'SOL.ACTIVA';
    private $where_tic_estado_apr = 'TIC.ID_ESTADO';
    private $where_planta_apr = 'PLA.ID_PLANTA';
    private $where_casino_apr = 'CAS.ID_CASINO';
    private $where_colacion_apr = 'CLA.ID_COLACION';
    private $where_fecha_apr = 'TIC.FECHA_COLACION';

    private $column_order_apr  = array('TIC.ID_TICKET','SOL.ID_SOLICITUD','SOL.FECHA_COLACION','COL.CODIGO','COL.NOMBRE','COL.DEPARTAMENTO','COL.EMPRESA','PLA.NOMBRE','CAS.NOMBRE','CLA.TIPO_COLACION','SOL.JUSTIFICACION',null);
    private $column_search_apr = array('COL.CODIGO','COL.NOMBRE','COL.DEPARTAMENTO','COL.EMPRESA','CAS.NOMBRE','PLA.NOMBRE','CLA.TIPO_COLACION');
    private $order_apr         = array('SOL.FECHA_COLACION' => 'SOL.FECHA_COLACION');


    /*=============================================
    =               Query Rechazado               =
    =============================================*/

    private $id_solicitud_rec = 'SOL.ID_SOLICITUD';
    private $fecha_rec = "TO_CHAR(SOL.FECHA_COLACION,'DD/MM/RRRR') FECHA";
    private $rut_rec = 'COL.CODIGO';
    private $nombre_rec = 'COL.NOMBRE';
    private $departamento_rec = 'COL.DEPARTAMENTO';
    private $empresa_rec = 'COL.EMPRESA';
    private $planta_rec = 'PLA.NOMBRE PLANTA';
    private $casino_rec = 'CAS.NOMBRE CASINO';
    private $colacion_rec = 'CLA.TIPO_COLACION COLACION';
    private $comentario_rec = 'SOL.JUSTIFICACION';

    private $table_solicitud_rec = 'PFSC_SOLICITUD SOL';
    private $table_colaborador_rec = 'PFSC_COLABORADOR COL';
    private $table_casino_rec = 'PFSC_CASINO CAS';
    private $table_colacion_rec = 'PFSC_COLACION CLA';
    private $table_planta_rec = 'PFSC_PLANTA PLA';


    private $fk_table_cola_rec = 'SOL.ID_COLABORADOR = COL.ID_COLABORADOR';
    private $fk_table_casi_rec = 'SOL.ID_CASINO = CAS.ID_CASINO';
    private $fk_table_clac_rec = 'SOL.ID_COLACION = CLA.ID_COLACION';
    private $fk_table_plan_rec = 'CAS.ID_PLANTA = PLA.ID_PLANTA';

    private $where_estado_rec = 'SOL.ACTIVA';
    private $where_planta_rec = 'PLA.ID_PLANTA';
    private $where_casino_rec = 'CAS.ID_CASINO';
    private $where_colacion_rec = 'CLA.ID_COLACION';
    private $where_fecha_rec = 'SOL.FECHA_COLACION';


    private $column_order_rec  = array('SOL.ID_SOLICITUD','SOL.FECHA_COLACION','COL.CODIGO','COL.NOMBRE','COL.DEPARTAMENTO','COL.EMPRESA','PLA.NOMBRE','CAS.NOMBRE','CLA.TIPO_COLACION','SOL.JUSTIFICACION',null);
    private $column_search_rec = array('COL.CODIGO','COL.NOMBRE','COL.DEPARTAMENTO','COL.EMPRESA','CAS.NOMBRE','PLA.NOMBRE','CLA.TIPO_COLACION');
    private $order_rec         = array('SOL.FECHA_COLACION' => 'SOL.FECHA_COLACION');


	public function __construct()
	{
		parent::__construct();
        ini_set('memory_limit', '586M');
        $this->load->database();
        $this->hpferi = $this->load->database('hpferi', TRUE);
	}

	public function deleted($id,$by_modify)
	{
        $this->db->trans_start();
        $this->db->set($this->by_modify, $by_modify);
        $this->db->set($this->state, 0);
        $this->db->where($this->id_solicitud,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
	}

    public function rejection($id,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->by_modify, $by_modify);
        $this->db->set($this->activity, 2);
        $this->db->where($this->id_solicitud,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function aproved($id,$by_modify,$rut)
    {
        #activity 1;
        $this->db->trans_start();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","APROBAR_SOLICITUD", array
            (
                array('name' =>':VAR_ID','value'=>$id,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_USER','value'=>$by_modify,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_RUT','value'=>$rut,'type'=>SQLT_CHR, 'length'=>-1),
            )
        );
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function save_solicitud($element)
    {
        $this->db->trans_start();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","GUARDAR_SOLICITUD", array
            (
                array('name' =>':COLABORADOR','value'=>$element->colaborador,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CASINO','value'=>$element->casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':COLACION','value'=>$element->colacion,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':FECHA','value'=>$element->fecha,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':DETALLE','value'=>$element->detalle,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':MODIFICADO','value'=>$element->creado,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':RUT','value'=>$element->rut,'type'=>SQLT_CHR, 'length'=>-1)
            )
        );
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function getCasino($id_plant,$id_col)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CASINO_COLABORADOR", array
            (
                array('name' =>':PLANTA','value'=>$id_plant,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':COLABORADOR','value'=>$id_col,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;      
    }

    public function getColation($id_casino,$id_col)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","COLACION_COLABORADOR", array
            (
                array('name' =>':CASINO','value'=>$id_casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':COLABORADOR','value'=>$id_col,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;        
    }




    /*=============================================
    =            Section comment block            =
    =============================================*/      

    private function _get_datatables_query()
    {
        // die(var_dump($_POST['empresa']));
        $this->db->select($this->id_solicitud_pen);
        $this->db->select($this->fecha_pend);
        $this->db->select($this->id_colaborador_pen);
        $this->db->select($this->codigo_colaborador_pen);
        $this->db->select($this->nombre_colaborador_pen);
        $this->db->select($this->departamento_pen);
        $this->db->select($this->empresa_pen);
        $this->db->select($this->nombre_planta_pen);
        $this->db->select($this->nombre_casino_pen);
        $this->db->select($this->nombre_colacion_pen);
        $this->db->select($this->comentario_pen);

        $this->db->from($this->table_pend_sol);

        $this->db->join($this->table_pend_col, $this->fk_table_col);
        $this->db->join($this->table_pend_cas, $this->fk_table_cas);
        $this->db->join($this->table_pend_pla, $this->fk_table_pla);
        $this->db->join($this->table_pend_cola, $this->fk_table_cola);

        $this->db->where($this->id_actividad, 3);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);

            // $desde           = '01/01/2017';
            // $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_pend_noas >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_pend_noas <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $this->db->where("$this->fecha_pend_noas >=", "to_date('$query_date','dd-mm-yyyy')",false);
            }
        }else{
                $query_date = date("d-m-Y");
                $this->db->where("$this->fecha_pend_noas >=", "to_date('$query_date','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->id_planta_pen,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->id_casino_pen,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->id_colacion_pen,$_POST['colacion']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->id_casino_pen,$this->session->casino);
        }

        
        if (!empty($_POST['empresa'])) {
            $this->db->where($this->empresa_pen,strtoupper($_POST['empresa']));
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->codigo_colaborador_pen, strtoupper($_POST['rut']) , 'both');
        }

        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select($this->id_solicitud_pen);
        $this->db->select($this->fecha_pend);
        $this->db->select($this->id_colaborador_pen);
        $this->db->select($this->codigo_colaborador_pen);
        $this->db->select($this->nombre_colaborador_pen);
        $this->db->select($this->departamento_pen);
        $this->db->select($this->empresa_pen);
        $this->db->select($this->nombre_planta_pen);
        $this->db->select($this->nombre_casino_pen);
        $this->db->select($this->nombre_colacion_pen);
        $this->db->select($this->comentario_pen);

        $this->db->from($this->table_pend_sol);

        $this->db->join($this->table_pend_col, $this->fk_table_col);
        $this->db->join($this->table_pend_cas, $this->fk_table_cas);
        $this->db->join($this->table_pend_pla, $this->fk_table_pla);
        $this->db->join($this->table_pend_cola, $this->fk_table_cola);

        $this->db->where($this->id_actividad, 3);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_pend_noas >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_pend_noas <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $this->db->where("$this->fecha_pend_noas >=", "to_date('$query_date','dd-mm-yyyy')",false);
            }
        }else{
                $query_date = date("d-m-Y");
                $this->db->where("$this->fecha_pend_noas >=", "to_date('$query_date','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->id_planta_pen,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->id_casino_pen,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->id_colacion_pen,$_POST['colacion']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->id_casino_pen,$this->session->casino);
        }

        
        if (!empty($_POST['empresa'])) {
            $this->db->where($this->empresa_pen,strtoupper($_POST['empresa']));
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->codigo_colaborador_pen, strtoupper($_POST['rut']) , 'both');
        }
                
        return $this->db->count_all_results();
    }

    /*=====  End of Section comment block  ======*/

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return $d && $d->format('d/m/Y') === $date;
    }



    public function solicitude_pending($casino,$data_d,$date_h)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","SOLICITUDES_PENDIENTE", array
            (
                array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':FECHA','value'=>$data_d,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':FECHA_H','value'=>$date_h,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;
    }

    public function solicitude_pending_all($casino)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","SOLICITUDES_PENDIENTE_TODO", array
            (
                array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;
    }


    /*=============================================
    =            Section comment block            =
    =============================================*/
    
    
    
    private function _get_datatables_query_apr()
    {
        $this->db->select($this->id_ticket_apr);
        $this->db->select($this->id_solicitud_apr);
        $this->db->select($this->fecha_apr);
        $this->db->select($this->id_rut_apr);
        $this->db->select($this->nombre_apr);
        $this->db->select($this->departamento_apr);
        $this->db->select($this->empresa_apr);
        $this->db->select($this->casino_apr);
        $this->db->select($this->planta_apr);
        $this->db->select($this->colacion_apr);
        $this->db->select($this->comentario_apr);

        $this->db->from($this->table_ticket_apr);

        $this->db->join($this->table_solicitud_apr, $this->fk_table_solicitud_apr);
        $this->db->join($this->table_colaborador_apr, $this->fk_table_colaborador_tick_apr);
        $this->db->join($this->table_colaborador_apr, $this->fk_table_colaborador_soli_apr);
        $this->db->join($this->table_casino_apr, $this->fk_table_casino_tick_apr);
        $this->db->join($this->table_casino_apr, $this->fk_table_casino_soli_apr);
        $this->db->join($this->table_colacion_apr, $this->fk_table_colacion_tick_apr);
        $this->db->join($this->table_colacion_apr, $this->fk_table_colacion_soli_apr);
        $this->db->join($this->table_planta_apr, $this->fk_table_planta_apr);

        $this->db->where($this->where_sol_estado_apr,1);
        $this->db->where($this->where_tic_estado_apr,1);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->where_fecha_apr >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_apr <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $this->db->where("$this->where_fecha_apr >=", "to_date('$query_date','dd-mm-yyyy')",false);
            }
        }else{
                $query_date = date("d-m-Y");
                $this->db->where("$this->where_fecha_apr >=", "to_date('$query_date','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->where_planta_apr,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->where_casino_apr,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->where_colacion_apr,$_POST['colacion']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->where_casino_apr,$this->session->casino);
        }

        
        if (!empty($_POST['empresa'])) {
            $this->db->where($this->empresa_apr,strtoupper($_POST['empresa']));
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->id_rut_apr, strtoupper($_POST['rut']) , 'both');
        }


        $i = 0;

        foreach ($this->column_search_apr as $item) {
            if ($_POST['search']['value']) {
                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_apr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_apr)){
            $order = $this->order_apr;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables_apr()
    {
        $this->_get_datatables_query_apr();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered_apr()
    {
        $this->_get_datatables_query_apr();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_apr()
    {
        $this->db->select($this->id_ticket_apr);
        $this->db->select($this->id_solicitud_apr);
        $this->db->select($this->fecha_apr);
        $this->db->select($this->id_rut_apr);
        $this->db->select($this->nombre_apr);
        $this->db->select($this->departamento_apr);
        $this->db->select($this->empresa_apr);
        $this->db->select($this->casino_apr);
        $this->db->select($this->planta_apr);
        $this->db->select($this->colacion_apr);
        $this->db->select($this->comentario_apr);

        $this->db->from($this->table_ticket_apr);

        $this->db->join($this->table_solicitud_apr, $this->fk_table_solicitud_apr);
        $this->db->join($this->table_colaborador_apr, $this->fk_table_colaborador_tick_apr);
        $this->db->join($this->table_colaborador_apr, $this->fk_table_colaborador_soli_apr);
        $this->db->join($this->table_casino_apr, $this->fk_table_casino_tick_apr);
        $this->db->join($this->table_casino_apr, $this->fk_table_casino_soli_apr);
        $this->db->join($this->table_colacion_apr, $this->fk_table_colacion_tick_apr);
        $this->db->join($this->table_colacion_apr, $this->fk_table_colacion_soli_apr);
        $this->db->join($this->table_planta_apr, $this->fk_table_planta_apr);

        $this->db->where($this->where_sol_estado_apr,1);
        $this->db->where($this->where_tic_estado_apr,1);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->where_fecha_apr >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_apr <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $this->db->where("$this->where_fecha_apr >=", "to_date('$query_date','dd-mm-yyyy')",false);
            }
        }else{
                $query_date = date("d-m-Y");
                $this->db->where("$this->where_fecha_apr >=", "to_date('$query_date','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->where_planta_apr,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->where_casino_apr,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->where_colacion_apr,$_POST['colacion']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->where_casino_apr,$this->session->casino);
        }

        
        if (!empty($_POST['empresa'])) {
            $this->db->where($this->empresa_apr,strtoupper($_POST['empresa']));
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->id_rut_apr, strtoupper($_POST['rut']) , 'both');
        }
                
        return $this->db->count_all_results();
    }



    /*=============================================
    =            Section comment block            =
    =============================================*/
    
    private function _get_datatables_query_rec()
    {

        $this->db->select($this->id_solicitud_rec);
        $this->db->select($this->fecha_rec);
        $this->db->select($this->rut_rec);
        $this->db->select($this->nombre_rec);
        $this->db->select($this->departamento_rec);
        $this->db->select($this->empresa_rec);
        $this->db->select($this->planta_rec);
        $this->db->select($this->casino_rec);
        $this->db->select($this->colacion_rec);
        $this->db->select($this->comentario_rec);

        $this->db->from($this->table_solicitud_rec);

        $this->db->join($this->table_colaborador_rec, $this->fk_table_cola_rec);
        $this->db->join($this->table_casino_rec, $this->fk_table_casi_rec);
        $this->db->join($this->table_colacion_rec, $this->fk_table_clac_rec);
        $this->db->join($this->table_planta_rec, $this->fk_table_plan_rec);

        $this->db->where($this->where_estado_rec,2);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->where_fecha_rec >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rec <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $this->db->where("$this->where_fecha_rec >=", "to_date('$query_date','dd-mm-yyyy')",false);
            }
        }else{
                $query_date = date("d-m-Y");
                $this->db->where("$this->where_fecha_rec >=", "to_date('$query_date','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->where_planta_rec,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->where_casino_rec,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->where_colacion_rec,$_POST['colacion']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->where_casino_rec,$this->session->casino);
        }

        
        if (!empty($_POST['empresa'])) {
            $this->db->where($this->empresa_rec,strtoupper($_POST['empresa']));
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->rut_rec, strtoupper($_POST['rut']) , 'both');
        }


        $i = 0;

        foreach ($this->column_search_rec as $item) {
            if ($_POST['search']['value']) {
                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_rec[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_rec)){
            $order = $this->order_rec;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables_rec()
    {
        $this->_get_datatables_query_rec();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered_rec()
    {
        $this->_get_datatables_query_apr();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_rec()
    {
        $this->db->select($this->id_solicitud_rec);
        $this->db->select($this->fecha_rec);
        $this->db->select($this->rut_rec);
        $this->db->select($this->nombre_rec);
        $this->db->select($this->departamento_rec);
        $this->db->select($this->empresa_rec);
        $this->db->select($this->planta_rec);
        $this->db->select($this->casino_rec);
        $this->db->select($this->colacion_rec);
        $this->db->select($this->comentario_rec);

        $this->db->from($this->table_solicitud_rec);

        $this->db->join($this->table_colaborador_rec, $this->fk_table_cola_rec);
        $this->db->join($this->table_casino_rec, $this->fk_table_casi_rec);
        $this->db->join($this->table_colacion_rec, $this->fk_table_clac_rec);
        $this->db->join($this->table_planta_rec, $this->fk_table_plan_rec);

        $this->db->where($this->where_estado_rec,2);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->where_fecha_rec >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->where_fecha_rec <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $this->db->where("$this->where_fecha_rec >=", "to_date('$query_date','dd-mm-yyyy')",false);
            }
        }else{
                $query_date = date("d-m-Y");
                $this->db->where("$this->where_fecha_rec >=", "to_date('$query_date','dd-mm-yyyy')",false);
        }
        
        if (!empty($_POST['planta'])) {
            $this->db->where($this->where_planta_rec,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->where_casino_rec,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->where_colacion_rec,$_POST['colacion']);
        }

        if ($this->session->profile_id == 3) {
            $this->db->where($this->where_casino_rec,$this->session->casino);
        }

        
        if (!empty($_POST['empresa'])) {
            $this->db->where($this->empresa_rec,strtoupper($_POST['empresa']));
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->rut_rec, strtoupper($_POST['rut']) , 'both');
        }

                
        return $this->db->count_all_results();
    }



    public function check_solicitud($id_colaborador,$id_casino,$id_colacion)
    {
        $this->db->select($this->id_colaborador);
        $this->db->select($this->servicio);
        $this->db->from($this->table_colaborador);
        $this->db->where($this->id_colaborador, $id_colaborador);
        $this->db->where($this->id_casino, $id_casino);
        $this->db->where($this->id_colacion, $id_colacion);
        $this->db->where($this->id_tipo, 3);
        $query = $this->db->get();
        return $query->result();        
    }

    public function get_email_supervisor($id_casino)
    {
        $this->db->select($this->id_casino);
        $this->db->select($this->correo_admin);
        $this->db->from($this->table_casino);
        $this->db->where($this->id_casino,$id_casino);
        $query = $this->db->get();
        return $query->result();
    }

    public function check_hour($id,$fecha)
    {   
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","GET_COLACION_TIEMPO_ACTUAL", array
            (
                array('name' =>':VAR_COLACION','value'=>$id,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );
        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }
        oci_free_statement($curs);
        $fecha = DateTime::createFromFormat('d/m/Y', $fecha)->format('d/m/Y');
        $fecha_actual = DateTime::createFromFormat('d/m/Y',substr($data[0]->FECHA_HORA_ACTUAL, 0, 10))->format('d/m/Y');
        
        if ($fecha > $fecha_actual) {
            return true;
        }else{
            if (strtotime($data[0]->HORA) <= strtotime($data[0]->CORTE_SOLICITUD)) {
                return true;
            }else{
                return false;
            }   
        }
    }

    public function get_empleado_jefatura($rut)
    {
        // die(var_dump($rut)});
        $curs = $this->hpferi->get_cursor();
        $this->hpferi->stored_procedure("PF_PACKAGE_CASINO","GET_EMPLEADO_JEFATURA", array
            (
                array('name' =>':VAR_RUT','value'=>$rut,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;       
    }

    public function get_solicitante($id)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","GET_SOLICITANTE", array
            (
                array('name' =>':VAR_ID','value'=>$id,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':VAR_CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data; 
    }

}

/* End of file Solicitude_model.php */
/* Location: ./application/models/Solicitude_model.php */