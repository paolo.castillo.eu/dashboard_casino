<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Casino_model extends CI_Model {
	
    private $table          = 'PFSC_CASINO';
    private $id_casino      = 'ID_CASINO';
    private $name           = 'NOMBRE';
    private $email_adim     = 'CORREO_ADMIN';
    private $email_admin_pf = 'CORREO_ADMIN_PF';
    private $capacity       = 'CAPACIDAD';
    private $id_planta      = 'ID_PLANTA';
    private $by_created     = 'CREADO_POR';
    private $by_modify      = 'MODIFICADO_POR';
    private $state          = 'ESTADO';
    private $flag_valida_marca = 'FLAG_VALIDA_MARCA';
    private $correo_notif_adm = 'CORREO_NOTIF_ADM';
    private $correo_notif_rrhh = 'CORREO_NOTIF_RRHH';
    private $correo_notif_seguridad = 'CORREO_NOTIF_SEGURIDAD';
    private $flag_envia_marca = 'FLAG_ENVIA_MARCA';
    private $correo_notif_marca = 'CORREO_NOTIF_MARCA';


	// TABLE Function //
	private $column_order  = array('ID_CASINO','NOMBRE','CORREO_ADMIN','CORREO_ADMIN_PF','CAPACIDAD','ID_PLANTA',null);
	private $column_search = array('NOMBRE');
	private $order         = array('ID_CASINO' => 'ID_CASINO');

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
	}

	public function add($element)
	{
        $this->db->trans_start();
        
        $this->db->set($this->id_casino, $element->id_casino);
        $this->db->set($this->name, $element->name);
        $this->db->set($this->email_adim, $element->email);
        $this->db->set($this->email_admin_pf, $element->email_pf);
        $this->db->set($this->capacity, $element->total);
        $this->db->set($this->id_planta, $element->plant);
        $this->db->set($this->by_created, $element->by_create);
        if (!empty($element->v_marca)) {
            $this->db->set($this->flag_valida_marca,$element->v_marca);
        }
        if (!empty($element->email_notif_adm)) {
            $this->db->set($this->correo_notif_adm,$element->email_notif_adm);
        }
        if (!empty($element->email_notifi_rrhh)) {
            $this->db->set($this->correo_notif_rrhh,$element->email_notifi_rrhh);
        }
        if (!empty($element->email_notifi_seg)) {
            $this->db->set($this->correo_notif_seguridad,$element->email_notifi_seg);
        }
        if (!empty($element->v_correo_marca)) {
            $this->db->set($this->flag_envia_marca,$element->v_correo_marca);
        }
        if (!empty($element->email_notifi_mark)) {
            $this->db->set($this->correo_notif_marca,$element->email_notifi_mark);
        }
        $this->db->insert($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

   	private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->where($this->state, 1);

        $i = 0;

        foreach ($this->column_search as $item) {

            if ($_POST['search']['value']) {

                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
     	   $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_by_id($id)
	{
		$this->db->select($this->id_casino);
		$this->db->select($this->name);
		$this->db->select($this->email_adim);
        $this->db->select($this->email_admin_pf);
		$this->db->select($this->capacity);
        $this->db->select($this->id_planta);
        $this->db->select($this->flag_valida_marca);
        $this->db->select($this->correo_notif_adm);
        $this->db->select($this->correo_notif_rrhh);
        $this->db->select($this->correo_notif_seguridad);
        $this->db->select($this->flag_envia_marca);
        $this->db->select($this->correo_notif_marca);
		$this->db->from($this->table);
		$this->db->where($this->id_casino, $id);
        $this->db->where($this->state, 1);
		$query = $this->db->get();
		return $query->row();
	}

    public function update($element)
    {
        $this->db->trans_start();

        $this->db->set($this->name,$element->name);
        $this->db->set($this->email_adim,$element->email);
        $this->db->set($this->email_admin_pf,$element->email_pf);
        $this->db->set($this->capacity,$element->total);
        $this->db->set($this->id_planta,$element->plant);
        $this->db->set($this->by_modify,$element->by_modify);

        $this->db->set($this->flag_valida_marca,$element->v_marca);

        // if (!empty($element->email_notif_adm)) {
            $this->db->set($this->correo_notif_adm,$element->email_notif_adm);
        // }
        // if (!empty($element->email_notifi_rrhh)) {
            $this->db->set($this->correo_notif_rrhh,$element->email_notifi_rrhh);
        // }
        // if (!empty($element->email_notifi_seg)) {
            $this->db->set($this->correo_notif_seguridad,$element->email_notifi_seg);
        // }

        $this->db->set($this->flag_envia_marca,$element->v_correo_marca);

        // if (!empty($element->email_notifi_mark)) {
            $this->db->set($this->correo_notif_marca,$element->email_notifi_mark);
        // }

        $this->db->where($this->id_casino,$element->id_casino);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($id,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->by_modify, $by_modify);
        $this->db->set($this->state, 0);
        $this->db->where($this->id_casino,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_casino_planta($id_planta)
    {
        $this->db->select($this->id_casino);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_planta,$id_planta);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_all()
    {
        $this->db->select($this->id_casino);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();        
    }

    public function get_name($id)
    {
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_casino,$id);
        $this->db->where($this->state, 1);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return (!empty($result[0]->NOMBRE)) ? $result[0]->NOMBRE : '' ;
    }

    public function get_fast($id)
    {    
        $this->db->distinct();
        $this->db->select('CA.ID_CASINO');
        $this->db->select('CA.NOMBRE');
        $this->db->from('PF_CS_CASINO CA');
        $this->db->from('PF_CS_COLACION CO');
        $this->db->where('CO.VALE_ID',$id);
        $this->db->where('CO.ESTADO', 1);
        $this->db->where('CO.CASINO_ID = CA.ID_CASINO');

        $query = $this->db->get();

        $data = $query->result();

        $result = array();

        if(!empty($data))
        {
            $data = $data[0];
            $result = (object) array(
                "ID_CASINO" => $data->ID_CASINO,
                "NOMBRE" => ucwords(mb_strtolower($data->NOMBRE)),
            );
        }
        
        return $result;
    }

}

/* End of file Casino_model.php */
/* Location: ./application/models/Casino_model.php */