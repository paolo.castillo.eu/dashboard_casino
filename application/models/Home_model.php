<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_quantity_week($casino)
	{
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CANTIDAD_POR_SEMANA", array
            (
				array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
				array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
		
		$result = array();
		
		if (!empty($data)) {
			foreach ($data as $key) {
				$result [] = (object) array(
					"day"      => ucwords(mb_strtolower($key->DIA)),
					"day_week" => $key->TICKET
				);
			}
		}else{
			$result [] = (object) array(
				"day"      => "Lunes",
				"day_week" => 0
			);
		}

       	return $result;
	}

	public function get_by_day($casino)
	{
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CANTIDAD_POR_DIA", array
            (
				array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
				array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
		
		$result = array();
		
		if (!empty($data)) {
			foreach ($data as $key) {
				$result [] = (object) array(
					"Colacion" => ucwords(mb_strtolower($key->NOMBRE)),
					"day"      => ucwords(mb_strtolower($key->TICKET)),
					"class"    => $this->elementClass()
				);
			}
		}else{
			$result [] = (object) array(
				"Colacion" => 'Sin colación',
				"day"      => 0,
				"class"    => $this->elementClass()
			);
		}
       	return $result;
	}

	public function get_pending($casino)
	{
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CANTIDAD_SOLICITUD_PENDIENTE", array
            (
				array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
				array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);

		$result = array();
		
		if (!empty($data)) {
			foreach ($data as $key) {
				$result [] = (object) array(
					"quantity_c" => $key->CANTIDAD_R
				);
			}
		}else{
			$result [] = (object) array(
				"quantity_a" => 'No tiene solicitud aprovada en este momento'
			);
		}
        return $result;
	}

	public function get_aproved($casino)
	{
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CANTIDAD_SOLICITUD_APROVADA", array
            (
				array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
				array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);

		$result = array();
		
		if (!empty($data)) {
			foreach ($data as $key) {
				$result [] = (object) array(
					"quantity_a" => $key->CANTIDAD_A
				);
			}
		}else{
			$result [] = (object) array(
				"quantity_a" => 'No tiene solicitud aprovada en este momento'
			);
		}
        return $result;
	}

	public function get_rejection($casino)
	{
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","CANTIDAD_SOLICITUD_RECHAZADA", array
            (
				array('name' =>':CASINO','value'=>$casino,'type'=>SQLT_CHR, 'length'=>-1),
				array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1),
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
		$result = array();
		
		if (!empty($data)) {
			foreach ($data as $key) {
				$result [] = (object) array(
					"quantity_d" => $key->CANTIDAD_R
				);
			}
		}else{
			$result [] = (object) array(
				"quantity_d" => 'No tiene solicitud rechazada en este momento'
			);
		}
        return $result;
	}

	private function elementClass()
	{
		$color = array("bg-green", "bg-teal", "bg-olive", "bg-yellow", "bg-orange","bg-aqua");
		return $color[array_rand($color)];
	}

	public function send_email($email,$body,$subject)
	{
        $this->db->stored_procedure("PF_PACKAGE_CASINO","ENVIAR_CORREO", array
            (
                array('name' =>':p_to','value'=>$email,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':p_html','value'=>$body,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':p_subject','value'=>$subject,'type'=>SQLT_CHR, 'length'=>-1)
            )
        );
	}
}

/* End of file Home_model.php */
/* Location: ./application/models/Home_model.php */