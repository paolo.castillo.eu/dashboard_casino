<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Collation_model extends CI_Model {
	
	private $table          = 'PFSC_COLACION';
	private $id_colacion    = 'ID_COLACION';
	private $id_casino      = 'ID_CASINO';
	private $state          = 'ESTADO';
	private $version        = 'VERSION';
	private $preci          = 'COSTO';
	private $date_init      = 'FECHA_VIGENCIA_INICIAL';
	private $date_vige      = 'FECHA_VIGENCIA_FINAL';
	private $hour_cut       = 'CORTE_SOLICITUD';
	private $hour_init      = 'HORA_INICIO';
	private $hour_end       = 'HORA_FIN';
	private $name           = 'NOMBRE';
	private $type_collation = 'TIPO_COLACION';
    private $min_ini        = 'MINUTO_INICIO';
    private $min_end        = 'MINUTO_CORTE';
    private $total          = 'TOTAL_COLACION';
	private $created_by     = 'CREADO_POR';
	private $modify_by      = 'MODIFICADO_POR';

	private $column_order  = array('ID_COLACION','ID_CASINO','VERSION','COSTO',"TO_CHAR(FECHA_VIGENCIA_INICIAL,'DD/MM/RRRR')","TO_CHAR(FECHA_VIGENCIA_FINAL,'DD/MM/RRRR')",'CORTE_SOLICITUD','HORA_INICIO','HORA_FIN','NOMBRE','TIPO_COLACION',null);
	private $column_search = array('NOMBRE');
	private $order         = array('ID_COLACION' => 'ID_COLACION');

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
	}

	public function add($id_codigo,$name,$type,$id_casino,$version,$preci,$date_ini,$date_end,$out_cut,$out_ini,$out_end,$by_create)
	{
        $this->db->trans_start();
        $this->db->set($this->id_colacion, $id_codigo);
        $this->db->set($this->name, $name);
        $this->db->set($this->type_collation, $type);
        $this->db->set($this->id_casino, $id_casino);
        $this->db->set($this->version, $version);
        $this->db->set($this->preci, $preci);
        $this->db->set($this->date_init,"to_date('$date_ini','dd/mm/yyyy')",false);
        $this->db->set($this->date_vige, "to_date('$date_end','dd/mm/yyyy')",false);
        $this->db->set($this->hour_cut, $out_cut);
        $this->db->set($this->hour_init, $out_ini);
        $this->db->set($this->hour_end, $out_end);
        $this->db->set($this->created_by, $by_create);
        $this->db->insert($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}
   	private function _get_datatables_query()
    {
        $this->db->select($this->id_colacion);
        $this->db->select($this->name);
        $this->db->select($this->type_collation);
        $this->db->select($this->id_casino);
        $this->db->select($this->total);
        $this->db->select($this->preci);
        $this->db->select("TO_CHAR(".$this->date_init.",'DD/MM/RRRR') FECHA_VIGENCIA_INICIAL");
        $this->db->select("TO_CHAR(".$this->date_vige.",'DD/MM/RRRR') FECHA_VIGENCIA_FINAL");
        $this->db->select($this->hour_cut);
        $this->db->select($this->hour_init);
        $this->db->select($this->hour_end);
        $this->db->select($this->min_ini);
        $this->db->select($this->min_end);

        $this->db->from($this->table);
        $this->db->where($this->state, 1);

        $i = 0;

        foreach ($this->column_search as $item) {

            if ($_POST['search']['value']) {

                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
     	   $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select($this->id_colacion);
        $this->db->select($this->name);
        $this->db->select($this->type_collation);
        $this->db->select($this->id_casino);
        $this->db->select($this->total);
        $this->db->select($this->preci);
        $this->db->select($this->date_init);
        $this->db->select($this->date_vige);
        $this->db->select($this->hour_cut);
        $this->db->select($this->hour_init);
        $this->db->select($this->hour_end);
        $this->db->select($this->min_ini);
        $this->db->select($this->min_end);

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_by_id($id)
	{
        $this->db->select($this->id_colacion);
        $this->db->select($this->name);
        $this->db->select($this->type_collation);
        $this->db->select($this->id_casino);
        $this->db->select($this->total);
        $this->db->select($this->preci);
        $this->db->select("TO_CHAR(FECHA_VIGENCIA_INICIAL,'DD/MM/RRRR') FECHA_VIGENCIA_INICIAL", FALSE);
        $this->db->select("TO_CHAR(FECHA_VIGENCIA_FINAL,'DD/MM/RRRR') FECHA_VIGENCIA_FINAL", FALSE);
        $this->db->select($this->hour_cut);
        $this->db->select($this->hour_init);
        $this->db->select($this->hour_end);
        $this->db->select($this->min_ini);
        $this->db->select($this->min_end);

		$this->db->from($this->table);
		$this->db->where($this->id_colacion, $id);
        $this->db->where($this->state, 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function update($id_codigo,$name,$type,$id_casino,$version,$preci,$date_ini,$date_end,$out_cut,$out_ini,$out_end,$min_ini,$min_end,$by_modify)
	{

        // die(var_dump($date_end));
        $this->db->trans_start();
        $this->db->set($this->name, $name);
        $this->db->set($this->type_collation, $type);
        $this->db->set($this->id_casino, $id_casino);
        $this->db->set($this->total, $version);
        $this->db->set($this->preci, $preci);
        $this->db->set($this->date_init,"to_date('$date_ini','dd/mm/yyyy')",false);
        $this->db->set($this->date_vige, "to_date('$date_end','dd/mm/yyyy')",false);
        $this->db->set($this->hour_cut, $out_cut);
        $this->db->set($this->hour_init, $out_ini);
        $this->db->set($this->hour_end, $out_end);
        $this->db->set($this->min_ini, $min_ini);
        $this->db->set($this->min_end, $min_end);

        $this->db->set($this->modify_by, $by_modify);
        $this->db->where($this->id_colacion,$id_codigo);
		$this->db->update($this->table);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

    public function delete($id,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->modify_by, $by_modify);
        $this->db->set($this->state, 0);
        $this->db->where($this->id_colacion,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_all()
    {
        $this->db->select($this->id_colacion);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();        
    }

    public function get_name($id)
    {
        $this->db->distinct();
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_colacion,$id);
        $this->db->where($this->state, 1);
        // $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_collation_casino($id_casino)
    {
        $this->db->distinct();
        $this->db->select($this->id_colacion);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_casino,$id_casino);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_collation_tipo($id_casino)
    {
        $this->db->distinct();
        $this->db->select($this->id_colacion);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_casino,$id_casino);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        // die(var_dump($query->result()));

        return $query->result();
    }
    
    public function get_by($id_casino)
    {
        $this->db->distinct();
        $this->db->select($this->id_colacion);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_casino,$id_casino);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();
    }
}

/* End of file Collation_model.php */
/* Location: ./application/models/Collation_model.php */