<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session_model extends CI_Model {
	
	private $table        = 'PFSC_COLABORADOR';
	private $collaborator = 'ID_COLABORADOR';
	private $codigo       = 'CODIGO';
	private $pass         = 'CLAVE';
	private $name         = 'NOMBRE';
	private $email        = 'CORREO';
	private $state        = 'ESTADO';
	private $id_perfil    = 'ID_PERFIL';
	private $deparment    = 'DEPARTAMENTO';
	private $id_area      = 'ID_AREA';
	private $card         = 'CREDENCIAL';
	private $by_modify    = 'MODIFICADO_POR';


    // Variable webUsuario
    private $rut_usuario = 'RUT_USUARIO';
    private $usuclave = 'USUCLAVE';
    private $table_webusuario = 'WEBUSUARIO';
    
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->web = $this->load->database('web', TRUE);
	}

	public function check($user)
	{
		// die(var_dump($user));
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","PERFIL_USUARIO", array
            (
				array('name' =>':RUT','value'=>$user,'type'=>SQLT_CHR, 'length'=>-1),
				array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );

        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }

        oci_free_statement($curs);
        return $data;
	}

	public function recover_pass_col($rut_col)
	{
		$this->db->select($this->collaborator);
		$this->db->select($this->codigo);
		$this->db->select($this->email);
		$this->db->from($this->table);
		$this->db->where($this->codigo,$rut_col);
		// $this->db->where($this->email,$email);
		$query = $this->db->get();
		return $query->result();
	}

	public function update_pass($upper_rut,$new,$id)
	{
		$salt     = '$bgr$/';
		$password = sha1(md5($salt . $new));
        $this->db->trans_start();
        $this->db->set($this->pass,$password);
        $this->db->set($this->by_modify,$id);
        $this->db->where($this->codigo,$upper_rut);
        $this->db->where($this->collaborator,$id);
		$this->db->update($this->table);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

    public function checkUser($element)
    {

    	// die(var_dump($element));
        $this->web->select($this->rut_usuario);
        $this->web->select($this->usuclave);
        $this->web->from($this->table_webusuario);
        $this->web->where($this->rut_usuario,$element->rut);
        // $this->web->where($this->usuclave,$element->usuclave);
        $query = $this->web->get();
        $result = $query->result();
        // die(var_dump($result));
        if (!empty($result)) {
            return true;
        }
        return false;
    }
}

/* End of file Session_model.php */
/* Location: ./application/models/Session_model.php */