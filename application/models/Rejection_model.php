<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rejection_model extends CI_Model {

	private $table          = 'PFSC_TICKET';
	private $id_ticket      = 'ID_TICKET';
	private $id_colaborador = 'ID_COLABORADOR';
	private $id_estado      = 'ID_ESTADO';
	private $id_casino      = 'ID_CASINO';
	private $id_colacion    = 'ID_COLACION';
	private $correlativo    = 'CORRELATIVO';
	private $date_collation = 'FECHA_COLACION';
	private $hour           = 'HORA_ENTREGA';
	private $date           = 'FECHA_ENTREGA';
	private $by_created     = 'CREADO_POR';
	private $by_modify      = 'FECHA_MODIFICACION';
	private $emp            = 'RUT_EMP';



    private $ticket = 'TICKET.CORRELATIVO';
    private $codigo = 'COLAB.CODIGO';
    private $cola_nombre = 'COLAB.NOMBRE';
    private $colaborador = 'COLAB.ID_COLABORADOR';
    private $casino = 'CASINO.ID_CASINO';
    private $planta_nombre = 'PLANTA.NOMBRE AS PLANTA';
    private $casino_nombre = 'CASINO.NOMBRE AS CASINO';
    private $ticket_hora = 'TICKET.HORA_ENTREGA';
    private $fecha = "TO_CHAR(TICKET.FECHA_ENTREGA,'DD/MM/RRRR') AS FECHA";
    private $colacion = 'COLAC.ID_COLACION';
    private $colacion_nombre = 'COLAC.TIPO_COLACION AS COLACION';
    private $estado = 'ESTADO.ID_ESTADO';
    private $estado_nombre = 'ESTADO.NOMBRE AS ESTADO';
    private $empresa = 'COLAB.EMPRESA';
    private $departamento = 'COLAB.DEPARTAMENTO';

    private $table_ticket = 'PFSC_TICKET TICKET';
    private $table_casino = 'PFSC_CASINO CASINO';
    private $table_colaborador = 'PFSC_COLABORADOR COLAB';
    private $table_colacion = 'PFSC_COLACION COLAC';
    private $table_estado = 'PFSC_ESTADO_TICKET ESTADO';
    private $table_planta = 'PFSC_PLANTA PLANTA';



    private $fk_ticket_casino = 'TICKET.ID_CASINO = CASINO.ID_CASINO';
    private $fk_colab_ticket = 'COLAB.ID_COLABORADOR = TICKET.ID_COLABORADOR';
    private $fk_colac_ticket = 'COLAC.ID_COLACION = TICKET.ID_COLACION';
    private $fk_estad_ticket = 'ESTADO.ID_ESTADO = TICKET.ID_ESTADO';
    private $fk_casino_plant = 'CASINO.ID_PLANTA = PLANTA.ID_PLANTA';


    private $nombre_planta_o = 'PLANTA.NOMBRE';
    private $nombre_casino_o = 'CASINO.NOMBRE';
    private $nombre_colacion_o = 'COLAC.TIPO_COLACION';
    private $estado_nombre_o = 'ESTADO.NOMBRE';

    private $id_planta = 'PLANTA.ID_PLANTA';
    private $fecha_entrega = 'TICKET.FECHA_ENTREGA';
    private $estado_ticket = 'TICKET.ID_ESTADO';


    // datatable function //
    private $column_order  = array('TICKET.CORRELATIVO','COLAB.CODIGO','COLAB.NOMBRE','COLAB.EMPRESA','COLAB.DEPARTAMENTO','COLAB.ID_COLABORADOR','CASINO.ID_CASINO','PLANTA.NOMBRE','CASINO.NOMBRE','TICKET.FECHA_ENTREGA','TICKET.HORA_ENTREGA','COLAC.ID_COLACION','COLAC.TIPO_COLACION','ESTADO.ID_ESTADO',null);
    private $column_search = array('COLAB.CODIGO','COLAB.NOMBRE','PLANTA.NOMBRE','CASINO.NOMBRE','COLAC.TIPO_COLACION');
    private $order         = array('TICKET.CORRELATIVO' => 'TICKET.CORRELATIVO');

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
        ini_set('memory_limit', '586M');
    }

    public function get_id_collabor($id_casino,$id_colacion,$id_estado)
    {
        $this->db->select($this->id_colaborador);
        $this->db->from($this->table);
        $this->db->where($this->id_casino,$id_casino);
        $this->db->where($this->id_colacion,$id_colacion);
        $this->db->where($this->id_estado,$id_estado);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result();
        	return $result[0]->ID_COLABORADOR;
        }else{
        	return FALSE;
        }
    }

    public function get_ticket_rejection_five($id_casino)
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","TICKET_RECHAZADO_ULTIMAS", array
            (
                array('name' =>':CASINO','value'=>$id_casino,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );
        oci_execute($curs);
        $data = array();
        while (($row = oci_fetch_object($curs)) != false) {
            $data[] = $row;
        }   
        oci_free_statement($curs);
        return $data;
    }

    /*=============================================
    =            Section comment block            =
    =============================================*/      

    private function _get_datatables_query()
    {
        $this->db->select($this->ticket);
        $this->db->select($this->codigo);
        $this->db->select($this->cola_nombre);
        $this->db->select($this->colaborador);
        $this->db->select($this->casino);
        $this->db->select($this->planta_nombre);
        $this->db->select($this->casino_nombre);
        $this->db->select($this->ticket_hora);
        $this->db->select($this->fecha);
        $this->db->select($this->colacion);
        $this->db->select($this->colacion_nombre);
        $this->db->select($this->estado);
        $this->db->select($this->estado_nombre);
        $this->db->select($this->empresa);
        $this->db->select($this->departamento);


        $this->db->from($this->table_ticket);

        $this->db->join($this->table_casino, $this->fk_ticket_casino);
        $this->db->join($this->table_colaborador, $this->fk_colab_ticket);
        $this->db->join($this->table_colacion, $this->fk_colac_ticket);
        $this->db->join($this->table_estado, $this->fk_estad_ticket);
        $this->db->join($this->table_planta, $this->fk_casino_plant);

        $this->db->where("$this->estado_ticket !=", 1);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
               
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
            $query_date = date("d-m-Y");
            // First day of the month.
            $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
            
            // Last day of the month.
            $hasta = date('t-m-Y', strtotime($query_date));
            $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
            $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        
        if ($this->session->profile_id == 3) {
            $this->db->where($this->casino,$this->session->casino);
        }

        if (!empty($_POST['planta'])) {
            $this->db->where($this->id_planta,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->casino,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->colacion,$_POST['colacion']);
        }

        if (!empty($_POST['estado'])) {
            $this->db->where($this->estado,$_POST['estado']);
        }else{
            $this->db->where("$this->estado <>", 1);
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->codigo, $_POST['rut'] , 'both');
        }

        if (!empty($_POST['empresa'])) {
            $empresa = $_POST['empresa'];
            $this->db->where("TRIM(UPPER($this->empresa)) =", "UPPER('$empresa')" , FALSE);
        }

        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }

        $this->db->group_by($this->ticket);
        $this->db->group_by($this->codigo);
        $this->db->group_by($this->cola_nombre);
        $this->db->group_by($this->colaborador);
        $this->db->group_by($this->casino);
        $this->db->group_by($this->nombre_planta_o);
        $this->db->group_by($this->nombre_casino_o);
        $this->db->group_by($this->ticket_hora);
        $this->db->group_by($this->fecha_entrega);
        $this->db->group_by($this->colacion);
        $this->db->group_by($this->nombre_colacion_o);
        $this->db->group_by($this->estado);
        $this->db->group_by($this->estado_nombre_o);
        $this->db->group_by($this->empresa);
        $this->db->group_by($this->departamento);

        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select($this->ticket);
        $this->db->select($this->codigo);
        $this->db->select($this->cola_nombre);
        $this->db->select($this->colaborador);
        $this->db->select($this->casino);
        $this->db->select($this->planta_nombre);
        $this->db->select($this->casino_nombre);
        $this->db->select($this->ticket_hora);
        $this->db->select($this->fecha);
        $this->db->select($this->colacion);
        $this->db->select($this->colacion_nombre);
        $this->db->select($this->estado);
        $this->db->select($this->estado_nombre);
        $this->db->select($this->empresa);
        $this->db->select($this->departamento);


        $this->db->from($this->table_ticket);

        $this->db->join($this->table_casino, $this->fk_ticket_casino);
        $this->db->join($this->table_colaborador, $this->fk_colab_ticket);
        $this->db->join($this->table_colacion, $this->fk_colac_ticket);
        $this->db->join($this->table_estado, $this->fk_estad_ticket);
        $this->db->join($this->table_planta, $this->fk_casino_plant);
        $this->db->where("$this->estado_ticket !=", 1);

        if (!empty($_POST['fecha'])) {
            $desde           = substr($_POST['fecha'], 0, 10);
            $hasta           = substr($_POST['fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        
        if ($this->session->profile_id == 3) {
            $this->db->where($this->casino,$this->session->casino);
        }

        if (!empty($_POST['planta'])) {
            $this->db->where($this->id_planta,$_POST['planta']);
        }

        if (!empty($_POST['casino'])) {
            $this->db->where($this->casino,$_POST['casino']);
        }

        if (!empty($_POST['colacion'])) {
            $this->db->where($this->colacion,$_POST['colacion']);
        }

        if (!empty($_POST['estado'])) {
            $this->db->where($this->estado,$_POST['estado']);
        }

        if (!empty($_POST['rut'])) {
            $this->db->like($this->codigo, $_POST['rut'] , 'both');
        }

        if (!empty($_POST['empresa'])) {
            $empresa = $_POST['empresa'];
            $this->db->where("TRIM(UPPER($this->empresa)) =", "UPPER('$empresa')" , FALSE);
        }

        $this->db->group_by($this->ticket);
        $this->db->group_by($this->codigo);
        $this->db->group_by($this->cola_nombre);
        $this->db->group_by($this->colaborador);
        $this->db->group_by($this->casino);
        $this->db->group_by($this->nombre_planta_o);
        $this->db->group_by($this->nombre_casino_o);
        $this->db->group_by($this->ticket_hora);
        $this->db->group_by($this->fecha_entrega);
        $this->db->group_by($this->colacion);
        $this->db->group_by($this->nombre_colacion_o);
        $this->db->group_by($this->estado);
        $this->db->group_by($this->estado_nombre_o);
        $this->db->group_by($this->empresa);
        $this->db->group_by($this->departamento);

        return $this->db->count_all_results();
    }

    /*=====  End of Section comment block  ======*/

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return $d && $d->format('d/m/Y') === $date;
    }

    public function export_excel_data()
    {
        $this->db->select($this->ticket);
        $this->db->select($this->codigo);
        $this->db->select($this->cola_nombre);
        $this->db->select($this->colaborador);
        $this->db->select($this->casino);
        $this->db->select($this->planta_nombre);
        $this->db->select($this->casino_nombre);
        $this->db->select($this->ticket_hora);
        $this->db->select($this->fecha);
        $this->db->select($this->colacion);
        $this->db->select($this->colacion_nombre);
        $this->db->select($this->estado);
        $this->db->select($this->estado_nombre);
        $this->db->select($this->empresa);
        $this->db->select($this->departamento);

        $this->db->from($this->table_ticket);

        $this->db->join($this->table_casino, $this->fk_ticket_casino);
        $this->db->join($this->table_colaborador, $this->fk_colab_ticket);
        $this->db->join($this->table_colacion, $this->fk_colac_ticket);
        $this->db->join($this->table_estado, $this->fk_estad_ticket);
        $this->db->join($this->table_planta, $this->fk_casino_plant);
        $this->db->where("$this->estado_ticket !=", 1);

        if (!empty($_POST['id_fecha'])) {
            $desde           = substr($_POST['id_fecha'], 0, 10);
            $hasta           = substr($_POST['id_fecha'],-10);
            if ($this->validateDate($desde) && $this->validateDate($hasta)) {
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
            }
        }else{
                $query_date = date("d-m-Y");
                // First day of the month.
                $desde =  date('01-m-Y',strtotime ( '-3 month' , strtotime($query_date)));
                
                // Last day of the month.
                $hasta = date('t-m-Y', strtotime($query_date));
                $this->db->where("$this->fecha_entrega >=", "to_date('$desde','dd-mm-yyyy')",false);
                $this->db->where("$this->fecha_entrega <=", "to_date('$hasta','dd-mm-yyyy')",false);   
        }
        
        if ($this->session->profile_id == 3) {
            $this->db->where($this->casino,$this->session->casino);
        }

        if (!empty($_POST['id_planta'])) {
            $this->db->where($this->id_planta,$_POST['id_planta']);
        }

        if (!empty($_POST['id_casino'])) {
            $this->db->where($this->casino,$_POST['id_casino']);
        }

        if (!empty($_POST['id_colacion'])) {
            $this->db->where($this->colacion,$_POST['id_colacion']);
        }

        if (!empty($_POST['id_estado'])) {
            $this->db->where($this->estado,$_POST['id_estado']);
        }

        if (!empty($_POST['id_rut'])) {
            $this->db->like($this->codigo, $_POST['id_rut'] , 'both');
        }

        if (!empty($_POST['id_empresa'])) {
            $empresa = $_POST['id_empresa'];
            $this->db->where("TRIM(UPPER($this->empresa)) =", "UPPER('$empresa')" , FALSE);
        }

        $this->db->group_by($this->ticket);
        $this->db->group_by($this->codigo);
        $this->db->group_by($this->cola_nombre);
        $this->db->group_by($this->colaborador);
        $this->db->group_by($this->casino);
        $this->db->group_by($this->nombre_planta_o);
        $this->db->group_by($this->nombre_casino_o);
        $this->db->group_by($this->ticket_hora);
        $this->db->group_by($this->fecha_entrega);
        $this->db->group_by($this->colacion);
        $this->db->group_by($this->nombre_colacion_o);
        $this->db->group_by($this->estado);
        $this->db->group_by($this->estado_nombre_o);
        $this->db->group_by($this->empresa);
        $this->db->group_by($this->departamento);

        $query = $this->db->get();
        return $query;
    }
}
/* End of file Rejection_model.php */
/* Location: ./application/models/Rejection_model.php */