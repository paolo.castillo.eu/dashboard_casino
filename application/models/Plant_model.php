<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plant_model extends CI_Model {
	
	private $table         = 'PFSC_PLANTA';
	private $id            = 'ID_PLANTA';
	private $name          = 'NOMBRE';
    private $state         = 'ESTADO';
    private $create_by     = 'CREADO_POR';
    private $modify_by     = 'MODIFICADO_POR';
    private $date_modify   = 'FECHA_MODIFICACION';
	// TABLE Function //
	private $column_order  = array('ID_PLANTA','NOMBRE',null);
	private $column_search = array('NOMBRE');
	private $order         = array('ID_PLANTA' => 'ID_PLANTA');

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
	}

	public function add($id,$name,$create_by)
	{
        $this->db->trans_start();
        $this->db->set($this->id, $id);
        $this->db->set($this->name, $name);
        $this->db->set($this->create_by, $create_by);
        $this->db->insert($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

   	private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->where($this->state, 1);

        $i = 0;

        foreach ($this->column_search as $item) {

            if ($_POST['search']['value']) {

                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
     	   $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function get_by_id($id)
	{
		$this->db->select($this->id);
		$this->db->select($this->name);
		$this->db->from($this->table);
		$this->db->where($this->id, $id);
        $this->db->where($this->state,1);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($name,$id,$modify_by)
	{
        $this->db->trans_start();
		$this->db->set($this->name,$name);
        $this->db->set($this->modify_by,$modify_by);
		$this->db->where($this->id,$id);
		$this->db->update($this->table);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

    public function delete($id,$modify_by)
    {
        $date = $this->get_date();
        $this->db->trans_start();
        $this->db->set($this->modify_by,$modify_by);
        $this->db->set($this->state, 0);
        $this->db->where($this->id,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_print()
    {
        $this->db->select($this->id);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();
    }

    
    public function get_all()
    {
        $this->db->select($this->id);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function getName_by_id($id)
    {
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id,$id);
        $this->db->where($this->state, 1);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->NOMBRE;
    }

    public function get_fast($id)
    {
        $this->db->select('P.NOMBRE');
        $this->db->from('PF_CS_PLANTA P');
        $this->db->join('PF_CS_CASINO','PLANTA_ID = P.ID_PLANTA');
        $this->db->where('ID_CASINO',$id);
        $query = $this->db->get();

        $data = $query->result();

        $result = array();

        if(!empty($data))
        {
            $data = $data[0];
            $result = (object) array(
                "NOMBRE" => ucwords(mb_strtolower($data->NOMBRE)),
            );
        }
        
        return $result;
    }

    private function get_date()
    {
        $aux = array();
        $sp = oci_parse($this->db->conn_id, "BEGIN PF_PACKAGE_CASINO.GET_DATE(:FECHA); END;");
        oci_bind_by_name($sp, ":FECHA", $aux[], 300);
        oci_execute($sp, OCI_DEFAULT);
        if(empty($aux))
        {
            return '*** FECHA NO ENTONTRADO ***';
        }
        $response = $aux[0];
        return $response;
    }
}

/* End of file Plant.php */
/* Location: ./application/models/Plant.php */