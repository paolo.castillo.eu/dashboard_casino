<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ticket_model extends CI_Model {
	
	private $table         = 'PFSC_ESTADO_TICKET';
	private $id_estado     = 'ID_ESTADO';
	private $name          = 'NOMBRE';
	private $state         = 'ESTADO';
	private $by_created    = 'CREADO_POR';
	private $by_modify     = 'MODIFICADO_POR';
    private $id_solicitud  = 'ID_SOLICITUD';

	private $column_order  = array('ID_ESTADO','NOMBRE',null);
	private $column_search = array('NOMBRE');
	private $order         = array('ID_ESTADO' => 'ID_ESTADO');

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
	}

   	private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->where($this->state, 1);

        $i = 0;

        foreach ($this->column_search as $item) {

            if ($_POST['search']['value']) {

                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
     	   $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function add($id_estado,$name,$by_create)
	{
        $this->db->trans_start();
        $this->db->set($this->id_estado, $id_estado);
        $this->db->set($this->name, $name);
        $this->db->set($this->by_created, $by_create);
        $this->db->insert($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

	public function update($id_estado,$name,$by_modify)
	{
        $this->db->trans_start();
        $this->db->set($this->name,$name);
        $this->db->set($this->by_modify,$by_modify);
        $this->db->where($this->id_estado,$id_estado);
		$this->db->update($this->table);
		$this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

    public function get_name($id)
    {
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_estado,$id);
        $this->db->where($this->state, 1);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->NOMBRE;
    }

	public function delete($id,$by_modify)
	{
        $this->db->trans_start();
        $this->db->set($this->by_modify, $by_modify);
        $this->db->set($this->state, 0);
        $this->db->where($this->id_estado,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
	}

	public function get_by_id($id)
	{
		$this->db->select($this->id_estado);
		$this->db->select($this->name);
		$this->db->from($this->table);
		$this->db->where($this->id_estado, $id);
        $this->db->where($this->state, 1);
		$query = $this->db->get();
		return $query->row();
	}

    public function get_all($step)
    {
        if ($step == 0) {
            $this->db->select($this->id_estado);
            $this->db->select($this->name);
            $this->db->from($this->table);
            $this->db->where($this->state, 1);
            $this->db->where($this->id_estado.' !=', 1);
            $query = $this->db->get();
            return $query->result();
        }else{
            $this->db->select($this->id_estado);
            $this->db->select($this->name);
            $this->db->from($this->table);
            $this->db->where($this->state, 1);
            $this->db->where($this->id_estado , 1);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function get_state_ticket()
    {
        $this->db->select($this->id_estado);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->state, 1);
        $this->db->where($this->id_estado.'>=', 1);
        $this->db->where($this->id_estado.'<=', 4);
        $query = $this->db->get();
        return $query->result();
    }
    // Modificación
    public function ticket_aproved()
    {
        $curs = $this->db->get_cursor();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","TICKET_APROVADO", array
            (
                array('name' =>':REF_CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
            )
        );
        oci_execute($curs);
        $data = array();
        $i = 0;
        while (($row = oci_fetch_object($curs)) != false && $i <= 10000) {
            $data[] = $row;
            $i++;
        }
        oci_free_statement($curs);
        return $data;
    }

    // // Modificación Por Casino
    // public function ticket_aproved_by($id_casino)
    // {
    //     // $curs = $this->db->get_cursor();
    //     // $this->db->stored_procedure("PF_PACKAGE_CASINO","TICKET_APROVADO_POR", array
    //     //     (
    //     //         array('name' =>':CASINO','value'=>$id_casino,'type'=>SQLT_CHR, 'length'=>-1),
    //     //         array('name' =>':REF_CUR','value'=>$curs,'type'=>OCI_B_CURSOR,'length'=>-1)
    //     //     )
    //     // );
    //     // oci_execute($curs);
    //     // $data = array();
    //     // while (($row = oci_fetch_object($curs)) != false) {
    //     //     $data[] = $row;
    //     // }
    //     // oci_free_statement($curs);
    //     // return $data; 
    // }

    public function last_id()
    {
        $str_sql = 'SELECT Nvl(Max(ID_ESTADO)+1,1) AS LASTID FROM PFSC_ESTADO_TICKET';
        $query = $this->db->query($str_sql);
        return $query->result();
    }

    public function aproved_ticket($id,$by_modify)
    {
        $this->db->trans_start();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","ACTUALIZAR_TICKET", array
            (
                array('name' =>':SOLICITUD','value'=>$id,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':MODIFICADO','value'=>$by_modify,'type'=>SQLT_CHR, 'length'=>-1)
            )
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function reject_ticket($id,$by_modify)
    {
        $this->db->trans_start();
        $this->db->stored_procedure("PF_PACKAGE_CASINO","ACTUALIZAR_TICKET_RECHAZO", array
            (
                array('name' =>':SOLICITUD','value'=>$id,'type'=>SQLT_CHR, 'length'=>-1),
                array('name' =>':MODIFICADO','value'=>$by_modify,'type'=>SQLT_CHR, 'length'=>-1)
            )
        );

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }
}

/* End of file Ticket_model.php */
/* Location: ./application/models/Ticket_model.php */