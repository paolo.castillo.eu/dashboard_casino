<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Type_service extends CI_Model {

	private $table        = 'PFSC_TIPO_SERVICIO';
	private $id_servicio  = 'ID_TIPO_SERVICIO';
	private $name         = 'NOMBRE';
	private $by_created   = 'CREADO_POR';
	private $by_modify    = 'MODIFICADO_POR'; 
	private $state        = 'ESTADO';

	// TABLE Function //
	private $column_order  = array('ID_TIPO_SERVICIO','NOMBRE',null);
	private $column_search = array('NOMBRE');
	private $order         = array('ID_TIPO_SERVICIO' => 'ID_TIPO_SERVICIO');

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
	}

	public function add($id_service,$name,$by_create)
	{
        $this->db->trans_start();
        $this->db->set($this->id_servicio, $id_service);
        $this->db->set($this->name, $name);
        $this->db->set($this->by_created, $by_create);
        $this->db->insert($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
        	return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}

   	private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $this->db->where($this->state, 1);

        $i = 0;

        foreach ($this->column_search as $item) {

            if ($_POST['search']['value']) {

                if ($i == 0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
     	   $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->select($this->id_servicio);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_servicio, $id);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->row();
    }

    public function update($id_service,$name,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->name,$name);
        $this->db->set($this->by_modify,$by_modify);
        $this->db->where($this->id_servicio,$id_service);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function delete($id,$by_modify)
    {
        $this->db->trans_start();
        $this->db->set($this->by_modify, $by_modify);
        $this->db->set($this->state, 0);
        $this->db->where($this->id_servicio,$id);
        $this->db->update($this->table);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }else{
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_all()
    {
        $this->db->select($this->id_servicio);
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->state, 1);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_name($id)
    {
        $this->db->select($this->name);
        $this->db->from($this->table);
        $this->db->where($this->id_servicio,$id);
        $this->db->where($this->state, 1);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result[0]->NOMBRE;
    }


}

/* End of file Type_service.php */
/* Location: ./application/models/Type_service.php */