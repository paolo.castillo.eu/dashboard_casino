# PF Alimentos

[![N|Solid](https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Productos_Fernandez%C2%BA.png/220px-Productos_Fernandez%C2%BA.png)](https://www.pfalimentos.cl/)

Sistema de casino, en lo cual se utilizara diferente herramientas para su desarrollo, entrete esta caracteristicas es la plataforma web en el framework
Codeigniter, entre otras. y otras cosas mas.

    - Framework Codeigniter 3.4.1.
    - Oracle db.
    - Jquery 2.2.3.
    - Boostrap Snippet 4.

# Prototipo de baja fidelidad

````sh
$ cd casino/PBF/
$ google-chrome index.html
````

### plugins

Se destacara los diferente elemento a utilizar para la creación de este proyecto web.

* [Codeigniter] - Framework
* [Boostrap] - HTML elemento web.
* [Font Awesome] - Iconos web basado en CSS y LESS.
* [Jquery] - Eventos para el backend
* [Date Picker] - Elementos para HTML etiquetas <input>.
* [Icheck] - Elemento para el Wireframe.
* [Data Table] - Elemento para HTML etiquetas <table>
* [Adminlte 2 css] - Estilo y elemento para el diseño de la página.
* [html5shiv] - Elemento para el Wireframe.
* [respond] - Elemento para el responsive en navegadores Internet explore.
* [notify] - Elemento HTML para notificación o mensajes.

### Instalación

La instalación depende del servidor donde se alojé el proyecto 

- Linux
`````sh
$ cp casino /var/www/
$ cp casino /etc/https/
`````
- Windows
`````sh
> mv c:\casino c:\wamp\www\
`````
- Activar mod_rewrite
````sh
$ RewriteEngine on
````

### Error page

| Erros | README |
| ------ | ------ |
| Error 404 | [Página que solicita no se encuentra] [PlDb] |
| Error Data Base | [Problema con la base de datos] [PlGh] |
| Error exception | [Error flujo en la pagina web] [PlGd] |
| Error general | [Error del servidor apache] [PlOd] |
| Error php | [Error de configuración de PHP] [PlMe] | 

### Verificar funciones de base url en el programa en caso de emigración de servidor

````sh
$vim /casino/application/config/config.php
````

````sh
$config['base_url'] = '\dirección del servidor/';
````

### Configuración de la base de datos

````sh
$vim /casino/application/config/database.php
````

- tnsname es la conexión de DB. 

````sh
$tnsname = '(DESCRIPTION =(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)
            (HOST = 139.10.10.80)(PORT = 1531)))(CONNECT_DATA =(SID = PFTEST)(SERVER = DEDICATED)))';
````

- Variables de conexión.

````sh
$db['default'] = array(
    'dsn'   => '',
    'hostname' => $tnsname,
    'username' => 'web',
    'password' => 'web',
    'database' => '',
    'dbdriver' => 'oci8',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => TRUE,
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
````

### Licencia Codeigniter
```sh
cd casino
cat :${package.license}
```

### Raiz del proyecto

````sh
├───application
│   ├───cache
│   ├───config
│   ├───controllers
│   ├───core
│   ├───helpers
│   ├───hooks
│   ├───language
│   │   └───english
│   ├───libraries
│   ├───logs
│   ├───models
│   ├───third_party
│   └───views
│       └───errors
│           ├───cli
│           └───html
├───assets
│   ├───bootstrap
│   │   ├───css
│   │   ├───fonts
│   │   └───js
│   ├───build
│   │   ├───bootstrap-less
│   │   │   └───mixins
│   │   └───less
│   │       └───skins
│   ├───dist
│   │   ├───css
│   │   │   ├───fonts
│   │   │   └───skins
│   │   ├───img
│   │   │   ├───admin
│   │   │   │   └───deleteImages
│   │   │   ├───credit
│   │   │   ├───documents
│   │   │   │   └───thumbnails
│   │   │   └───logos
│   │   └───js
│   │       └───pages
│   └───plugins
│       ├───ajaxfileupload
│       ├───bootstrap-fileinput
│       │   ├───css
│       │   ├───examples
│       │   ├───img
│       │   ├───js
│       │   │   ├───locales
│       │   │   └───plugins
│       │   ├───nuget
│       │   ├───sass
│       │   └───themes
│       │       ├───explorer
│       │       ├───fa
│       │       └───gly
│       ├───bootstrap-slider
│       ├───bootstrap-wysihtml5
│       ├───chartjs
│       │   └───samples
│       │       ├───animation
│       │       ├───bar
│       │       ├───legend
│       │       ├───line
│       │       ├───radar
│       │       ├───scales
│       │       │   ├───linear
│       │       │   ├───logarithmic
│       │       │   └───time
│       │       ├───scatter
│       │       └───tooltips
│       ├───chartjs_old
│       ├───ckeditor
│       │   ├───adapters
│       │   ├───lang
│       │   ├───plugins
│       │   │   ├───a11yhelp
│       │   │   │   └───dialogs
│       │   │   │       └───lang
│       │   │   ├───about
│       │   │   │   └───dialogs
│       │   │   │       └───hidpi
│       │   │   ├───clipboard
│       │   │   │   └───dialogs
│       │   │   ├───dialog
│       │   │   ├───image
│       │   │   │   ├───dialogs
│       │   │   │   └───images
│       │   │   ├───link
│       │   │   │   ├───dialogs
│       │   │   │   └───images
│       │   │   │       └───hidpi
│       │   │   ├───magicline
│       │   │   │   └───images
│       │   │   │       └───hidpi
│       │   │   ├───pastefromword
│       │   │   │   └───filter
│       │   │   ├───scayt
│       │   │   │   └───dialogs
│       │   │   ├───specialchar
│       │   │   │   └───dialogs
│       │   │   │       └───lang
│       │   │   ├───table
│       │   │   │   └───dialogs
│       │   │   ├───tabletools
│       │   │   │   └───dialogs
│       │   │   └───wsc
│       │   │       └───dialogs
│       │   ├───samples
│       │   │   ├───css
│       │   │   ├───img
│       │   │   ├───js
│       │   │   ├───old
│       │   │   │   ├───assets
│       │   │   │   │   ├───inlineall
│       │   │   │   │   ├───outputxhtml
│       │   │   │   │   └───uilanguages
│       │   │   │   ├───dialog
│       │   │   │   │   └───assets
│       │   │   │   ├───enterkey
│       │   │   │   ├───htmlwriter
│       │   │   │   │   └───assets
│       │   │   │   │       └───outputforflash
│       │   │   │   ├───magicline
│       │   │   │   ├───toolbar
│       │   │   │   └───wysiwygarea
│       │   │   └───toolbarconfigurator
│       │   │       ├───css
│       │   │       ├───font
│       │   │       ├───js
│       │   │       └───lib
│       │   │           └───codemirror
│       │   └───skins
│       │       └───moono
│       │           └───images
│       │               └───hidpi
│       ├───colorpicker
│       │   └───img
│       ├───datatables
│       │   ├───extensions
│       │   │   ├───autofill
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   ├───images
│       │   │   │   └───js
│       │   │   ├───bootstrap_extensions
│       │   │   │   ├───css
│       │   │   │   └───js
│       │   │   ├───ColReorder
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   ├───images
│       │   │   │   └───js
│       │   │   ├───colvis
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   └───js
│       │   │   ├───FixedColumns
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   └───js
│       │   │   ├───FixedHeader
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   └───js
│       │   │   ├───KeyTable
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   └───js
│       │   │   ├───Responsive
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   │   ├───child-rows
│       │   │   │   │   ├───display-control
│       │   │   │   │   ├───initialisation
│       │   │   │   │   └───styling
│       │   │   │   └───js
│       │   │   ├───Scroller
│       │   │   │   ├───css
│       │   │   │   ├───examples
│       │   │   │   │   └───data
│       │   │   │   ├───images
│       │   │   │   └───js
│       │   │   └───TableTools
│       │   │       ├───css
│       │   │       ├───examples
│       │   │       ├───images
│       │   │       │   └───psd
│       │   │       ├───js
│       │   │       └───swf
│       │   ├───images
│       │   └───responsive
│       │       ├───css
│       │       └───js
│       ├───datepicker
│       │   └───locales
│       ├───daterangepicker
│       ├───fastclick
│       ├───flot
│       ├───fullcalendar
│       ├───iCheck
│       │   ├───flat
│       │   ├───futurico
│       │   ├───line
│       │   ├───minimal
│       │   ├───polaris
│       │   └───square
│       ├───input-mask
│       │   └───phone-codes
│       ├───ionslider
│       │   └───img
│       ├───JGallery
│       │   ├───css
│       │   └───js
│       ├───jQuery
│       ├───jqueryfileupload
│       │   ├───css
│       │   ├───img
│       │   └───js
│       ├───jQueryUI
│       ├───jvectormap
│       ├───knob
│       ├───morris
│       ├───pace
│       ├───select2
│       │   └───i18n
│       ├───slimScroll
│       ├───sparkline
│       ├───tagsinput
│       │   ├───dist
│       │   ├───examples
│       │   │   └───assets
│       │   ├───src
│       │   └───test
│       │       └───bootstrap-tagsinput
│       └───timepicker
└───system
    ├───core
    │   └───compat
    ├───database
    │   └───drivers
    │       ├───cubrid
    │       ├───ibase
    │       ├───mssql
    │       ├───mysql
    │       ├───mysqli
    │       ├───oci8
    │       ├───odbc
    │       ├───pdo
    │       │   └───subdrivers
    │       ├───postgre
    │       ├───sqlite
    │       ├───sqlite3
    │       └───sqlsrv
    ├───fonts
    ├───helpers
    ├───language
    │   └───english
    └───libraries
        ├───Cache
        │   └───drivers
        ├───Javascript
        └───Session
            └───drivers
````

