jQuery.extend(jQuery.validator.messages, {
  required: "Este campo es obligatorio.",
  remote: "Por favor, rellena este campo.",
  email: "Por favor, escribe una dirección de correo válida",
  url: "Por favor, escribe una URL válida.",
  date: "Por favor, escribe una fecha válida.",
  dateISO: "Por favor, escribe una fecha (ISO) válida.",
  number: "Por favor, escribe un número entero válido.",
  digits: "Por favor, escribe sólo dígitos.",
  creditcard: "Por favor, escribe un número de tarjeta válido.",
  equalTo: "Por favor, escribe el mismo valor de nuevo.",
  accept: "Por favor, escribe un valor con una extensión aceptada.",
  maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
  minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
  rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
  range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
  max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
  min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
  alphas: "Por favor, escribe solo caracteres",
  alphas_number: "Por favor, solo escribe letras y numeros"
});

jQuery.validator.addMethod("alphas", function(value, element) {
  return this.optional(element) || /^[a-zA-z/\s/]+$/.test( value );
});

jQuery.validator.addMethod("alphas_number", function(value, element) {
  return this.optional(element) || /^[a-zA-z/\s/]+[1-9]+$/.test( value );
});

jQuery.validator.addMethod("number", function(value, element) {
  return this.optional(element) || /^[0-9]+$/.test( value );
});

jQuery.validator.addMethod("email", function(value, element) {
  return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value );
});

 // add the rule here
 jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg != value;
 }, "Debe seleccionar una opción.");

jQuery.validator.addMethod("alphanumeric", function(value, element) {
  return this.optional(element) ||  /^[a-z0-9\-\s]+$/i.test(value);
}, "Solo letras, números, espacios.");  

jQuery.validator.addMethod("alpha_numeric", function(value, element) {
  return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
}, "Solo letras, números."); 
/**
  * Return true, if the value is a valid date, also making this formal check dd/mm/yyyy.
  *
  * @example jQuery.validator.methods.date("01/01/1900")
  * @result true
  *
  * @example jQuery.validator.methods.date("01/13/1990")
  * @result false
  *
  * @example jQuery.validator.methods.date("01.01.1900")
  * @result false
  *
  * @example <input name="pippo" class="{dateITA:true}" />
  * @desc Declares an optional input element whose value must be a valid date.
  *
  * @name jQuery.validator.methods.dateITA
  * @type Boolean
  * @cat Plugins/Validate/Methods
  */
jQuery.validator.addMethod(
  "dateITA",
  function(value, element) {
    var check = false;
    var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
    if( re.test(value)){
      var adata = value.split('/');
      var gg = parseInt(adata[0],10);
      var mm = parseInt(adata[1],10);
      var aaaa = parseInt(adata[2],10);
      var xdata = new Date(aaaa,mm-1,gg);
      if ( ( xdata.getFullYear() == aaaa ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == gg ) )
        check = true;
      else
        check = false;
    } else
      check = false;
    return this.optional(element) || check;
  }, 
  "Introduzca una fecha correcta."
);

jQuery.validator.addMethod("dateNL", function(value, element) {
    return this.optional(element) || /^\d\d?[\.\/-]\d\d?[\.\/-]\d\d\d?\d?$/.test(value);
  }, "Vul hier een geldige datum in."
);

jQuery.validator.addMethod("time", function(value, element) {
    return this.optional(element) || /^([01][0-9])|(2[0123]):([0-5])([0-9])$/.test(value);
  }, "Por favor, introduzca una hora válida, entre 00:00 y 23:59."
);

function validaRut(campo){
  if ( campo.length == 0 ){ return false; }
  if ( campo.length < 8 ){ return false; }

  campo = campo.replace('-','')
  campo = campo.replace(/\./g,'')

  var suma = 0;
  var caracteres = "1234567890kK";
  var contador = 0;    
  for (var i=0; i < campo.length; i++){
    u = campo.substring(i, i + 1);
    if (caracteres.indexOf(u) != -1)
    contador ++;
  }
  if ( contador==0 ) { return false }
  
  var rut = campo.substring(0,campo.length-1)
  var drut = campo.substring( campo.length-1 )
  var dvr = '0';
  var mul = 2;
  
  for (i= rut.length -1 ; i >= 0; i--) {
    suma = suma + rut.charAt(i) * mul
                if (mul == 7)   mul = 2
            else  mul++
  }
  res = suma % 11
  if (res==1)   dvr = 'k'
                else if (res==0) dvr = '0'
  else {
    dvi = 11-res
    dvr = dvi + ""
  }
  if ( dvr != drut.toLowerCase() ) { return false; }
  else { return true; }
}

/* La siguiente instrucción extiende las capacidades de jquery.validate() para que
  admita el método RUT, por ejemplo:
$('form').validate({
  rules : { rut : { required:true, rut:true} } ,
  messages : { rut : { required:'Escriba el rut', rut:'Revise que esté bien escrito'} }
})
// Nota: el meesage:rut sobrescribe la definición del mensaje de más abajo
*/
// comentar si jquery.Validate no se está usando
$.validator.addMethod("rut", function(value, element) { 
        return this.optional(element) || validaRut(value); 
}, "Revise el RUT");