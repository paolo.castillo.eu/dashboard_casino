function pf_notify(titulo, mensaje, tipo, icon) {
	$.notify({
		icon: icon,
		title: "<strong>" + titulo + "</strong> <br/>",
		message: mensaje
	}, {
			type: tipo,
			showProgressbar: false,
			placement: {
			from: "bottom",
			align: "right"
		},
			delay: 4000,
			timer: 3000,
			z_index: 9999,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutUp'
		}
	});
}

function pf_date(id,formato,estado) {
    id.datepicker({
        language: "es",
        format: formato,
        startDate: estado
    });
}

function pf_date_other(id,formato) {
    id.datepicker({
        language: "es",
        format: formato
    });
}

function pf_month(id,formato,estado) {
    id.datepicker({
        language: "es",
        format: formato,
        viewMode: "months", 
        minViewMode: "months",
        startDate: estado
    });
}

function pf_dayMonthYears(separador) {
   var tdate = new Date();
   var dd = tdate.getDate();
   var MM = tdate.getMonth();
   var yyyy = tdate.getFullYear(); 
   var currentDate= dd + separador +( MM+1) + separador + yyyy;
   return currentDate;
}

function pf_monthYears() {
	var months = ['Enero', 'febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];;
    var date = new Date();
    return months[date.getMonth()] + ' ' + date.getFullYear();
}

function pf_blockUI(){
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000','-webkit-border-radius': '10px','-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: '<h1><i class="fa fa-spinner fa-pulse" aria-hidden="true"></i> Espere por favor...</h1>'
    });
};

function pf_unblockUI(){
    $.unblockUI();
};

function call_datecalendar(id) {
$(id).daterangepicker({
  "locale": {
      "format": "DD/MM/YYYY",
      "separator": " - ",
      "applyLabel": "Aplicar",
      "cancelLabel": "Cancelar",
      "fromLabel": "De",
      "toLabel": "a",
      "customRangeLabel": "Custom",
      "daysOfWeek": [
      "Do",
      "Lu",
      "Ma",
      "Mi",
      "Ju",
      "Vi",
      "Sa"
      ],
      "monthNames": [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
      ],
      "firstDay": 1
  }
});
$('#daterange-btn').daterangepicker(
{
ranges: {
  'Today': [moment(), moment()],
  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  'This Month': [moment().startOf('month'), moment().endOf('month')],
  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
},
startDate: moment().subtract(29, 'days'),
endDate: moment()
},
  function (start, end) {
    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  }
);
}

function grafico(etiqueta,datos,areaChart) {
    var areaChartData = {
        labels  : etiqueta,
        datasets: [
        {
            label               : 'Control de documentación',
            fillColor           : '#00B0FF',
            strokeColor         : '#00B0FF',
            pointColor          : '#00B0FF',
            pointStrokeColor    : '#00B0FF',
            pointHighlightFill  : '#00B0FF',
            pointHighlightStroke: '#00B0FF',
            data                : datos
        }
        ]
    }
    var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : false,
        //String - Colour of the grid lines
        scaleGridLineColor      : '#0091EA',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : false,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
    }
    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);
}

function select2_pf(etiqueta){
  $(etiqueta).select2({
    placeholder: 'Seleccione una opción',
  });
}


var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}