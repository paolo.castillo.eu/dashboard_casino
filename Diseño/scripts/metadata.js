define(function(){
    return {
        pageGroups: [{"id":"b9c360e0-97ed-642c-667c-1bf9d6168b82","name":"Default group","pages":[{"id":"6306d3b1-6fe2-7580-0ccc-d024cb5fb662","name":"Login"},{"id":"aee58ccb-f90d-846e-0964-cb0fd25d9354","name":"Recover Pass"},{"id":"fbb9b8e0-9a00-3844-1c57-e61ff5e361fe","name":"Dashboard"},{"id":"714ed870-3736-1098-f500-1e7434d359a4","name":"Dashboard Scroll"},{"id":"dd49c56b-ac84-245e-1f38-698e83c6164f","name":"Dashboard Perfil"},{"id":"8e1a8b42-c902-aa80-3f79-0f5e1354bef9","name":"Dashboard Pass"},{"id":"d5640826-1862-a601-a5fe-ec53f8e45af1","name":"Vale Emitidos"},{"id":"b72b77b4-46ad-a3ee-bd6f-82231e3c408e","name":"Solicitar editar especial"},{"id":"fd13e9de-733d-ea56-6a49-ff38f24fd304","name":"Vale Mensaje"},{"id":"76b3dd83-42b0-b19b-964f-36e543e32d0b","name":"borrar vale especial"},{"id":"41deed90-caa8-45e7-6235-2c52c8458742","name":"Locales"},{"id":"f01c5eab-b8da-2420-31c1-db82d50dffbf","name":"Casino"},{"id":"cd355918-c7d3-0b9f-5655-dd1b7bac8ee6","name":"Usuario"},{"id":"c6d54879-77e0-8f92-419a-dbc6db2932cd","name":"Solicitud especial"},{"id":"15d43820-027c-a31d-f9a6-af3d3482630b","name":"Solicitud modal"},{"id":"606ce8b5-1a7c-c102-10d3-0230ff8974d5","name":"Solicitud agregar"},{"id":"27d55f23-e254-2d08-dc01-0fb42dc0e614","name":"Locales Agregar"},{"id":"3b23a3a1-7954-976a-9867-55317cb9a315","name":"Casino Agregar"}]}],
        downloadLink: "//services.ninjamock.com/html/htmlExport/download?shareCode=5ZHQZ&projectName=Casino",
        startupPageId: 0,

        forEachPage: function(func, thisArg){
        	for (var i = 0, l = this.pageGroups.length; i < l; ++i){
                var group = this.pageGroups[i];
                for (var j = 0, k = group.pages.length; j < k; ++j){
                    var page = group.pages[j];
                    if (func.call(thisArg, page) === false){
                    	return;
                    }
                }
            }
        },
        findPageById: function(pageId){
        	var result;
        	this.forEachPage(function(page){
        		if (page.id === pageId){
        			result = page;
        			return false;
        		}
        	});
        	return result;
        }
    }
});
